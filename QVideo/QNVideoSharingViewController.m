//
//  QNVideoSharingViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/3/20.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoSharingViewController.h"
#import "QNVideoTool.h"
#import "QNVideoShareCollectionViewController.h"

#import <QNAPFramework/QNVideoCollectionResponse.h>
#import <QNAPFramework/QNCollection.h>
#import <QNAPFramework/QNVideoCollectionCreateResponse.h>

#import "QNVideoCommonSectionView.h"
@interface QNVideoSharingViewController ()

@end

@implementation QNVideoSharingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.sharingLinkURLLabel setText:[self chooseNotNullURL]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated{
    [self.navigationController.toolbar setHidden:_originalHiddenToolbar];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _originalHiddenToolbar = self.navigationController.toolbar.hidden;
    [self.navigationController.toolbar setHidden:YES];

    [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(backNavi:) withTitleText:NSLocalizedString(@"Back", nil)];
//    _rightBtn = [QNVideoTool generateCustomRightBarItem:self withSelect:@selector(shareAction:) withTitleText:NSLocalizedString(@"Share", nil)];

}

- (void)shareAction:(id)sender{
    [ZAActivityBar showWithStatus:NSLocalizedString(@"Loading", nil)];
    //TODO share API calling sequence
    [self createShareCollection];
    
//    _rightBtn.enabled = NO;
}

- (void)disableRightButton{
}

- (void)createShareCollection{
    __block QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    NSDate *date = [NSDate date];
    
    NSDateComponents *offsetComponent = [[NSDateComponents alloc] init];
    [offsetComponent setMonth:1];
    
    NSDate *expireDate = [[NSCalendar currentCalendar] dateByAddingComponents:offsetComponent
                                                                       toDate:date
                                                                      options:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    __block NSString *collectionTitle = [formatter stringFromDate:date];
    //create album
    [videoStation createCollection:collectionTitle
                      collectionType:videoAlbums
                            isOpened:YES
                            isShared:YES
                       isEditByOwner:YES
                  withExpireDateTime:[formatter stringFromDate:expireDate]
                    withSuccessBlock:^(RKObjectRequestOperation *o,RKMappingResult *r, QNVideoCollectionCreateResponse *response){
                        DDLogVerbose(@"create collection's ID %@:", response);
                        [self addFilmsIntoCollection:response.output withFilmIDs:self.fileIds];
                    }
                    withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                        DDLogError(@"create collection error!");
                        [ZAActivityBar dismiss];
//                        _rightBtn.enabled = YES;
                    }];
}

- (void)queryShareCollection:(NSString *)collectionTitle{
    __block QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    __block QNVideoSharingViewController *blockSelf = self;
    [videoStation getCollections:videoAlbums
                        sortType:videoFileListSortByName
                           isASC:YES
                      pageNumber:1
                     countInPage:200
                   withFilterDic:nil
                withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                    //add file into Collection
                    QNVideoCollectionResponse *response = (QNVideoCollectionResponse *)r.firstObject;
                    NSArray *allCollections = [response.relationship_DataList allObjects];
                    BOOL isFindResult = NO;
                    for (QNCollection *collection in allCollections) {
                        if ([collection.cAlbumTitle isEqualToString:collectionTitle]) {
                            [blockSelf addFilmsIntoCollection:collection.iVideoAlbumId withFilmIDs:blockSelf.collectionViewController.selectedFilmIDs];
                            isFindResult = YES;
                            break;
                        }
                    }
                    if (!isFindResult) {
                        [QNVideoTool toastWithTargetViewController:blockSelf
                                                          withText:NSLocalizedString(@"shareCollectionError", nil)
                                                         isSuccess:NO];
                    }
                }
                withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                    [QNVideoTool toastWithTargetViewController:blockSelf
                                                      withText:NSLocalizedString(@"shareCollectionError", nil)
                                                     isSuccess:NO];
                    [ZAActivityBar dismiss];
                }];
}

- (void)addFilmsIntoCollection:(NSString *)collectionID withFilmIDs:(NSArray *)films{
    __block QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    [videoStation addToCollection:collectionID
                      withFileIDS:films
                 withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                     [ZAActivityBar dismiss];

                     if (_sendShareLinkBlock) {
                         NSString *sharelink = [self sharelinkURL:collectionID withIPAddress:[NSString stringWithFormat:@"%@:%@", self.sharingLinkURLLabel.text, [self choosePort]]];
                         _sendShareLinkBlock(sharelink);
                     }
/***
*  This is the previous version, sending the sharelink by email function on the NAS.
*/
/***
                     [self sendmailWithCollection:collectionID
                                    withReceivers:self.receiverLabel.text
                                     withSubtitle:self.subject.text
                                      withComment:self.messageText.text
                                    withIPAddress:[NSString stringWithFormat:@"%@:%@", self.sharingLinkURLLabel.text, [self choosePort]]];
 */
                 }
                 withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                     [QNVideoTool toastWithTargetViewController:self.navigationController
                                                       withText:NSLocalizedString(@"createCollectionError", nil)
                                                      isSuccess:NO];
                     [ZAActivityBar dismiss];
//                     _rightBtn.enabled = YES;
                 }];
}

- (void)sendmailWithCollection:(NSString *)collectionID withReceivers:(NSString *)receiverEmail withSubtitle:(NSString *)subTitle withComment:(NSString *)comment withIPAddress:(NSString *)ipAddress{
    __block QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    
    [videoStation sendEmailForSharing:collectionID
                               withIP:ipAddress //TODO: the value should be from the screen.
                               sender:videoStation.loginInfo.notification
                             receiver:receiverEmail
                         withSubtitle:subTitle
                          withComment:comment
                     withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *m){
                         [QNVideoTool toastWithTargetViewController:self.navigationController
                                                           withText:[NSString stringWithFormat:NSLocalizedString(@"sendMailSuccess", nil), receiverEmail]
                                                          isSuccess:YES];
//                         _rightBtn.enabled = YES;
                         [ZAActivityBar dismiss];
                     }
                     withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                         NSString *errorString = [self mailErrorMessage:e.code];
                         [QNVideoTool toastWithTargetViewController:self.navigationController
                                                           withText:errorString
                                                          isSuccess:NO];
//                         _rightBtn.enabled = YES;
                         [ZAActivityBar dismiss];
                     }];
}

- (IBAction)addUserAction:(id)sender{
    UITextField *userEmails = nil;
    CXAlertView *alertView = [CXAlertView showInputTextfieldAlert:NSLocalizedString(@"Edit", nil)
                                                  withActionTitle:NSLocalizedString(@"Done", nil)
                                                     isSecureType:NO
                                            withTextFieldDelegate:self
                                                withActionHandler:^(CXAlertView *alertView, CXAlertButtonItem *btn){
                                                    UITextField *_myEmails = (UITextField *)alertView.contentView;
                                                    self.receiverLabel.text = _myEmails.text;
                                                    [alertView dismiss];
                                                }];
    userEmails = (UITextField *)alertView.contentView;
    [userEmails setPlaceholder:NSLocalizedString(@"addEmailComment", nil)];
    [userEmails setFont:[UIFont systemFontOfSize:12.0f]];
    if ([self.receiverLabel.text length] > 0) {
        userEmails.text = self.receiverLabel.text;
    }
}

#pragma mark - UITableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = [indexPath section];
    if (self.collectionViewController) {
        DDLogVerbose(@"collectionView size %@", NSStringFromCGSize(self.collectionViewController.collectionView.frame.size));
    }
    return (section == 0)? 120.0f:44.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSString *r = [self sectionTitle:section];

    CGFloat estimateTextHeight = [QNVideoTool heightOfCellWithIngredientLine:r
                                                          withSuperviewWidth:tableView.frame.size.width];
    
    QNVideoCommonSectionView *backgroundView = [[[NSBundle mainBundle] loadNibNamed:@"QNVideoCommonSectionView" owner:nil options:nil] objectAtIndex:0];
    [backgroundView setFrame:CGRectMake(0, 0, tableView.frame.size.width, 25 + 13 +estimateTextHeight)];
    [backgroundView.sectionLabel setText:r];
    [backgroundView.sectionLabel sizeToFit];
    return backgroundView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    NSString *r = [self sectionTitle:section];
//    return [QNVideoTool heightOfCellWithIngredientLine:r
//                                    withSuperviewWidth:tableView.frame.size.width] + 25 + 13;
    return 44.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DDLogVerbose(@"select here row:%i section:%i", [indexPath row], [indexPath section]);
    if ([indexPath isEqual:[NSIndexPath indexPathForRow:0 inSection:1]]) { // press the link
        IQActionSheetPickerView *selectionView = [[IQActionSheetPickerView alloc] initWithViewController:self];
        NASServer *thisServer = [QNVideoTool share].nasServerInfo;
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (thisServer.wanIP && ![thisServer.wanIP isEqualToString:@""]) {
            [dic setValue:thisServer.wanIP forKey:thisServer.wanIP];
        }
        if (thisServer.lanIP && ![thisServer.lanIP isEqualToString:@""]) {
            [dic setValue:thisServer.lanIP forKey:thisServer.lanIP];
        }
        if (thisServer.myQNAPCloud && ![thisServer.myQNAPCloud isEqualToString:@""]) {
            [dic setValue:thisServer.myQNAPCloud forKey:thisServer.myQNAPCloud];
        }

        selectionView.userInfo = dic;
        
        selectionView.titlesForComponenets = @[
                                               [dic allKeys]
                                               ];
        selectionView.title = NSLocalizedString(@"AvailableLink", nil);
        selectionView.delegate = self;
        [selectionView showInView:self.parentViewController.view];
    }else if ([indexPath isEqual:[NSIndexPath indexPathForRow:1 inSection:1]]){
        if (self.navigationController.toolbar.isHidden) {
            UIToolbar *targetToolBar = self.navigationController.toolbar;
            [self settingToolbarItemsForShare:targetToolBar withViewController:self];
            
            if (targetToolBar.frame.origin.y >= self.view.frame.size.height) {
                [targetToolBar setFrame:CGRectMake(targetToolBar.frame.origin.x,
                                                   targetToolBar.frame.origin.y - targetToolBar.frame.size.height, targetToolBar.frame.size.width,
                                                   targetToolBar.frame.size.height)];
            }
            [self.navigationController.toolbar setHidden:NO];
        }else
            [self.navigationController.toolbar setHidden:YES];

    }
}

- (void)settingToolbarItemsForShare:(UIToolbar *)targetToolbar withViewController:(UIViewController *)viewController{
    NSMutableArray *actionBtns = [NSMutableArray array];
    UIBarButtonItem *email = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"email"] style:UIBarButtonItemStylePlain target:viewController action:@selector(shareByEmail)];
    UIBarButtonItem *sms = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"sms"] style:UIBarButtonItemStylePlain target:viewController action:@selector(shareBySMS)];
    UIBarButtonItem *sharelink = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"linked"] style:UIBarButtonItemStylePlain target:viewController action:@selector(copyShareLinked)];
    
    [actionBtns addObjectsFromArray:@[email, sms, sharelink]];
    NSMutableArray *allBtns = [NSMutableArray arrayWithObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    
    for (UIBarButtonItem *item in actionBtns) {
        [allBtns addObject:item];
        [allBtns addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    }
    [viewController setToolbarItems:allBtns];
}

#pragma mark - Navigation
- (void)backNavi:(id)sender{
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"goToSelectedCollectionViewController"]) {
        QNVideoShareCollectionViewController *shareCollection = (QNVideoShareCollectionViewController *)segue.destinationViewController;
        self.collectionViewController = shareCollection;
        if (self.fileIds && [self.fileIds count]) {
            shareCollection.selectedFilmIDs = [self.fileIds mutableCopy];
        }
    }
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text{
    // Any new character added is passed in as the "text" parameter
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

#pragma mark - PrivateMethod
- (NSString *)sectionTitle:(NSInteger)section{
    NSString *r = nil;
    switch (section) {
        case 0:
            r = NSLocalizedString(@"SelectVideo", nil);
            break;
        case 1:
            r = NSLocalizedString(@"To", nil);
            break;
        case 2:
            r = NSLocalizedString(@"Subject", nil);
            break;
        case 3:
            r = NSLocalizedString(@"DomainIP", nil);
            break;
        case 4:
            r = NSLocalizedString(@"Message", nil);
            break;
        default:
            break;
    }
    return r;
}

- (NSString *)chooseNotNullURL{
    NSString *r = nil;
    NASServer *targetServer = [QNVideoTool share].nasServerInfo;
    if (targetServer.myQNAPCloud != nil && ![targetServer.myQNAPCloud isEqualToString:@""]) {
        r = targetServer.myQNAPCloud;
    }else if (targetServer.wanIP != nil && ![targetServer.wanIP isEqualToString:@""]) {
        r = targetServer.wanIP;
    }else if (targetServer.lanIP != nil && ![targetServer.lanIP isEqualToString:@""]) {
        r = targetServer.lanIP;
    }
    return r;
}

- (NSString *)choosePort{
    NASServer *targetServer = [QNVideoTool share].nasServerInfo;
    return ([targetServer.isSSL boolValue])?[NSString stringWithFormat:@"%i", [targetServer.sslPort intValue]]:
    [NSString stringWithFormat:@"%i", [targetServer.normalPort intValue]];
}

- (NSString *)mailErrorMessage:(NSInteger)errorCode{
    NSString *returnString = nil;
    switch (errorCode) {
        case -3:
        case -1:
            returnString = NSLocalizedString(@"smtpSettingError", nil);
            break;
        default:
            returnString = NSLocalizedString(@"emailSendingError", nil);
            break;
    }
    return returnString;
}

- (NSString *)sharelinkURL:(NSString *)collectionID withIPAddress:(NSString *)ip{
    return [NSString stringWithFormat:@"http://%@/video/play.php?a=%@", ip, collectionID];
}

- (NSString *)sharelinkContent:(NSString *)shareLink isHTML:(BOOL)isHTML{
    NSString *rString = [NSString stringWithFormat: NSLocalizedString(@"ShareLinkContent", nil), shareLink];
    if (!isHTML) {
        rString = [rString stringByReplacingOccurrencesOfString:@"<br>" withString:@""];
    }
    return rString;
}

- (NSString *)sharelinkContent:(NSString *)shareLink{
    return [self sharelinkContent:shareLink isHTML:YES];
}

#pragma mark - ActionSheetDelegate
- (void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray*)titles{
    NSString *selectedTitle = [titles objectAtIndex:0];
    [self.sharingLinkURLLabel setText:selectedTitle];
}

#pragma mark - ShareAction
- (void)shareByEmail{
    __block QNVideoSharingViewController *blockSelf = self;
    _sendShareLinkBlock = ^(NSString *shareLink){
        [ZAActivityBar dismiss];
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc]init];
        mailViewController.mailComposeDelegate = blockSelf;
        [mailViewController setSubject:NSLocalizedString(@"ShareLinkTitle", nil)];
        [mailViewController setMessageBody:[blockSelf sharelinkContent:shareLink] isHTML:YES];
        [blockSelf presentViewController:mailViewController animated:YES completion:nil];
    };
    [ZAActivityBar showWithStatus:NSLocalizedString(@"Loading", nil)];
    //TODO share API calling sequence
    [blockSelf createShareCollection];
}

- (void)shareBySMS{
    __block QNVideoSharingViewController *blockSelf = self;
    _sendShareLinkBlock = ^(NSString *shareLink){
        [ZAActivityBar dismiss];

        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            controller.body = [blockSelf sharelinkContent:shareLink isHTML:NO];
            controller.messageComposeDelegate = blockSelf;
            [blockSelf presentViewController:controller animated:YES completion:nil];
        }else
            [QNVideoTool toastWithTargetViewController:blockSelf.navigationController
                                              withText:NSLocalizedString(@"CantSMS", nil)
                                             isSuccess:NO];
    };
    [ZAActivityBar showWithStatus:NSLocalizedString(@"Loading", nil)];
    //TODO share API calling sequence
    [blockSelf createShareCollection];

}

- (void)copyShareLinked{
    __block QNVideoSharingViewController *blockSelf = self;
    _sendShareLinkBlock = ^(NSString *shareLink){
        [ZAActivityBar dismiss];
        UIPasteboard *pb = [UIPasteboard generalPasteboard];
        [pb setString:shareLink];
        
        [QNVideoTool toastWithTargetViewController:blockSelf.navigationController
                                          withText:NSLocalizedString(@"CopyToPasteBoard", nil)
                                         isSuccess:YES];

    };
    [ZAActivityBar showWithStatus:NSLocalizedString(@"Loading", nil)];
    //TODO share API calling sequence
    [blockSelf createShareCollection];

}

#pragma mark - mailDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    //    MFMailComposeResultSent,
    //MFMailComposeResultFailed
    
    NSString *messageString = nil;
    BOOL isSent = NO;
    switch (result) {
        case MFMailComposeResultSent:{
            isSent = YES;
        }
            break;
        default:
            break;
    }
    [QNVideoTool toastWithTargetViewController:controller.navigationController
                                      withText:messageString
                                     isSuccess:isSent];
    [controller dismissViewControllerAnimated:YES completion:nil];
}
@end
