//
//  QNSettingViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/31.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNSettingViewController.h"
#import "QNVideoTool.h"
#import "QNSettingPopoverTableView.h"
#import "QNVideoCommonSectionView.h"

typedef NS_ENUM(NSInteger, DataPickerType){
    DataPickerTypeFolderSize,
    DataPickerTypeResolution
};

@interface QNSettingViewController ()

@end

@implementation QNSettingViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _resolutionDataArray = @[@"Default", @"240p", @"360p", @"720p", @"480p", @"1080p"];
    _folderSizeArray = @[@"1 GB", @"2 GB", @"5 GB", NSLocalizedString(@"Unlimited", nil)];
    if (self.isNaviMode) {
        [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(backNavi) withTitleText:NSLocalizedString(@"Back", nil)];
    }else{
        [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
    }
    [self.navigationItem setRightBarButtonItem:nil];

    [self settingVideoDownloadfolderLabel];
    [self settingCacheLabel];
    [self settingResolutionLabel];
    [self settingFolderSizeLimitLabel];
    [self settingLocalizable];
    [self settingAutoLogin];
    [self settingDownloadInWifi];
    
    _sections = [NSMutableArray array];
    for (int i=0; i<3; i++) {
        [_sections addObject: [self generateSectionView:i]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)settingLocalizable{
    self.title = NSLocalizedString(@"Setting", nil);
    [self.clearCacheBtn.titleLabel setText:NSLocalizedString(@"Clear", nil)];
    [self.changeBtn.titleLabel setText:NSLocalizedString(@"Change", nil)];
    [self.resolutionBtn.titleLabel setText:NSLocalizedString(@"Change", nil)];
    [self.autoLoginLabel setText:NSLocalizedString(@"AutoLogin", nil)];
    [self.wifiDownloadLabel setText:NSLocalizedString(@"wifiDownload", nil)];
    [self.noteLabel setText:NSLocalizedString(@"resolutionNote", nil)];
}

- (void)leftPanelAction:(id)sender{
    [self.parentPaneViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:NO completion:nil];
}

- (void)rightPanelAction:(id)sender{
    [self.parentPaneViewController topRightDropdownMenuAction:self];
}

#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (void)backNavi{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UITable
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [_sections objectAtIndex:section];
    return sectionView.frame.size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    return [_sections objectAtIndex:section];
    NSString *r = nil;
    switch (section) {
        case 0:
            r = NSLocalizedString(@"DownloadFolderAndCache", nil);
            break;
        case 1:
            r = NSLocalizedString(@"DefaultResolutionDescription", nil);
            break;
        case 2:
            r = NSLocalizedString(@"UPLOADINGPOLICY", nil);
            break;
        default:
            break;
    }
    CGFloat estimateTextHeight = [QNVideoTool heightOfCellWithIngredientLine:r
                                                          withSuperviewWidth:tableView.frame.size.width];

    QNVideoCommonSectionView *backgroundView = [[[NSBundle mainBundle] loadNibNamed:@"QNVideoCommonSectionView" owner:nil options:nil] objectAtIndex:0];
    [backgroundView setFrame:CGRectMake(0, 0, tableView.frame.size.width, 25 + 13 +estimateTextHeight)];
    [backgroundView.sectionLabel setText:r];
    [backgroundView.sectionLabel sizeToFit];
    return backgroundView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch ([indexPath section]) {
        case 0:{
            switch ([indexPath row]) {
                case 0: //Change folder size
                    [self changeFolderSizeLimitation:nil];
                    break;
                case 1: //clear cache
                    [self clearCache:nil];
                    break;
            }
        }
            break;
        case 1: //select the resolution
            [self changeResolution:nil];
            break;
    }
}

#pragma mark - ActionMethod
- (IBAction)clearCache:(id)sender{
    __block QNSettingViewController *blockSelf = self;

    self.showAlert = [CXAlertView showMessageAlert:NSLocalizedString(@"Clear", nil) withActionTitle:NSLocalizedString(@"Done", nil) withMessage:NSLocalizedString(@"ClearCache", nil) withActionHandler:^(CXAlertView *view, CXAlertButtonItem *item){
        [QNVideoTool cleanSDWebImageDisk];
        [blockSelf settingCacheLabel];
        [view dismiss];
    }];
}

- (IBAction)downloadInWifiAction:(id)sender{
    UISwitch *switcher = (UISwitch *)sender;
    [[NSUserDefaults standardUserDefaults] setBool:switcher.on forKey:@"isDownloadInWifi"];
}

- (IBAction)autoLoginAction:(id)sender{
    UISwitch *switcher = (UISwitch *)sender;
    [[NSUserDefaults standardUserDefaults] setBool:switcher.on forKey:@"isAutoLogin"];
}

- (IBAction)changeResolution:(id)sender{
    __block NSArray *_blockDataArray = _resolutionDataArray;
    __block QNSettingViewController *blockSelf = self;
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(5, 0, 260, 0)];
    pickerView.tag = DataPickerTypeResolution;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    NSUInteger pickerRow =[_resolutionDataArray indexOfObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"resolution"]];
    [pickerView selectRow:pickerRow inComponent:0 animated:NO];
    
    self.showAlert = [CXAlertView showContentViewAlert:NSLocalizedString(@"ChangeResolution", nil) withActionTitle:NSLocalizedString(@"Done", nil) withContentView:pickerView withActionHandler:^(CXAlertView *view, CXAlertButtonItem *item){
        UIPickerView *picker = pickerView;
        NSInteger selectedIndex = [picker selectedRowInComponent:0];
        [[NSUserDefaults standardUserDefaults] setValue:[_blockDataArray objectAtIndex:selectedIndex] forKey:@"resolution"];
        [blockSelf settingResolutionLabel];
        [view dismiss];
    }];
}

- (IBAction)changeFolderSizeLimitation:(id)sender{
    __block QNSettingViewController *blockSelf = self;
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(5, 0, 200, 0)];
    pickerView.tag = DataPickerTypeFolderSize;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    NSUInteger pickerRow =(NSUInteger)[[[NSUserDefaults standardUserDefaults] valueForKey:@"folderSize"] intValue];
    [pickerView selectRow:pickerRow inComponent:0 animated:NO];
    self.showAlert = [CXAlertView showContentViewAlert:NSLocalizedString(@"SelectFolderSize", nil)
                                       withActionTitle:NSLocalizedString(@"Done", nil)
                                       withContentView:pickerView
                                     withActionHandler:^(CXAlertView *view, CXAlertButtonItem *item){
                                         UIPickerView *picker = pickerView;
                                         NSInteger selectedIndex = [picker selectedRowInComponent:0];
                                         [[NSUserDefaults standardUserDefaults] setValue:@(selectedIndex) forKey:@"folderSize"];
                                         [blockSelf settingFolderSizeLimitLabel];
                                         [view dismiss];
                                     }];    
}

#pragma mark - SettingLabel
- (void)settingCacheLabel{
    double cacthSize = [QNVideoTool caculateSDWebImageCacheSize];
    [self.cacheLabel setText:[NSString stringWithFormat:@"%@ %0.2f MB",NSLocalizedString(@"cacheUsed", nil), cacthSize]];
}
- (void)settingVideoDownloadfolderLabel{
    NSString *folderString = [QNVideoTool creatingDownloadFolder];
    double sizeInMB = [QNVideoTool folderSize:folderString];
    [self.usedSizeLabel setText:[NSString stringWithFormat:@"%@ %0.2f MB", NSLocalizedString(@"DownloadUsed", nil) ,sizeInMB]];
}

- (void)settingResolutionLabel{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"resolution"]) {
        [self.resolutionLabel setText: [[NSUserDefaults standardUserDefaults] valueForKey:@"resolution"]];
    }else{
        [[NSUserDefaults standardUserDefaults] setValue:[_resolutionDataArray objectAtIndex:0] forKey:@"resolution"];
        [self settingResolutionLabel];
    }
}

- (void)settingFolderSizeLimitLabel{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"folderSize"]) {
        [self.folderSizeLabel setText:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"FolderSize", nil), [[NSUserDefaults standardUserDefaults] valueForKey:@"folderSize"]]];
    }else{
        [[NSUserDefaults standardUserDefaults] setValue:[_folderSizeArray objectAtIndex:0] forKey:@"folderSize"];
        [self settingFolderSizeLimitLabel];
    }
}

- (void)settingAutoLogin{ //isAutoLogin
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"isAutoLogin"]) {
        [self.autoLoginSwitch setOn:[[[NSUserDefaults standardUserDefaults] valueForKey:@"isAutoLogin"] boolValue]];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAutoLogin"];
        [self settingAutoLogin];
    }
}

- (void)settingDownloadInWifi{ //isAutoLogin
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"isDownloadInWifi"]) {
        [self.downloadInWifiSwitch setOn:[[[NSUserDefaults standardUserDefaults] valueForKey:@"isDownloadInWifi"] boolValue]];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isDownloadInWifi"];
        [self settingDownloadInWifi];
    }
}
#pragma mark - FolderSizePickerDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    NSArray *targetArray = (pickerView.tag == DataPickerTypeFolderSize)? _folderSizeArray: _resolutionDataArray;
    return [targetArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSArray *targetArray = (pickerView.tag == DataPickerTypeFolderSize)? _folderSizeArray: _resolutionDataArray;
    return [targetArray objectAtIndex:row];
}

#pragma mark - PrivateAPI
- (UIView *)generateSectionView:(NSInteger)section{
    NSString *r = nil;
    switch (section) {
        case 0:
            r = @"     ";
            break;
        case 1:
            r = NSLocalizedString(@"DefaultResolutionDescription", nil);
            break;
        case 2:
            r = NSLocalizedString(@"UPLOADINGPOLICY", nil);
            break;
        default:
            break;
    }
    CGFloat superWidth = self.tableView.frame.size.width - 30.0f;
    CGFloat estimateTextHeight = [QNVideoTool heightOfCellWithIngredientLine:r
                                                          withSuperviewWidth:superWidth];
    UILabel *returnLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 25, superWidth, estimateTextHeight)];
    returnLabel.lineBreakMode = NSLineBreakByCharWrapping;
    [returnLabel setNumberOfLines:0];
    [returnLabel setText:r];
    [returnLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [returnLabel setTextColor:[UIColor colorWithHexString:@"#4f4f4f"]];
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 25 + 13 +estimateTextHeight)];
    [backgroundView setBackgroundColor:[UIColor colorWithHexString:@"#efeff4"]];
    [backgroundView addSubview:returnLabel];
    
    UIView *upperLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, backgroundView.frame.size.width, 1)];
    UIView *lowerLine = [[UIView alloc] initWithFrame:CGRectMake(0, backgroundView.frame.size.height - 1, backgroundView.frame.size.width, 1)];
    [upperLine setBackgroundColor:[UIColor colorWithHexString:@"#c7c7c7"]];
    [lowerLine setBackgroundColor:[UIColor colorWithHexString:@"#c7c7c7"]];
    
    [backgroundView addSubview:upperLine];
    [backgroundView addSubview:lowerLine];
    return backgroundView;
}
@end
