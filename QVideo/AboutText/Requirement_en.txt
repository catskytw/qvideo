<h2>Requirement & Feature</h2>
<p1>Qvideo is available for QNAP Turbo NAS running firmware version 4.1 and Video Station or above. The latest NAS firmware is available for free download at <a>http://www.qnap.com/download.asp|http://www.qnap.com/download.asp</a></p1>
<h3>Connect to the NAS</h3>
<p>Add a connection: Tap "+" and Qvideo will find the available NAS servers on the network automatically. You can also tap "Add server manually" to add a connection. When done, Qvideo will record the network settings including the LAN and WAN IP, MyCloudNAS name, and the DDNS of the NAS.</p>
<h3>Note:</h3>
<p>Video Station must be enabled on the QNAP NAS. Use Video Station user accounts of the NAS to login with Qvideo. To access the NAS via the Internet, configure the myQNAPcloud service on the QNAP NAS or set up port forwarding on the router. The default port number is 80.</p>
<h3>Qvideo features:</h3>
<li>Browse the video files on the NAS’s Video Station.</li>
<li>Browse the video content by Timeline, Thumbnail, list or folder.</li>
<li>Video management.</li>
<li>Download video files from the NAS to the mobile device for offline use.</li>
<li>Supported video formats: MP4 (H.264) (mt2s, avi, mpg, wmv, ts, asf, mtd, mov, m2v, mpeg, 3gp, mkv, mts, tod, mod, trp, m1v, m4v, divx, flv, rmvb, rm will be converted into the MP4 format for online playing)</li>
