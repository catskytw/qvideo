//
//  QNLoginAggregateController.h
//  QNAPFramework
//
//  Created by Change.Liao on 2013/11/12.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MHCustomTabBarController/MHCustomTabBarController.h>

@interface QNLoginAggregateController : UIViewController
@property (nonatomic, strong) IBOutlet UIView *container;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *buttons;

- (void)setLoginActionWithSuccessBlock:(void(^)(void))success withFailureBlock:(void(^)(void))failure;
- (void)runSuccessAfterLogin;
- (void)runFailureAfterLogin;
@end
