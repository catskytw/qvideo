//
//  QNLoginAggregateController.m
//  QNAPFramework
//
//  Created by Change.Liao on 2013/11/12.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNLoginAggregateController.h"
#import "UIImage+QVideoExt.h"

@interface QNLoginAggregateController (){
    void (^_successBlock)(void);
    void (^_failureBlock)(void);
    BOOL _firstLoad;
}
@end

@implementation QNLoginAggregateController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    for (UIButton *btn in self.buttons) {
        [btn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:41.0f/256.0f green:149.0f/256.0f blue:180.0f/256.0f alpha:1.0f]] forState:UIControlStateSelected];
    }
    //set the default page in AddingNAS
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLoginActionWithSuccessBlock:(void(^)(void))success withFailureBlock:(void(^)(void))failure{
    _successBlock = success;
    _failureBlock = failure;
}

- (void)runSuccessAfterLogin{
    _successBlock();
}

- (void)runFailureAfterLogin{
    _failureBlock();
}
@end
