//
//  QNLoginErrorOptionView.m
//  QVideo
//
//  Created by Change.Liao on 2014/6/8.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//
#import <MagicalRecord/CoreData+MagicalRecord.h>

#import "QNLoginErrorOptionView.h"
#import "LoginWaySelectionTableView.h"
#import "ASViewController.h"

@implementation QNLoginErrorOptionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)editAccountPwd:(id)sender{
    NSString *bundleName = @"FixAccountPwdView";
    ASViewController *loginViewController =[[[NSBundle mainBundle] loadNibNamed:bundleName
                                                                          owner:self
                                                                        options:nil] objectAtIndex:0];
    
    loginViewController.loginResultBlock = nil;
    [loginViewController.userNameField setText:self.targetNAS.account];
    [loginViewController.userNameField setEnabled:NO];
    loginViewController.targetNASServer = self.targetNAS;
    [loginViewController settingCheckBoxIconImage];
    CXAlertView *alert = [CXAlertView showContentViewAlert:NSLocalizedString(@"IncorrectLoginInfo", nil)
                                           withActionTitle:NSLocalizedString(@"Done", nil)
                                           withContentView:loginViewController
                                         withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                                             self.targetNAS.password = loginViewController.passwordField.text;
                                             [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error){
                                                 if (success) {
                                                     if ([self.loginDelgate respondsToSelector:@selector(loginNASServer:withCompletionBlock:)]) {
                                                         [self.loginDelgate loginNASServer:self.targetNAS withCompletionBlock:^(BOOL success, NSError *error){
                                                         }];
                                                     }
                                                 }else{
                                                     [loginViewController.errorLabel setText:[error domain]];
                                                 }
                                             }];
                                         }];
    [alert setTitleFont:[UIFont systemFontOfSize:16.0f]];
}

- (IBAction)selectOtherLoginWay:(id)sender{
    LoginWaySelectionTableView *selectionTable = [[LoginWaySelectionTableView alloc] initWithFrame:CGRectMake(0, 0, 240, 200) withNASServer:self.targetNAS];
    selectionTable.delegate = selectionTable;
    selectionTable.dataSource = selectionTable;
    [selectionTable setBackgroundColor:[UIColor clearColor]];
    selectionTable.loginDelgate = self.loginDelgate;
    CXAlertView *alert = [CXAlertView showContentViewAlert:NSLocalizedString(@"loginwaySelectionTitle", nil)
                                           withActionTitle:nil
                                           withContentView:selectionTable
                                         withActionHandler:nil];
    [alert setTitleFont:[UIFont systemFontOfSize:16.0f]];
    
}


@end
