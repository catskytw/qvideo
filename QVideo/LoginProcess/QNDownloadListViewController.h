//
//  QNDownloadListViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/6.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import "QNVideosViewController.h"
#import "QNVideoStatusView.h"

@interface QNDownloadListViewController : UITableViewController <NSFetchedResultsControllerDelegate>{
    NSMutableArray *_selecttions; //<NSIndexPath>
}
@property (nonatomic) BOOL isOnlyDownloadedFiles;
@property (nonatomic, strong) NSFetchedResultsController *fetchResultController;
@property (nonatomic, weak) QNVideoStationAPIManager *videoStation;
@property (nonatomic, weak) QNVideosViewController *parentPaneViewController;
@property (nonatomic, strong) QNVideoStatusView *statusView;
@end
