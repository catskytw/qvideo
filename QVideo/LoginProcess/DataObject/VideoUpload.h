//
//  VideoUpload.h
//  QVideo
//
//  Created by Change.Liao on 2014/8/5.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "VideoSessionTask.h"


@interface VideoUpload : VideoSessionTask

@property (nonatomic, retain) NSNumber * uploadStatus;
@property (nonatomic, retain) id thumbnail;

@end
