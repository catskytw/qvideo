//
//  NASServer.m
//  QVideo
//
//  Created by Change.Liao on 2014/7/8.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "NASServer.h"
#import "VideoDownload.h"


@implementation NASServer

@dynamic account;
@dynamic cloudLink;
@dynamic currentAddress;
@dynamic isAutoLogin;
@dynamic isRememberPwd;
@dynamic isSSL;
@dynamic lanIP;
@dynamic mappingPort;
@dynamic modelName;
@dynamic myQNAPCloud;
@dynamic needLoginInPrivate;
@dynamic normalPort;
@dynamic password;
@dynamic serverName;
@dynamic sslPort;
@dynamic wanIP;
@dynamic isAutoDetectPort;
@dynamic normal_internal_port;
@dynamic normal_external_port;
@dynamic ssl_internal_port;
@dynamic ssl_external_port;
@dynamic portPriority;
@dynamic isLoginAfterSaving;
@dynamic relationship_videoDownload;
@end
