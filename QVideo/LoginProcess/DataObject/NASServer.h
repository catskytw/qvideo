//
//  NASServer.h
//  QVideo
//
//  Created by Change.Liao on 2014/7/8.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class VideoDownload;

@interface NASServer : NSManagedObject

@property (nonatomic, retain) NSString * account;
@property (nonatomic, retain) NSString * cloudLink;
@property (nonatomic, retain) NSString * currentAddress;
@property (nonatomic, retain) NSNumber * isAutoLogin;
@property (nonatomic, retain) NSNumber * isRememberPwd;
@property (nonatomic, retain) NSNumber * isSSL;
@property (nonatomic, retain) NSString * lanIP;
@property (nonatomic, retain) NSString * mappingPort;
@property (nonatomic, retain) NSString * modelName;
@property (nonatomic, retain) NSString * myQNAPCloud;
@property (nonatomic, retain) NSNumber * needLoginInPrivate;
@property (nonatomic, retain) NSNumber * normalPort;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * serverName;
@property (nonatomic, retain) NSNumber * sslPort;
@property (nonatomic, retain) NSString * wanIP;
@property (nonatomic, retain) NSNumber * isAutoDetectPort;
@property (nonatomic, retain) NSNumber * normal_internal_port;
@property (nonatomic, retain) NSNumber * normal_external_port;
@property (nonatomic, retain) NSNumber * ssl_internal_port;
@property (nonatomic, retain) NSNumber * ssl_external_port;
@property (nonatomic, retain) NSNumber * portPriority;
@property (nonatomic, retain) NSNumber * isLoginAfterSaving;
@property (nonatomic, retain) NSSet *relationship_videoDownload;
@end

@interface NASServer (CoreDataGeneratedAccessors)

- (void)addRelationship_videoDownloadObject:(VideoDownload *)value;
- (void)removeRelationship_videoDownloadObject:(VideoDownload *)value;
- (void)addRelationship_videoDownload:(NSSet *)values;
- (void)removeRelationship_videoDownload:(NSSet *)values;
@end
