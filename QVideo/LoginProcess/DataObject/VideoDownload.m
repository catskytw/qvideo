//
//  VideoDownload.m
//  QVideo
//
//  Created by Change.Liao on 2014/8/5.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "VideoDownload.h"
#import "NASServer.h"


@implementation VideoDownload

@dynamic downloadStatus;
@dynamic duration;
@dynamic mediaType;
@dynamic resumeKey;
@dynamic relationship_nasServer;

@end
