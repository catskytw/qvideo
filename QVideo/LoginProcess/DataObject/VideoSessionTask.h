//
//  VideoSessionTask.h
//  QVideo
//
//  Created by Change.Liao on 2014/8/5.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface VideoSessionTask : NSManagedObject

@property (nonatomic, retain) NSString * filmID;
@property (nonatomic, retain) NSString * filmTitle;
@property (nonatomic, retain) NSNumber * isDownloadTask;
@property (nonatomic, retain) NSNumber * fileSize;

@end
