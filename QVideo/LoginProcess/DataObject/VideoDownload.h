//
//  VideoDownload.h
//  QVideo
//
//  Created by Change.Liao on 2014/8/5.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "VideoSessionTask.h"

@class NASServer;

@interface VideoDownload : VideoSessionTask

@property (nonatomic, retain) NSNumber * downloadStatus;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSString * mediaType;
@property (nonatomic, retain) NSData * resumeKey;
@property (nonatomic, retain) NASServer *relationship_nasServer;

@end
