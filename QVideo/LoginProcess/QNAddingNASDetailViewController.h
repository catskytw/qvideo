//
//  QNAddingNASDetailViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/5.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NASServer.h"
#import "QNActionCell.h"
#import "QNNASDetailViewController.h"

@interface QNAddingNASDetailViewController : UIViewController<UITableViewDataSource>{
}
@property (nonatomic, strong) NASServer *nasServer;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) QNNASDetailViewController *tableViewController;
@property (weak, nonatomic) IBOutlet UILabel *deleteLabel;
@property (weak, nonatomic) IBOutlet UIView *deleteView;
@property (weak, nonatomic) IBOutlet UISwitch *loginAfterSavingSwitcher;
@property (nonatomic) BOOL isEditAction;
- (IBAction)saveNAS:(id)sender;
- (IBAction)deleteNAS:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)loginAfterSavingValueChanged:(id)sender;

- (void)generateNewNAS;
@end
