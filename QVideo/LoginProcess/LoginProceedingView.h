//
//  LoginProcessView.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/16.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NASServer.h"
#import "QNButton.h"

@interface LoginProceedingView : UIView
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *loginURL;
@property (weak, nonatomic) IBOutlet UILabel *loginName;
@property (weak, nonatomic) IBOutlet QNButton *selectBtn;
@end
