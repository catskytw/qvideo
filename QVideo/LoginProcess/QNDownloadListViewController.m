//
//  QNDownloadListViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/6.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNDownloadListViewController.h"
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <QNAPFramework/QNAPCommunicationManager.h>
#import <QNAPFramework/MLFile.h>
#import <QNAPFramework/MLMediaLibrary.h>

#import <MDRadialProgress/MDRadialProgressTheme.h>
#import <MDRadialProgress/MDRadialProgressLabel.h>
#import "VideoDownload.h"
#import "QNLocalVideoFileListCell.h"
#import "NSString_TimeDuration.h"
#import "QNHeaderTextLabel.h"
#import "QNVideoTool.h"
#import "QNMoviePlayerViewController.h"
#import "NSString+SupportedMedia.h"
#import "VideoSessionTask.h"
#import "VideoUpload.h"

static NSInteger normalHeight = 32.0f;
@interface QNDownloadListViewController (){
    NSArray *_sections;
    NSArray *_sortMLFiles; //sorted by playCount
    NSArray *_uploadVideos;
}

@end

@implementation QNDownloadListViewController

- (id)initWithStyle:(UITableViewStyle)style{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    _selecttions = [NSMutableArray array];
    self.statusView = [[QNVideoStatusView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height)];
    if (self.parentPaneViewController) {
        self.statusView.delegate = self.parentPaneViewController;
        [self.statusView setVideoStatus:QNVideoStatusNoItem];
    }else{
        [self.statusView.statusDescription setText:NSLocalizedString(@"NoItem", nil)];
    }
    
    [self.tableView addSubview:self.statusView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showDataView) name:@"hasDataInDownloadFolder" object:nil];
    [self hideDataView];
    
    [self reloadMLFiles];
    [self reloadUploadVideos];
    
    if (self.isOnlyDownloadedFiles) {
        [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(backNavi) withTitleText:NSLocalizedString(@"Back", nil)];
    }else{
        [self generateFetchedResultsController:nil];
        if (self.parentPaneViewController) {
            [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
        }else
            [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(backNavi) withTitleText:NSLocalizedString(@"Back", nil)];
    }
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_edit"] style:UIBarButtonItemStylePlain target:self action:@selector(enterEditMode)];
    [self.navigationItem setRightBarButtonItem:item];

    self.videoStation = [QNAPCommunicationManager share].videoStationManager;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTable:) name:@"UploadCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTable:) name:@"DownloadCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTable:) name:kReachabilityChangedNotification object:nil];
}
- (void)multipleDeleteAction{
    NSMutableArray *tmpSelections = [NSMutableArray arrayWithArray:_selecttions];
    void(^__block recuresiveBlock)() = ^{
        if ([tmpSelections count] > 0) {
            NSIndexPath *indexPath = [tmpSelections objectAtIndex:0];
            [tmpSelections removeObjectAtIndex:0];
            [self deleteAction:self.tableView atIndexPath:indexPath withCompletedBlock:recuresiveBlock];
        }else{
            [_selecttions removeAllObjects];
            [self updateTable:nil];
        }
    };
    
    recuresiveBlock();
    [self enterEditMode];
}
- (void)enterEditMode{
    [self.navigationItem setRightBarButtonItem:nil];
    if (self.tableView.isEditing) {
        [self.tableView setEditing:NO animated:YES];
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_edit"] style:UIBarButtonItemStylePlain target:self action:@selector(enterEditMode)];
        [self.navigationItem setRightBarButtonItem:item];
    }else{
        [self.tableView setEditing:YES animated:YES];
        UIBarButtonItem *cancelItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(enterEditMode)];
        UIBarButtonItem *deleteItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"multiple-select_delete"] style:UIBarButtonItemStylePlain target:self action:@selector(multipleDeleteAction)];
        [self.navigationItem setRightBarButtonItems:@[cancelItem, deleteItem]];
    }
}

- (void)reloadMLFiles{
    _sortMLFiles = [[MLFile allFiles] sortedArrayWithOptions:NSSortStable
                                             usingComparator:^(id obj1, id  obj2){
                                                 MLFile *file1 = (MLFile *)obj1;
                                                 MLFile *file2 = (MLFile *)obj2;
                                                 return ([file1.playCount compare:file2.playCount]);
                                             }];
}

- (void)reloadUploadVideos{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.uploadStatus != %@", @(videoUploadCompleted)];
    _uploadVideos = [VideoUpload MR_findAllWithPredicate:predicate];
}

- (void)refreshTable:(NSNotification *)noti{
    [self reloadMLFiles];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeDownloadProgress:) name:@"downloadProgress" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable:) name:@"mediaFilesChanged" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadProgress" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"mediaFilesChanged" object:nil];
}

- (void)rightPanelAction:(id)sender{
    [self.parentPaneViewController topRightDropdownMenuAction:self];
}

- (void)leftPanelAction:(id)sender{
    [self.parentPaneViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:NO completion:nil];
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //download, upload, localfiles
    return 3;
}
#pragma mark - TableViewDelegate
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle != UITableViewCellEditingStyleDelete) return;
    
    [self deleteAction:tableView atIndexPath:indexPath withCompletedBlock:^(){
        [_selecttions removeAllObjects];
        [self updateTable:nil];
    }];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    //main text string
    QNHeaderTextLabel *label = [[QNHeaderTextLabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, normalHeight)];
    UIColor *background = [UIColor colorWithRed:226.0f/256.0f green:226.0f/256.0f blue:226.0f/256.0f alpha:1.0f];
    UIColor *textColor = [UIColor colorWithRed:79.0f/256.0f green:79.0f/256.0f blue:79.0f/256.0f alpha:1.0f];
    UIFont *textFont = [UIFont systemFontOfSize:14.0f];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentLeft;

    BOOL allow = [QNVideoTool allowDownload];

    NSString *contentString = nil;
    switch (section) {
        case 0:
            contentString = NSLocalizedString(@"DownloadStatus", nil);
            break;
        case 1:
            contentString = NSLocalizedString(@"UploadStatus", nil);
            break;
        default:
            contentString = NSLocalizedString(@"DownloadFiles", nil);
            break;
    }
    NSDictionary *attrs = @{NSParagraphStyleAttributeName:paragraphStyle,
                            NSFontAttributeName:textFont,
                            NSForegroundColorAttributeName:textColor
                            };
    NSMutableAttributedString *mainString = [[NSMutableAttributedString alloc] initWithString:contentString
                                                                                   attributes:attrs];
    //sub string
    if (section == 0 && !allow) {
        NSMutableParagraphStyle *subParagraphStyle = [[NSMutableParagraphStyle alloc] init];
        subParagraphStyle.firstLineHeadIndent = 5.0f;
        UIColor *subTextColor = [UIColor brownColor];
        UIFont *subTextFont = [UIFont systemFontOfSize:9.0f];
        NSDictionary *subAttrs = @{NSFontAttributeName:subTextFont,
                                   NSForegroundColorAttributeName:subTextColor,
                                   NSParagraphStyleAttributeName:subParagraphStyle
                                   };
        NSAttributedString *subString = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"suspendDownload", nil) attributes:subAttrs];
        [mainString appendAttributedString:subString];
    }
    [label setBackgroundColor:background];
    [label setAttributedText:mainString];
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    CGFloat r = 0.0f;
    NSInteger normalHeight = 32.0f;
    switch (section) {
        case 0:{
            id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchResultController sections] objectAtIndex:section];
            NSInteger count = [sectionInfo numberOfObjects];
            r = (count == 0)?0:normalHeight;
        }
            break;
        case 1:{
            NSInteger count = [_uploadVideos count];
            r = (count == 0)?0:normalHeight;
        }
            break;
        case 2:{
            NSInteger count = [_sortMLFiles count];
            r = (count == 0)?0:normalHeight;
        }
            break;
        default:
            break;
    }
    return r;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger r = 0;
    switch (section) {
        case 0:{
            id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchResultController sections] objectAtIndex:section];
            r = [sectionInfo numberOfObjects];
        }
            break;
        case 1:{
            r = [_uploadVideos count];
        }
            break;
        case 2:
            r = [_sortMLFiles count];
            break;
        default:
            break;
    }
    
    if (r > 0)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"hasDataInDownloadFolder" object:nil userInfo:nil];
    return r;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"videoLocalFileCell";
    QNLocalVideoFileListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    switch ([indexPath section]) {
        case 0:
            cell = [self configureDownloadCell:cell withIndexPath:indexPath];
            break;
        case 1:
            cell = [self configureUploadCell:cell withIndexPath:indexPath];
            break;
        case 2:
            cell = [self configureExistingVideoFileCell:cell withIndexPath:indexPath];
            break;
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.isEditing) {
        [self selectInEditingMode:tableView atIndexPath:indexPath];
    }else
        [self selectNoInEditingMode:tableView atIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_selecttions containsObject:indexPath]) {
        [_selecttions removeObject:indexPath];
    }
}

- (void)selectInEditingMode:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath{
    if ([_selecttions containsObject:indexPath]) {
        [_selecttions removeObject:indexPath];
    }else{
        [_selecttions addObject:indexPath];
    }
    DDLogVerbose(@"operating NSArray selections");
}

- (void)selectNoInEditingMode:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath{
    switch ([indexPath section]) {
        case 0:{ //pause the downloadTask
            QNLocalVideoFileListCell *_cell = (QNLocalVideoFileListCell *)[tableView cellForRowAtIndexPath:indexPath];
            NSURLSessionDownloadTask *task = [self.videoStation findDownloadTask:_cell.fID];
            if (task.state == NSURLSessionTaskStateSuspended) {
                [self.videoStation resumeDownloadTaskWithID:_cell.fID];
            }else if (task.state == NSURLSessionTaskStateRunning){
                [self.videoStation pauseDownloadTaskWithID:_cell.fID];
            }
            [tableView reloadData];
        }
            break;
        case 1:{
            QNLocalVideoFileListCell *_cell = (QNLocalVideoFileListCell *)[tableView cellForRowAtIndexPath:indexPath];
            NSURLSessionUploadTask *task = [self.videoStation findUploadTask:_cell.fID];
            if (task.state == NSURLSessionTaskStateSuspended) {
                [task resume];
            }else if (task.state == NSURLSessionTaskStateRunning){
                [task suspend];
            }
            [tableView reloadData];
        }
            break;
        case 2:{ //play the video
            /**
             *  QNMoviePlayerViewController_potrait
             */
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"QVideo_iphone" bundle:[NSBundle mainBundle]];
            QNMoviePlayerViewController *vc = (QNMoviePlayerViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"QNMoviePlayerViewController_potrait"];
            
            QNLocalVideoFileListCell *thisCell =(QNLocalVideoFileListCell *) [tableView cellForRowAtIndexPath:indexPath];
            vc.mediaItem =thisCell.userInfo;
            [self.navigationController setNavigationBarHidden:YES];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}
#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - NSFetchedResultController Delegate
- (NSFetchedResultsController *)generateFetchedResultsController:(NSDictionary *)info{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.downloadStatus != %@", @(videoDownloadCompleted)];
    NSFetchedResultsController *fetch = [VideoDownload MR_fetchAllGroupedBy:nil
                                                              withPredicate:predicate
                                                                   sortedBy:@"downloadStatus"
                                                                  ascending:YES];
    fetch.delegate = self;
    self.fetchResultController = fetch;
    [self.fetchResultController performFetch:nil];
    return fetch;
}

#pragma mark - Configuration Cell
- (QNLocalVideoFileListCell *)configureUploadCell:(QNLocalVideoFileListCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = [indexPath row];
    
    VideoUpload *upload = [_uploadVideos objectAtIndex:row];
    MDRadialProgressTheme *newTheme = [[MDRadialProgressTheme alloc] init];
    [cell.progressView setTheme:[[MDRadialProgressTheme alloc] init]];
    newTheme.sliceDividerHidden = YES;
    UIColor *color = [UIColor colorWithRed:25.0/255.0 green:159.0/255.0 blue:200.0/255.0 alpha:1.0];
    newTheme.completedColor = color;
    newTheme.incompletedColor = [UIColor colorWithRed:204.0/255.0 green:234.0/255.0 blue:243.0/255.0 alpha:1.0];
    newTheme.centerColor = [UIColor clearColor];
    newTheme.thickness = 10.0f;
    
    [cell.progressView setTheme:newTheme];
    //
    cell.progressView.innerCircleColor = [UIColor colorWithRed:204.0/255.0 green:234.0/255.0 blue:243.0/255.0 alpha:1.0];
    cell.progressView.innerCircleWidth = 1.0f;
    
    cell.progressView.progressTotal = 100;
    
    NSURLSessionUploadTask *uploadTask = [self.videoStation findUploadTask:upload.filmID];
    BOOL isRunning = (uploadTask.state == NSURLSessionTaskStateRunning);

//    if (uploadTask.state == NSURLSessionTaskStateSuspended || uploadTask == nil) {
//        [cell.progressView.label setText:NSLocalizedString(@"Pause", nil)];
//    }else
//        [cell.progressView.label setText:@"0"];
    [cell.progressView settingPlayPauseImage:isRunning];

    [cell.progressView.label setTextColor:[UIColor colorWithRed:51.0/255.0  green:51.0/255.0 blue:51.0/255.0 alpha:1]];
    [cell.progressView.label setFont:[UIFont systemFontOfSize:11.0f]];
    [cell.progressView.label setTextAlignment:NSTextAlignmentCenter];
    
    cell.fID = upload.filmID;
    [cell.screenShot setImage:upload.thumbnail];
    [cell.screenShot setContentMode:UIViewContentModeScaleAspectFit];
    [cell.fileTitle setText:upload.filmTitle];
    [cell.sizeLabel setText:[QNVideoTool transformedValue:upload.fileSize]];
    [[NSNotificationCenter defaultCenter] addObserver:cell selector:@selector(changeDownloadProgress:) name:@"downloadProgress" object:nil];
    return cell;
}

- (QNLocalVideoFileListCell *)configureDownloadCell:(QNLocalVideoFileListCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    VideoDownload *thisItem = [self.fetchResultController objectAtIndexPath: indexPath];
    MDRadialProgressTheme *newTheme = [[MDRadialProgressTheme alloc] init];
    [cell.progressView setTheme:[[MDRadialProgressTheme alloc] init]];
    newTheme.sliceDividerHidden = YES;
    UIColor *color = [UIColor colorWithRed:25.0/255.0 green:159.0/255.0 blue:200.0/255.0 alpha:1.0];
    newTheme.completedColor = color;
    newTheme.incompletedColor = [UIColor colorWithRed:204.0/255.0 green:234.0/255.0 blue:243.0/255.0 alpha:1.0];
    newTheme.centerColor = [UIColor clearColor];
    newTheme.thickness = 10.0f;
    
    [cell.progressView setTheme:newTheme];
    //
    cell.progressView.innerCircleColor = [UIColor colorWithRed:204.0/255.0 green:234.0/255.0 blue:243.0/255.0 alpha:1.0];
    cell.progressView.innerCircleWidth = 1.0f;
    
    cell.progressView.progressTotal = 100;
    
//    cell.progressView.progressCounter = 1;
    
    NSURLSessionDownloadTask *task = [self.videoStation findDownloadTask:thisItem.filmID];
    BOOL isRunning = (task.state == NSURLSessionTaskStateRunning);
//    if (task.state == NSURLSessionTaskStateSuspended || task == nil) {
//        [cell.progressView.label setText:NSLocalizedString(@"Pause", nil)];
//    }else
//        [cell.progressView.label setText:@"0"];
    [cell.progressView settingPlayPauseImage:isRunning];
    [cell.progressView.label setTextColor:[UIColor colorWithRed:51.0/255.0  green:51.0/255.0 blue:51.0/255.0 alpha:1]];
    [cell.progressView.label setFont:[UIFont systemFontOfSize:11.0f]];
    [cell.progressView.label setTextAlignment:NSTextAlignmentCenter];
    
    cell.fID = thisItem.filmID;
    [cell.fileTitle setText:thisItem.filmTitle];
    [cell.sizeLabel setText:[QNVideoTool transformedValue:thisItem.fileSize]];
    [self.videoStation lazyloadingImage:cell.screenShot
                        withPlaceHolder:nil
                             withFileId:thisItem.filmID
                      withProgressBlock:nil
                     withCompletedBlock:nil];
    [[NSNotificationCenter defaultCenter] addObserver:cell selector:@selector(changeDownloadProgress:) name:@"downloadProgress" object:nil];
    return cell;
}

- (QNLocalVideoFileListCell *)configureExistingVideoFileCell:(QNLocalVideoFileListCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    MLFile *thisFile = [_sortMLFiles objectAtIndex:[indexPath row]];
    cell.fID = nil;
    [cell.fileTitle setText: thisFile.title];
    
    MDRadialProgressTheme *newTheme = [[MDRadialProgressTheme alloc] init];
    [cell.progressView setTheme:[[MDRadialProgressTheme alloc] init]];
    newTheme.sliceDividerHidden = YES;
    UIColor *color = [UIColor colorWithRed:37.0/255.0 green:202.0/255.0 blue:169.0/255.0 alpha:1.0];
    newTheme.completedColor = color;
    newTheme.incompletedColor = [UIColor clearColor];
    newTheme.centerColor = [UIColor clearColor];
    newTheme.thickness = 10.0f;
    //140, 245, 224
    UIColor *innerColor = [UIColor colorWithRed:140.0f/256.0f green:245.0f/256.0f blue:224.0f/256.0f alpha:1.0f];
    cell.progressView.innerCircleColor = innerColor;
    cell.progressView.innerCircleWidth = 1.0f;
    [cell.progressView setTheme:newTheme];
    
    cell.progressView.progressTotal = 100;
    cell.progressView.progressCounter = 100;
    
    [cell.progressView settingTextLabel];
    [cell.progressView.label setText:NSLocalizedString(@"Open", nil)];
    [cell.progressView.label setTextColor:color];
    [cell.progressView.label setFont:[UIFont boldSystemFontOfSize:9.0f]];
    [cell.progressView.label setTextAlignment:NSTextAlignmentCenter];
    
    [cell.fileTitle setText:thisFile.title];
    [cell.sizeLabel setText: [QNVideoTool transformedValue:@(thisFile.fileSizeInBytes)]];
    if (thisFile.computedThumbnail) {
        [cell.screenShot setImage:thisFile.computedThumbnail];
    }else{
        [NSObject cancelPreviousPerformRequestsWithTarget:self.tableView selector:@selector(reloadData) object:nil];
        [self.tableView performSelector:@selector(reloadData) withObject:nil afterDelay:2.0f];
    }
    cell.userInfo = thisFile;
    return cell;
}

#pragma mark - DownloadOperation
- (void)changeDownloadProgress:(NSNotification *)noti{
    NSDictionary *dic = noti.userInfo;
    NSUInteger progress = [[dic valueForKey:@"downloadProgress"] integerValue];
    if (progress >=100)
        [self.tableView performSelector:@selector(reloadData) withObject:nil afterDelay:1.0f];
}
- (void)laterUpdateTable:(NSNotification *)noti{
    [self performSelector:@selector(updateTable:) withObject:noti afterDelay:0.5];
}

- (void)updateTable:(NSNotification *)noti{
    [self reloadMLFiles];
    [self reloadUploadVideos];
    [self.fetchResultController performFetch:nil];
    NSRange range = NSMakeRange(0, 3);
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:range] withRowAnimation: UITableViewRowAnimationFade];
    
    [self hiddenTableViewWithNoVideo];
}

- (void)hiddenTableViewWithNoVideo{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchResultController sections] objectAtIndex:0];
    NSInteger numberOfDownloadTasks = [sectionInfo numberOfObjects];
    NSInteger numberOfDownloadFiles = [_sortMLFiles count];
    NSInteger numberOfUploadFiles = [_uploadVideos count];
    if (numberOfDownloadFiles == 0 && numberOfDownloadTasks == 0 && numberOfUploadFiles == 0) {
        [self hideDataView];
    }else
        [self showDataView];
}
#pragma mark - PrivateMethod
- (void)deleteAction:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath withCompletedBlock:(void(^)(void))completed{
    QNLocalVideoFileListCell *targetCell = (QNLocalVideoFileListCell *)[tableView cellForRowAtIndexPath:indexPath];
    [[NSNotificationCenter defaultCenter] removeObserver:targetCell];

    MLFile *mlFile = (MLFile *)targetCell.userInfo;
    NSInteger section = [indexPath section];
    switch (section) {
        case 0:{ //kill the download process
            VideoDownload *downloadItem = [VideoDownload MR_findFirstByAttribute:@"filmID" withValue:targetCell.fID];
            [downloadItem MR_deleteEntity];
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *e){
                [self.videoStation cancelDownloadTaskWithID:targetCell.fID];
                if (completed) {
                    completed();
                }
            }];
        }
            break;
        case 1:{ // kill the upload process
            VideoUpload *uploadItem = [VideoUpload MR_findFirstByAttribute:@"filmID" withValue:targetCell.fID];
            [uploadItem MR_deleteEntity];
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *e){
                [self.videoStation cancelUploadTask:targetCell.fID];
                if (completed) {
                    completed();
                }

            }];
        }
            break;
        case 2: //remove the local video-file
            [self removeMediaObject:mlFile];
            if (completed) {
                completed();
            }
            break;
    }
}

- (void)backNavi{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)removeMediaObject:(MLFile *)mediaObject{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *folderLocation = [[((NSURL *)[NSURL URLWithString:mediaObject.url]) path] stringByDeletingLastPathComponent];
    NSArray *allfiles = [fileManager contentsOfDirectoryAtPath:folderLocation error:nil];
    NSString *fileName = [[[((NSURL *)[NSURL URLWithString:mediaObject.url]) path] lastPathComponent] stringByDeletingPathExtension];
    NSIndexSet *indexSet = [allfiles indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return ([obj rangeOfString:fileName].location != NSNotFound);
    }];
    unsigned int count = indexSet.count;
    NSString *additionalFilePath;
    NSUInteger currentIndex = [indexSet firstIndex];
    for (unsigned int x = 0; x < count; x++) {
        additionalFilePath = allfiles[currentIndex];
        if ([additionalFilePath isSupportedSubtitleFormat])
            [fileManager removeItemAtPath:[folderLocation stringByAppendingPathComponent:additionalFilePath] error:nil];
        currentIndex = [indexSet indexGreaterThanIndex:currentIndex];
    }
    [fileManager removeItemAtPath:[((NSURL *)[NSURL URLWithString:mediaObject.url]) path] error:nil];
    [[MLMediaLibrary sharedMediaLibrary] updateMediaDatabase];
}

- (void)hideDataView{
    [UIView animateWithDuration:0.5f animations:^(void){
        self.statusView.alpha = 1.0f;
        self.tableView.separatorColor = [UIColor clearColor];
    }];
}

- (void)showDataView{
    [UIView animateWithDuration:0.5f animations:^(void){
        self.statusView.alpha = 0.0f;
        self.tableView.separatorColor = [UIColor grayColor];
    }];
}

@end
