//
//  LoginWaySelectionTableView.m
//  QVideo
//
//  Created by Change.Liao on 2014/1/17.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "LoginWaySelectionTableView.h"
#import "LoginWaySelectionTableViewCell.h"
#import <QNAPFramework/QNAPCommunicationManager.h>
#import "QNVideoTool.h"

@implementation LoginWaySelectionTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withNASServer:(NASServer *)server{
    self = [self initWithFrame:frame];
    self.serverData = server;
    return self;
}
#pragma mark - UITableViewDatasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LoginWaySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"selectionWayCell"];
    if (!cell) {
        cell = (LoginWaySelectionTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"LoginSelections" owner:nil options:nil] objectAtIndex:0];
    }
    /**
     #1: LAN IP
     #2: myQNAPCloud
     #3: CloudLink
     #4: DDNS
     #5: WAN IP
     
     TODO: localizable
     */
    switch ([indexPath row]) {
        case 0:{
            [cell.loginAction setText:@"By LAN IP:"];
            [cell.loginURL setText:self.serverData.lanIP];
        }
            break;
        case 1:{
            [cell.loginAction setText:@"By myQNAPcloud:"];
            [cell.loginURL setText:self.serverData.myQNAPCloud];
        }
            break;
        case 2:{
            [cell.loginAction setText:@"By CloudLink:"];
            [cell.loginURL setText:self.serverData.cloudLink];
        }
            break;
        case 3:{
            [cell.loginAction setText:@"By WAN IP:"];
            [cell.loginURL setText:self.serverData.wanIP];
        }
            break;
        default:
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 63.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    void(^loginProcess)(void) = ^(void){
        [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *err){
            if ([self.loginDelgate respondsToSelector:@selector(loginNASServer:withCompletionBlock:)]) {
                [self.loginDelgate loginNASServer:self.serverData withCompletionBlock:^(BOOL success, NSError *error){
                    
                }];
            }
        }];
    };
    
    if ([indexPath row] == 2 && self.serverData.cloudLink != nil) {
        [[QNVideoTool share] connectToTUTK:self.serverData.cloudLink completionBlock:^(BOOL success, NSError *e, NSDictionary *dic){
            if (success) {
                self.serverData.mappingPort = dic[@"tutkMappingPort"];
                self.serverData.currentAddress = @"http://127.0.0.1";
                loginProcess();
            }else
                [QNVideoTool toastWithTargetViewController:(UIViewController *)self.loginDelgate
                                                  withText:NSLocalizedString(@"ConnectToCloudLinkFailed", nil)
                                                 isSuccess:NO];
        }];
    }else{
        LoginWaySelectionTableViewCell *cell = (LoginWaySelectionTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        NSString *url = cell.loginURL.text;
        self.serverData.currentAddress = url;
        
        loginProcess();
    }
}
@end
