//
//  QNNasListTableCell.h
//  QNAPFramework
//
//  Created by Change.Liao on 2013/10/24.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNButton.h"

@interface QNAllNasListTableCell : UITableViewCell
@property (nonatomic, strong)IBOutlet UIImageView *nasIcon;
@property (nonatomic, strong)IBOutlet UILabel *nasTitle;
@property (nonatomic, strong)IBOutlet UILabel *nasAddress;
@property (nonatomic, strong)IBOutlet UILabel *nasAccount;
@property (nonatomic, strong)IBOutlet QNButton *detailBtn;
@property (nonatomic, strong)id userInfo;
@end
