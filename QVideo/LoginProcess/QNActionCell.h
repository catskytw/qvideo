//
//  QNActionCell.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/5.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNActionCell : UITableViewCell
@property(nonatomic, weak) IBOutlet UILabel *actionString;
@end
