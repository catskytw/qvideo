//
//  QNLocalNASListViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/4.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNLocalNASListViewController.h"
#import "UPnPManager.h"
#import <QNAPFramework/QNAPFramework.h>
#import "QNAllNasListTableCell.h"
#import "QNAddingNASDetailViewController.h"
#import "QNSearchNASRefreshView.h"
#import <QNAPFramework/QNAPCommunicationManager.h>
#import <SVPullToRefresh/SVPullToRefresh.h>
#import "QNVideoTool.h"

@interface QNLocalNASListViewController (){
    NSArray *_UPNPdevices;
    NSArray *_filteredUPNPDevices;
    BOOL _isSearching;
}

@end

@implementation QNLocalNASListViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    _udpSearchNASDic = [NSMutableDictionary dictionary];
    _udpIncomingSequence = [NSMutableArray array];
    _isSearching = NO;
    [self settingLocalizable];
    [self settingAttributeNoteLabel];
    self.tableView.contentOffset = CGPointMake(0, -150.0f);
    [self.noNASErrorLabel setText:NSLocalizedString(@"noNASInLocal", nil)];
    self.noNASErrorLabel.hidden = YES;
    [self.tableView addPullToRefreshWithActionHandler:^(void){
        [self startUDPSearch];
    }];
}

- (void)settingAttributeNoteLabel{
    [self.noteLabel setBackgroundColor:[UIColor colorWithHexString:@"#f2f2f2"]];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.headIndent = 15.0f;
    paragraphStyle.firstLineHeadIndent = 15.0f;
    
    NSAttributedString *noteString = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"AddNasNote", nil)
                                                                     attributes:@{NSParagraphStyleAttributeName: paragraphStyle}];
    [self.noteLabel setAttributedText:noteString];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSUInteger count = [NASServer MR_countOfEntitiesWithContext:[NSManagedObjectContext MR_defaultContext]];
    if (self.needBackNaviItem || count > 0) {
        [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(backNavi:) withTitleText:NSLocalizedString(@"Back", nil)];
    }else{
        UIBarButtonItem *barBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_navigationbar_login-nas_info"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(gotoAboutPage)];
        [barBtn setTintColor:[UIColor blackColor]];
        [self.navigationItem setLeftBarButtonItem:barBtn];
    }
    
    QNButton *btn = [QNVideoTool generateCustomRightBarItem:self withSelect:@selector(reloadNASs:) withTitleText:nil];
    [btn setFrame:CGRectMake(0, 0, 36.0f, 36.0f)];
    [btn setImage:[UIImage imageNamed:@"icon_navigationbar_refresh.png"] forState:UIControlStateNormal];
    btn.showsTouchWhenHighlighted = YES;
}

- (void)gotoAboutPage{
    DDLogVerbose(@"about page");
    [self performSegueWithIdentifier:@"enterAbout" sender:nil];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self reloadNASs:nil];
}

- (void)reloadNASs:(id)sender{
    [self.tableView triggerPullToRefresh];
    [self startUDPSearch];
}

- (void)startUDPSearch{
    self.noNASErrorLabel.hidden = YES;
    [self.tableView setSeparatorStyle:([_udpIncomingSequence count] == 0)?UITableViewCellSeparatorStyleNone:UITableViewCellSeparatorStyleSingleLine];
    
    [[QNAPCommunicationManager share] startUDPSearch:^(NSDictionary *dataDic){
        /**
         NSString *serverName, NSString *modelName, NSString *host
         */
        NSString *host = dataDic[@"host"];
        
        NSString *newHost = [[host componentsSeparatedByString:@":"] lastObject];
        if (![_udpIncomingSequence containsObject:newHost] && newHost != nil) {
            [_udpIncomingSequence addObject:newHost];
            [_udpSearchNASDic setValue:dataDic forKey:newHost];
            [self.tableView reloadData];
        }
    }];
    [self performSelector:@selector(stopRefreshing) withObject:nil afterDelay:5.0f];
}

- (void)stopRefreshing{
    [self.tableView setSeparatorStyle:([_udpIncomingSequence count] == 0)?UITableViewCellSeparatorStyleNone:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView.pullToRefreshView stopAnimating];
    
    NSInteger count = [[_udpSearchNASDic allKeys] count];
    self.noNASErrorLabel.hidden = count == 0 ? NO:YES;

}
- (void)backNavi:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)settingLocalizable{
    [self.addLabel setText:NSLocalizedString(@"AddManually", nil)];
    [self.navigationItem setTitle:NSLocalizedString(@"AddNAS", nil)];
}
#pragma mark - Search Service
- (void)_startUPNPDiscovery{
    _isSearching = YES;
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    UPnPManager *managerInstance = [UPnPManager GetInstance];
    
    _UPNPdevices = [[managerInstance DB] rootDevices];
    
    if (_UPNPdevices.count > 0)
        [self UPnPDBUpdated:nil];
    
    [[managerInstance DB] addObserver:(UPnPDBObserver*)self];
    
    //Optional; set User Agent
    [[managerInstance SSDP] setUserAgentProduct:[NSString stringWithFormat:@"VLC for iOS/%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]] andOS:@"iOS"];
    
    //Search for UPnP Devices
    [[managerInstance SSDP] startSSDP];
    [[managerInstance SSDP] searchSSDP];
    [[managerInstance SSDP] SSDPDBUpdate];
}

-(void)UPnPDBWillUpdate:(UPnPDB*)sender{
}

- (void)UPnPDBUpdated:(UPnPDB*)sender{
    NSUInteger count = _UPNPdevices.count;
    BasicUPnPDevice *device;
    NSMutableArray *mutArray = [[NSMutableArray alloc] init];
    for (NSUInteger x = 0; x < count; x++) {
        device = _UPNPdevices[x];
        DDLogVerbose(@"device searched, urn:%@, udid:%@, type:%@, udn:%@, usn:%@", [device urn], [device uuid], [device type], [device udn], [device usn]);
        if ([[device urn] isEqualToString:@"urn:schemas-upnp-org:device:MediaServer:1"]){
            [mutArray addObject:device];
        }
        else
            DDLogError(@"found device '%@' with unsupported urn '%@'", [device friendlyName], [device urn]);
    }
    _filteredUPNPDevices = nil;
    _filteredUPNPDevices = [NSArray arrayWithArray:mutArray];
    _isSearching = NO;
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = [[_udpSearchNASDic allKeys] count];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    NSString *CellIdentifier = (row == 0 && _isSearching)? @"searchCell":@"nasListCell";
    
    QNAllNasListTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    int index = [indexPath indexAtPosition:1];
    /**
     *              [_udpSearchNASDic setValue:@{@"serverName":serverName, @"modelName":modelName}
     */
    NSString *key = [_udpIncomingSequence objectAtIndex:index];
    NSDictionary *value = [_udpSearchNASDic objectForKey:key];
    
    [cell.nasTitle setText: [value valueForKey:@"serverName"]];
    [cell.nasIcon setImage:[UIImage imageNamed:[QNVideoTool getImageFromModelName:[value valueForKey:@"modelName"]]]];
    [cell.nasAddress setText:key];
    
    cell.userInfo = @{@"serverName": [value valueForKey:@"serverName"],
                      @"serverHost": key,
                      @"modelName":[value valueForKey:@"modelName"],
                      @"sslPort":[value valueForKey:@"sslPort"],
                      @"normalPort":[value valueForKey:@"port"]};
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 89.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0f;
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /* addingNAS */
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    /**
     *  @{@"serverName": value, @"serverHost": key};
     */
    if ([segue.identifier isEqualToString:@"addingNASByNASItem"]){
        NSDictionary *infoDic = ((QNAllNasListTableCell *)sender).userInfo;
        QNAddingNASDetailViewController *targetViewController = (QNAddingNASDetailViewController *)segue.destinationViewController;
        [targetViewController generateNewNAS];
        [targetViewController.nasServer setServerName:infoDic[@"serverName"]];
        [targetViewController.nasServer setLanIP:infoDic[@"serverHost"]];
        [targetViewController.nasServer setCurrentAddress:infoDic[@"serverHost"]];
        [targetViewController.nasServer setModelName:infoDic[@"modelName"]];
        [targetViewController.nasServer setMyQNAPCloud:[NSString stringWithFormat:@"%@.myqnapcloud.com", infoDic[@"modelName"]]];

        [targetViewController.nasServer setSsl_external_port:infoDic[@"sslPort"]];
        [targetViewController.nasServer setSsl_internal_port:infoDic[@"sslPort"]];
        [targetViewController.nasServer setSslPort:infoDic[@"sslPort"]];
        
        [targetViewController.nasServer setNormal_external_port:infoDic[@"normalPort"]];
        [targetViewController.nasServer setNormal_internal_port:infoDic[@"normalPort"]];
        [targetViewController.nasServer setNormalPort:infoDic[@"normalPort"]];

        targetViewController.isEditAction = NO;
        targetViewController.nasServer.isLoginAfterSaving = @(YES);
    }else if ([segue.identifier isEqualToString:@"addingNAS"]){
        QNAddingNASDetailViewController *targetViewController = (QNAddingNASDetailViewController *)segue.destinationViewController;
        [targetViewController generateNewNAS];
        targetViewController.isEditAction = NO;
    }
    
}
#pragma mark - ScrollTrigger
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
}

#pragma mark - PrivateMethod
- (NSString *)fetchingIPAddressFromUpnpDevice:(BasicUPnPDevice *)device{
    NSString *url =[[device baseURL] absoluteString];
    NSString *address = [[[url stringByReplacingOccurrencesOfString:@"http://" withString:@""] componentsSeparatedByString:@"/"] objectAtIndex:0];
    
    return [[address componentsSeparatedByString:@":"] objectAtIndex:0];
}

@end
