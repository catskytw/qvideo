//
//  QNTurtorialViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/2/25.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNTurtorialViewController.h"
#import <PPiAwesomeButton/UIButton+PPiAwesome.h>
#import "QNVideoTool.h"
#import "QNAppDelegate.h"
#import "QNIntroPageViewController.h"
#import <PPiAwesomeButton/UIButton+PPiAwesome.h>
#import <PPiAwesomeButton/UIAwesomeButton.h>

@interface QNTurtorialViewController ()

@end

@implementation QNTurtorialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self generateIntroPages];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //TODO isFirstRun
    if (self.forceTutorial) {
        [QNVideoTool generateBlackLeftBarItem:self withSelect:@selector(backNavi:)];
    }else{
        [self.navigationItem setBackBarButtonItem:nil];
        BOOL isFirstRun = [[[NSUserDefaults standardUserDefaults] valueForKey:@"isFirstRun"] boolValue];
        if (!isFirstRun && [[NSUserDefaults standardUserDefaults] valueForKey:@"isFirstRun"]) {
            [self performSegueWithIdentifier:@"enterNasListFromTutorial" sender:nil];
        }else{
            [QNVideoTool generateCustomRightBarItem:self withSelect:@selector(feedback) withTitleText:NSLocalizedString(@"Feedback", nil)];
            
            UIImage *_image = [UIImage imageNamed:@"icon_navigationbar_qvideo"];
            UIImageView *titleImage = [[UIImageView alloc] initWithImage:_image];
            [self.navigationItem setTitleView:titleImage];
        }
    }
}

- (void)backNavi:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)feedback{
    [[QNVideoTool share] feedBack: self];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - GenerateIntroduction

- (void)generateIntroPages{
    NSMutableArray *contentArray = [NSMutableArray array];
    NSArray *files = @[@"Welcome", @"Watch", @"Management", @"Streaming", @"Share", @"Login"];

    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];

    NSInteger index = 0;
    for (NSString *fileName in files) {
//introPageViewController
        QNIntroPageViewController *pageViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"introPageViewController"];
        pageViewController.contentFileName = fileName;
        
        pageViewController.contentImageFileName = (index == 0)? @"about_index_logo.png":[NSString stringWithFormat:@"Introduction_%i.png", index];
        EAIntroPage *page = [EAIntroPage pageWithCustomView:pageViewController.view];
        [contentArray addObject:page];
        
        index ++;
    }
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andPages:contentArray];
    intro.delegate = self;
    [intro setBackgroundColor:[UIColor whiteColor]];
    [intro showInView:self.view animateDuration:0.0];
    intro.pageControl.pageIndicatorTintColor = [UIColor grayColor];
    intro.pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    intro.pageControl.autoresizingMask = UIViewAutoresizingNone;
    intro.pageControl.frame = CGRectMake(20.0f, self.view.frame.size.height - 84.0f, intro.pageControl.frame.size.width, intro.pageControl.frame.size.height);
    intro.swipeToExit = NO;
    intro.showSkipButtonOnlyOnLastPage = NO;
    
    [intro.skipButton setIsAwesome:NO];
    [intro.skipButton setIconPosition:IconPositionRight];
    [intro.skipButton setSeparation:0];
    [intro.skipButton setImage:[UIImage imageNamed:@"about_introduction_Start-Done.png"] forState:UIControlStateNormal];
    [intro.skipButton setButtonText:NSLocalizedString(@"Skip", nil)];

    [intro.skipButton addTarget:intro action:@selector(skipIntroduction) forControlEvents:UIControlEventTouchUpInside];
    
    intro.skipButton.translatesAutoresizingMaskIntoConstraints = NO;
    [intro addConstraint:[NSLayoutConstraint constraintWithItem:intro.skipButton attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:intro attribute:NSLayoutAttributeRight multiplier:1 constant:-10]];
    [intro addConstraint:[NSLayoutConstraint constraintWithItem:intro.skipButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:intro attribute:NSLayoutAttributeBottom multiplier:1 constant:-20]];
}

#pragma mark - Introduction Delegate
- (void)introDidFinish:(EAIntroView *)introView{
    if (self.forceTutorial) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self performSegueWithIdentifier:@"enterNasListFromTutorial" sender:nil];
        [[NSUserDefaults standardUserDefaults] setValue:@(NO) forKey:@"isFirstRun"];
    }
    //enterNasList
}

- (void)intro:(EAIntroView *)introView pageAppeared:(EAIntroPage *)page withIndex:(NSInteger)pageIndex{
    
}

- (void)intro:(EAIntroView *)introView pageStartScrolling:(EAIntroPage *)page withIndex:(NSInteger)pageIndex{
    
}

- (void)intro:(EAIntroView *)introView pageEndScrolling:(EAIntroPage *)page withIndex:(NSInteger)pageIndex{
    
}


@end
