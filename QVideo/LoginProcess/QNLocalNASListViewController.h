//
//  QNLocalNASListViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/4.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <MNMPullToRefresh/MNMPullToRefreshManager.h>
#import "QNVideoStatusView.h"
@interface QNLocalNASListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    NSMutableDictionary *_udpSearchNASDic;
    NSMutableArray *_udpIncomingSequence;
}
@property (weak, nonatomic) IBOutlet UILabel *noNASErrorLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) BOOL needBackNaviItem;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UILabel *searchingNASLabel;
@property (weak, nonatomic) IBOutlet UILabel *addLabel;
//@property (strong, nonatomic) MNMPullToRefreshManager *pullManager;
@end
