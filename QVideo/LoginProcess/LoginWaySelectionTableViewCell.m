//
//  LoginWaySelectionTableViewCell.m
//  QVideo
//
//  Created by Change.Liao on 2014/1/17.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "LoginWaySelectionTableViewCell.h"

@implementation LoginWaySelectionTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
