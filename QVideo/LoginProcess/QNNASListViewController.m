//
//  QNNASListViewController.m
//  QNAPFramework
//
//  Created by Change.Liao on 2013/10/24.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNNASListViewController.h"
#import "QNAllNasListTableCell.h"
#import "NASServer.h"
#import "QNAppDelegate.h"
#import "QNVideosViewController.h"
#import "QNAddingNASDetailViewController.h"
#import "loginProceedingView.h"
#import "LoginWaySelectionTableView.h"

#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <QNAPFramework/QNAPFramework.h>
#import <QNAPFramework/QNAPCommunicationManager.h>
#import <QNAPFramework/QNDomainIPListResponse.h>
#import <QNAPFramework/QNAPFrameworkUtil.h>
#import <QNAPFramework/QNLogin.h>
#import <QNAPFramework/QNVideoRender.h>
#import <QNAPFramework/QNCollection.h>
#import <QNAPFramework/QNService.h>
#import <QNAPFramework/QPKGItem.h>
#import <QNAPFramework/QPKGPlatform.h>
#import <QNAPFramework/QNQPKGInstallStatus.h>
#import <QNAPFramework/QNFileModel.h>
#import <QNAPFramework/MLFile.h>
#import <TSMessages/TSMessage.h>

#import "QNVideoTool.h"
#import "QNDownloadListViewController.h"
#import "QNSettingViewController.h"
#import "QNLocalNASListViewController.h"
#import "QNLoginErrorOptionView.h"
#import "QVideoEnumberator.h"

#define noAskResolutionMachines @[@"TS-269"]
#define tutkPrefix @"http://127.0.0.1"
@interface QNNASListViewController (){
    NSMutableArray *_foundMLItems;
    QNAPCommunicationManager *_communicationManager;
    CXAlertView *_alert;
}

/**
 *  This methos is invoked while any error in the filestation's login.
 *  There are two options:
 *  1) Edit the account/password and try again
 *  2) Select another domain name/ip to login.
 */
- (void)showSelections:(NASServer *)targetNAS;
@end

@implementation QNNASListViewController

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    _isSortingByName = YES;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    _communicationManager = [QNAPCommunicationManager share];
    [_communicationManager activateDebugLogLevel:ddLogLevel];
    _thisContext = [NSManagedObjectContext MR_defaultContext];
    _needAutoLogin = YES;
    
    [[QNAPCommunicationManager share] activateAllStation:@{
                                                           @"NASURL":@"",
                                                           @"MyCloudURL":MyCloudServerBaseURL,
                                                           @"ClientId":CLIENT_ID,
                                                           @"ClientSecret":CLIENT_SECRET
                                                           }];
    [self settingLocalizable];
}

- (void)settingLocalizable{
    [self.downloadLabel setText:NSLocalizedString(@"Download", nil)];
    [self.addLabel setText:NSLocalizedString(@"Add", nil)];
    [self.settingLabel setText:NSLocalizedString(@"Setting", nil)];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self settingSortingSegmentedControl];
    [self updateContentOfServers];
    [self settingNavibar];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (_needAutoLogin) {
        [self autoLoginProcess];
    }

    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    if ([_foundMLItems count] <= 0 && [[MLFile allFiles] count] == 0) {
        [self performSegueWithIdentifier:@"enterAddingNASList" sender:self];
    }

}

- (void)settingSortingSegmentedControl{
    NSArray *items = @[@{@"text":NSLocalizedString(@"ServerName", nil), @"icon":[UIImage imageNamed:@"login-nas_storting_descending"]},
                       @{@"text":NSLocalizedString(@"HostIP", nil), @"icon":[UIImage imageNamed:@"login-nas_storting_descending"]}
                       ];
    _sortingSegmentedControl = [[QNFlatSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 24.0f)
                                                                        items:items
                                                                 iconPosition:IconPositionLeft
                                                            andSelectionBlock:^(NSUInteger segmentIndex){
                                                                _isSortingByName = (segmentIndex == 0)?YES:NO;
                                                                [self updateContentOfServers];
                                                            }
                                                               iconSeparation:0.0f];
    [_sortingSegmentedControl setBorderWidth:0.0f];
    [_sortingSegmentedControl setColor:[UIColor colorWithHexString:@"#f2f2f2"]];
    [_sortingSegmentedControl setSelectedColor:[UIColor colorWithHexString:@"#d4d4d4"]];
    [_sortingSegmentedControl setTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12], NSForegroundColorAttributeName:[UIColor blackColor]}];
    [_sortingSegmentedControl setSelectedTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12], NSForegroundColorAttributeName:[UIColor blackColor]}];
    [_sortingSegmentedControl setEnabled:YES forSegmentAtIndex:1];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self settingSortingSegmentedControl];
    [self.tableView reloadData];
}

- (void)settingNavibar{
    //setting TitleView
    UIImage *_image = [UIImage imageNamed:@"icon_navigationbar_qvideo"];
    UIImageView *titleImage = [[UIImageView alloc] initWithImage:_image];
    [self.navigationItem setTitleView:titleImage];

    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_navigationbar_login-nas_info"]
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(gotoAboutPage)];
    [barBtn setTintColor:[UIColor blackColor]];
    [self.navigationItem setLeftBarButtonItem:barBtn];
}

- (void)gotoAboutPage{
    DDLogVerbose(@"about page");
    [self performSegueWithIdentifier:@"enterAbout" sender:nil];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateContentOfServers{
    @synchronized(_foundMLItems){
        NSArray *allNASServer = [NASServer MR_findAllSortedBy:_isSortingByName?@"serverName":@"currentAddress" ascending:YES inContext:[NSManagedObjectContext MR_defaultContext]];
        _foundMLItems = [[NSMutableArray alloc] initWithArray:allNASServer];
        if ([_foundMLItems count] > 0){
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//            [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(backNavi:) withTitleText:NSLocalizedString(@"Back", nil)];
        }
        else{
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            [self.navigationItem setLeftBarButtonItem:nil];
        }
        [self.tableView reloadData];
    }
}

- (IBAction)returnBack:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)backNavi:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    return [_foundMLItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 89.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"nasListCell";
    NASServer *_server = [_foundMLItems objectAtIndex:[indexPath row]];
    QNAllNasListTableCell *cell = (QNAllNasListTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    // Configure the cell...
    [cell.nasTitle setText: _server.serverName];
    [cell.nasIcon setImage:[UIImage imageNamed:[QNVideoTool getImageFromModelName:_server.modelName]]];
    [cell.nasAddress setText:_server.currentAddress];
    [cell.nasAccount setText:_server.account];
    cell.detailBtn.userInfo = [_foundMLItems objectAtIndex:[indexPath row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = [indexPath row];
    NASServer *targetServer = [_foundMLItems objectAtIndex:row];
    QNAllNasListTableCell *cell = (QNAllNasListTableCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    void(^loginProcess)(void) = ^(void){
        [self loginNASServer:targetServer withCompletionBlock:^(BOOL success, NSError *error){
            if (!success) {
                QNFileLoginError *loginError = [error.userInfo valueForKey:@"additional"];
                NSString *message = nil;
                BOOL b = [loginError.authPassed boolValue];
                if ([loginError.errorValue intValue] == -1011) {
                    message = NSLocalizedString(@"QNLoginErrorOptionTitle", nil);
                }else if (!b){
                    message = NSLocalizedString(@"IncorrectLoginInfo", nil);
                }
                
                [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                              withActionTitle:NSLocalizedString(@"EditNASInfo", nil)
                              withCancelTitle:NSLocalizedString(@"Cancel", nil)
                                  withMessage:message
                            withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *item){
                                [alert dismiss];
                                [self performSegueWithIdentifier:@"serverInfo" sender:cell.detailBtn];
                            }];
            }
        }];
    };
    
    if ([targetServer.currentAddress hasPrefix:tutkPrefix]) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:nil
                          withMessage:[NSString stringWithFormat:@"%@ CloudLink...", NSLocalizedString(@"ConnectTo", nil)]
                    withActionHandler:nil];
        [QNVideoTool share].nasServerInfo = targetServer;
        [[QNVideoTool share] connectToTUTK:targetServer.cloudLink completionBlock:^(BOOL success, NSError *e, NSDictionary *dic){
            if (success) {
                targetServer.mappingPort = dic[@"tutkMappingPort"];
                loginProcess();
            }else
                [QNVideoTool toastWithTargetViewController:self
                                                  withText:NSLocalizedString(@"ConnectToCloudLinkFailed", nil)
                                                 isSuccess:NO];
        }];
    }else
        loginProcess();
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return _sortingSegmentedControl.frame.size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return _sortingSegmentedControl;
}
#pragma mark - PrivateMethod
- (void)loginNASServer:(NASServer *)nasData withCompletionBlock:(void(^)(BOOL success, NSError *error))completion{
    //save a ref in [QNVideoTool share]
    [QNVideoTool share].nasServerInfo = nasData;
    [[QNVideoTool share] logoutClean];

    void (^runLoginProcess)(void) = ^(){
        _isCancelLogin = NO;
        NASServer *targetServer = nasData;
        LoginProceedingView *loginProceedingView = [[[NSBundle mainBundle] loadNibNamed:@"LoginProceedingView" owner:nil options:nil] objectAtIndex:0];
        [loginProceedingView.activityIndicator startAnimating];
        loginProceedingView.selectBtn.userInfo = targetServer;
        [loginProceedingView.selectBtn setTitle:NSLocalizedString(@"SelectOtherConnection", nil) forState:UIControlStateNormal];
        [loginProceedingView.selectBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [loginProceedingView.selectBtn addTarget:self action:@selector(selectLoginWay:) forControlEvents:UIControlEventTouchUpInside];
        
        [loginProceedingView.loginName setText:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"ConnectTo", nil),targetServer.serverName]];
        [loginProceedingView.loginURL setText:targetServer.currentAddress];
        
        //first login
        if (targetServer.wanIP == nil && targetServer.lanIP == nil) {
            loginProceedingView.frame = CGRectMake(loginProceedingView.frame.origin.x, loginProceedingView.frame.origin.y, loginProceedingView.frame.size.width, loginProceedingView.frame.size.height - loginProceedingView.selectBtn.frame.size.height);
        }
        UIView *contentView = [[UIView alloc] initWithFrame:loginProceedingView.frame];
        [contentView addSubview:loginProceedingView];
        _alert = [[CXAlertView alloc] initWithTitle:nil contentView:contentView cancelButtonTitle:nil];
        [_alert addButtonWithTitle:NSLocalizedString(@"CancelLogin", nil) type:CXAlertViewButtonTypeCancel handler:^(CXAlertView *alert, CXAlertButtonItem *button){
            _isCancelLogin = YES;
            [[QNAPCommunicationManager share].fileStationsManager cancelLoginRequest];
            [alert dismiss];
        }];
        [_alert show];
        
        [CXAlertView cleanAllAlertBesideThis:_alert];
        NSString *correctURL = [[QNVideoTool share] correctURL:targetServer];
        [[QNAPCommunicationManager share] activateAllStation:@{
                                                               @"NASURL":correctURL,
                                                               @"MyCloudURL":MyCloudServerBaseURL,
                                                               @"ClientId":CLIENT_ID,
                                                               @"ClientSecret":CLIENT_SECRET
                                                               }];
        //TODO test code
        [self fileStationLogin:targetServer withCompletionBlock:completion];
    };
    
    if (![nasData.isRememberPwd boolValue]) {
        [CXAlertView showInputTextfieldAlert:NSLocalizedString(@"NeedPwdTitle", nil)
                             withActionTitle:NSLocalizedString(@"Done", nil)
                       withTextFieldDelegate:self
                           withActionHandler:^(CXAlertView *v, CXAlertButtonItem *btn){
                               UITextField *_textField = (UITextField *)v.contentView;
                               [v dismiss];
                               nasData.password = _textField.text;
                               runLoginProcess();
                           }];
    }else
        runLoginProcess();
}

- (LoginWaySelectionTableView *)createLoginSelectionTable:(NASServer *)serverData{
    LoginWaySelectionTableView *selectionTable = [[LoginWaySelectionTableView alloc] initWithFrame:CGRectMake(0, 0, 240, 200) withNASServer:serverData];
    selectionTable.delegate = selectionTable;
    selectionTable.dataSource = selectionTable;
    [selectionTable setBackgroundColor:[UIColor clearColor]];
    return selectionTable;
}

- (void)loginErrorSteps:(QNFileLoginError *)e{
    if (_alert) {
        [_alert dismiss];
        _alert = nil;
    }
    NSString *subTitle = (([e.authPassed intValue] == 0) || ([e.errorCode intValue] == 10000))?NSLocalizedString(@"authError", nil):[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"LoginErrorValue", nil),e.errorValue];
    [TSMessage showNotificationInViewController:self
                                          title:NSLocalizedString(@"LoginError", nil)
                                       subtitle:subTitle
                                           type:TSMessageNotificationTypeError];
}
#pragma mark - ActionMethod
- (void)selectLoginWay:(id)sender{
    [_alert cleanAllPenddingAlert];
    [_alert dismiss];

    NASServer *_thisServer = ((QNButton *)sender).userInfo;
    
    LoginWaySelectionTableView *selectionTable = [self createLoginSelectionTable:_thisServer];
    [selectionTable registerNib:[UINib nibWithNibName:@"LoginSelections"
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"selectionWayCell"];
    [selectionTable setBackgroundColor:[UIColor clearColor]];
    selectionTable.loginDelgate = self;
    
    _alert = [CXAlertView showContentViewAlert:nil
                               withActionTitle:nil
                               withContentView:selectionTable
                             withActionHandler:nil];
}

- (IBAction)deletingNASServer:(id)sender{
    QNButton *detailBtn = (QNButton *)sender;
    [self performSegueWithIdentifier:@"deletingNAS" sender:detailBtn.userInfo];
}

- (void)fileStationLogin:(NASServer *)server withCompletionBlock:(void(^)(BOOL success, NSError *error))completion{
    __block NASServer *_thisServer = server;
    QNFileStationAPIManager *_fileManager = [QNAPCommunicationManager share].fileStationsManager;
    //TODO wait cursor here
    [_fileManager loginWithAccount:_thisServer.account
                      withPassword:_thisServer.password
                  withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, QNFileLogin *loginInfo){
                      [QNVideoTool share].loginInfo = loginInfo;
                      QNFileModel *modelInfo = (QNFileModel *)loginInfo.relationship_model;
                      _thisServer.modelName = modelInfo.modelName;
                      
                      if ([noAskResolutionMachines containsObject: _thisServer.modelName]) {
                          [[NSUserDefaults standardUserDefaults] setValue:@(NO) forKeyPath:@"needAskResolution"];
                      }
                      
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *error){
                          [self multimediaLogin:_thisServer];
                      }];
                  }
                  withFailureBlock:^(RKObjectRequestOperation *o, QNFileLoginError *e){
                      completion(NO, [NSError errorWithDomain:e.errorValue code:[e.errorCode intValue] userInfo:@{@"additional": e}]);
                  }];

}

- (void)showSelections:(NASServer *)targetNAS{
    QNLoginErrorOptionView *contentView = [[[NSBundle mainBundle] loadNibNamed:@"QNLoginErrorOptionView" owner:nil options:nil] objectAtIndex:0];
    contentView.targetNAS = targetNAS;
    contentView.loginDelgate = self;
    
    UIView *containerView = [[UIView alloc] initWithFrame:contentView.frame];
    [containerView addSubview:contentView];

    CXAlertView *alert = [CXAlertView showContentViewAlert:NSLocalizedString(@"QNLoginErrorOptionTitle", nil)
                                           withActionTitle:nil
                                           withContentView:containerView
                                         withActionHandler:nil];
    [alert setTitleFont:[UIFont systemFontOfSize:16.0f]];
}

- (void)multimediaLogin:(NASServer *)server{
    __block NASServer *_thisServer = server;
    __block QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    __block QNFileStationAPIManager *_fileManager = [QNAPCommunicationManager share].fileStationsManager;

    [QNLogin MR_truncateAll];
    [videoStation loginViaVideoStation:server.account
                          withPassword:server.password
                      withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *m){
                          [CXAlertView cleanAllAlertBesideThis:_alert];
                          [_alert dismiss];

                          if (_isCancelLogin) {
                              _isCancelLogin = NO;
                              [_alert dismiss];
                              return ;
                          }
                          
                          //1.setting for next viewController.
                          QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
                          QNVideosViewController *videosViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"videosViewController"];
                          videosViewController.thisServer = _thisServer;
                          
                          [self.navigationController pushViewController:videosViewController animated:YES];
                          [self.navigationController hidesBottomBarWhenPushed];
                          
                          //2.setting the autologin value
                          NASServer *previousAutoLoginNAS = [NASServer MR_findFirstByAttribute:@"isAutoLogin" withValue:@(YES)];
                          if (previousAutoLoginNAS) {
                              previousAutoLoginNAS.isAutoLogin = @(NO);
                          }
                          if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isAutoLogin"] boolValue]){
                              _thisServer.isAutoLogin = @(YES);
                          }
                          
                          [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                          
                          //3.restore all downloadTask
                          [[QNVideoTool share] controlDownloadTask:nil];
                          
                          //4. fill the network information of this nasserver
                          [_fileManager getDomainIPListWithSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, QNDomainIPListResponse *obj){
                              _thisServer.wanIP = obj.external_ip;
                              _thisServer.lanIP = obj.local_ip;
                              _thisServer.myQNAPCloud = obj.mycloudnas_hn;
                              
                              if (obj.mycloudnas_hn) {
                                  NSArray *elements = [obj.mycloudnas_hn componentsSeparatedByString:@"."];
                                  if ([elements count] > 0 ) {
                                      _thisServer.cloudLink = [elements objectAtIndex:0];
                                  }
                              }

                              [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                              
                          }
                                                       withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                                           DDLogError(@"error while fetching domain ip list, %@", e);
                                                       }];
                          //5. fetching render list
                          QNLogin *loginInfo = (QNLogin *)m.firstObject;
                          [QNVideoTool share].multimediaLogin = loginInfo;
                          DDLogInfo(@"multimedia login successfully! videoStation ver: %f, firmware ver: %f", [loginInfo.appVersion floatValue], [loginInfo.builtinFirmwareVersion floatValue]);
                          CGFloat appVersionValue = [loginInfo.appVersion floatValue];
                          CGFloat firmwareVersionValue = [loginInfo.builtinFirmwareVersion floatValue];
                          //appVersion & firmwareVersion
                          [QNVideoRender MR_truncateAll]; //clear all renders
                          if (appVersionValue > 2.0 && firmwareVersionValue > 4.1) {
                              //ask NAS for renders
                              [videoStation getRenders:^(RKObjectRequestOperation *o, RKMappingResult *m){
                                  DDLogVerbose(@"fetching NAS renders successfully!");
                                  [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                              }
                                      withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                          DDLogError(@"error while fetching NAS renders %@", e);
                                      }];
                          }
                          
                      }
                      withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                          DDLogError(@"multimedia login error! %@", e);
                          if (e.code == -1011) { //You don't have permission to access
                              [self installOrEnableProcess];
                              return ;
                          }else{
                              QNFileLoginError *error = [QNFileLoginError MR_createEntity];
                              error.errorValue = [NSString stringWithFormat:@"%i", e.code];
                              [self loginErrorSteps: error];
                              //Don't save it into DB
                              [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
                          }
                      }];
}

- (void)installOrEnableProcess{
    
    //if you have no permission, you can't install/enable any Service
    if (![[QNVideoTool share].loginInfo.isAdmin boolValue]) {
        [QNVideoTool toastWithTargetViewController:self withText:NSLocalizedString(@"noPermissionEnableInstall", nil) isSuccess:NO];
        [CXAlertView cleanAllAlertBesideThis:nil];
        return;
    }
    
    __block QNAppCenterAPIManager *appCenter = [QNAPCommunicationManager share].appCenterManager;
    [QNService MR_truncateAll];
    [appCenter getNASServiceStatus:^(RKObjectRequestOperation *o, RKMappingResult *r){
        
        __block QNService *targetService = nil;
        
        //search the service's status
        for (QNService *service in r.array) {
            if ([service.service_id isEqualToString:@"videoStationPro"]) {
                targetService = service;
                break;
            }
        }
        
        //action: install or enable
        if (![targetService.installed boolValue])
            [self installVideoStationQPKG:targetService];
        else if ([targetService.installed boolValue] && ![targetService.enabled boolValue])
            [self enableVideoStationQPKG:targetService];
        
    }
                  withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                  }];
}

- (void)installVideoStationQPKG:(QNService *)serviceStatus{
    __block QNAppCenterAPIManager *appCenter = [QNAPCommunicationManager share].appCenterManager;
    
    [CXAlertView showMessageAlert:NSLocalizedString(@"Installation", nil)
                  withActionTitle:NSLocalizedString(@"install", nil)
                      withMessage:NSLocalizedString(@"ProceedInstallVideoStation", nil)
                withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                    [alert dismiss];
                    __block QNNASListViewController *blockSelf = self;
                    [ZAActivityBar showWithStatus:NSLocalizedString(@"InstallVideoStationProcessing", nil)];

                    [appCenter searchQPKGDownloadInfo:@"VideoStationPro"
                                     withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, QPKGItem *item){
                                         //In videoStation, we DON'T distinguish from arm to x86.
                                         QPKGPlatform *platForm = [[item.relationship_platform allObjects] objectAtIndex:0];
                                         [appCenter installQPKGFromURL:platForm.location //platForm.location
                                                      withInternalName:item.internalName
                                                      withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                                          [self enableVideoStationQPKG:serviceStatus];
                                                      }
                                                      withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                                          [ZAActivityBar dismiss];
                                                          [QNVideoTool toastWithTargetViewController:blockSelf
                                                                                            withText:NSLocalizedString(@"VideoStationInstallFailure", nil)
                                                                                           isSuccess:NO];
                                                      }];
                                     }
                                     withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                         [ZAActivityBar dismiss];
                                         [QNVideoTool toastWithTargetViewController:blockSelf
                                                                           withText:NSLocalizedString(@"NoInstallInfo", nil)
                                                                          isSuccess:NO];
                                     }];
                }];
}

- (void)enableVideoStationQPKG:(QNService *)serviceStatus{
    __block QNAppCenterAPIManager *appCenter = [QNAPCommunicationManager share].appCenterManager;

    [CXAlertView showMessageAlert:NSLocalizedString(@"Enable", nil)
                  withActionTitle:NSLocalizedString(@"EnableBtnTitle", nil)
                      withMessage:NSLocalizedString(@"EnableDescription", nil)
                withActionHandler:^(CXAlertView *v, CXAlertButtonItem *btn){
                    [v dismiss];
                    [ZAActivityBar showWithStatus:NSLocalizedString(@"ProceedEnable", nil)];
                    [appCenter settingQPKGActivationWithQPKGName:serviceStatus.service_id
                                                     enableValue:YES
                                                withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                                    [ZAActivityBar dismiss];
                                                    [self loginNASServer:[QNVideoTool share].nasServerInfo withCompletionBlock:^(BOOL s, NSError *error){}];
                                                }
                                                withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                                    [ZAActivityBar dismiss];
                                                    [QNVideoTool toastWithTargetViewController:self.navigationController
                                                                                      withText:NSLocalizedString(@"EnableFailure", nil)
                                                                                     isSuccess:NO];
                                                }];
                    
                }];
}
#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"serverInfo"]){
        QNAddingNASDetailViewController *targetViewController = (QNAddingNASDetailViewController *)segue.destinationViewController;
        targetViewController.nasServer = ((QNButton *)sender).userInfo;
        targetViewController.isEditAction = YES;
    }else if ([segue.identifier isEqualToString:@"downloadFile"]){
        QNDownloadListViewController *targetViewController = (QNDownloadListViewController *)segue.destinationViewController;
        targetViewController.isOnlyDownloadedFiles = YES;
    }else if ([segue.identifier isEqualToString:@"gotoSettingViewController"]){
        QNSettingViewController *settingViewController = (QNSettingViewController *)segue.destinationViewController;
        settingViewController.isNaviMode = YES;
    }else if ([segue.identifier isEqualToString:@"enterAddingNASList"]){
        QNLocalNASListViewController *localNASListViewController = (QNLocalNASListViewController *)segue.destinationViewController;
        localNASListViewController.needBackNaviItem = ([_foundMLItems count] > 0) ? YES:NO;
    }
}

#pragma mark - IntroDelegate
- (void)autoLoginProcess{
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isAutoLogin"] boolValue]){
        NASServer *previousAutoLoginNAS = [NASServer MR_findFirstByAttribute:@"isAutoLogin" withValue:@(YES)];
        if (previousAutoLoginNAS) {
            if ([previousAutoLoginNAS.currentAddress hasPrefix:tutkPrefix]) {
                [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                              withActionTitle:nil
                                  withMessage:[NSString stringWithFormat:@"%@ CloudLink...", NSLocalizedString(@"ConnectTo", nil)]
                            withActionHandler:nil];
                [QNVideoTool share].nasServerInfo = previousAutoLoginNAS;
                [[QNVideoTool share] connectToTUTK:previousAutoLoginNAS.cloudLink completionBlock:^(BOOL success, NSError *e, NSDictionary *dic){
                    if (success) {
                        previousAutoLoginNAS.mappingPort = dic[@"tutkMappingPort"];
                        [self loginNASServer:previousAutoLoginNAS withCompletionBlock:^(BOOL success, NSError *error){
                        }];
                    }else
                        [QNVideoTool toastWithTargetViewController:self
                                                          withText:NSLocalizedString(@"ConnectToCloudLinkFailed", nil)
                                                         isSuccess:NO];
                }];
            }else
                [self loginNASServer:previousAutoLoginNAS withCompletionBlock:^(BOOL success, NSError *error){
                }];

        }
        _needAutoLogin = NO; //once autologin occured, we never need to do it again in whole app life.
    }
}
@end
