//
//  QNNASListViewController.h
//  QNAPFramework
//
//  Created by Change.Liao on 2013/10/24.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNNASLoginDelegate.h"
#import <EAIntroView/EAIntroView.h>
#import "QNFlatSegmentedControl.h"

@interface QNNASListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITabBarDelegate, QNNASLoginDelegate, EAIntroDelegate, UITextFieldDelegate>{
    NSManagedObjectContext *_thisContext;
    BOOL _needAutoLogin;
    BOOL _isSortingByName;
    BOOL _isCancelLogin;
    NSInteger previousSegmentSelected;
    QNFlatSegmentedControl *_sortingSegmentedControl;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *downloadLabel;
@property (weak, nonatomic) IBOutlet UILabel *addLabel;
@property (weak, nonatomic) IBOutlet UILabel *settingLabel;

- (IBAction)returnBack:(id)sender;
- (IBAction)deletingNASServer:(id)sender;

@end
