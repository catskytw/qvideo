//
//  QNNASDetailViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/13.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NASServer.h"
#import "IQActionSheetPickerView.h"

#define SIMPLE_MODE 0x01
#define ADVANCED_UNIVERSAL_MODE 0x02
#define ADVANCED_COMPLEX_MODE 0x04

@interface QNNASDetailViewController : UITableViewController <UITextFieldDelegate, UITableViewDelegate, IQActionSheetPickerView>{
    NSArray *_heightForEachRow;
    NSInteger previousAdvancedMode;
    NSInteger _portSelectionStrategy;
    NSDictionary *_proirityDic;
}
@property (nonatomic) NSInteger optionMode;
@property (weak, nonatomic) IBOutlet UITextField *serverAddress;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UISwitch *remPasswdSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *sslSwitch;
@property (weak, nonatomic) IBOutlet UITextField *serverName;

@property (weak, nonatomic) IBOutlet UILabel *serverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *hostIPLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UILabel *rememberPwdLabel;
@property (weak, nonatomic) IBOutlet UILabel *isSSLLabel;
@property (weak, nonatomic) IBOutlet UILabel *lanIPLabel;
@property (weak, nonatomic) IBOutlet UILabel *externalIPLabel;
@property (weak, nonatomic) IBOutlet UILabel *myQNAPCloudLabel;
@property (weak, nonatomic) IBOutlet UILabel *myDDNSLabel;
@property (weak, nonatomic) IBOutlet UIButton *showPlainPwdBtn;
@property (weak, nonatomic) IBOutlet UILabel *deleteLabel;
@property (nonatomic, weak) NASServer *nasServer;
@property (weak, nonatomic) IBOutlet UILabel *settingStringLabel;
@property (weak, nonatomic) IBOutlet UIButton *advancedBtn;
@property (weak, nonatomic) IBOutlet UISwitch *autoDetectPortSwitch;
@property (weak, nonatomic) IBOutlet UITextField *universalPortTextField;
@property (weak, nonatomic) IBOutlet UITextField *internalPortTextField;
@property (weak, nonatomic) IBOutlet UITextField *externalPortTextField;
@property (weak, nonatomic) IBOutlet UILabel *universalPortTitle;

@property (weak, nonatomic) IBOutlet UILabel *internalPortTitle;
@property (weak, nonatomic) IBOutlet UILabel *internalPortDescription;
@property (weak, nonatomic) IBOutlet UILabel *externalPortTitle;
@property (weak, nonatomic) IBOutlet UILabel *externalPortDescription;
@property (weak, nonatomic) IBOutlet UILabel *portPriorityTitle;
@property (weak, nonatomic) IBOutlet UILabel *portPriorityDescrption;
@property (weak, nonatomic) IBOutlet UIButton *portPriorityBtn;


@property (nonatomic) BOOL needDeleteSection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *needDisabledBtns;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *needDisabledTitles;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *needDisabledDescription;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *needDisabledTextFields;


- (IBAction)portPrioritySelectAction:(id)sender;
- (IBAction)sslSwitchValueChanged:(id)sender;
- (IBAction)enterAdvancedComplexMode:(id)sender;
- (IBAction)showPwdOrNot:(id)sender;
- (IBAction)autoDetectValueChanged:(id)sender;
- (void)settingAccountPwd:(BOOL)isStartingAccountPwd;
- (NSNumber *)portPriorityValue;

@end
