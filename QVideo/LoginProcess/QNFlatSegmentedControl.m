//
//  QNFlatSegmentedControl.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/25.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNFlatSegmentedControl.h"

@implementation QNFlatSegmentedControl
- (id)initWithFrame:(CGRect)frame items:(NSArray*)items iconPosition:(IconPosition)position andSelectionBlock:(selectionBlock)block iconSeparation:(CGFloat)separation{
    self = [super initWithFrame:frame items:items iconPosition:position andSelectionBlock:block iconSeparation:separation];
    self.layer.cornerRadius=0.0f;
    return self;
}

@end
