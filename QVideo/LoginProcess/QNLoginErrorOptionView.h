//
//  QNLoginErrorOptionView.h
//  QVideo
//
//  Created by Change.Liao on 2014/6/8.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NASServer.h"
#import "QNNASLoginDelegate.h"

@interface QNLoginErrorOptionView : UIView
@property (nonatomic, strong) NASServer *targetNAS;
@property (nonatomic, strong) id<QNNASLoginDelegate>loginDelgate;

- (IBAction)editAccountPwd:(id)sender;
- (IBAction)selectOtherLoginWay:(id)sender;

@end
