//
//  QNNASAddingViewController.h
//  QNAPFramework
//
//  Created by Change.Liao on 2013/10/28.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNNASAddingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *addressText;
@property (weak, nonatomic) IBOutlet UITextField *accountText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UISwitch *isRemeberPassword;
@property (weak, nonatomic) IBOutlet UISwitch *isSSL;
@property (weak, nonatomic) IBOutlet UILabel *portNumber;
@property (weak, nonatomic) IBOutlet UIButton *btn;

- (IBAction)completingSetting:(id)sender;
- (IBAction)cancelBtn:(id)sender;
@end
