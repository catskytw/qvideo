//
//  QNAddingNASDetailViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/5.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNAddingNASDetailViewController.h"
#import <QNAPFramework/QNAPFramework.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import "QNVideoTool.h"
#import "QNNASListViewController.h"

@interface QNAddingNASDetailViewController ()

@end

@implementation QNAddingNASDetailViewController


- (void)viewDidLoad{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.leftBarButtonItem = nil;
    [self.navigationItem setTitle:(self.isEditAction)?NSLocalizedString(@"EditNASInfo", nil): NSLocalizedString(@"AddNAS", nil)];
    [QNVideoTool generateCustomLeftBarItem:self
                                withSelect:@selector(cancel:)
                             withTitleText:NSLocalizedString(@"Cancel", nil)];
    [QNVideoTool generateCustomRightBarItem:self
                                 withSelect:@selector(saveNAS:)
                              withTitleText:NSLocalizedString(@"Done", nil)];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tableViewController = [self.childViewControllers objectAtIndex:0];
    self.loginAfterSavingSwitcher.on = [self.nasServer.isLoginAfterSaving boolValue];
    [self.tableViewController.serverName setText:self.nasServer.serverName];
    [self.tableViewController.serverAddress setText:self.nasServer.currentAddress];
    [self.tableViewController.userName setText: (self.isEditAction)?self.nasServer.account:@"admin"];
    [self.tableViewController.password setText: ([self.nasServer.isRememberPwd boolValue])?self.nasServer.password:@""];
    [self.tableViewController.sslSwitch setOn:[self.nasServer.isSSL boolValue]];
    [self.tableViewController.remPasswdSwitch setOn:(self.nasServer.isRememberPwd)?[self.nasServer.isRememberPwd boolValue]:YES];
    [self.tableViewController.lanIPLabel setText:self.nasServer.lanIP];
    [self.tableViewController.externalIPLabel setText:self.nasServer.wanIP];
    [self.tableViewController.myQNAPCloudLabel setText:self.nasServer.myQNAPCloud];
    [self.tableViewController.myDDNSLabel setText:self.nasServer.cloudLink];
    [self.deleteView setHidden:!self.isEditAction];
    [self.tableViewController.sslSwitch addTarget:self action:@selector(sslSwitchChanged:) forControlEvents:UIControlEventValueChanged];
    [self settingLocalized];

}

- (void)settingLocalized{
    [self.tableViewController.serverName setPlaceholder:NSLocalizedString(@"serverNamePH", nil)];
    [self.tableViewController.serverAddress setPlaceholder:NSLocalizedString(@"serverAddressPH", nil)];
    [self.tableViewController.password setPlaceholder:NSLocalizedString(@"passwordPH", nil)];

}

- (void)sslSwitchChanged:(id)sender{
    UISwitch *switcher = (UISwitch *)sender;
    self.nasServer.isSSL = @(switcher.on);
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)generateNewNAS{
    if (!self.nasServer) {
        self.nasServer = [NASServer MR_createInContext:[NSManagedObjectContext MR_defaultContext]];
    }
}

#pragma mark - Navigation
- (IBAction)saveNAS:(id)sender{
    void(^loginNASAction)(void) = ^(){
        for (UIViewController *sampleViewController in [self.navigationController viewControllers]) {
            if ([sampleViewController isKindOfClass:[QNNASListViewController class]]) {
                QNNASListViewController *targetViewController = (QNNASListViewController *)sampleViewController;
                [targetViewController loginNASServer:self.nasServer withCompletionBlock:^(BOOL success, NSError *error){
                    if (!success) {
                        QNFileLoginError *loginError = [error.userInfo valueForKey:@"additional"];
                        NSString *message = nil;
                        BOOL b = [loginError.authPassed boolValue];
                        if ([loginError.errorValue intValue] == -1011) {
                            message = NSLocalizedString(@"QNLoginErrorOptionTitle", nil);
                        }else if (!b){
                            message = NSLocalizedString(@"IncorrectLoginInfo", nil);
                            [self.tableViewController settingAccountPwd:YES];
                        }
                        
                        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                                      withActionTitle:NSLocalizedString(@"SaveAnyWay", nil)
                                      withCancelTitle:NSLocalizedString(@"ModifyLoginInfo", nil)
                                          withMessage:message
                                    withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *item){
                                        [alert dismiss];
                                        [QNVideoTool toastWithTargetViewController:self withText:NSLocalizedString(@"SaveNASSuccess", nil) isSuccess:YES];
                                        [self returnBackMethod];
                                    }];
                    }
                }];
                break;
            }
        }
    };
    
    if ([self.tableViewController.serverName.text isEqualToString:@""] || [self.tableViewController.serverAddress.text isEqualToString:@""]) {
        [QNVideoTool toastWithTargetViewController:self
                                          withText:NSLocalizedString(@"fillServerIP", nil)
                                         isSuccess:NO];
        return;
    }

    //cloase the keyboard anyway.
    [self.tableViewController.view endEditing:YES];
    
    //if adding a new NAS here, we should login it automatically!
    [self saveNasInfo: ^(BOOL s, NSError *e){
        if ([self.nasServer.isLoginAfterSaving boolValue]) {
            loginNASAction();
        }else{
            UIViewController *viewController = [self.navigationController popViewControllerAnimated:YES];
            NSString *str = (s) ? NSLocalizedString(@"SaveNASSuccess", nil):NSLocalizedString(@"SaveNASFailure", nil);
            [QNVideoTool toastWithTargetViewController:viewController withText:str isSuccess:s];
        }
    }];
}

- (IBAction)deleteNAS:(id)sender{
    [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                  withActionTitle:NSLocalizedString(@"Delete", nil)
                      withMessage:NSLocalizedString(@"confirmDeleteNAS", nil)
                withActionHandler:^(CXAlertView *v, CXAlertButtonItem *btn){
                    [v dismiss];
                    [self.nasServer MR_deleteEntity];
                    __block UINavigationController *parentNaviController = self.navigationController;
                    
                    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *e){
                        [parentNaviController popViewControllerAnimated:YES];
                        
                        NSString *str = (s)?NSLocalizedString(@"DeleteNASSuccess", nil):NSLocalizedString(@"DeleteNASFailure", nil);
                        [QNVideoTool toastWithTargetViewController:parentNaviController withText:str isSuccess:s];
                    }];
                }];
}

- (IBAction)cancel:(id)sender{
    if (!self.isEditAction) {
        [self.nasServer MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *e){
            [self returnBackMethod];
        }];
    }else{
        [self returnBackMethod];
    }
}

- (IBAction)loginAfterSavingValueChanged:(id)sender {
    UISwitch *switcher = (UISwitch *)sender;
    self.nasServer.isLoginAfterSaving = @(switcher.on);
}

- (void)returnBackMethod{
    if ([[NASServer MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] count] > 0) {
        [[QNVideoTool share] popToNASListViewController:self];
    }else
        [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark - PrivateMethod
- (void)saveNasInfo:(void(^)(BOOL s, NSError *e))completionBlock{
    self.nasServer.serverName = self.tableViewController.serverName.text;
    self.nasServer.currentAddress = self.tableViewController.serverAddress.text;
    self.nasServer.account = self.tableViewController.userName.text;
    self.nasServer.password = self.tableViewController.password.text;
    self.nasServer.isSSL = @(self.tableViewController.sslSwitch.on);
    self.nasServer.isRememberPwd = @(self.tableViewController.remPasswdSwitch.on);
    self.nasServer.isAutoDetectPort = @(self.tableViewController.autoDetectPortSwitch.on);
    self.nasServer.isLoginAfterSaving = @(self.loginAfterSavingSwitcher.on);
    
    if ([self.nasServer.isSSL boolValue]) {
        if (self.tableViewController.optionMode == SIMPLE_MODE || self.tableViewController.optionMode == ADVANCED_UNIVERSAL_MODE) {
            self.nasServer.ssl_external_port = @([self.tableViewController.universalPortTextField.text intValue]);
            self.nasServer.ssl_internal_port = @([self.tableViewController.universalPortTextField.text intValue]);
        }else{
            self.nasServer.ssl_external_port = @([self.tableViewController.externalPortTextField.text intValue]);
            self.nasServer.ssl_internal_port = @([self.tableViewController.internalPortTextField.text intValue]);
        }
    }else{
        if (self.tableViewController.optionMode == SIMPLE_MODE || self.tableViewController.optionMode == ADVANCED_UNIVERSAL_MODE) {
            self.nasServer.normal_external_port = @([self.tableViewController.universalPortTextField.text intValue]);
            self.nasServer.normal_internal_port = @([self.tableViewController.universalPortTextField.text intValue]);
        }else{
            self.nasServer.normal_external_port = @([self.tableViewController.externalPortTextField.text intValue]);
            self.nasServer.normal_internal_port = @([self.tableViewController.internalPortTextField.text intValue]);
        }
    }
    self.nasServer.portPriority = [self.tableViewController portPriorityValue];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:completionBlock];
}

- (BOOL)checkIfNotModifySomething{
    BOOL returnValue = YES;
    returnValue &= [self.nasServer.serverName isEqualToString:self.tableViewController.serverName.text];
    returnValue &= [self.nasServer.currentAddress isEqualToString: self.tableViewController.serverAddress.text];
    returnValue &= [self.nasServer.account isEqualToString: self.tableViewController.userName.text];
    returnValue &= [self.nasServer.password isEqualToString: self.tableViewController.password.text];
    returnValue &= ([self.nasServer.isSSL boolValue] == self.tableViewController.sslSwitch.on);
    returnValue &= self.nasServer.isRememberPwd.boolValue == self.tableViewController.remPasswdSwitch.on;
    return returnValue;
}

#pragma mark - UITableViewDataSource
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqual:@"containNASDetail"]) {
        QNNASDetailViewController *detailViewController = segue.destinationViewController;
        detailViewController.needDeleteSection = self.isEditAction;
        detailViewController.nasServer = self.nasServer;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
