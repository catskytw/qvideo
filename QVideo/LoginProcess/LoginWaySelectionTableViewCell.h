//
//  LoginWaySelectionTableViewCell.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/17.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginWaySelectionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *loginAction;
@property (weak, nonatomic) IBOutlet UILabel *loginURL;

@end
