//
//  QNNASDetailViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/13.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNNASDetailViewController.h"
#import "QNVideoCommonSectionView.h"
#import "QNAddingNASDetailViewController.h"

@interface QNNASDetailViewController ()

@end

@implementation QNNASDetailViewController

- (id)initWithStyle:(UITableViewStyle)style{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    _heightForEachRow = @[@(60),@(60),@(60),@(60),@(44),@(44),@(44),@(60),@(90),@(110),@(90), @(44)];
    _proirityDic = @{
                     NSLocalizedString(@"internalPortFirst", nil):@(PortSelection_InternalPortFirst),
                     NSLocalizedString(@"externalPortFirst", nil):@(PortSelection_ExternalPortFirst),
                     NSLocalizedString(@"internalPortOnly", nil):@(PortSelection_InternalPortOnly),
                     NSLocalizedString(@"externalPortOnly", nil):@(PortSelection_ExternalPortOnly)
                     };

    self.optionMode = SIMPLE_MODE;
    //TODO if internal port is not same as external port, if should be ADVANCED_COMPLEX_MODE
    previousAdvancedMode = ADVANCED_UNIVERSAL_MODE;
    self.autoDetectPortSwitch.on = [self.nasServer.isAutoDetectPort boolValue];
    [self.myQNAPCloudLabel setText:(self.nasServer.myQNAPCloud)?self.nasServer.myQNAPCloud:@""];
    [self settingLocalizable];
    [self settingElementDisabled];
    [self settingAllValuesForPort];
}

- (void)settingLocalizable{
    [self.serverNameLabel setText:NSLocalizedString(@"ServerName", nil)];
    [self.hostIPLabel setText:NSLocalizedString(@"HostIP", nil)];
    [self.rememberPwdLabel setText:NSLocalizedString(@"RememberPwd", nil)];
//    [self.isSSLLabel setText:NSLocalizedString(@"SSL", nil)];
    [self.lanIPLabel setText:NSLocalizedString(@"LANIP", nil)];
    [self.externalIPLabel setText:NSLocalizedString(@"ExternalIP", nil)];
    [self.showPlainPwdBtn setTitle:NSLocalizedString(@"Show", nil) forState:UIControlStateNormal];
    [self settingAccountPwd:NO];
    [self.deleteLabel setText:NSLocalizedString(@"Delete", nil)];
    [self.advancedBtn setTitle:NSLocalizedString(@"More", nil) forState:UIControlStateNormal];
}

- (void)settingAllValuesForPort{
    BOOL isSSL = [self.nasServer.isSSL boolValue];
    if (isSSL) {
        [self.universalPortTextField setText:[NSString stringWithFormat:@"%i", [self.nasServer.ssl_internal_port intValue]]];
        [self.internalPortTextField setText:[NSString stringWithFormat:@"%i", [self.nasServer.ssl_internal_port intValue]]];
        [self.externalPortTextField setText:[NSString stringWithFormat:@"%i", [self.nasServer.ssl_external_port intValue]]];
    }else{
        [self.universalPortTextField setText:[NSString stringWithFormat:@"%i", [self.nasServer.normal_internal_port intValue]]];
        [self.internalPortTextField setText:[NSString stringWithFormat:@"%i", [self.nasServer.normal_internal_port intValue]]];
        [self.externalPortTextField setText:[NSString stringWithFormat:@"%i", [self.nasServer.normal_external_port intValue]]];
    }
    _portSelectionStrategy = [self.nasServer.portPriority intValue];
    [self.portPriorityBtn setTitle:[self portSelectionString] forState:UIControlStateNormal];
}

- (void)settingAccountPwd:(BOOL)isStartingAccountPwd{
    if (!isStartingAccountPwd) {
        [self.userNameLabel setText:NSLocalizedString(@"UserName", nil)];
        [self.passwordLabel setText:NSLocalizedString(@"Password", nil)];
    }else{
        NSString *note = @" *";
        
        NSString *name = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"UserName", nil), note];
        NSString *pwd = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"Password", nil), note];
        NSMutableAttributedString *nameString = [[NSMutableAttributedString alloc] initWithString:name
                                                                                       attributes:nil];
        NSMutableAttributedString *pwdString = [[NSMutableAttributedString alloc] initWithString:pwd
                                                                                       attributes:nil];

        [nameString addAttribute:NSForegroundColorAttributeName
                           value:[UIColor redColor]
                           range:NSMakeRange([name length] - [note length], [note length])];
        [pwdString addAttribute:NSForegroundColorAttributeName
                          value:[UIColor redColor]
                          range:NSMakeRange([pwd length] - [note length], [note length])];
        
        [self.userNameLabel setAttributedText:nameString];
        [self.passwordLabel setAttributedText:pwdString];
    }
}

- (IBAction)portPrioritySelectAction:(id)sender {
    DDLogVerbose(@"Change priority here!");
    IQActionSheetPickerView *selectionView = [[IQActionSheetPickerView alloc] initWithViewController:self];
    selectionView.delegate = self;
    selectionView.titlesForComponenets = @[[_proirityDic allKeys]];
    [selectionView showInView:self.parentViewController.view];
}

- (IBAction)sslSwitchValueChanged:(id)sender {
    UISwitch *switcher = (UISwitch *)sender;
    self.nasServer.isSSL = @(switcher.on);
    
    [self settingAllValuesForPort];
}

- (IBAction)enterAdvancedComplexMode:(id)sender {
    previousAdvancedMode = ADVANCED_COMPLEX_MODE;
    self.optionMode = previousAdvancedMode;
    [self reloadFirstSection];
}

- (IBAction)showPwdOrNot:(id)sender{
    self.password.secureTextEntry = !self.password.secureTextEntry;
    [self.showPlainPwdBtn setTitle:(self.password.secureTextEntry == YES)?NSLocalizedString(@"Show", nil):NSLocalizedString(@"Hide", nil)
                              forState:UIControlStateNormal];
}

- (IBAction)autoDetectValueChanged:(id)sender {
    UISwitch *switcher = (UISwitch *)sender;
    self.nasServer.isAutoDetectPort = @(switcher.on);
    [self settingElementDisabled];
}

- (NSNumber *)portPriorityValue{
    NSString *key = self.portPriorityBtn.titleLabel.text;
    return [_proirityDic valueForKey:key];
}


- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return (section == 1)?0.0f:35.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    QNVideoCommonSectionView *backgroundView = [[[NSBundle mainBundle] loadNibNamed:@"QNVideoCommonSectionView" owner:nil options:nil] objectAtIndex:0];
    [backgroundView setFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
    
    switch (section) {
        case 0:
            [backgroundView.sectionLabel setText:NSLocalizedString(@"Setting", nil)];
            break;
        case 1:
            backgroundView = nil;
            break;
        case 2:
            break;
        default:
            break;
    }
    return backgroundView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = [indexPath section];
    CGFloat r = 0.0f;
    if (section == 0) {
        NSInteger row = [indexPath row];
        switch (row) {
            case 0:
                r = ([self isCellNeedShow:indexPath])?[[_heightForEachRow objectAtIndex:row] floatValue]:0.0f;
                break;
            case 1:
                r = (self.nasServer.myQNAPCloud)? 80.0f:[[_heightForEachRow objectAtIndex:row] floatValue];
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            default:
                r = [[_heightForEachRow objectAtIndex:row] floatValue];
                break;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                r = ([self isCellNeedShow:indexPath])?[[_heightForEachRow objectAtIndex:row] floatValue]:0.0f;
                break;
        }
    }else if (section != 1 && self.needDeleteSection){
        r = 44.0f;
    }
    return r;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.needDeleteSection?3:2;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = [indexPath section];
    NSInteger row = [indexPath row];
    if ([indexPath isEqual:[NSIndexPath indexPathForRow:0 inSection:2]]) { //if pressing delete-button
        if ([self.parentViewController isKindOfClass:[QNAddingNASDetailViewController class]]) {
            QNAddingNASDetailViewController *targetViewController = (QNAddingNASDetailViewController *)self.parentViewController;
            [targetViewController deleteNAS:self];
        }
    }
    
    if (section == 0){
        switch (row) {
            case 11:{
                //If the internalport doesn't equal external port, it should always be ADVANCED_COMPLEX_MODE.
                if (![self.internalPortTextField.text isEqualToString:self.externalPortTextField.text])
                    previousAdvancedMode = ADVANCED_COMPLEX_MODE;
                    
                self.optionMode = (self.optionMode == SIMPLE_MODE)?previousAdvancedMode:SIMPLE_MODE;
                [self.settingStringLabel setText:(self.optionMode == SIMPLE_MODE)? NSLocalizedString(@"AdvancedSetting", nil): NSLocalizedString(@"SimpleSetting", nil)];
                [self reloadFirstSection];
            }
                break;
            default:
                break;
        }
    }
}
#pragma mark - DelegateMethod
- (void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray*)titles{
    [self.portPriorityBtn setTitle:[titles objectAtIndex:0] forState:UIControlStateNormal];
}

#pragma mark - PrivateMethod
- (BOOL)isCellNeedShow:(NSIndexPath *)indexPath{
    NSInteger row = [indexPath row];
    NSInteger section = [indexPath section];
    BOOL r = YES;
    
    if (section != 0)
        return r;

    switch (row) {
        case 0:
            if (self.optionMode == SIMPLE_MODE && self.nasServer.serverName)
                r = NO;
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        default:
            break;

        case 6:
            if (self.optionMode == SIMPLE_MODE)
                r = NO;
            break;
        case 7:
            if (self.optionMode != ADVANCED_UNIVERSAL_MODE)
                r = NO;
            break;
        case 8:
        case 9:
        case 10:
            if (self.optionMode != ADVANCED_COMPLEX_MODE)
                r = NO;
            break;

    }
    return r;
}

- (void)reloadFirstSection{
    [UIView transitionWithView:self.tableView
                      duration:0.35f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void){
         [self.tableView reloadData];
     }
                    completion: ^(BOOL isFinished){
     }];
}

- (void)settingElementDisabled{
    BOOL b = [self.nasServer.isAutoDetectPort boolValue];
    if (b) {
        for (UIButton *btn in self.needDisabledBtns) {
            [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [btn setUserInteractionEnabled:NO];
        }
        for (UITextField *text in self.needDisabledTextFields){
            [text setTextColor:[UIColor grayColor]];
            [text setUserInteractionEnabled:NO];
        }
        for (UILabel *titleLabel in self.needDisabledTitles) {
            [titleLabel setTextColor:[UIColor grayColor]];
        }
        for (UILabel *description in self.needDisabledDescription) {
            [description setTextColor:[UIColor grayColor]];
        }
    }else{
        UIColor *lightBlueColor = [UIColor colorWithRed:0.0f/256.0f green:90.0f/256.0f blue:255.0f/256.0f alpha:1.0f];
        for (UIButton *btn in self.needDisabledBtns) {
            [btn setTitleColor:lightBlueColor forState:UIControlStateNormal];
            [btn setUserInteractionEnabled:YES];
        }
        for (UITextField *text in self.needDisabledTextFields){
            [text setTextColor:lightBlueColor];
            [text setUserInteractionEnabled:YES];
        }
        for (UILabel *titleLabel in self.needDisabledTitles) {
            [titleLabel setTextColor:[UIColor blackColor]];
        }
        for (UILabel *description in self.needDisabledDescription) {
            [description setTextColor:[UIColor colorWithRed:76.0f/256.0f green:76.0f/256.0f blue:76.0f/256.0f alpha:1.0f]];
        }
    }
}

- (NSString *)portSelectionString{
    NSString *rString = nil;
    switch (_portSelectionStrategy) {
        default:
        case PortSelection_InternalPortFirst:
            rString = NSLocalizedString(@"internalPortFirst", nil);
            break;
        case PortSelection_ExternalPortFirst:
            rString = NSLocalizedString(@"externalPortFirst", nil);
            break;
        case PortSelection_InternalPortOnly:
            rString = NSLocalizedString(@"internalPortOnly", nil);
            break;
        case PortSelection_ExternalPortOnly:
            rString = NSLocalizedString(@"externalPortOnly", nil);
            break;
    }
    return rString;
}
/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
