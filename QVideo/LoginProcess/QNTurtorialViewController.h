//
//  QNTurtorialViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/2/25.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EAIntroView/EAIntroView.h>

@interface QNTurtorialViewController : UIViewController<EAIntroDelegate>
@property (nonatomic) BOOL forceTutorial;
@end
