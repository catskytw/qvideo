//
//  LoginWaySelectionTableView.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/17.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NASServer.h"
#import "QNNASLoginDelegate.h"

@interface LoginWaySelectionTableView : UITableView<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) NASServer *serverData;
@property (nonatomic, strong) id<QNNASLoginDelegate>loginDelgate;

- (id)initWithFrame:(CGRect)frame withNASServer:(NASServer *)server;

@end
