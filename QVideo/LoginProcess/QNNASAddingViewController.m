//
//  QNNASAddingViewController.m
//  QNAPFramework
//
//  Created by Change.Liao on 2013/10/28.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNNASAddingViewController.h"
#import "NASServer.h"
#import "CoreData+MagicalRecord.h"
#import "MagicalRecord+Actions.h"


@interface QNNASAddingViewController ()

@end

@implementation QNNASAddingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ActionMethod
- (IBAction)completingSetting:(id)sender{
    NSManagedObjectContext *myContext = [NSManagedObjectContext MR_defaultContext];
    NASServer *newServer = [NASServer MR_findFirstByAttribute:@"serverName" withValue:self.nameText.text inContext:myContext];
    if (!newServer) {
        newServer = [NASServer MR_createInContext:myContext];
    }
    newServer.currentAddress = self.addressText.text;
    newServer.account = self.accountText.text;
    newServer.password = self.passwordText.text;
    newServer.isSSL = [NSNumber numberWithBool:self.isSSL.isOn];
    newServer.serverName = self.nameText.text;
    
    [myContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error){
        if(success){
            NSLog(@"saving successfully!!");
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (IBAction)cancelBtn:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)isDomainName:(NSString *)address{
    NSRange range = NSMakeRange(0, [address length]);
    /**
     [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.{0,1}
     */
    NSError *error;
    NSRegularExpression *regular = [[NSRegularExpression alloc] initWithPattern:@"[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.{0,1}" options:NSRegularExpressionCaseInsensitive error:&error];
    NSString *findString = [regular stringByReplacingMatchesInString:address options:0 range:range withTemplate:@"$1"];
    return ([findString isEqualToString:@"$1"])?YES:NO;
}
@end
