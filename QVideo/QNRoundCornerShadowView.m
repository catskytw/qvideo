//
//  QNRoundCornerShadowView.m
//  QVideo
//
//  Created by Change.Liao on 2014/1/20.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "QNRoundCornerShadowView.h"

@implementation QNRoundCornerShadowView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect
{
    CGContextRef ref = UIGraphicsGetCurrentContext();
    CGFloat _shadowRadius = 3.0f;
    CGFloat _cornerRadius = 3.0f;
    UIColor *_shadowColor = [UIColor blackColor];
    UIColor *_fillColor = [UIColor whiteColor];
    /* We can only draw inside our view, so we need to inset the actual 'rounded content' */
    CGRect contentRect = CGRectInset(rect, _shadowRadius, _shadowRadius);
    
    /* Create the rounded path and fill it */
    UIBezierPath *roundedPath = [UIBezierPath bezierPathWithRoundedRect:contentRect cornerRadius:_cornerRadius];
    CGContextSetFillColorWithColor(ref, _fillColor.CGColor);
    CGContextSetShadowWithColor(ref, CGSizeMake(1.0f, 1.0f), _shadowRadius, _shadowColor.CGColor);
    [roundedPath fill];
    
    /* Draw a subtle white line at the top of the view */
    [roundedPath addClip];
    CGContextSetStrokeColorWithColor(ref, [UIColor colorWithWhite:1.0 alpha:0.6].CGColor);
    CGContextSetBlendMode(ref, kCGBlendModeOverlay);
    
    CGContextMoveToPoint(ref, CGRectGetMinX(contentRect), CGRectGetMinY(contentRect)+0.5);
    CGContextAddLineToPoint(ref, CGRectGetMaxX(contentRect), CGRectGetMinY(contentRect)+0.5);
    CGContextStrokePath(ref);
}


@end
