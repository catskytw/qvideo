//
//  QNVideoInfoViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/22.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoInfoViewController.h"
#import <QNAPFramework/QNAPFramework.h>
#import <MarqueeLabel/MarqueeLabel.h>
#import <QNAPFramework/QNAPCommunicationManager.h>
#import <TSMessages/TSMessage.h>
#import <AMTagListView/AMTagListView.h>
#import "NSString_TimeDuration.h"
#import "QNVideoTool.h"
#import "QNVideoColorLabelSelectionView.h"
#import <RFKeyboardToolbar/RFKeyboardToolbar.h>
#import <RFKeyboardToolbar/RFToolbarButton.h>

@interface QNVideoInfoViewController ()

@end

@implementation QNVideoInfoViewController

- (id)initWithStyle:(UITableViewStyle)style{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self settingData];
    [self settingBarItems];
    [self settingLocalizable];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self textViewDidChange:self.descriptionTextView];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)settingLocalizable{
    [self.fixClassificationLabel setText:NSLocalizedString(@"Classification", nil)];
    [self.fixColorLabel setText:NSLocalizedString(@"ColorLabel", nil)];
    [self.fixTitleLabel setText:NSLocalizedString(@"Title", nil)];
    [self.fixRatingLabel setText:NSLocalizedString(@"Rating", nil)];
    [self.fixTagLabel setText:NSLocalizedString(@"Tag", nil)];
    [self.fixDescription setText:NSLocalizedString(@"Description", nil)];
    [self.descriptionTextView setPlaceholder:NSLocalizedString(@"DescriptionPlaceHolder", nil)];
    [self.descriptionTextView setPlaceholderTextColor:[UIColor lightGrayColor]];
    [self.descriptionTextView setTextColor:[UIColor blackColor]];
}

- (void)settingBarItems{
    
    QNButton *leftBtn = [QNVideoTool generateCustomLeftBarItem:self.outsideDetailViewController withSelect:nil withTitleText:NSLocalizedString(@"Back", nil)];
    [leftBtn addTarget:self action:@selector(leftBarItemAction:) forControlEvents:UIControlEventTouchUpInside];
    self.outsideDetailViewController.navigationItem.rightBarButtonItem = nil;
}

- (void)settingData{
    __block QNVideoFileItem *blockVideoItem = self.videoFileItem;
    __block QNVideoItemDetailViewController *blockOutsideViewController = self.outsideDetailViewController;
    //titld
    [self.titleLabel setText:self.videoFileItem.cPictureTitle];
    [self.titleLabel setMarqueeType:MLContinuous];
    [self.titleLabel setAnimationCurve:UIViewAnimationOptionCurveLinear];
    [self.titleLabel setFadeLength:20.0f];

    [self.titleLabel setTextColor:defaultGreenColor];
    //tag
    [self.tagLabel setText:[NSString stringWithFormat:@"%@: ", NSLocalizedString(@"Tag", nil)]];

    //description
    [self.descriptionLabel setText:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Description", nil),(self.videoFileItem.comment)?self.videoFileItem.comment:@""]];

    //classification
    [self.classificationLabel setText:[QNVideoTool classificationValueToString:[self.videoFileItem.mask integerValue]]];
    [self.classificationLabel setTextColor:defaultGreenColor];
    //rating
    [QNVideoTool configureStarRating:self.starRating];
    CGFloat rating = [self.videoFileItem.rating floatValue]/20.0f;
    self.starRating.rating = rating;
    
    //colorImage
    [self.colorImage setImage:[QNVideoTool colorLevel:[self.videoFileItem.colorLevel integerValue]]];
    
    //tags
    __block NSMutableArray *tags = [NSMutableArray arrayWithArray: [self.videoFileItem.keywords componentsSeparatedByString:@";"]];
    if (!self.tagListView) {
        self.tagListView = [[AMTagListView alloc] initWithFrame:CGRectMake(0, 0, self.tagView.frame.size.width, self.tagView.frame.size.height)];
        [self.tagView addSubview:self.tagListView];
    }
    [self reloadTags];
    
    __block QNVideoInfoViewController *blockSelf = self;
    //setting the action while tapping the tag
    [self.tagListView setTapHandler:^(AMTagView *tagView){
        __block NSString *tagText = tagView.tagText;
        __block QNVideoInfoViewController *doubleBlockSelf = blockSelf;
        UITextField *tagTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 250, 30)];
        tagTextField.delegate = blockSelf;
        [tagTextField setText:tagText];
        tagTextField.layer.cornerRadius = 5.0f;
        tagTextField.layer.borderWidth = 1.0f;
        tagTextField.layer.borderColor = [UIColor colorWithHexString:@"#a0a0a0" alpha:1.0f].CGColor;
        tagTextField.backgroundColor = [UIColor whiteColor];
        tagTextField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);

        //TODO: the code here is too deep.
        CXAlertView *targetAlert = [[CXAlertView alloc] initWithTitle:NSLocalizedString(@"ModifyTag", nil) contentView:tagTextField cancelButtonTitle:nil];
        [targetAlert addButtonWithTitle:NSLocalizedString(@"DeleteTag", nil)
                                   type:CXAlertViewButtonTypeCustom
                                handler:^(CXAlertView *alertView, CXAlertButtonItem *button){
                                    [tags removeObject:tagText];
                                    QNVideoStationAPIManager *videStation = [QNAPCommunicationManager share].videoStationManager;
                                    [videStation setVideoKeyword:blockVideoItem.f_id
                                                    withKeywords:tags
                                                        isAppend:NO
                                                withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                                    [QNVideoTool toastWithTargetViewController:blockOutsideViewController withText:NSLocalizedString(@"DeleteTagSuccess", nil) isSuccess:YES];
                                                    blockVideoItem.keywords = [doubleBlockSelf composeKeywords:tags];
                                                    AMTagView *targetTagView = nil;
                                                    for (AMTagView *eachView in doubleBlockSelf.tagListView.tags) {
                                                        if ([eachView.tagText isEqualToString:tagText]) {
                                                            targetTagView = eachView;
                                                            break;
                                                        }
                                                    }
                                                    [doubleBlockSelf.tagListView removeTag:targetTagView];
                                                }
                                                withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                                    [QNVideoTool toastWithTargetViewController:blockOutsideViewController withText:NSLocalizedString(@"DeleteTagFailure", nil) isSuccess:NO];
                                                }];
                                    
                                    [alertView dismiss];
        }];
        
        [targetAlert addButtonWithTitle:NSLocalizedString(@"Done", nil)
                                   type:CXAlertViewButtonTypeCustom
                                handler:^(CXAlertView *alertView, CXAlertButtonItem *btn){
                                    UITextField *_textField = tagTextField;
                                        //if the textfield isn't changed, do nothing.
                                    if (![_textField.text isEqualToString:tagText]) {
                                        [tags removeObject:tagText];
                                        [tags addObject:tagTextField.text];
                                        QNVideoStationAPIManager *videStation = [QNAPCommunicationManager share].videoStationManager;
                                        [videStation setVideoKeyword:blockVideoItem.f_id
                                                        withKeywords:tags
                                                            isAppend:NO
                                                    withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                                        [QNVideoTool toastWithTargetViewController:blockOutsideViewController withText:NSLocalizedString(@"ModifyTagSuccess", nil) isSuccess:YES];
                                                        [doubleBlockSelf.tagListView removeAllTags];
                                                        [doubleBlockSelf.tagListView addTags:tags];
                                                    }
                                                    withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                                        [QNVideoTool toastWithTargetViewController:blockOutsideViewController withText:NSLocalizedString(@"ModifyTagFailure", nil) isSuccess:NO];
                                                    }];
                                    }
                                    [alertView dismiss];
                                }];
        
        [targetAlert show];
        [CXAlertView cleanAllAlertBesideThis:targetAlert];

    }];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)leftBarItemAction:(id)sender{
    [self.outsideDetailViewController settingActionMenu];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = [indexPath row];
    CGFloat value = 60.0f;
    switch (row) {
        case 4:{
            CGFloat tagHeight = self.tagListView.contentSize.height;
            value = tagHeight + 35.0f;
            self.tagListView.frame = CGRectMake(self.tagListView.frame.origin.x,
                                                self.tagListView.frame.origin.y,
                                                self.tagListView.frame.size.width,
                                                tagHeight);
            self.tagListView.bounds = self.tagListView.frame;
        }
            break;
        case 5:
            value = 140.0f;
        default:
            break;
    }
    return value;
}

//lots of functions here!
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = [indexPath row];
    __block UINavigationController *toastTarget = self.outsideDetailViewController.navigationController;
    __block QNVideoFileItem *fileItem = self.videoFileItem;
    __block QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    __block QNVideoInfoViewController *blockSelf = self;

    switch (row) {
        case 0:{ //change video title
            CXAlertView *_alert = [CXAlertView showInputTextfieldAlert:NSLocalizedString(@"ChangeTitle", nil)
                                                       withActionTitle:NSLocalizedString(@"Done", nil)
                                                 withTextFieldDelegate:self
                                                     withActionHandler:^(CXAlertView *view, CXAlertButtonItem *item)
                                   {
                                       [view dismiss];
                                       __block UITextField *textField = (UITextField *)view.contentView;
                                       
                                       //Change Title
                                       [videoStation setVideoFileTitle:fileItem.f_id
                                                          withNewTitle:textField.text
                                                      withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                                          fileItem.cPictureTitle = [NSString stringWithFormat:@"%@.%@",textField.text, [fileItem.cFileName pathExtension]];
                                                          [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *e){
                                                              if (s) {
                                                                  [QNVideoTool toastWithTargetViewController:toastTarget
                                                                                                    withText:NSLocalizedString(@"UpdateVideoTitleSuccess", nil)
                                                                                                   isSuccess:YES];
                                                                  [textField setText:fileItem.cPictureTitle];
                                                                  [blockSelf settingData];
                                                              }else
                                                                  [QNVideoTool toastWithTargetViewController:toastTarget
                                                                                                    withText:NSLocalizedString(@"ChangeTitleError", nil)
                                                                                                   isSuccess:NO];
                                                              
                                                          }];
                                                      }
                                                      withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                                          [QNVideoTool toastWithTargetViewController:toastTarget
                                                                                            withText:NSLocalizedString(@"ChangeTitleError", nil)
                                                                                           isSuccess:NO];
                                                      }];
                                   }];
            UITextField *titleTextField = (UITextField *)_alert.contentView;
            titleTextField.secureTextEntry = NO;
            NSString *fullName = self.videoFileItem.cPictureTitle;
            titleTextField.text = [fullName stringByDeletingPathExtension];
        }
            break;
        case 1:{ //Classification
            UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(5, 0, 200, 0)];
            __block UILabel *blockClassificationLabel = self.classificationLabel;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            [pickerView selectRow:[QNVideoTool classificationStringToValue:[self.classificationLabel text]] inComponent:0 animated:NO];
            
            [CXAlertView showContentViewAlert:NSLocalizedString(@"SelectClassification", nil)
                              withActionTitle:NSLocalizedString(@"Done", nil)
                              withContentView:pickerView
                            withActionHandler:^(CXAlertView *view, CXAlertButtonItem *item){
                                [view dismiss];
                                UIPickerView *picker = pickerView;
                                NSInteger selectedIndex = [picker selectedRowInComponent:0];
                                [videoStation setVideoMediaType:selectedIndex
                                                     withFileID:fileItem.f_id
                                               withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                                   fileItem.mask = @(selectedIndex);
                                                   [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *e){
                                                       if (s) {
                                                           [QNVideoTool toastWithTargetViewController:toastTarget
                                                                                             withText: NSLocalizedString(@"UpdateClassificationSuccess", nil)
                                                                                            isSuccess:YES];
                                                           [blockClassificationLabel setText:[QNVideoTool classificationValueToString:selectedIndex]];
                                                       }else
                                                           [QNVideoTool toastWithTargetViewController:toastTarget
                                                                                             withText: NSLocalizedString(@"UpdateClassificationError", nil)
                                                                                            isSuccess:NO];
                                                   }];
                                                   
                                               }
                                               withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                                   [QNVideoTool toastWithTargetViewController:toastTarget
                                                                                     withText: [NSString stringWithFormat:@"%@ %i", NSLocalizedString(@"UpdateClassificationError", nil), e.code]
                                                                                    isSuccess:NO];
                                               }];
                                
                            }];
        }
            break;
        case 2:{ //Color Label
            QNVideoColorLabelSelectionView *colorBtnView = [[[NSBundle mainBundle] loadNibNamed:@"QNVideoColorLabelSelectionView" owner:nil options:nil] objectAtIndex:0];
            colorBtnView.colorOption = [fileItem.colorLevel integerValue];
            [colorBtnView setting];
            
            
            
            void(^failureBlock)(void) = ^(void){
                [QNVideoTool toastWithTargetViewController:toastTarget
                                                  withText:NSLocalizedString(@"SelectColorError", nil)
                                                 isSuccess:NO];
            };
            
            [CXAlertView showContentViewAlert:NSLocalizedString(@"SelectColorLabel", nil)
                              withActionTitle:NSLocalizedString(@"Done", nil)
                              withContentView:colorBtnView
                            withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                                [alert dismiss];
                                QNVideoColorLabelSelectionView *selectionView = colorBtnView;
                                NSInteger selection = [selectionView.selectedBtn.userInfo intValue];
                                
                                [videoStation setVideoColorLabel:fileItem.f_id
                                                       withColor:selection
                                                withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                                    fileItem.colorLevel = @(selection);
                                                    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *e){
                                                        if (s){
                                                            [QNVideoTool toastWithTargetViewController:toastTarget
                                                                                              withText:NSLocalizedString(@"ChangeColorSuccess", nil)
                                                                                             isSuccess:YES];
                                                            [blockSelf settingData];
                                                        }
                                                        else
                                                            failureBlock();
                                                    }];
                                                }
                                                withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                                    failureBlock();
                                                }];
                            }];

        }
            break;
        case 3:{//Rating
            EDStarRating *starRating = [[EDStarRating alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
            [QNVideoTool configureStarRating:starRating];
            starRating.editable = YES;
            starRating.delegate = self;
            starRating.rating = [fileItem.rating floatValue]/20.f;
            
            void(^failuerBlock)() = ^(void){
                [QNVideoTool toastWithTargetViewController:toastTarget
                                                  withText:NSLocalizedString(@"SetRatingError", nil)
                                                 isSuccess:NO];
            };
            
            [CXAlertView showContentViewAlert:NSLocalizedString(@"ChangeYourRating", nil)
                              withActionTitle:NSLocalizedString(@"Done", nil)
                              withContentView:starRating
                            withActionHandler:^(CXAlertView *view, CXAlertButtonItem *item){
                                [view dismiss];
                                NSInteger rating = ceil(starRating.rating * 20.0f);
                                [videoStation setVideoRating:fileItem.f_id
                                                    withRate:rating
                                            withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                                fileItem.rating = @(rating);
                                                [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error){
                                                    if (success) {
                                                        [QNVideoTool toastWithTargetViewController:toastTarget
                                                                                          withText:NSLocalizedString(@"RatingSuccess", nil)
                                                                                         isSuccess:YES];
                                                        [blockSelf settingData];
                                                    }else
                                                        failuerBlock();
                                                }];
                                            }
                                            withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                                failuerBlock();
                                            }];
                            }];
        }
            break;
        case 4:{
            
        }
            break;
        default:
            break;
    }
}

- (IBAction)addTagAction:(id)sender{
    __block QNVideoFileItem *filmItem = self.videoFileItem;
    __block QNVideoItemDetailViewController *blockOutsideDetailViewController = self.outsideDetailViewController;
    __block QNVideoInfoViewController *blockSelf = self;
        
    [CXAlertView showInputTextfieldAlert:NSLocalizedString(@"AddTag", nil)
                         withActionTitle:NSLocalizedString(@"Done", nil)
                            isSecureType:NO
                   withTextFieldDelegate:self
                       withActionHandler:^(CXAlertView *alertView, CXAlertButtonItem *item){
                           QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
                           UITextField *contentView = (UITextField *)alertView.contentView;
                           
                           __block QNVideoItemDetailViewController *blockOutsideViewController = blockOutsideDetailViewController;
                           __block QNVideoInfoViewController *douleBlockSelf = blockSelf;
                           __block NSMutableArray *tags = [[blockSelf.videoFileItem.keywords componentsSeparatedByString:@";"] mutableCopy];
                           
                           if (tags == nil) {
                               tags = [NSMutableArray array];
                           }
                           
                           [tags addObject:contentView.text];
                           
                           [videoStation setVideoKeyword:filmItem.f_id
                                            withKeywords:tags
                                                isAppend:YES
                                        withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                            douleBlockSelf.videoFileItem.keywords = [douleBlockSelf composeKeywords:tags];
                                            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *e){
                                                if (success) {
                                                    [QNVideoTool toastWithTargetViewController:blockOutsideViewController
                                                                                      withText:[NSString stringWithFormat: NSLocalizedString(@"AddTagSuccess", nil), contentView.text]
                                                                                     isSuccess:YES];
                                                    [[douleBlockSelf tagListView] addTag:contentView.text];
                                                    [douleBlockSelf.tableView reloadData];
                                                }
                                            }];
                                        }
                                        withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                            [QNVideoTool toastWithTargetViewController:blockOutsideViewController
                                                                              withText:[NSString stringWithFormat: NSLocalizedString(@"AddTagFail", nil), contentView.text]
                                                                             isSuccess:NO];
                                        }
                            ];
                           
                           [alertView dismiss];
                       }];
    
}

- (IBAction)modifyDescription:(id)sender{
    __block QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    __block QNVideoFileItem *fileItem = self.videoFileItem;
    __block QNVideoInfoViewController *blockSelf = self;
    __block QNVideoItemDetailViewController *blockOutsideDetailViewController = self.outsideDetailViewController;

    CXAlertView *_alert = [CXAlertView showInputTextfieldAlert:NSLocalizedString(@"ChangeDescription", nil)
                                               withActionTitle:NSLocalizedString(@"Done", nil)
                                         withTextFieldDelegate:self
                                             withActionHandler:^(CXAlertView *view, CXAlertButtonItem *item)
                           {
                               [view dismiss];
                               __block UITextField *textField = (UITextField *)view.contentView;
                               
                               //Change Description
                               
                               [videoStation setVideoComment:fileItem.f_id
                                                 withComment:textField.text
                                            withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                                fileItem.comment = textField.text;
                                                [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfWithCompletion:^(BOOL s, NSError *e){
                                                    if (s) {
                                                        [QNVideoTool toastWithTargetViewController:blockOutsideDetailViewController
                                                                                          withText:NSLocalizedString(@"UpdateVideoCommentSuccess", nil)
                                                                                         isSuccess:YES];
                                                        [textField setText:fileItem.comment];
                                                        [blockSelf settingData];
                                                    }else
                                                        [QNVideoTool toastWithTargetViewController:blockOutsideDetailViewController
                                                                                          withText:NSLocalizedString(@"UpdateCommentFailure", nil)
                                                                                         isSuccess:NO];
                                                    
                                                    
                                                }];
                                            }
                                            withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                                [QNVideoTool toastWithTargetViewController:blockOutsideDetailViewController
                                                                                  withText:NSLocalizedString(@"UpdateCommentFailure", nil)
                                                                                 isSuccess:NO];
                                            }];
                               
                               
                               
                           }];
    UITextField *titleTextField = (UITextField *)_alert.contentView;
    titleTextField.secureTextEntry = NO;
    titleTextField.text = self.videoFileItem.comment;
}

#pragma mark - starValueChanged
-(void)starsSelectionChanged:(EDStarRating*)control rating:(float)rating{
    DDLogVerbose(@"star rating changed! %f", rating);
}

#pragma mark - TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    DDLogVerbose(@"showAlert %@", NSStringFromCGRect(self.showAlert.frame));
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
//    [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - UIPickerView for Classification
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 5;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSArray *titles = @[@"Uncategorized", @"Movie", @"TV show", @"Music video", @"Home video"];
    return [titles objectAtIndex:row];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView{
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    RFToolbarButton *exampleButton = [RFToolbarButton buttonWithTitle:NSLocalizedString(@"Done", nil)];
    
    [exampleButton addEventHandler:^{
        [textView resignFirstResponder];
    } forControlEvents:UIControlEventTouchUpInside];
    
    textView.inputAccessoryView = [RFKeyboardToolbar toolbarViewWithButtons:@[exampleButton]];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    DDLogVerbose(@"testView begin Editing");
}


- (void)textViewDidEndEditing:(UITextView *)textView{
    DDLogVerbose(@"testView end Editing");
    textView.inputAccessoryView = nil;
}

- (void)dismissKeyboard:(QNButton *)sender{
    UITextView *targetTextView = sender.userInfo;
    [targetTextView resignFirstResponder];
}

- (void)keyboardWasShown:(NSNotification *)noti{

}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification{

}
#pragma mark - Private
- (NSString *)composeKeywords:(NSArray *)keywords{
    NSMutableString *r = [NSMutableString string];
    BOOL isFirst = YES;
    for (NSString *keyword in keywords) {
        if (isFirst) {
            [r appendString:keyword];
        }else
            [r appendFormat:@";%@",keyword];
        isFirst = NO;
    }
    return r;
}

- (void)reloadTags{
    while ([self.tagListView.tags count] > 0) {
        [self.tagListView removeTag:[self.tagListView.tags firstObject]];
    }
    
    NSMutableArray *tags = [[self.videoFileItem.keywords componentsSeparatedByString:@";"] mutableCopy];
    [tags removeObject:@""];
    [self.tagListView addTags:tags];
    
    CGFloat tagHeight = self.tagListView.contentSize.height;
    
    self.tagListView.frame = CGRectMake(self.tagListView.frame.origin.x,
                                        self.tagListView.frame.origin.y,
                                        self.tagListView.frame.size.width,
                                        tagHeight);
    self.tagListView.bounds = self.tagListView.frame;
}
@end
