//
//  QNImageTextButton.m
//  QVideo
//
//  Created by Change.Liao on 2014/1/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNImageTextButton.h"

@implementation QNImageTextButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
        BOOL isPortraitBool = (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown);
        NSString *bundleName = isPortraitBool?@"QNImageTextButton":@"QNImageTextButton_landscape";
        UIView *subView =[[[NSBundle mainBundle] loadNibNamed:bundleName
                                                        owner:self
                                                      options:nil] objectAtIndex:0];
        [self addSubview:subView];
    }
    return self;
}/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
