//
//  VLCMovieViewController+Extend.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/28.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "VLCMovieViewController.h"

@interface VLCMovieViewController (Extend)

- (void)setControlsHidden:(BOOL)hidden animated:(BOOL)animated ;
- (void)toggleControlsVisible;
@end
