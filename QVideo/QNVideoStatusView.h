//
//  QNVideoStatusView.h
//  QVideo
//
//  Created by Change.Liao on 2014/3/18.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  Giving the definition of status which shown in the main view.
 */
typedef NS_ENUM(NSInteger, QNVideoStatus){
    /**
     *  give the transfering animation
     */
    QNVideoStatusTransfering = 0,
    /**
     *  No video item in this view mode
     */
    QNVideoStatusNoItem,
    /**
     *  No video item in the collection
     */
    QNVideoStatusNoItemInCollection,
    /**
     *  No video item in this smart collection
     */
    QNVideoStatusNoItemInSmartCollection,
    /**
     *  No video item in trashcan.
     */
    QNVideoStatusNoItemInTrashcan,
    /**
     *  No video item in Private(私人典藏)
     */
    QNVideoStatusNoItemInPrivate,
    /**
     *  No video item in Qsync
     */
    QNVideoStatusNoItemInQsync,
    /**
     *  Something Error
     */
    QNVideoStatusError
};

@protocol QNVideoStatusDelegate <NSObject>
@optional
/**
 *  <#Description#>
 *
 *  @param statusView <#statusView description#>
 *  @param status     <#status description#>
 */
- (void)changeStatus:(UIView *)statusView withStatus:(QNVideoStatus)status;
@end

@interface QNVideoStatusView : UIView
@property (nonatomic) QNVideoStatus videoStatus;
@property (weak, nonatomic) IBOutlet UIImageView *statusIcon;
@property (weak, nonatomic) IBOutlet UILabel *statusDescription;
@property (weak, nonatomic) id<QNVideoStatusDelegate> delegate;
/**
 *  <#Description#>
 *
 *  @param status <#status description#>
 */
- (void)setVideoStatus:(QNVideoStatus)status;
@end
