//
//  QNVideoColorLabelSelectionView.h
//  QVideo
//
//  Created by Change.Liao on 2014/2/21.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNButton.h"

@interface QNVideoColorLabelSelectionView : UIView
@property (weak, nonatomic) IBOutlet QNButton *noneColor;
@property (weak, nonatomic) IBOutlet QNButton *yellowColor;
@property (weak, nonatomic) IBOutlet QNButton *redColor;
@property (weak, nonatomic) IBOutlet QNButton *greenColor;
@property (weak, nonatomic) IBOutlet QNButton *brownColor;
@property (weak, nonatomic) IBOutlet QNButton *purpleColor;

@property (weak, nonatomic) QNButton *selectedBtn;
@property (nonatomic) NSInteger colorOption;
- (void)setting;
- (IBAction)changeColor:(id)sender;

@end
