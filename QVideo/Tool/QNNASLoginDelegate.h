//
//  QNNASLoginDelegate.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/18.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NASServer;

@protocol QNNASLoginDelegate <NSObject>
- (void)loginNASServer:(NASServer *)nasData;
- (void)loginNASServer:(NASServer *)nasData withCompletionBlock:(void(^)(BOOL success, NSError *error))completion;

@end
