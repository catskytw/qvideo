//
//  NSString+NSString_TimeDuration.m
//  QVideo
//
//  Created by Change.Liao on 2013/11/29.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "NSString_TimeDuration.h"
#import <QNAPFramework/QNAPFramework.h>

@implementation NSString (NSString_TimeDuration)

+ (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return (ti > 3600)?[NSString stringWithFormat:@"%02i:%02i:%02i", hours, minutes, seconds]:[NSString stringWithFormat:@"%02i:%02i", minutes, seconds];
}

+ (NSString *)convertNumberToMonth:(NSString *)number{
    NSString *r = @"";
    @try {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM"];
        NSArray *monthArray = [formatter shortMonthSymbols];
        NSUInteger index = [number integerValue] - 1;
        if (index < [monthArray count]) {
            r = [monthArray objectAtIndex:index];
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"error while converting month from int to string, %@", exception);
    }
    @finally {
        return r;
    }
}

- (CGSize)caculateSizeOfDuration{
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont systemFontOfSize:12.0f], NSFontAttributeName,
                                          nil];
    
    CGRect frame = [self boundingRectWithSize:CGSizeMake(300, 21.0f)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributesDictionary
                                      context:nil];
    return frame.size;
}
@end
