//
//  UIView+QVideoExt.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/11.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (QVideoExt)

- (void)removeAllSubViews;
- (BOOL)hasSubViewWithTag:(NSInteger)tag;

@end
