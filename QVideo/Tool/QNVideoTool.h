//
//  QNVideoTool.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/10.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QNAPFramework/QNVideoStationAPIManager.h>
#import <QNAPFramework/QNAPCommunicationManager.h>
#import <EDStarRating/EDStarRating.h>
#import <Reachability/Reachability.h>
#import <QNAPFramework/QNVideoFileItem.h>
#import <QNAPFramework/QNVideoRender.h>
#import <QNAPFramework/QNLogin.h>
#import <MessageUI/MessageUI.h>
#import <CocoaHTTPServer/HTTPServer.h>

#import "NASServer.h"
#import "QNButton.h"
#import <AsyncSocket.h>
#import <QNAPFramework/QNChromecastTool.h>

#define STOP_RECURSIVE_FETCH @"stopRecursiveUpdate"

@interface QNVideoTool : NSObject <NSURLSessionDownloadDelegate, AsyncSocketDelegate, MFMailComposeViewControllerDelegate>{
    NSDate *previousDate;
    UIActivityIndicatorView *_activityIndicator;
    NSInteger _videoCount;
    NSString *_currentUUID;
    
    AsyncSocket *socket;
    NSString *correctURL;
    BOOL canOpen;
    
    QNChromecastTool *_chromecast;
    HTTPServer *_httpServer;
}
@property (nonatomic, strong) QNChromecastTool *chromecast;
@property (nonatomic, strong) Reachability *reachability;
@property (nonatomic, strong) QNVideoRender *defaultRender;
@property (nonatomic, strong) QNFileLogin *loginInfo;
@property (nonatomic, strong) QNLogin *multimediaLogin;
@property (nonatomic, strong) NASServer *nasServerInfo;
@property (nonatomic) NSInteger videoCount;
@property (nonatomic, strong) void(^loginResultBlock)(BOOL success, NSError *e);

+ (QNVideoTool *)share;
- (NSString *)urlInHTTPServer:(NSString *)fileName;
- (void)stopAllRequests;
- (void)recursiveUpdateTimelineVideoFiles:(VideoFileHomePath)targetPath withTimeline:(NSString *)timeline withPageNumber:(NSInteger)page withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic withEachPageSuccessBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;
/**
 *  fetching the list of video files RECURSIVLY!
 *
 *  @param targetPath targetPath
 *  @param page       which pageNumber you want to start, it should be from zero in common.
 *  @param success    success block
 *  @param failure    failure block
 */
- (void)recursiveUpdateVideoFiles:(VideoFileHomePath)targetPath withPageNumber:(NSInteger)page withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic withEachPageSuccessBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  get the video items in collection
 *
 *  @param targetPath only allowed by videoFileHomePathCollections & videoFileHomePathSmartCollections
 *  @param success    success block
 *  @param failure    failure block
 */
- (void)recursiveLoadCollections:(VideoFileHomePath)targetPath withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withEachPageSuccessBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withFailureBlock:(QNFailureBlock)failure;
/**
 *  <#Description#>
 *
 *  @param albumId     <#albumId description#>
 *  @param sortType    <#sortType description#>
 *  @param isASC       <#isASC description#>
 *  @param page        <#page description#>
 *  @param homePath    <#homePath description#>
 *  @param uuid        <#uuid description#>
 *  @param eachSuccess <#eachSuccess description#>
 *  @param success     <#success description#>
 *  @param failure     <#failure description#>
 */
- (void)recursiveLoadCollectionVideoFiles:(NSString *)albumId withSortType:(VideoFileListSortType)sortType isASC:(BOOL)isASC withStartPage:(NSInteger)page withHomePath:(VideoFileHomePath)homePath withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic withEachBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;
/**
 *  get videoItems in trashCan
 *
 *  @param success success block
 *  @param failure failure block
 */
- (void)recursiveLoadTrashCanVideoFilesWithUUID:(NSString *)uuid withStartPage:(NSInteger)page withSuccessBlock:
(QNQNVideoFileListSuccessBlock)success withFilterDic:(NSDictionary *)filterDic  withEachPageSuccessBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withFailureBlock:(QNFailureBlock)failure;

/**
 *  <#Description#>
 *
 *  @param prefix      <#prefix description#>
 *  @param homepath    <#homepath description#>
 *  @param uuid        <#uuid description#>
 *  @param page        <#page description#>
 *  @param success     <#success description#>
 *  @param eachSuccess <#eachSuccess description#>
 *  @param failure     <#failure description#>
 */
- (void)recursiveLoadFolderWithDir:(NSString *)prefix withHomePath:(VideoFileHomePath)homepath withUUID:(NSString *)uuid withStartPage:(NSInteger)page withFilterDic:(NSDictionary *)filterDic withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withEachPageSuccessBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withFailureBlock:(QNFailureBlock)failure;
/**
 *  Given a toast if needed
 *
 *  @param targetViewController which viewController the toast needs to display over on.
 *  @param text                 the content of the toast (string)
 *  @param isSuccess            success or not? This would display the different background color of the toast.
 */
+ (void)toastWithTargetViewController:(UIViewController *)targetViewController withText:(NSString *)text isSuccess:(BOOL)isSuccess;

/**
 *  <#Description#>
 *
 *  @param targetStarRating <#targetStarRating description#>
 */
+ (void)configureStarRating:(EDStarRating *)targetStarRating;

/**
 *  <#Description#>
 *
 *  @return <#return value description#>
 */
+ (NSString *)creatingDownloadFolder;

/**
 *  caculate the directory's size in bytes.
 *
 *  @param folderPath folder path
 *
 *  @return the size of this folder
 */
+ (double)folderSize:(NSString *)folderPath;

/**
 *  <#Description#>
 *
 *  @return <#return value description#>
 */
+ (double)caculateSDWebImageCacheSize;

/**
 *  <#Description#>
 */
+ (void)cleanSDWebImageDisk;

/**
 *  <#Description#>
 *
 *  @param target   <#target description#>
 *  @param selector <#selector description#>
 */
+ (void)generateLeftBarItem:(UIViewController *)target withSelect:(SEL)selector;

/**
 *  <#Description#>
 *
 *  @param target   <#target description#>
 *  @param selector <#selector description#>
 */
+ (void)generateRightBarItem:(UIViewController *)target withSelect:(SEL)selector;

/**
 *  <#Description#>
 *
 *  @param target   <#target description#>
 *  @param selector <#selector description#>
 */
+ (void)generateBlackLeftBarItem:(UIViewController *)target withSelect:(SEL)selector;

/**
 *  <#Description#>
 *
 *  @param target   <#target description#>
 *  @param selector <#selector description#>
 */
+ (void)generateBlackRightBarItem:(UIViewController *)target withSelect:(SEL)selector;

/**
 *  <#Description#>
 *
 *  @param target   <#target description#>
 *  @param selector <#selector description#>
 *  @param title    <#title description#>
 *
 *  @return <#return value description#>
 */
+ (QNButton *)generateCustomLeftBarItem:(UIViewController *)target withSelect:(SEL)selector withTitleText:(NSString *)title;

/**
 *  <#Description#>
 *
 *  @param target   <#target description#>
 *  @param selector <#selector description#>
 *  @param title    <#title description#>
 *
 *  @return <#return value description#>
 */
+ (QNButton *)generateCustomRightBarItem:(UIViewController *)target withSelect:(SEL)selector withTitleText:(NSString *)title;

/**
 *  <#Description#>
 *
 *  @param image <#image description#>
 *
 *  @return <#return value description#>
 */
+ (UIImage *)flipImageHorizontal:(UIImage *)image;

/**
 *  <#Description#>
 *
 *  @param color <#color description#>
 *
 *  @return <#return value description#>
 */
+ (UIImage *)colorLevel:(NSInteger)color;

/**
 *  <#Description#>
 *
 *  @param classificationString <#classificationString description#>
 *
 *  @return <#return value description#>
 */
+ (NSInteger)classificationStringToValue:(NSString *)classificationString;

/**
 *  <#Description#>
 *
 *  @param indicator <#indicator description#>
 *
 *  @return <#return value description#>
 */
+ (NSString *)classificationValueToString:(NSInteger)indicator;

/**
 *  <#Description#>
 *
 *  @param ingredientLine <#ingredientLine description#>
 *  @param superviewWidth <#superviewWidth description#>
 *
 *  @return <#return value description#>
 */
+ (CGFloat)heightOfCellWithIngredientLine:(NSString *)ingredientLine
                       withSuperviewWidth:(CGFloat)superviewWidth;

- (void)initializeResumeVideoDownload;

- (NSString *)videoPlayingURL:(NSString *)fID isDownload:(BOOL)isDownload;

#pragma mark - DownloadTask Operation
- (void)restoreAllDownloadTaskBySystem:(NSManagedObjectContext *)context;

- (void)restoreAllDownloadTaskBySystem;

- (void)pauseAllDownloadTaskBySystem:(NSManagedObjectContext *)context;

+ (NSString *)transformedValue:(id)value;

+ (NetworkStatus)detectNetworkStatus;

+ (BOOL)downloadFile:(QNVideoFileItem *)filmItem;

- (void)controlDownloadTask:(NSNotification *)noti;

#pragma mark -
/**
 *  <#Description#>
 *
 *  @param f_id       <#f_id description#>
 *  @param isDownload <#isDownload description#>
 *
 *  @return <#return value description#>
 */
+ (NSString *)generateGetVideoURL:(NSString *)f_id isDownload:(BOOL)isDownload;

+ (NSString *)generateVideoOfflineURL:(NSString *)f_id isDownload:(BOOL)isDownload;

+ (NSString *)generateVideoOnlineURL:(NSString *)f_id withResolution:(NSString *)resolution;


/**
 *  <#Description#>
 *
 *  @param filename <#filename description#>
 *
 *  @return <#return value description#>
 */
+ (NSString *)textFromTextFileNamed:(NSString *)filename;
#pragma mark - open url
/**
 *  <#Description#>
 */
- (void)feedBack:(UIViewController *)targetViewController;


/**
 *  <#Description#>
 *
 *  @param targetViewController <#targetViewController description#>
 */
- (void)popToNASListViewController:(UIViewController *)targetViewController;

#pragma mark - TUTK
- (void)connectToTUTK:(NSString *)deviceName completionBlock:(void(^)(BOOL success, NSError *error, NSDictionary *tutkDicInfo))completion;
#pragma mark - Loading Indicator
/**
 *  <#Description#>
 *
 *  @param targetBar <#targetBar description#>
 */
- (void)loadingStartAnimation:(UINavigationBar *)targetBar;

/**
 *  <#Description#>
 */
- (void)loadingStopAnimation;

/**
 *  <#Description#>
 *
 *  @return <#return value description#>
 */
+ (NSString*)deviceModelName;

/**
 *  <#Description#>
 *
 *  @param targetNAS   <#targetNAS description#>
 *  @param loginResult <#loginResult description#>
 */
- (void)showLoginDialog:(NASServer *)targetNAS withResultBlock:(void(^)(BOOL success, NSError *e))loginResult;

/**
 *  choose the correct NAS icon
 *
 *  @param modelName model name
 *
 *  @return The NAS icon string
 */
+ (NSString *)getImageFromModelName:(NSString*)modelName;

/**
 *  response the current network status, including NotReachable, ReachableViaWiFi, ReachableViaWWAN
 *
 *  @return NetworkStatus
 */
+ (NetworkStatus)currentNetworkStatus;

/**
 *  is allowed to download the video files?
 *
 *  @return YES/NO
 */
+ (BOOL)allowDownload;

- (NSString *)correctURL:(NASServer *)nasServer;

- (NSNumber *)correctPort:(NASServer *)nasServer;

- (void)logoutClean;
@end
