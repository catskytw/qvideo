//
//  QNVideoTool.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/10.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoTool.h"
#import <QNAPFramework/QNVideoFileList.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <TSMessages/TSMessage.h>
#import <SDWebImage/SDImageCache.h>
#import <QNAPFramework/QNCollection.h>
#import <AFNetworking/AFNetworking.h>
#import <QNAPFramework/QNMyCloudManager.h>
#import <QNAPFramework/MyCloudDeviceResponse.h>

#import "VideoDownload.h"
#import "QVideoEnumberator.h"
#import "VideoDownload.h"
#import "QNAppDelegate.h"
#import "QNNASListViewController.h"
#import "QNVideoAlbumViewController.h"
#import <sys/utsname.h>
#import "ASViewController.h"
#import <QNAPFramework/MyCloudDevice.h>
#import <QNAPFramework/MyCloudDeviceLocalService.h>
#import <QNAPFramework/QFTUTKTunnel.h>
#import <QNAPFramework/MyCloudCloudLinkResponse.h>
#import "QNTimelineViewController.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>

#define COUNT_PER_PAGE 200

#define IOS_CELLULAR    @"pdp_ip0"
#define IOS_WIFI        @"en0"
#define IP_ADDR_IPv4    @"ipv4"
#define IP_ADDR_IPv6    @"ipv6"

static QNVideoTool *shareInstance = nil;
typedef BOOL(^CheckConditionBlock)(void);

@implementation QNVideoTool

+ (QNVideoTool *)share{
    if (shareInstance == nil) {
        shareInstance = [[QNVideoTool alloc] init];
        [shareInstance settingMisc];
        shareInstance.videoCount = 0;
        shareInstance.reachability = [Reachability reachabilityForInternetConnection];
        [shareInstance.reachability startNotifier];
    }
    return shareInstance;
}

- (void)settingMisc{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(controlDownloadTask:) name:kReachabilityChangedNotification object:nil];
    
    /**
     *  Setting chromecast searching instance
     */
    _chromecast = [[QNChromecastTool alloc] init];
    [_chromecast initGooglecaseSender];
    [_chromecast startScanDevices];
    
    /**
     *  create a httpServer for casting the localfiles via chromecast
     */
    _httpServer = [[HTTPServer alloc] init];
    [_httpServer setType:@"_http._tcp."];
    NSString *docRoot = [@"~/Document" stringByExpandingTildeInPath];
    [_httpServer setDocumentRoot:docRoot];
    NSError *error = nil;
    if(![_httpServer start:&error])
    {
        DDLogError(@"Error starting HTTP Server: %@", error);
    }else
        DDLogInfo(@"http server: %@", _httpServer.domain);

}

- (NSString *)urlInHTTPServer:(NSString *)fileName{
    return [NSString stringWithFormat:@"%@/%@", _httpServer.domain, fileName];
}


- (void)controlDownloadTask:(NSNotification *)noti{
    if ([QNVideoTool allowDownload]) {
        [self restoreAllDownloadTaskBySystem];
    }else
        [self pauseAllDownloadTaskBySystem:[NSManagedObjectContext MR_defaultContext]];
}

- (void)stopAllRequests{
    _currentUUID = @"";
}
- (void)recursiveUpdateTimelineVideoFiles:(VideoFileHomePath)targetPath withTimeline:(NSString *)timeline withPageNumber:(NSInteger)page withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic withEachPageSuccessBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    if (page == 1)
        _currentUUID = [NSString stringWithString:uuid];

    NSString *_localUUID = [NSString stringWithString:uuid];

    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    [videoStation getTimeLineFileListWithTimeLineLabel:timeline
                                            withSortBy:videoFileListSortByTimeLine
                                        withPageNumber:page
                                     withCountsPerPage:COUNT_PER_PAGE
                                          withHomePath:targetPath
                                              withUUID:uuid
                                         withFilterDic:filterDic
                                                 isASC:NO
                                      withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoFileList *obj){
                                          //It should return if the currentUUID doesn't equal localUUID(which means a new query generated and need to terminate this recursive fetching.
                                          if (![_localUUID isEqualToString:_currentUUID])
                                              return ;
                                          
                                          //fetching the total video count
                                          if (page == 1)
                                              _videoCount = [obj.videoCount integerValue];
                                          
                                          //recursive
                                          BOOL hasNext = _videoCount > (page * COUNT_PER_PAGE);
                                          if (hasNext){
                                              //run eachSuccess block if we have the next query
                                              if (eachSuccess) {
                                                  eachSuccess(o, r, obj);
                                              }
                                              [self recursiveUpdateTimelineVideoFiles:targetPath
                                                                         withTimeline:timeline
                                                                       withPageNumber:page + 1
                                                                             withUUID:uuid
                                                                        withFilterDic:filterDic
                                                             withEachPageSuccessBlock:success
                                                                     withSuccessBlock:success
                                                                     withFailureBlock:failure];
                                          }else{
                                              _videoCount = 0;
                                              success(o, r, obj);
                                          }
                                      }
 
                                       withFailueBlock:^(RKObjectRequestOperation *o, NSError *e){
                                           DDLogError(@"fetching video files error!");
                                           _videoCount = 0;
                                           failure(o, e);
                                       }];
    
}

- (void)recursiveUpdateVideoFiles:(VideoFileHomePath)targetPath withPageNumber:(NSInteger)page withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic withEachPageSuccessBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    if (page == 1)
        _currentUUID = [NSString stringWithString:uuid];
    
    NSString *_localUUID = [NSString stringWithString:uuid];
    
    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    [videoStation getAllFileListWithSortBy:videoFileListSortByTimeLine
                            withPageNumber:page
                         withCountsPerPage:COUNT_PER_PAGE
                              withHomePath:targetPath
                                  withUUID:uuid
                             withFilterDic:filterDic
                                     isASC:NO
                          withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoFileList *obj){
                              //It should return if the currentUUID doesn't equal localUUID(which means a new query generated and need to terminate this recursive fetching.
                              if (![_localUUID isEqualToString:_currentUUID])
                                  return ;
                              
                              //fetching the total video count
                              if (page == 1)
                                  _videoCount = [obj.videoCount integerValue];
                              
                              //recursive
                              BOOL hasNext = _videoCount > (page * COUNT_PER_PAGE);
                              if (hasNext){
                                  //run eachSuccess block if we have the next query
                                  if (eachSuccess) {
                                      eachSuccess(o, r, obj);
                                  }
                                  [self recursiveUpdateVideoFiles:targetPath withPageNumber:page + 1 withUUID:uuid withFilterDic:filterDic withEachPageSuccessBlock:success withSuccessBlock:success withFailureBlock:failure];
                              }else{
                                  _videoCount = 0;
                                  success(o, r, obj);
                              }
                          }withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                              DDLogError(@"fetching video files error!");
                              _videoCount = 0;
                              failure(o, e);
                          }];
}

- (void)recursiveLoadTrashCanVideoFilesWithUUID:(NSString *)uuid withStartPage:(NSInteger)page withSuccessBlock:
(QNQNVideoFileListSuccessBlock)success withFilterDic:(NSDictionary *)filterDic  withEachPageSuccessBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withFailureBlock:(QNFailureBlock)failure{
    if (page == 1)
        _currentUUID = [NSString stringWithString:uuid];
    
    NSString *_localUUID = [NSString stringWithString:uuid];
    
    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    [videoStation getTrashCanWithSortType:videoFileListSortByTimeLine
                                    isASC:NO
                               pageNumber:page
                              countInPage:COUNT_PER_PAGE
                                 withUUID:uuid
                            withFilterDic:filterDic
                         withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                             QNVideoFileList *obj = r.firstObject;
                             if (![_localUUID isEqualToString:_currentUUID])
                                 return ;
                             
                             //fetching the total video count
                             if (page == 1)
                                 _videoCount = [obj.videoCount integerValue];
                             
                             //recursive
                             BOOL hasNext = _videoCount > (page * COUNT_PER_PAGE);
                             if (hasNext){
                                 //run eachSuccess block if we have the next query
                                 if (eachSuccess) {
                                     eachSuccess(o, r, obj);
                                 }
                                 [[QNVideoTool share] recursiveLoadTrashCanVideoFilesWithUUID:uuid withStartPage:page + 1 withSuccessBlock:success withFilterDic:filterDic withEachPageSuccessBlock:success withFailureBlock:failure];
                             }else{
                                 _videoCount = 0;
                                 success(o, r, obj);
                             }
                             success(o, r, obj);
                         }
                         withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                             failure(o, e);
                         }];
}

- (void)recursiveLoadCollections:(VideoFileHomePath)targetPath withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withEachPageSuccessBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withFailureBlock:(QNFailureBlock)failure{
    _currentUUID = @"";
    VideoAlbumType albumType = videoAlbums;
    switch (targetPath) {
        case videoFileHomePathCollections:
            albumType = videoAlbums;
            break;
        case videoFileHomePathSmartCollections:
            albumType = videoSmartAlbums;
            break;
        default:
            // do nothing if the homepath is listed on above.
            return;
    }
    
    [QNCollection MR_truncateAll];
    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    //TODO you should get the collection recursively.
    [videoStation getCollections:albumType
                        sortType:videoFileListSortByName
                           isASC:YES
                      pageNumber:1
                     countInPage:500
                   withFilterDic:filterDic
                withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *m){
                    QNVideoFileList *list = (QNVideoFileList *)m.firstObject;
                    success(o, m, list);
                }
 
                withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                    failure(o, e);
                }];
}

- (void)recursiveLoadCollectionVideoFiles:(NSString *)albumId withSortType:(VideoFileListSortType)sortType isASC:(BOOL)isASC withStartPage:(NSInteger)page withHomePath:(VideoFileHomePath)homePath withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic withEachBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    if (page == 1)
        _currentUUID = [NSString stringWithString:uuid];
    NSString *_localUUID = [NSString stringWithString:uuid];
    
    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    [videoStation getCollectionVideoItems:albumId
                                 sortType:sortType
                               pageNumber:page
                              countInPage:CountInPage
                                 homePath:homePath
                                 withUUID:uuid
                            withFilterDic:filterDic
                                    isASC:isASC
                         withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                             QNVideoFileList *obj = r.firstObject;
                             
                             if (![_localUUID isEqualToString:_currentUUID])
                                 return ;
                             
                             //fetching the total video count
                             if (page == 1)
                                 _videoCount = [obj.videoCount integerValue];
                             
                             //recursive
                             BOOL hasNext = _videoCount > (page * COUNT_PER_PAGE);
                             if (hasNext){
                                 //run eachSuccess block if we have the next query
                                 if (eachSuccess)
                                     eachSuccess(o, r, obj);
                                 [self recursiveLoadCollectionVideoFiles:albumId withSortType:sortType isASC:isASC withStartPage:page+1 withHomePath:homePath withUUID:_localUUID withFilterDic:filterDic withEachBlock:success withSuccessBlock:success withFailureBlock:failure];
                             }else{
                                 _videoCount = 0;
                                 success(o, r, obj);
                             }
                             success(o, r, obj);
                         }
                         withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                             failure(o, e);
                         }];
}

- (void)recursiveLoadFolderWithDir:(NSString *)prefix withHomePath:(VideoFileHomePath)homepath withUUID:(NSString *)uuid withStartPage:(NSInteger)page withFilterDic:(NSDictionary *)filterDic withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withEachPageSuccessBlock:(QNQNVideoFileListSuccessBlock)eachSuccess withFailureBlock:(QNFailureBlock)failure{
    if (page == 1)
        _currentUUID = [NSString stringWithString:uuid];
    NSString *_localUUID = [NSString stringWithString:uuid];
    
    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    [videoStation getFolderFileList:prefix
                       withHomePath:homepath
                       withSortType:videoFileListSortByName
                         pageNumber:page
                        countInPage:COUNT_PER_PAGE
                           withUUID:_localUUID
                      withFilterDic:filterDic
                            isASC:YES
                   withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                       QNVideoFileList *obj = r.firstObject;
                       if (![_localUUID isEqualToString:_currentUUID])
                           return ;
                       //fetching the total video count
                       if (page == 1)
                           _videoCount = [obj.videoCount integerValue];
                       
                       //recursive
                       BOOL hasNext = _videoCount > (page * COUNT_PER_PAGE);
                       if (hasNext){
                           //run eachSuccess block if we have the next query
                           if (eachSuccess)
                               eachSuccess(o, r, obj);
                           [self recursiveLoadFolderWithDir:prefix withHomePath:homepath withUUID:_localUUID withStartPage:page + 1 withFilterDic:filterDic withSuccessBlock:success withEachPageSuccessBlock:success withFailureBlock:failure];
                       }else{
                           _videoCount = 0;
                           success(o, r, obj);
                       }
                       success(o, r, obj);
                   }
                   withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                       DDLogError(@"fetching folder information failure!");
                   }];
}

+ (void)toastWithTargetViewController:(UIViewController *)targetViewController withText:(NSString *)text isSuccess:(BOOL)isSuccess{
    [TSMessage showNotificationInViewController:targetViewController
                                          title:isSuccess?NSLocalizedString(@"Success", nil):NSLocalizedString(@"Failure", nil)
                                       subtitle:text
                                           type:(isSuccess)?TSMessageNotificationTypeSuccess:TSMessageNotificationTypeError];
}

+ (void)configureStarRating:(EDStarRating *)targetStarRating{
    targetStarRating.starImage = [UIImage imageNamed:@"icon_rating_30_off.png"];
    targetStarRating.starHighlightedImage = [UIImage imageNamed:@"icon_rating_30_on.png"];
    targetStarRating.maxRating = 5.0f;
    targetStarRating.horizontalMargin = 5.0f;
    targetStarRating.editable = NO;
    targetStarRating.displayMode = EDStarRatingDisplayFull;
    [targetStarRating setBackgroundColor:[UIColor clearColor]];
}

+ (double)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary fileSize];
    }
    
    double sizeInMB = (double)fileSize/1048576;
    return sizeInMB;
}

+ (NSString *)creatingDownloadFolder{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    NSError *error;
    NSString *downloadPath = [NSString stringWithFormat:@"%@/videoDownload/", basePath];
    //[NSString stringWithFormat:@"%@/%@", basePath, @"videoDownload"];
    if ( ![[NSFileManager defaultManager] fileExistsAtPath:downloadPath]){
        BOOL isSuccess = [[NSFileManager defaultManager] createDirectoryAtPath:downloadPath
                                                   withIntermediateDirectories:YES
                                                                    attributes:nil
                                                                         error:&error];
        if (isSuccess) {
            DDLogVerbose(@"create directory success %@", downloadPath);
        }else{
            DDLogError(@"path %@ created in failure!", downloadPath);
            downloadPath = nil;
        }
    }
    return downloadPath;
}

#pragma mark - SDImageCache
/**
 *  get the size of imageCache directory
 *
 *  @return how many size in MB
 */
+ (double)caculateSDWebImageCacheSize{
    unsigned long long size = [[SDImageCache sharedImageCache] getSize];
    double doubleSize = (double)size;
    return (doubleSize / 1048576.0f);
}

+ (void)cleanSDWebImageDisk{
    //TODO: synchronized?
    [[SDImageCache sharedImageCache] clearDisk];
}

+ (void)generateLeftBarItem:(UIViewController *)target withSelect:(SEL)selector{
    NSMutableArray *allBtns = [NSMutableArray array];

    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setFrame:CGRectMake(0, 0, 22, 22)];
    [leftBtn setImage:[UIImage imageNamed:@"icon_navigationbar_sidebar.png"] forState:UIControlStateNormal];
    [leftBtn setImage:[UIImage imageNamed:@"icon_navigationbar_sidebar_pres.png"] forState:UIControlStateHighlighted];
    [leftBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarBtn = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    [allBtns addObject:leftBarBtn];
    if ([target respondsToSelector:@selector(selectTimeline:)]){
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftBtn setFrame:CGRectMake(0, 0, 22, 22)];
        [leftBtn setImage:[UIImage imageNamed:@"btn_icon_navigationbar_time_select_normal"] forState:UIControlStateNormal];
        [leftBtn setImage:[UIImage imageNamed:@"btn_icon_navigationbar_time_select_pressed"] forState:UIControlStateHighlighted];
        [leftBtn addTarget:target action:@selector(selectTimeline:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarBtn = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
        
        [allBtns addObject:leftBarBtn];
    }
    
    [target.navigationItem setLeftBarButtonItems:allBtns];
}

+ (void)generateBlackLeftBarItem:(UIViewController *)target withSelect:(SEL)selector{
    NSMutableArray *allBtns = [NSMutableArray array];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setFrame:CGRectMake(0, 0, 22, 22)];
    [leftBtn setImage:[UIImage imageNamed:@"icon_playback_navigationbar_back.png"] forState:UIControlStateNormal];

    [leftBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarBtn = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    [allBtns addObject:leftBarBtn];

    [target.navigationItem setLeftBarButtonItems:allBtns];
}

+ (void)generateBlackRightBarItem:(UIViewController *)target withSelect:(SEL)selector{
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setFrame:CGRectMake(0, 0, 22, 22)];
    [rightBtn setImage:[UIImage imageNamed:@"icon_playback_navigationbar_more.png"] forState:UIControlStateNormal];
    [rightBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightBarBtn = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    [target.navigationItem setRightBarButtonItem:rightBarBtn];
}

+ (void)generateRightBarItem:(UIViewController *)target withSelect:(SEL)selector{
    NSMutableArray *allBtns = [NSMutableArray array];
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setFrame:CGRectMake(0, 0, 22, 22)];
    [rightBtn setImage:[UIImage imageNamed:@"icon_navigationbar_action.png"] forState:UIControlStateNormal];
    [rightBtn setImage:[UIImage imageNamed:@"icon_navigationbar_action_pres.png"] forState:UIControlStateHighlighted];
    [rightBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarBtn = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];

    [allBtns addObject:rightBarBtn];
    UIBarButtonItem *seletionBtn = nil;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    if ([target respondsToSelector:@selector(switchMultiSelectMode)]) {
        UIButton *multiSelecBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [multiSelecBtn setFrame:CGRectMake(0, 0, 22, 22)];
        [multiSelecBtn setImage:[UIImage imageNamed:@"btn_icon_navigationbar_multiple_select_normal"] forState:UIControlStateNormal];
        [multiSelecBtn setImage:[UIImage imageNamed:@"btn_icon_navigationbar_multiple_select_pressed"] forState:UIControlStateHighlighted];
        [multiSelecBtn addTarget:target action:@selector(switchMultiSelectMode) forControlEvents:UIControlEventTouchUpInside];

        seletionBtn = [[UIBarButtonItem alloc] initWithCustomView:multiSelecBtn];
        [allBtns addObject:seletionBtn];
    }
    
    if ([target isMemberOfClass:[QNVideoAlbumViewController class]]) {
        UIBarButtonItem *addCollectionBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:target action:@selector(createCollectionProcess)];
        [addCollectionBtn setTintColor:defaultGreenColor];
        [allBtns addObject:addCollectionBtn];
    }
#pragma pop
    [target.navigationItem setRightBarButtonItems:allBtns];
}

+ (QNButton *)generateCustomRightBarItem:(UIViewController *)target withSelect:(SEL)selector withTitleText:(NSString *)title{
    [target.navigationItem setRightBarButtonItem:nil];

    QNButton *rightBtn = [QNButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setFrame:CGRectMake(0, 0, 80, 22)];
    [rightBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [rightBtn setTitle:title forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];

    UIBarButtonItem *rightBarBtn = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    [target.navigationItem setRightBarButtonItem:rightBarBtn];
    return rightBtn;
}

+ (QNButton *)generateCustomLeftBarItem:(UIViewController *)target withSelect:(SEL)selector withTitleText:(NSString *)title{
    
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:14.0f]];
    
    QNButton *leftBtn = [QNButton buttonWithType:UIButtonTypeCustom];
    [leftBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [leftBtn setFrame:CGRectMake(0, 0, size.width, 22)];
    [leftBtn setTitle:title forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    if (target !=nil && selector != nil) {
        [leftBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }
    if ([title isEqualToString:NSLocalizedString(@"Back", nil)]) {
        UIImageView *backImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_navigationbar_back_black.png"]];
        [backImage setFrame:CGRectMake(0, 0, 12, 22)];
        [leftBtn addSubview:backImage];
        [leftBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 18.0f, 0.0f, 0.0f)];

    }
    [leftBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    UIBarButtonItem *leftBarBtn = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    [target.navigationItem setLeftBarButtonItem:leftBarBtn];
    return leftBtn;
}

+ (UIImage *)flipImageHorizontal:(UIImage *)image{
    return [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationDownMirrored];
}

+ (UIImage *)colorLevel:(NSInteger)color{
    NSString *colorPngName = nil;
    switch (color) {
        case 0:
            colorPngName = @"icon_color label_20_0.png";
            break;
        case 1:
            colorPngName = @"icon_color label_20_1.png";
            break;
        case 2:
            colorPngName = @"icon_color label_20_2.png";
            break;
        case 3:
            colorPngName = @"icon_color label_20_3.png";
            break;
        case 4:
            colorPngName = @"icon_color label_20_4.png";
            break;
        case 5:
            colorPngName = @"icon_color label_20_5.png";
            break;
        default:
            break;
    }
    // maybe [NSString stringWithFormat:@"icon_color label_20_%i.png", color]?
    return [UIImage imageNamed:colorPngName];
}

+ (NSInteger)classificationStringToValue:(NSString *)classificationString{
    NSInteger r = 0;
    if ([classificationString isEqualToString:@"Movie"]) {
        r = 1;
    }else if ([classificationString isEqualToString:@"TV show"]) {
        r = 2;
    }else if ([classificationString isEqualToString:@"Music video"]) {
        r = 3;
    }else if ([classificationString isEqualToString:@"Home video"]) {
        r = 4;
    }else if ([classificationString isEqualToString:@"Uncategorized"]) {
        r = 0;
    }
    return r;
}

+ (NSString *)classificationValueToString:(NSInteger)indicator{
    NSString *r = nil;
    switch (indicator) {
        case 1:
            r = @"Movie";
            break;
        case 2:
            r = @"TV show";
            break;
        case 3:
            r = @"Music video";
            break;
        case 4:
            r = @"Home video";
            break;
        case 0:
        default:
            r = @"Uncategorized";
            break;
    }
    return r;
}

- (void)initializeResumeVideoDownload{
    NSArray *allDownload = [VideoDownload MR_findAll];
    for (VideoDownload *downloadItem in allDownload) {
        NSString *urlString = [self videoPlayingURL:downloadItem.filmID isDownload:YES];
        [[QNAPCommunicationManager share].videoStationManager downloadVideoFilm:urlString
                                                                  withLocalPath:nil
                                                                      witFilmID:downloadItem.filmID
                                                                   withDelegate:self];
    }
}

- (NSString *)videoPlayingURL:(NSString *)fID isDownload:(BOOL)isDownload{
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    return [NSString stringWithFormat:@"%@video/api/video.php?a=%@&f=%@&vt=default&sid=%@", communicationManager.videoStationManager.baseURL, (isDownload)?@"download":@"display",fID, communicationManager.sidForMultimedia];
}

#pragma mark - DownloadOperation
- (void)pauseAllDownloadTaskBySystem:(NSManagedObjectContext *)context{
    __block NSManagedObjectContext *shareContext = context;
    [[QNAPCommunicationManager share].videoStationManager pauseAllDownloadTasks];
    [[QNAPCommunicationManager share].videoStationManager cancelAllDownloadTask:^(NSString *filmID, NSData *resumeData){
        //TODO
        VideoDownload *thisDownload = [VideoDownload MR_findFirstByAttribute:@"filmID" withValue:filmID inContext:shareContext];
        if (thisDownload && resumeData) {
            thisDownload.resumeKey = resumeData;
            thisDownload.downloadStatus = ([thisDownload.downloadStatus integerValue] == videoDownloadProgressing)?@(videoDownloadPauseBySystem):thisDownload.downloadStatus;
            [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error){
                QNAppDelegate *appDelegate = (QNAppDelegate *)[UIApplication sharedApplication].delegate;
                appDelegate.isFinishSaving = YES;
            }];
        }
    }];
}
- (void)restoreAllDownloadTaskBySystem{
    NSArray *allPauseTask = [VideoDownload MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"downloadStatus == %@ or downloadStatus == %@", @(videoDownloadPauseBySystem), @(videoDownloadProgressing)]];
    for (VideoDownload *downloadTask in allPauseTask) {
        //If the task has resumeKey, it could resume the downloadtask; otherwise, download it as a new one.
        if (downloadTask.resumeKey) {
            [[QNAPCommunicationManager share].videoStationManager downloadResumeTask:downloadTask.resumeKey withTaskID:downloadTask.filmID];
        }else{
            NSString *urlString = [self videoPlayingURL:downloadTask.filmID isDownload:YES];
            [[QNAPCommunicationManager share].videoStationManager downloadVideoFilm:urlString withLocalPath:nil witFilmID:downloadTask.filmID withDelegate:self];
        }
         downloadTask.downloadStatus = @(videoDownloadProgressing);
    }
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
}


#pragma mark - NSURLSessionDelegate
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    //resolve the SSL self-signed certification
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location{
    DDLogInfo(@"downloading completed! the path %@, downloadTask id: %@", location, downloadTask.taskDescription);
    previousDate = nil;
    NSString *downloadFolder = [QNVideoTool creatingDownloadFolder];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    VideoDownload *thisVideo = [VideoDownload MR_findFirstByAttribute:@"filmID" withValue:downloadTask.taskDescription];
    
    if (!downloadTask.taskDescription) {
        [thisVideo MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        return;
    }
    
    NSURL *destinationURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@" ,downloadFolder, thisVideo.filmTitle]];

    if ([fileManager moveItemAtURL:location
                             toURL:destinationURL
                             error:&error]) {
        DDLogVerbose(@"move the video file %@ successfully", downloadTask.taskDescription);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadProgress"
                                                            object:nil
                                                          userInfo:@{@"downloadProgress":[NSNumber numberWithFloat:100.0f],
                                                                     @"fID":downloadTask.taskDescription}];
//        [thisVideo MR_deleteEntity]; //this record should not be existed in CoreData.
    }else{
        //TODO remove the VideoDownload item from CoreData or give a hint for user?
        DDLogError(@"failed to move: %@, destination folder: %@, location: %@, error: %@", downloadTask.taskDescription, destinationURL, location, error);
        thisVideo.downloadStatus = @(videoDownloadUnknowError); //needed?
    }
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
}

/* Sent periodically to notify the delegate of download progress. */
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    NSDate *nowDate = [NSDate date];
    CGFloat downloadRate = 0.0f;
    if (previousDate) {
        NSTimeInterval duration = [nowDate timeIntervalSince1970] - [previousDate timeIntervalSince1970];
        downloadRate = (duration == 0)? 0:bytesWritten / duration;
    }
    previousDate = nowDate;

    CGFloat progress = 0.0f;
    if (totalBytesExpectedToWrite >= 0) {
        progress = ((double)totalBytesWritten / (double)(totalBytesExpectedToWrite)) * 100.0f;
    }
    DDLogVerbose(@"didWriteData %lld totalWrite %lld expectedToWrite %lld, progress %f", bytesWritten,totalBytesWritten, totalBytesExpectedToWrite, progress);

    [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadProgress"
                                                        object:nil
                                                      userInfo:@{@"downloadProgress":[NSNumber numberWithFloat:progress],
                                                                 @"fID":(downloadTask.taskDescription)?downloadTask.taskDescription:@"",
                                                                 @"downloadRate": @(downloadRate)}];
}

/* Sent when a download has been resumed. If a download failed with an
 * error, the -userInfo dictionary of the error will contain an
 * NSURLSessionDownloadTaskResumeData key, whose value is the resume
 * data.
 */
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
 didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes{
    DDLogError(@"Session %@ download task %@ resumed at offset %lld bytes out of an expected %lld bytes.\n",
               session, downloadTask, fileOffset, expectedTotalBytes);
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    NSData *resumeData = [error.userInfo valueForKey:NSURLSessionDownloadTaskResumeData];
    /** TODO: You should fetch the video item which's download status in progressing
     */
    DDLogError(@"download error %@", error);
    previousDate = nil;
    NSManagedObjectContext *defaultContext = [NSManagedObjectContext MR_defaultContext];
    VideoDownload *errorDownloadItem = [VideoDownload MR_findFirstByAttribute:@"filmID" withValue:task.taskDescription inContext:defaultContext];
    if (!errorDownloadItem) {
        DDLogError(@"There is no record in CoreData while the videoDownload completed!, Something is wrong! taskID:%@", task.taskDescription);
        errorDownloadItem = [VideoDownload MR_createEntity];
        errorDownloadItem.filmID = task.taskDescription;
    }
    if (resumeData) {
        errorDownloadItem.resumeKey = resumeData;
    }
    errorDownloadItem.downloadStatus = (error)?@(videoDownloadPauseBySystem):@(videoDownloadCompleted);
    
    [defaultContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *e){
        if (success) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DownloadCompleted" object:nil];
            DDLogVerbose(@"saving download status into CoreData successfully!");
        }else
            DDLogError(@"Error %@", e);
    }];
}

+ (NSString *)transformedValue:(id)value{
    
    double convertedValue = [value doubleValue];
    int multiplyFactor = 0;
    
    NSArray *tokens = @[@"bytes",@"KB",@"MB",@"GB",@"TB"];
    
    while (convertedValue > 1024) {
        convertedValue /= 1024;
        multiplyFactor++;
    }
    
    return [NSString stringWithFormat:@"%4.2f %@",convertedValue, tokens[multiplyFactor]];
}

+ (CGFloat)heightOfCellWithIngredientLine:(NSString *)ingredientLine
                       withSuperviewWidth:(CGFloat)superviewWidth{
    CGFloat labelWidth                  = superviewWidth;
    //    use the known label width with a maximum height of 100 points
    CGSize labelContraints              = CGSizeMake(labelWidth, 3000.0f);
    NSStringDrawingContext *context     = [[NSStringDrawingContext alloc] init];
    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14] forKey: NSFontAttributeName];

    CGRect labelRect                    = [ingredientLine boundingRectWithSize:labelContraints
                                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                                    attributes:stringAttributes
                                                                       context:context];
    
    //    return the calculated required height of the cell considering the label
    return labelRect.size.height;
}

+ (NetworkStatus)detectNetworkStatus{
    Reachability *reachability = [QNVideoTool share].reachability;
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}

+ (BOOL)downloadFile:(QNVideoFileItem *)filmItem{
    BOOL _allow = [QNVideoTool allowDownload];
    NSString *urlString = [[QNVideoTool share] videoPlayingURL:filmItem.f_id isDownload:YES];
    VideoDownload *videoDownload = [VideoDownload MR_findFirstByAttribute:@"filmID" withValue:filmItem.f_id];
    if (videoDownload == nil) {
        videoDownload = [VideoDownload MR_createEntity];
        videoDownload.filmID = filmItem.f_id;
        videoDownload.filmTitle = filmItem.cFileName;
        videoDownload.duration = filmItem.duration;
        videoDownload.fileSize = filmItem.iFileSize;
    }
    videoDownload.downloadStatus = _allow?@(videoDownloadProgressing):@(videoDownloadPauseBySystem);
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *e){
        if (_allow) {
            [[QNAPCommunicationManager share].videoStationManager downloadVideoFilm:urlString withLocalPath:nil witFilmID:filmItem.f_id withDelegate:[QNVideoTool share]];
        }
    }];
    return YES;
}

+ (NSString *)generateGetVideoURL:(NSString *)f_id isDownload:(BOOL)isDownload{
    id currentResolution = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentResolution"];
    if ([currentResolution isKindOfClass:[NSDictionary class]]) {
        NSDictionary *infoDic = (NSDictionary *)currentResolution;
        NSString *resolutionString = [infoDic valueForKeyPath:@"resolutionItem"];
        return [QNVideoTool generateVideoOnlineURL:f_id withResolution:resolutionString];
    }else
        return [QNVideoTool generateVideoOfflineURL:f_id isDownload:isDownload];
}

+ (NSString *)generateVideoOnlineURL:(NSString *)f_id withResolution:(NSString *)resolutionString{
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    return [NSString stringWithFormat:@"%@video/api/video.php?a=%@&f=%@&s=%@&sid=%@&rt=1", communicationManager.videoStationManager.baseURL, @"display",f_id, resolutionString,communicationManager.sidForMultimedia];
}

+ (NSString *)generateVideoOfflineURL:(NSString *)f_id isDownload:(BOOL)isDownload{
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    NSString *resolutionSetting = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"resolution"];
    NSString *resolutionString = @"default";
    
    if ([resolutionSetting isEqualToString:@"240p"] || [resolutionSetting isEqualToString:@"360p"] || [resolutionSetting isEqualToString:@"480p"] || [resolutionSetting isEqualToString:@"720p"] || [resolutionSetting isEqualToString:@"1080p"]) {
        resolutionString = resolutionSetting;
    }

    QNVideoFileItem *fileItem = [QNVideoFileItem MR_findFirstByAttribute:@"f_id" withValue:f_id];
    
    BOOL hasTargetResolution = NO;
    if ([resolutionString isEqualToString:@"240p"]) {
        hasTargetResolution = [[fileItem v240P] boolValue];
    }else if ([resolutionString isEqualToString:@"360p"]){
        hasTargetResolution = [[fileItem v360P] boolValue];
    }else if ([resolutionString isEqualToString:@"480p"]){
        hasTargetResolution = [[fileItem v480P] boolValue];
    }else if ([resolutionString isEqualToString:@"720p"]){
        hasTargetResolution = [[fileItem v720P] boolValue];
    }else if ([resolutionString isEqualToString:@"1080p"]){
        hasTargetResolution = [[fileItem v1080P] boolValue];
    }
    resolutionString = (hasTargetResolution)?resolutionString:@"default";
    
    return [NSString stringWithFormat:@"%@video/api/video.php?a=%@&f=%@&vt=%@&sid=%@", communicationManager.videoStationManager.baseURL, (isDownload)?@"download":@"display",f_id, resolutionString,communicationManager.sidForMultimedia];
}

- (void)feedBack:(UIViewController *)targetViewController{
    MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc]init];
    mailViewController.mailComposeDelegate = self;
    
    [mailViewController setSubject:[NSString stringWithFormat:@"Qvideo issue report[%@]", [self emailFeedbackTitle]]];
    [mailViewController setMessageBody:[self emailFeedbackContent] isHTML:NO];
    [mailViewController setToRecipients:@[@"mobile@qnap.com"]];
    [targetViewController presentViewController:mailViewController animated:YES completion:nil];
}

- (NSString *)emailFeedbackTitle{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd hh:mm:ss"];
    NSDate *date = [NSDate date];
    return [formatter stringFromDate:date];
}

- (NSString *)emailFeedbackContent{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSMutableString *content = [NSMutableString stringWithFormat:@"[Qvideo Version:%@] \n", version];
    [content appendFormat:@"[Device Information: %@] \n", [QNVideoTool deviceModelName]];
    [content appendFormat:@"[iOS Version: %@] \n", [[UIDevice currentDevice] systemVersion]];
    return  content;
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    //    MFMailComposeResultSent,
    //MFMailComposeResultFailed

    NSString *messageString = nil;
    BOOL isSent = NO;
    switch (result) {
        case MFMailComposeResultSent:{
            isSent = YES;
        }
            break;
        default:
            break;
    }
    [QNVideoTool toastWithTargetViewController:controller.navigationController
                                      withText:messageString
                                     isSuccess:isSent];
    [controller dismissViewControllerAnimated:YES completion:nil];
}


- (void)popToNASListViewController:(UIViewController *)targetViewController{
    for (UIViewController *sampleViewController in targetViewController.navigationController.viewControllers) {
        if ([sampleViewController isKindOfClass:[QNNASListViewController class]]) {
            [targetViewController.navigationController popToViewController:sampleViewController animated:YES];
            break;
        }
    }
}

+ (NSString *)textFromTextFileNamed:(NSString *)filename{
    NSString *name = [filename stringByDeletingPathExtension];
    NSString *extension = [filename pathExtension];
    
    return [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:name ofType:extension] encoding:NSUTF8StringEncoding error:nil];
}

- (void)loadingStartAnimation:(UINavigationBar *)targetBar{
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [_activityIndicator setFrame:CGRectMake(50, 5, _activityIndicator.frame.size.width, _activityIndicator.frame.size.height)];
        [targetBar addSubview:_activityIndicator];
    [_activityIndicator startAnimating];
}

- (void)loadingStopAnimation{
    [_activityIndicator stopAnimating];
}

+ (NSString*)deviceModelName {
    
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString *modelName = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if([modelName isEqualToString:@"i386"]) {
        modelName = @"iPhone Simulator";
    }
    else if([modelName isEqualToString:@"iPhone1,1"]) {
        modelName = @"iPhone";
    }
    else if([modelName isEqualToString:@"iPhone1,2"]) {
        modelName = @"iPhone 3G";
    }
    else if([modelName isEqualToString:@"iPhone2,1"]) {
        modelName = @"iPhone 3GS";
    }
    else if([modelName isEqualToString:@"iPhone3,1"]) {
        modelName = @"iPhone 4";
    }
    else if([modelName isEqualToString:@"iPhone4,1"]) {
        modelName = @"iPhone 4S";
    }
    else if([modelName isEqualToString:@"iPod1,1"]) {
        modelName = @"iPod 1st Gen";
    }
    else if([modelName isEqualToString:@"iPod2,1"]) {
        modelName = @"iPod 2nd Gen";
    }
    else if([modelName isEqualToString:@"iPod3,1"]) {
        modelName = @"iPod 3rd Gen";
    }
    else if([modelName isEqualToString:@"iPad1,1"]) {
        modelName = @"iPad";
    }
    else if([modelName isEqualToString:@"iPad2,1"]) {
        modelName = @"iPad 2(WiFi)";
    }
    else if([modelName isEqualToString:@"iPad2,2"]) {
        modelName = @"iPad 2(GSM)";
    }
    else if([modelName isEqualToString:@"iPad2,3"]) {
        modelName = @"iPad 2(CDMA)";
    }
    
    return modelName;
}

- (void)showLoginDialog:(NASServer *)targetNAS withResultBlock:(void(^)(BOOL success, NSError *e))loginResult{
    NSString *bundleName = @"ConfirmLoginView";
    ASViewController *loginViewController =[[[NSBundle mainBundle] loadNibNamed:bundleName
                                                                          owner:self
                                                                        options:nil] objectAtIndex:0];
    
    loginViewController.loginResultBlock = loginResult;
    [loginViewController.userNameField setText:targetNAS.account];
    [loginViewController.userNameField setEnabled:NO];
    loginViewController.targetNASServer = targetNAS;
    [loginViewController settingCheckBoxIconImage];
    
    [CXAlertView showContentViewAlert:NSLocalizedString(@"PwdConfirm", nil)
                      withActionTitle:NSLocalizedString(@"OK", nil)
                      withContentView:loginViewController
                    withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                        ASViewController *contentView = loginViewController;
                        QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
                        [videoStation authLoginViaVideoStation:loginViewController.userNameField.text
                                                  withPassword:loginViewController.passwordField.text
                                              withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                                  [alert dismiss];
                                                  
                                                  [loginViewController.errorLabel setText:@""];
                                                  loginResult(YES, nil);
                                              }
                                              withFailureBlock:^(RKObjectRequestOperation *o, NSError *error){
                                                  [contentView.errorLabel setText:error.domain];
                                                  loginResult(NO, error);
                                              }];
                    }];
}


- (void)connectToTUTK:(NSString *)deviceName completionBlock:(void(^)(BOOL success, NSError *error, NSDictionary *tutkDicInfo))completion{
    //需要device id, port
    __block NSString *deviceId = nil;
    __block NSInteger port = -1;
    
    QNMyCloudManager *myCloudManager = [QNAPCommunicationManager share].myCloudManager;
    void(^fetchCloudLink)(NSString *deviceId) = ^(NSString *deviceId){
        [myCloudManager getCloudLinkWithDeviceId:deviceId
                                withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, MyCloudCloudLinkResponse *obj){
                                    NASServer *server = [QNVideoTool share].nasServerInfo;
                                    NSDictionary *dic = [[QFTUTKTunnel sharedManager] openTUTKTunnel:obj.cloud_link_id port:port account:server.account password:server.password];
                                    //TODO, change the url as 127.0.0.1:mappingport
                                    completion(YES, nil, dic);
                                }
                                withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                    completion(NO, e, nil);
                                }];
    };
    void(^fetchDeviceId)(NSString *deviceName) = ^(NSString *deviceName){
        [myCloudManager getDeviceByName:deviceName
                       withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *result){
                           MyCloudDeviceResponse *response = (MyCloudDeviceResponse *)result.firstObject;
                           NSArray *allDevices = [response.relationship_result allObjects];
                           
                           for (MyCloudDevice *device in allDevices) {
                               if ([device.device_name isEqualToString:deviceName]) {
                                   deviceId = device.device_id;
                                   NSArray *allService = [[device relationship_localService] allObjects];
                                   NSArray *targets =[allService filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.service_name == 'WebAdministration'"]];
                                   if ([targets count] > 0) {
                                       MyCloudDeviceLocalService *service = [targets objectAtIndex:0];
                                       DDLogVerbose(@"service port %i", [service.external_port intValue]);
                                       port = [service.external_port intValue];
                                       fetchCloudLink(deviceId);
                                   }
                                   break;
                               }
                           }
                       }
                       withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
                           DDLogError(@"error %@", error.domain);
                           completion(NO, error, nil);
                       }];
    };
    [myCloudManager fetchOAuthToken:^(AFOAuthCredential *credentail){
        fetchDeviceId(deviceName);
    } withFailureBlock:^(NSError *error){
        completion(NO, error, nil);
    }];

//    [myCloudManager fetchOAuthToken:MyCloud_ACCOUNT withPassword:MyCloud_PASSWORD withSuccessBlock:^(AFOAuthCredential *credentail){
//        fetchDeviceId(deviceName);
//    } withFailureBlock:^(NSError *error){
//        DDLogError(@"OAuth Error: %@", [error domain]);
//    }];
}

/**
 *  This code is passed from Eric Lin.
 *  The logic in this method is too deep to maintain it in future.
 *  Need refactory.
 *
 *  @param modelName NAS module
 *
 *  @return The mapping icon
 */
+ (NSString *)getImageFromModelName:(NSString*)modelName{
    NSString *imageName = @"ic_nas_unknown.png";
    
    if (modelName)
    {
        NSString *deviceModelName = [modelName lowercaseString];
        if ([deviceModelName hasPrefix:@"tgb"] || [deviceModelName hasPrefix:@"qgenie"])
        {
            imageName = @"ic_qgenie.png";
        }
        else
        {
            NSArray *array = [deviceModelName componentsSeparatedByString:@"-"];
            if (array && array.count > 0)
            {
                NSString *seriesString = [array objectAtIndex:0];
                if ([seriesString isEqualToString:@"is"])
                {
                    imageName = @"ic_nas_is.png";
                }
                else if ([seriesString isEqualToString:@"hs"])
                {
                    imageName = @"ic_nas_hs.png";
                }
                else if ([seriesString isEqualToString:@"ts"] || [seriesString isEqualToString:@"ss"] || [seriesString isEqualToString:@"gs"] || [seriesString isEqualToString:@"cs"])
                {
                    NSString *bayString = [array objectAtIndex:1];
                    if (bayString)
                    {
                        BOOL isU_Series = NO;
                        
                        NSRange tmpRange = [bayString rangeOfString:@"u"];
                        if (tmpRange.length)
                        {
                            isU_Series = YES;
                        }
                        
                        NSString *pureNumberString = [[bayString componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                        int pureNumber = [pureNumberString intValue];
                        int bayNumber = pureNumber/100;
                        
                        if (isU_Series)
                        {
                            if (bayNumber > 0 && bayNumber <= 4)
                            {
                                imageName = @"ic_nas_1u.png";
                            }
                            else if (bayNumber > 4 && bayNumber <= 8)
                            {
                                imageName = @"ic_nas_2u.png";
                            }
                            else if (bayNumber > 8 && bayNumber <= 12)
                            {
                                imageName = @"ic_nas_3u.png";
                            }
                            else if (bayNumber > 12)
                            {
                                imageName = @"ic_nas_4u.png";
                            }
                        }
                        else
                        {
                            if (bayNumber == 1)
                            {
                                imageName = @"ic_nas_1.png";
                            }
                            else if (bayNumber == 2)
                            {
                                imageName = @"ic_nas_2.png";
                            }
                            else if (bayNumber == 4)
                            {
                                imageName = @"ic_nas_4.png";
                            }
                            else if (bayNumber == 5)
                            {
                                imageName = @"ic_nas_5.png";
                            }
                            else if (bayNumber == 6)
                            {
                                imageName = @"ic_nas_6.png";
                            }
                            else if (bayNumber == 8)
                            {
                                imageName = @"ic_nas_8.png";
                            }
                            else if (bayNumber >= 10)
                            {
                                imageName = @"ic_nas_10.png";
                            }
                        }
                    }
                }
                else if ([seriesString isEqualToString:@"vs"])
                {
                    NSString *bayString = [array objectAtIndex:1];
                    if (bayString)
                    {
                        BOOL isU_Series = NO;
                        
                        NSRange tmpRange = [bayString rangeOfString:@"u"];
                        if (tmpRange.length)
                            
                        {
                            isU_Series = YES;
                        }
                        
                        NSString *pureNumberString = [[bayString componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
                        int pureNumber = [pureNumberString intValue];
                        int bayNumber = pureNumber/1000;
                        
                        if (isU_Series)
                        {
                            if (bayNumber > 0 && bayNumber <= 4)
                            {
                                imageName = @"ic_nas_1u.png";
                            }
                            else if (bayNumber > 4 && bayNumber <= 8)
                            {
                                imageName = @"ic_nas_2u.png";
                            }
                            else if (bayNumber > 8 && bayNumber <= 12)
                            {
                                imageName = @"ic_nas_3u.png";
                            }
                            else if (bayNumber > 12)
                            {
                                imageName = @"ic_nas_4u.png";
                            }
                        }
                        else
                        {
                            if (bayNumber == 1)
                            {
                                imageName = @"ic_nas_1.png";
                            }
                            else if (bayNumber == 2)
                            {
                                imageName = @"ic_nas_2.png";
                            }
                            else if (bayNumber == 4)
                            {
                                imageName = @"ic_nas_4.png";
                            }
                            else if (bayNumber == 5)
                            {
                                imageName = @"ic_nas_5.png";
                            }
                            else if (bayNumber == 6)
                            {
                                imageName = @"ic_nas_6.png";
                            }
                            else if (bayNumber == 8)
                            {
                                imageName = @"ic_nas_8.png";
                            }
                            else if (bayNumber >= 10)
                            {
                                imageName = @"ic_nas_10.png";
                            }
                        }
                    }
                }
            }
        }
    }
    
    return imageName;
}

+ (NetworkStatus)currentNetworkStatus{
    Reachability *reachbility = [Reachability reachabilityForInternetConnection];
    return [reachbility currentReachabilityStatus];
}

+ (BOOL)allowDownload{
    BOOL r = NO;
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isDownloadInWifi"] boolValue] &&
        [QNVideoTool currentNetworkStatus] == ReachableViaWiFi) {
        r = YES;
    }else if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"isDownloadInWifi"] boolValue])
        r = YES;
    
    return r;
}


- (NSString *)correctURL:(NASServer *)nasServer{
    NSString *url = nasServer.currentAddress;
    if (!correctURL) {
        if ([url hasPrefix:@"http://127.0.0.1"]) {
            correctURL = [url stringByAppendingFormat:@":%@/", nasServer.mappingPort];
        }else if(url == nil || [url isEqualToString:@""]){
            correctURL = nil;
        }else{
            NSNumber *correctPort = [self correctPort:nasServer];
            NSString *newHost = [QNVideoTool getRidOf_HTTPS_HTTP:url];
            correctURL = [NSString stringWithFormat:([nasServer.isSSL boolValue])?@"https://%@:%i/":@"http://%@:%i/", newHost, [correctPort intValue]];
        }
    }
    return correctURL;
}

- (NSNumber *)correctPort:(NASServer *)nasServer{
    NSNumber *returnNumber = nil;
    NSNumber *externalPort = nil;
    NSNumber *internalPort = nil;
    NSString *host = nasServer.currentAddress;
    
    //decide externalPort/internalPort
    if ([nasServer.isSSL boolValue]) {
        externalPort = nasServer.ssl_external_port;
        internalPort = nasServer.ssl_internal_port;
    }else{
        externalPort = nasServer.normal_external_port;
        internalPort = nasServer.normal_internal_port;
    }
    
    //return externalPort if equaling with internalPort
    if (externalPort == internalPort) {
        returnNumber = externalPort;
    }else{
        //result in portPriority
        switch ([nasServer.portPriority intValue]) {
            case PortSelection_ExternalPortOnly:
                returnNumber = externalPort;
                break;
            case PortSelection_InternalPortOnly:
                returnNumber = internalPort;
                break;
            case PortSelection_InternalPortFirst:
                returnNumber = ([self isPortOpen:internalPort withHost:host])?internalPort:externalPort;
                break;
            case PortSelection_ExternalPortFirst:
                returnNumber = ([self isPortOpen:externalPort withHost:host])?externalPort:internalPort;
                break;
        }
    }
    
    [self closeSocket];
    return returnNumber;
}

- (BOOL)isPortOpen:(NSNumber *)portNumber withHost:(NSString *)host{
    canOpen = NO;
    NSString *newHost = [QNVideoTool getRidOf_HTTPS_HTTP:host];
    if (!socket) {
        socket = [[AsyncSocket alloc] initWithDelegate:self];
    }
    [socket connectToHost:newHost onPort:[portNumber intValue] error:nil];
    [QNVideoTool waitUntilTimeOut:30 withWaitCondition:^(void){
        BOOL r = (canOpen == NO);
        return r;
    }];
    return canOpen;
}

- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port{
    DDLogVerbose(@"Socket connection to %@:%i successfully!", host, port);
    canOpen = YES;
}

+ (void)waitUntilTimeOut:(NSInteger)seconds withWaitCondition:(CheckConditionBlock)condition{
    NSInteger count = 0;
    NSInteger totalCount = seconds * 10;
    while (count <= totalCount && condition()){
        NSDate* nextTry = [NSDate dateWithTimeIntervalSinceNow:0.1];
        [[NSRunLoop currentRunLoop] runUntilDate:nextTry];
        count ++;
    }
}

- (void)closeSocket{
    [socket disconnect];
    socket = nil;
}

- (void)logoutClean{
    if (socket) {
        [self closeSocket];
    }
    correctURL = nil;
}

+ (NSString *)getRidOf_HTTPS_HTTP:(NSString *)url{
    NSString *urlString = nil;
    if ([url rangeOfString:@"http://"].location != NSNotFound) {
        urlString = [url stringByReplacingOccurrencesOfString:@"http://" withString:@""];
    }else if ([url rangeOfString:@"https://"].location != NSNotFound){
        urlString = [url stringByReplacingOccurrencesOfString:@"https://" withString:@""];
    }else
        urlString = url;
    return urlString;
}
@end
