//
//  UIImage+QVideoExt.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/6.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "UIImage+QVideoExt.h"

@implementation UIImage (QVideoExt)

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
@end
