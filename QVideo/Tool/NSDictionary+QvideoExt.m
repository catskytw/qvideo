//
//  NSDictionary+QvideoExt.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/10.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "NSDictionary+QvideoExt.h"

@implementation NSDictionary (QvideoExt)
- (NSString *)searchFirstKeyFromStringValue:(NSString *)value{
    NSString *r = nil;
    
    NSArray *allKeys = [self allKeys];
    for (NSString *key in allKeys) {
        NSString *_value = [self valueForKey:key];
        if ([_value isEqualToString:value]) {
            r = key;
            break;
        }
    }
    return r;
}

@end
