//
//  UIView+QVideoExt.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/11.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "UIView+QVideoExt.h"

@implementation UIView (QVideoExt)

- (void)removeAllSubViews{
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
}

- (BOOL)hasSubViewWithTag:(NSInteger)tag{
    for (UIView *subView in self.subviews) {
        if (subView.tag == tag) {
            return YES;
        }
    }
    return NO;
}

@end
