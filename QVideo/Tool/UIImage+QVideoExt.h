//
//  UIImage+QVideoExt.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/6.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (QVideoExt)
+ (UIImage *)imageWithColor:(UIColor *)color ;

@end
