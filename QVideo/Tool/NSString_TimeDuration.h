//
//  NSString+NSString_TimeDuration.h
//  QVideo
//
//  Created by Change.Liao on 2013/11/29.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSString_TimeDuration)
+ (NSString *)stringFromTimeInterval:(NSTimeInterval)interval;
+ (NSString *)convertNumberToMonth:(NSString *)number;
- (CGSize)caculateSizeOfDuration;

@end
