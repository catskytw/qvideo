//
//  NSDictionary+QvideoExt.h
//  QVideo
//
//  Created by Change.Liao on 2014/4/10.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (QvideoExt)
- (NSString *)searchFirstKeyFromStringValue:(NSString *)value;
@end
