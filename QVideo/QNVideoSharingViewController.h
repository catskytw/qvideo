//
//  QNVideoSharingViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/3/20.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import "IQActionSheetPickerView.h"

#import "QNVideoShareCollectionViewController.h"
#import "QNButton.h"

@interface QNVideoSharingViewController : UITableViewController<UITextFieldDelegate, UITextViewDelegate, IQActionSheetPickerView, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>{
    BOOL _originalHiddenToolbar;
    QNButton *_rightBtn;
    void(^_sendShareLinkBlock)(NSString *sharelinkURL);
}
@property (nonatomic, strong) NSArray *fileIds;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UICollectionViewController *selectedCollectionViewController;
@property (nonatomic, weak) QNVideoShareCollectionViewController *collectionViewController;


@property (weak, nonatomic) IBOutlet UILabel *receiverLabel;
@property (weak, nonatomic) IBOutlet UITextField *subject;
@property (weak, nonatomic) IBOutlet UITextView *messageText;
@property (weak, nonatomic) IBOutlet UILabel *sharingLinkURLLabel;

- (IBAction)addUserAction:(id)sender;
- (NSString *)sharelinkContent:(NSString *)shareLink;
- (NSString *)sharelinkContent:(NSString *)shareLink isHTML:(BOOL)isHTML;

- (void)shareByEmail;
- (void)shareBySMS;
- (void)copyShareLinked;
@end
