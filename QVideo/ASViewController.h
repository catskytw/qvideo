//
//  ASViewController.h
//  ASTextViewDemo
//
//  Created by Adil Soomro on 4/14/14.
//  Copyright (c) 2014 Adil Soomro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNButton.h"
#import "NASServer.h"

@interface ASViewController : UIView<UITextFieldDelegate>{
    BOOL _isShowLoginNextTime;
}

//fields
@property (weak, nonatomic) IBOutlet UILabel *contentMessage;
@property (strong, nonatomic) NASServer *targetNASServer;
@property (weak, nonatomic) IBOutlet UIImageView *checkBoxIcon;
@property (weak, nonatomic) IBOutlet QNButton *forbiddenBtn;
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) void(^loginResultBlock)(BOOL success, NSError *error);
- (IBAction)showThisAlertOrNot:(id)sender;
- (void)settingCheckBoxIconImage;
@end
