//
//  QNAboutQNAPViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/28.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNAboutQNAPViewController.h"

@interface QNAboutQNAPViewController ()

@end

@implementation QNAboutQNAPViewController

- (void)settingCoreText{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSString *textName = [NSString stringWithFormat:@"AboutQNAP_%@.txt", language];
    self.coreTextView.text = [[self class] textFromTextFileNamed:textName];

}
@end
