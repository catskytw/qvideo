//
//  QNVideoActionViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/20.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoActionViewController.h"
#import <QNAPFramework/QNAPCommunicationManager.h>
#import <QNAPFramework/VLCMovieViewController.h>
#import <TSMessages/TSMessage.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>

#import "QNVideoInfoViewController.h"
#import "QNVideoTool.h"
#import "VideoDownload.h"
#import "QVideoEnumberator.h"

@interface QNVideoActionViewController ()

@end

@implementation QNVideoActionViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch ([indexPath row]) {
        case 0:{ //play as streaming
            NSString *urlString = [self videoPlayingURL:self.videoItem.f_id isDownload:NO];
            VLCMovieViewController *movieController = [[VLCMovieViewController alloc] initWithNibName:nil bundle:nil];
            movieController.url = [NSURL URLWithString:urlString];
            [self.parentViewController.navigationController pushViewController:movieController animated:YES];
        }
            break;
        case 1: //play with other app
            
            break;
        case 2:{
            [QNVideoTool downloadFile:self.videoItem];
            [QNVideoTool toastWithTargetViewController:self
                                              withText:NSLocalizedString(@"addDownloadQueue", nil)
                                             isSuccess:YES];
        }
            break;
        default:
            break;
    }
}

- (NSString *)videoPlayingURL:(NSString *)fID isDownload:(BOOL)isDownload{
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    return [NSString stringWithFormat:@"%@video/api/video.php?a=%@&f=%@&vt=default&sid=%@", communicationManager.videoStationManager.baseURL, (isDownload)?@"download":@"display",fID, communicationManager.sidForMultimedia];
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"videoComment"]) {
        QNVideoInfoViewController *infoViewController = (QNVideoInfoViewController *)segue.destinationViewController;
        UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:infoViewController action:@selector(rightBarItemAction:)];
        [self.navigationController.navigationItem setRightBarButtonItem:barItem];
        infoViewController.videoFileItem = sender; //The sender here is videoFileitem(CoreData)
    }
}

@end
