//
//  QNIntroPageViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/5/2.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FTCoreText/FTCoreTextView.h>

@interface QNIntroPageViewController : UIViewController<FTCoreTextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *introImageView;
@property (strong, nonatomic) NSString *contentFileName;
@property (strong, nonatomic) NSString *contentImageFileName;
@property (weak, nonatomic) IBOutlet FTCoreTextView *coreTextView;
@property (weak, nonatomic) IBOutlet UIImageView *tutorialImage;
@end
