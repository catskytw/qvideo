//
//  QNVideoSettingViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/11/25.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NASServer.h"
#import "QNVideosViewController.h"
#import <EDStarRating/EDStarRating.h>
#import "InlineDatePickerTableViewController.h"

@interface QNVideoSettingViewController : UITableViewController<UISearchBarDelegate, EDStarRatingProtocol>{
    BOOL _firstLoad;
    QNVideoFilter _currentFilter;
    CXAlertView *_tmpAlert;
}

@property (nonatomic) NSInteger previousSelection;
@property (nonatomic, strong) NASServer *thisServer;

@property (weak, nonatomic) IBOutlet UILabel *sharedVideoLabel;
@property (weak, nonatomic) IBOutlet UILabel *privateCollectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *qSyncLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoCollectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *smartCollectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *trashCanLabel;
@property (weak, nonatomic) IBOutlet UILabel *settingLabel;
@property (weak, nonatomic) IBOutlet UILabel *localVideoFilesLabel;
@property (weak, nonatomic) IBOutlet UILabel *logoutLabel;
@property (weak, nonatomic) IBOutlet UIButton *filterBtn;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

- (IBAction)changeFilterFactor:(id)sender;

@end
