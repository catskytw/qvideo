//
//  QNVideoItemDetailViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/11/28.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoItemDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <QNAPFramework/QNAPCommunicationManager.h>
#import <QNAPFramework/VLCMovieViewController.h>
#import "QNAppDelegate.h"
#import "QNDownloadListViewController.h"
#import "NSString_TimeDuration.h"
#import "QNVideoActionViewController.h"
#import "QNVideoTool.h"
#import "QNVideoInfoViewController.h"
#import "QNVideoShowInfoViewController.h"
#import "QNMoviePlayerViewController.h"
#import "VideoDownload.h"
#import <QNAPFramework/QNCollection.h>
#import "QNVideoSharingViewController.h"

@interface QNVideoItemDetailViewController ()
@end

@implementation QNVideoItemDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];

    _resolutions = @[@"240p", @"360p", @"480p", @"720p", @"1080p"];
    //@"aceplayer", @"gplayer", @"oplayerlite", @"oplayer"
    _appNameMapping = @{@"aceplayer": @"Ace Player",
                        @"gplayer": @"GPlayer",
                        @"oplayerlite": @"OPlayer Lite",
                        @"oplayer": @"OPlayer"};
    _selectedResolutions = [NSMutableArray array];
    
    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    [videoStation lazyloadingImage:self.screenShot
                   withPlaceHolder:nil
                        withFileId:self.videoItem.f_id
                 withProgressBlock:nil
                withCompletedBlock:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];

    [self settingActionMenu];
    [self.navigationItem setTitle:NSLocalizedString(@"VideoInformation", nil)];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DataSetting
- (void)settingActionMenu{
    [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(backNav:) withTitleText:NSLocalizedString(@"Back", nil)];
    [QNVideoTool generateCustomRightBarItem:self withSelect:@selector(showActionItems:) withTitleText:NSLocalizedString(@"Action", nil)];
    
    REMenuItem *playWithOther = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"PlayWithOtherApp", nil)
                                                           image:nil
                                                highlightedImage:nil
                                                          action:^(REMenuItem *item){
                                                              [self playWithOtherApp:item];
                                                          }];
    REMenuItem *downloadItem = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"Download", nil)
                                                           image:nil
                                                highlightedImage:nil
                                                          action:^(REMenuItem *item){
                                                              [self downloadProcess];
                                                          }];
    REMenuItem *copyToAlbum = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"CopyToAlbum", nil)
                                                           image:nil
                                                highlightedImage:nil
                                                          action:^(REMenuItem *item){
                                                              [self addToAlbumAction];
                                                          }];
    
    REMenuItem *addToTranscode = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"AddToTransCode", nil)
                                                           image:nil
                                                highlightedImage:nil
                                                          action:^(REMenuItem *item){
                                                              [self addToTransCode];
                                                          }];
    REMenuItem *share = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"Share", nil)
                                                           image:nil
                                                highlightedImage:nil
                                                   action:^(REMenuItem *item){
                                                       [self performSegueWithIdentifier:@"goToSharingViewController" sender:nil];
                                                   }];

    REMenuItem *deleteItem = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"Delete", nil)
                                                           image:nil
                                                highlightedImage:nil
                                                          action:^(REMenuItem *item){
                                                              [self deleteThisFileIntoTrashcan:self.videoItem.f_id];
                                                          }];
    self.menu = [[REMenu alloc] initWithItems:@[playWithOther, downloadItem, copyToAlbum, addToTranscode, share, deleteItem]];
    [self settingActionMenuLayout:self.menu];
}

- (void)playWithOtherApp:(REMenuItem *)actionMenu{
    NSString *urlString = [[QNVideoTool share] videoPlayingURL:self.videoItem.f_id isDownload:NO];
    NSArray *openApps = [_appNameMapping allKeys];
    NSMutableArray *existApps = [NSMutableArray array];
    
    for (NSString *appName in openApps) {
        NSString *openString = [NSString stringWithFormat:([appName isEqualToString:@"gplayer"])?@"%@:%@":@"%@://%@", appName, urlString];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:openString]])
            [existApps addObject:appName];
    }
    
    if ([existApps count] == 0) { //no other app to play
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:nil
                          withMessage:NSLocalizedString(@"NoOtherPlayer", nil)
                    withActionHandler:nil];
    }else{
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"PlayWithOtherApp", nil)
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:nil];
        for (NSString *appName in existApps) {
            NSString *appShowName = [_appNameMapping valueForKey:appName];
            [actionSheet addButtonWithTitle:appShowName];
        }
        [actionSheet showInView:self.view];
    }
    
}

#pragma mark - UIActionSheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *appShowName = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([appShowName isEqualToString:NSLocalizedString(@"Cancel", nil)])
        return;
    NSString *appName = [_appNameMapping searchFirstKeyFromStringValue:appShowName];
    NSString *urlString = [[QNVideoTool share] videoPlayingURL:self.videoItem.f_id isDownload:NO];

    NSString *openString = [NSString stringWithFormat:([appName isEqualToString:@"gplayer"])?@"%@:%@":@"%@://%@", appName, urlString];
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:openString]]) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Error", nil)
                      withActionTitle:nil
                          withMessage:[NSString stringWithFormat:NSLocalizedString(@"OpenWithOtherAppError", nil), appShowName]
                    withActionHandler:^(CXAlertView *v, CXAlertButtonItem *btn){
                        [v dismiss];
                        DDLogVerbose(@".....");
                    }];
    }
}


- (void)settingActionMenuLayout:(REMenu *)actionMenu{
    actionMenu.backgroundColor = [UIColor colorWithRed:_backgrounColorValue green:_backgrounColorValue blue:_backgrounColorValue alpha:0.95];
    actionMenu.itemHeight = 44.0f;
    actionMenu.textColor = [UIColor whiteColor];
    actionMenu.font = [UIFont systemFontOfSize:18.0f];
    actionMenu.separatorHeight = 0.0f;
    actionMenu.separatorColor = [UIColor clearColor];
    actionMenu.highlightedBackgroundColor = [UIColor colorWithRed:_highlightColorValue green:_highlightColorValue blue:_highlightColorValue alpha:1.0f];
    actionMenu.textAlignment = NSTextAlignmentLeft;
    actionMenu.textOffset = CGSizeMake(18, 0);
    actionMenu.borderWidth = 0.0f;
}
#pragma mark - Action
- (IBAction)backNav:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)showActionItems:(id)sender{
    CGFloat width = 180.0f;
    if (self.menu.isOpen) {
        [self.menu close];
    }else
        [self.menu showFromRect:CGRectMake(self.view.frame.size.width - width, 0, width, self.view.frame.size.height) inView:self.view];
}

- (void)downloadProcess{
    //TODO reach the upper bound of downloading space limitation?
    CGFloat oneGBValue = 1073741824.0f;
    CGFloat base = 0.0f;
    /**
           [[NSUserDefaults standardUserDefaults] setValue:[_blockDataArray objectAtIndex:selectedIndex] forKey:@"folderSize"];
     */
    NSInteger option = [[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"folderSize"] intValue];
    switch (option) {
        case VideoDownload1GB:
            base = 1.0f;
            break;
        case VideoDownload2GB:
            base = 2.0f;
            break;
        case VideoDownload5GB:
            base = 5.0f;
            break;
        case VideoDownloadUnlimit:
        default:
            base = 1000.0f;
            break;
    }
    double limitSize = base * oneGBValue;
    if ([self.videoItem.iFileSize longLongValue] > limitSize) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:nil
                      withCancelTitle:NSLocalizedString(@"Cancel", nil)
                          withMessage:NSLocalizedString(@"OverSize", nil)
                    withActionHandler:nil];
        
    }else if ([self.videoItem.iFileSize longLongValue] >= ( 2 * oneGBValue)){ //2GB
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:NSLocalizedString(@"DownloadTerm", nil)
                          withMessage:[NSString stringWithFormat:NSLocalizedString(@"downloadLimit", nil), [QNVideoTool transformedValue:self.videoItem.iFileSize]]
                    withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                        [self downloadVideo];
                        [alert dismiss];
                    }];
        
    }else
        [self downloadVideo];
}

- (void)downloadVideo{
    BOOL r = [QNVideoTool downloadFile:self.videoItem];

    if (r) {
        QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
        QNDownloadListViewController *downloadViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"downloadListViewController"];
        if (self.navigationController) {
            [self.navigationController pushViewController:downloadViewController animated:YES];
        }
        
        [QNVideoTool toastWithTargetViewController:self.navigationController
                                          withText:NSLocalizedString(@"addDownloadQueue", nil)
                                         isSuccess:YES];
    }
}

- (void)deleteThisFileIntoTrashcan:(NSString *)filmID{
    [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                  withActionTitle:NSLocalizedString(@"Delete", nil) withCancelTitle:NSLocalizedString(@"Cancel", nil) withMessage:NSLocalizedString(@"deleteFileWarning", nil) withActionHandler:^(CXAlertView *alertView, CXAlertButtonItem *item){
                      QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
                      [videoStation deleteFileToTrashCan:videoFileHomePathPublic
                                               withFiles:@[filmID]
                                        withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                            [QNVideoTool toastWithTargetViewController:self.navigationController
                                                                              withText:NSLocalizedString(@"DeleteSuccess", nil)
                                                                             isSuccess:YES];
                                            UINavigationController *targetNavigationController = self.navigationController;
                                            [self.navigationController popViewControllerAnimated:YES];
                                            
                                            UIViewController *previousViewController = [targetNavigationController topViewController];
                                            if ([previousViewController respondsToSelector:@selector(sendRequestAndReload)]) {
                                                [previousViewController performSelector:@selector(sendRequestAndReload) withObject:nil afterDelay:0];
                                            }
                                        }
                                        withFailureBlock:^(RKObjectRequestOperation *o, NSError *error){
                                            [QNVideoTool toastWithTargetViewController:self.navigationController
                                                                              withText:NSLocalizedString(@"DeleteFailure", nil)
                                                                             isSuccess:NO];
                                        }];
                      
                      [alertView dismiss];
                  }];
}

- (void)addToAlbumAction{
    _collections = [QNCollection MR_findAll];
    if ([_collections count] == 0) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil) withActionTitle:nil withMessage:NSLocalizedString(@"NoCollection", nil) withActionHandler:nil];
    }else{
        UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
        picker.delegate = self;
        picker.dataSource = self;
        
        [CXAlertView showContentViewAlert:NSLocalizedString(@"ChooseAlbum", nil) withActionTitle:NSLocalizedString(@"Done", nil) withContentView:picker withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
            QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
            UIPickerView *_pickerView = picker;
            NSInteger selectedIndex = [_pickerView selectedRowInComponent:0];
            QNCollection *targetCollection = [_collections objectAtIndex:selectedIndex];
            
            [videoStation addToCollection:targetCollection.iVideoAlbumId
                              withFileIDS:@[self.videoItem.f_id]
                         withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                             [QNVideoTool toastWithTargetViewController:self.navigationController
                                                               withText:[NSString stringWithFormat:NSLocalizedString(@"AddAlbumSuccess", nil), targetCollection.cAlbumTitle]
                                                              isSuccess:YES];
                         }
                         withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                             [QNVideoTool toastWithTargetViewController:self.navigationController
                                                               withText:[NSString stringWithFormat:NSLocalizedString(@"AddAlbumError", nil), targetCollection.cAlbumTitle]
                                                              isSuccess:NO];
                         }];
            [alert dismiss];
        }];
    }
}

- (void)addToTransCode{
    UITableView *transCodeView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 200, 80) style:UITableViewStylePlain];
    transCodeView.delegate = self;
    transCodeView.dataSource = self;
    [transCodeView setBackgroundColor:[UIColor clearColor]];
    
    [CXAlertView showContentViewAlert:NSLocalizedString(@"SelectResolutions", nil) withActionTitle:NSLocalizedString(@"Done", nil) withContentView:transCodeView withActionHandler:^(CXAlertView *alertView, CXAlertButtonItem *item){
        if ([_selectedResolutions count] == 0) {
            [QNVideoTool toastWithTargetViewController:self.navigationController
                                              withText:NSLocalizedString(@"needResolutions", nil)
                                             isSuccess:YES];
            return;
        }
        QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
        [videoStation videoTransCode:videoDegree360
                     withResolutions:_selectedResolutions
                          withFileID:self.videoItem.f_id
                    withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                        [QNVideoTool toastWithTargetViewController:self.navigationController
                                                          withText:NSLocalizedString(@"addTransCodeSuccess", nil)
                                                         isSuccess:YES];
                    }
                    withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                        [QNVideoTool toastWithTargetViewController:self.navigationController
                                                          withText:NSLocalizedString(@"addTransCodeFailure", nil)
                                                         isSuccess:NO];
                    }];
        [alertView dismiss];
    }];    
}

- (void)playWithOtherApp{
    //TODO AcePlayer, GPlayer, Oplayer Lite, Oplayer
}

- (void)playVideo:(id)sender{
    QNVideoFileItem *videoItem = self.videoItem;
    
    if ([videoItem.iWidth intValue] > 720 || [videoItem.iHeight intValue] > 720) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:NSLocalizedString(@"Continue", nil)
                          withMessage:NSLocalizedString(@"HDVidePlayWarning", nil)
                    withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                        [self performSegueWithIdentifier:@"playMovie" sender:sender];
                        [alert dismiss];
                    }];
    }else
        [self performSegueWithIdentifier:@"playMovie" sender:sender];
}

#pragma mark - UIPickerDataSource & Delegate
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_collections count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    QNCollection *targetCollection = (QNCollection *)[_collections objectAtIndex: row];
    return targetCollection.cAlbumTitle;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"showInfo"]) {
        UINavigationController *navi = (UINavigationController *)segue.destinationViewController;
        QNVideoShowInfoViewController *destinationViewController = (QNVideoShowInfoViewController *)[navi.viewControllers objectAtIndex:0];
        destinationViewController.videoItem = self.videoItem;
        destinationViewController.outsideDetailViewController = self;
    }else if ([segue.identifier isEqualToString:@"playMovie"]){ //playMovie
        QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
        QNVideoFileItem *item = self.videoItem;
        NSString *urlString = [NSString stringWithFormat:@"%@video/api/video.php?a=display&f=%@&vt=default&sid=%@", communicationManager.videoStationManager.baseURL, item.f_id, communicationManager.sidForMultimedia];
        QNMoviePlayerViewController *movieController = (QNMoviePlayerViewController *)segue.destinationViewController;
        movieController.parentPaneViewController = self.parentVideoViewController;

        movieController.thisVideoFileItem = item;
        movieController.url = [NSURL URLWithString:urlString];
    }else if ([segue.identifier isEqualToString:@"goToSharingViewController"]){
        QNVideoSharingViewController *sharingViewController = (QNVideoSharingViewController *)segue.destinationViewController;
        sharingViewController.fileIds = @[self.videoItem.f_id];
    }
}

#pragma mark - UITableViewDelegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_resolutions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"resolutionCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"resolutionCell"];
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    NSString *contentString = [_resolutions objectAtIndex:[indexPath row]];
    [cell.textLabel setText:contentString];
    cell.accessoryType = ([self isContainThisResolution:contentString])?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *selectedString = [_resolutions objectAtIndex:[indexPath row]];
    if ([self isContainThisResolution: selectedString]) {
        [_selectedResolutions removeObject:selectedString];
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    }else{
        [_selectedResolutions addObject:selectedString];
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    }
    return indexPath;
}


#pragma mark - PrivateMethod
- (BOOL)isContainThisResolution:(NSString *)resolutionCompareString{
    BOOL r = NO;
    for (NSString *resolution in _selectedResolutions) {
        if ([resolution isEqualToString:resolutionCompareString]) {
            r = YES;
            break;
        }
    }
    return r;
}

- (void)playVideoItem:(id)sender{
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    DDLogVerbose(@"sid %@", communicationManager.sidForMultimedia);
    QNVideoFileItem *item = (QNVideoFileItem *)((QNButton *)sender).userInfo;
    //    NSString *urlString = [NSString stringWithFormat:@"%@video/api/video.php?a=display&f=%@&vt=default&sid=%@", communicationManager.videoStationManager.baseURL, item.f_id, communicationManager.sidForMultimedia];
    NSString *urlString = [QNVideoTool generateGetVideoURL:item.f_id isDownload:NO];
    QNMoviePlayerViewController *movieController = [[QNMoviePlayerViewController alloc] initWithNibName:nil bundle:nil];
    movieController.url = [NSURL URLWithString:urlString];
    movieController.thisVideoFileItem = item;
    
    [self.navigationController pushViewController:movieController animated:YES];
}

@end
