//
//  QNVideoShareCell.h
//  QVideo
//
//  Created by Change.Liao on 2014/3/20.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNButton.h"

@interface QNVideoShareCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *snapShot;
@property (weak, nonatomic) IBOutlet UILabel *filmTitle;
@property (weak, nonatomic) IBOutlet QNButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIImageView *hdImageIndicator;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

@end
