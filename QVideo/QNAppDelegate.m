//
//  QNAppDelegate.m
//  QVideo
//
//  Created by Change.Liao on 2013/11/19.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNAppDelegate.h"
#import <QNAPFramework/UIDevice+SpeedCategory.h>
#import <QNAPFramework/MLMediaLibrary.h>
#import <QNAPFramework/QNAPCommunicationManager.h>
#import <QNAPFramework/VLCMediaFileDiscoverer.h>
#import <IQKeyboardManager/IQKeyboardManager.h>

#import "QNVideoTool.h"

@implementation QNAppDelegate

+ (void)initialize
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSNumber *skipLoopFilterDefaultValue;
    int deviceSpeedCategory = [[UIDevice currentDevice] speedCategory];
    if (deviceSpeedCategory < 3)
        skipLoopFilterDefaultValue = kVLCSettingSkipLoopFilterNonKey;
    else
        skipLoopFilterDefaultValue = kVLCSettingSkipLoopFilterNonRef;
    
    NSDictionary *appDefaults = @{kVLCSettingPasscodeKey : @"", kVLCSettingPasscodeOnKey : @(NO), kVLCSettingContinueAudioInBackgroundKey : @(YES), kVLCSettingStretchAudio : @(NO), kVLCSettingTextEncoding : kVLCSettingTextEncodingDefaultValue, kVLCSettingSkipLoopFilter : skipLoopFilterDefaultValue, kVLCSettingSubtitlesFont : kVLCSettingSubtitlesFontDefaultValue, kVLCSettingSubtitlesFontColor : kVLCSettingSubtitlesFontColorDefaultValue, kVLCSettingSubtitlesFontSize : kVLCSettingSubtitlesFontSizeDefaultValue, kVLCSettingDeinterlace : kVLCSettingDeinterlaceDefaultValue};
    
    [defaults registerDefaults:appDefaults];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
    /**
     Note: We use the defaultSessionConfiguration here, the purpose of this idea is NOT to increase the usage of consuming the quanitity
     of electric charge on the device, especially the video films are bigger than hundreds of MBs almost.
     Indeed, the downloading in the background session is fancy and required in some cases; 
     The developer should modify the relation codes here if the spec is changed about the video downloading.
     */
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:[QNVideoTool share] delegateQueue:[QNAPCommunicationManager share].downloadQueue];
    [[QNAPCommunicationManager share] settingMisc:nil withDownloadSession:session];
    [[MLMediaLibrary sharedMediaLibrary] applicationWillStart];
    [self setUpScreenConnectionNotificationHandlers];
    self.isFinishSaving = NO;
    
    VLCMediaFileDiscoverer *discoverer = [VLCMediaFileDiscoverer sharedInstance];
    [discoverer addObserver:self];
    [discoverer startDiscovering:[self directoryPath]];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    
    [[NSUserDefaults standardUserDefaults] setValue:@(0) forKeyPath:@"currentResolution"];
    [QNVideoTool share];
    
    //setting SDWebImageKey
    [[SDWebImageManager sharedManager] setCacheKeyFilter:^(NSURL *url){
         NSString *urlString = [url absoluteString];
         NSRange range = [urlString rangeOfString:@"&sid="];
         return [urlString substringWithRange:NSMakeRange(0, range.location)];
     }];
    
    [SDWebImageManager sharedManager].imageDownloader.maxConcurrentDownloads = 1;
    [SDWebImageManager sharedManager].imageDownloader.executionOrder = SDWebImageDownloaderLIFOExecutionOrder;

    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[MLMediaLibrary sharedMediaLibrary] applicationWillExit];
    
        // Do the work associated with the task.
    [[QNVideoTool share] pauseAllDownloadTaskBySystem:[NSManagedObjectContext MR_defaultContext]];
    while (!self.isFinishSaving) {
        NSDate* nextTry = [NSDate dateWithTimeIntervalSinceNow:0.2];
        [[NSRunLoop currentRunLoop] runUntilDate:nextTry];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[MLMediaLibrary sharedMediaLibrary] applicationWillStart];
    self.isFinishSaving = NO;
    if ([QNAPCommunicationManager share].sidForQTS && [QNAPCommunicationManager share].sidForMultimedia) {
        [[QNVideoTool share] controlDownloadTask:nil];
    }
}

#pragma mark - DirectoryPath
- (NSString *)directoryPath
{
    return [QNVideoTool creatingDownloadFolder];
//    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *directoryPath = searchPaths[0];
//    return directoryPath;
}

#pragma mark - MediaFileDiscovererDelegate
- (void)mediaFileAdded:(NSString *)fileName loading:(BOOL)isLoading {
    if (!isLoading) {
        MLMediaLibrary *sharedLibrary = [MLMediaLibrary sharedMediaLibrary];
        [sharedLibrary addFilePaths:@[fileName]];
        
        /* exclude media files from backup (QA1719) */
        NSURL *excludeURL = [NSURL fileURLWithPath:fileName];
        [excludeURL setResourceValue:@YES forKey:NSURLIsExcludedFromBackupKey error:nil];
        
        // TODO Should we update media db after adding new files?
        [sharedLibrary updateMediaDatabase];
        //add notification, mediaFilesChanged
        [[NSNotificationCenter defaultCenter] postNotificationName:@"mediaFilesChanged" object:nil];
    }
}

- (void)mediaFileDeleted:(NSString *)name {
    [[MLMediaLibrary sharedMediaLibrary] updateMediaDatabase];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"mediaFilesChanged" object:nil];
}

#pragma mark - MultiScreen
- (void)setUpScreenConnectionNotificationHandlers
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self selector:@selector(handleScreenDidConnectNotification:)
                   name:UIScreenDidConnectNotification object:nil];
    [center addObserver:self selector:@selector(handleScreenDidDisconnectNotification:)
                   name:UIScreenDidDisconnectNotification object:nil];
    
}

- (void)handleScreenDidConnectNotification:(NSNotification*)aNotification
{
    UIScreen *newScreen = [aNotification object];
    CGRect screenBounds = newScreen.bounds;
    
    if (!self.secondWindow)
    {
        self.secondWindow = [[UIWindow alloc] initWithFrame:screenBounds];
        self.secondWindow.screen = newScreen;
        
        // Set the initial UI for the window.
    }
    DDLogInfo(@"The airplay %@ disconnected!", self.secondWindow);
}

- (void)handleScreenDidDisconnectNotification:(NSNotification*)aNotification
{
    if (self.secondWindow)
    {
        // Hide and then delete the window.
        self.secondWindow.hidden = YES;
        self.secondWindow = nil;
        
    }
    DDLogInfo(@"The airplay %@ connected!", self.secondWindow);
    
}
@end
