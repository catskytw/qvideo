//
//  QNResolutionAlert.m
//  QVideo
//
//  Created by Change.Liao on 2014/6/19.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNResolutionAlert.h"

@implementation QNResolutionAlert

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib{
    _needAskNextTime = YES;
    
    [self.mainLabel setText:NSLocalizedString(@"resolutionMain", nil)];
    [self.subLabel setText:NSLocalizedString(@"resolutionSub", nil)];
    [self.checkImage setImage:(_needAskNextTime)? [UIImage imageNamed:@"icon_checkbox"]:[UIImage imageNamed:@"icon_checkbox_on"]];

}

- (IBAction)switchCheckAsking:(id)sender{
    _needAskNextTime = !_needAskNextTime;
    [[NSUserDefaults standardUserDefaults] setValue:@(_needAskNextTime) forKeyPath:@"needAskResolution"];
    [self.checkImage setImage:(_needAskNextTime)? [UIImage imageNamed:@"icon_checkbox"]:[UIImage imageNamed:@"icon_checkbox_on"]];
}

@end
