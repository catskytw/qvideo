//
//  QNVideoColorLabelSelectionView.m
//  QVideo
//
//  Created by Change.Liao on 2014/2/21.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoColorLabelSelectionView.h"

@interface QNVideoColorLabelSelectionView (){
    NSArray *_colorOptions;
}
@end

@implementation QNVideoColorLabelSelectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setting{
    _colorOptions = @[self.noneColor, self.yellowColor, self.redColor, self.greenColor, self.brownColor, self.purpleColor];
    self.selectedBtn = [_colorOptions objectAtIndex:self.colorOption];
    self.selectedBtn.backgroundColor = defaultBackgroundColor;
    
    for (QNButton *thisBtn in _colorOptions) {
        thisBtn.userInfo = @([_colorOptions indexOfObject:thisBtn]);
    }
}

- (IBAction)changeColor:(id)sender{
    [self.selectedBtn setBackgroundColor:[UIColor clearColor]];
    QNButton *thisBtn = (QNButton *)sender;
    [thisBtn setBackgroundColor:defaultBackgroundColor];
    self.selectedBtn = thisBtn;    
}
@end
