//
//  QNFolderCell.h
//  QVideo
//
//  Created by Change.Liao on 2014/2/10.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNFolderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *folderName;
@property (nonatomic, strong) id userInfo;
@end
