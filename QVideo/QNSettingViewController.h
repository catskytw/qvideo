//
//  QNSettingViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/31.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNVideosViewController.h"
#import <PopoverView/PopoverView.h>

@interface QNSettingViewController : UITableViewController<PopoverViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>{
    NSArray *_resolutionDataArray;
    NSArray *_folderSizeArray;
    NSMutableArray *_sections;
}
@property (nonatomic, strong) CXAlertView *showAlert;
@property (nonatomic, strong) PopoverView *targetPopoverView;
@property (nonatomic, weak) QNVideosViewController *parentPaneViewController;
@property (weak, nonatomic) IBOutlet UILabel *folderSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *usedSizeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *autoLoginSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *downloadInWifiSwitch;
@property (weak, nonatomic) IBOutlet UILabel *cacheLabel;
@property (weak, nonatomic) IBOutlet UILabel *resolutionLabel;

@property (weak, nonatomic) IBOutlet UIButton *clearCacheBtn;
@property (weak, nonatomic) IBOutlet UIButton *changeBtn;
@property (weak, nonatomic) IBOutlet UIButton *resolutionBtn;

@property (weak, nonatomic) IBOutlet UILabel *autoLoginLabel;
@property (weak, nonatomic) IBOutlet UILabel *wifiDownloadLabel;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;

@property (nonatomic) BOOL isNaviMode;

- (IBAction)clearCache:(id)sender;
- (IBAction)downloadInWifiAction:(id)sender;
- (IBAction)autoLoginAction:(id)sender;
- (IBAction)changeResolution:(id)sender;
- (IBAction)changeFolderSizeLimitation:(id)sender;
@end
