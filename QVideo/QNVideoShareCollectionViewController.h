//
//  QNVideoShareCollectionViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/3/20.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNVideoShareCollectionViewController : UICollectionViewController
@property (nonatomic, strong) NSMutableArray *selectedFilmIDs;

- (IBAction)removeFilmItem:(id)sender;
@end
