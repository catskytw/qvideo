//
//  QNLocalVideoFileListCell.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/8.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoListCell.h"
#import <MDRadialProgress/MDRadialProgressView.h>
#import "QNRadialProgressView.h"

@interface QNLocalVideoFileListCell : UITableViewCell
@property (nonatomic, strong) NSString *fID;
@property (weak, nonatomic) IBOutlet UIImageView *screenShot;
@property (weak, nonatomic) IBOutlet UILabel *fileTitle;
@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;
@property (weak, nonatomic) IBOutlet QNRadialProgressView *progressView;
@property (strong, nonatomic) id userInfo;
@property (nonatomic) CGFloat tmpCount;
@property (nonatomic) CGFloat tmpInvoke;
@property (nonatomic) NSTimeInterval lastTimeStamp;
- (void)changeDownloadProgress:(NSNotification *)nofi;
@end
