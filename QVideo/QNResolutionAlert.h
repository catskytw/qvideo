//
//  QNResolutionAlert.h
//  QVideo
//
//  Created by Change.Liao on 2014/6/19.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNButton.h"

@interface QNResolutionAlert : UIView{
    BOOL _needAskNextTime;
}
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;
@property (weak, nonatomic) IBOutlet QNButton *switchBtn;
@property (weak, nonatomic) IBOutlet UIImageView *checkImage;

- (IBAction)switchCheckAsking:(id)sender;
@end
