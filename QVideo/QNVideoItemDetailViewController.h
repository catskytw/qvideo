//
//  QNVideoItemDetailViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/11/28.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNVideoFileItem.h"
#import <MarqueeLabel/MarqueeLabel.h>
#import <EDStarRating/EDStarRating.h>
#import <REMenu/REMenu.h>

#import "QNVideosViewController.h"

@interface QNVideoItemDetailViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>{
    NSArray *_collections;
    NSArray *_resolutions;
    NSMutableArray *_selectedResolutions;
    
    NSDictionary *_appNameMapping;
}
@property (nonatomic, strong) QNVideoFileItem *videoItem;
@property (weak, nonatomic) IBOutlet UIImageView *screenShot;
@property (nonatomic, strong) REMenu *menu;
@property (nonatomic, strong) QNVideosViewController *parentVideoViewController;
@property (nonatomic, weak) id<QNVideoDataSource> dataSourceViewController;

- (IBAction)backNav:(id)sender;
- (void)settingActionMenu;

- (IBAction)playVideo:(id)sender;
@end
