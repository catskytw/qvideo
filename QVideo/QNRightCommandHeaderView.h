//
//  QNRightCommandHeaderView.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/22.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNRightCommandHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *actionSection;
@property (weak, nonatomic) IBOutlet UIButton *refreshBtn;

@end
