//
//  QNLeftSettingHeaderView.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/22.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MarqueeLabel/MarqueeLabel.h>
#import "QNButton.h"

@interface QNLeftSettingHeaderView : UIControl
@property (weak, nonatomic) IBOutlet MarqueeLabel *serverTitle;
@property (weak, nonatomic) IBOutlet QNButton *logout;
- (IBAction)logoutAction:(id)sender;

@end
