//
//  main.m
//  QVideo
//
//  Created by Change.Liao on 2013/11/19.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        int r = 0;
        @try {
            r = UIApplicationMain(argc, argv, nil, NSStringFromClass([QNAppDelegate class]));
        }
        @catch (NSException *exception) {
            NSLog(@"exception %@", exception);
        }
        @finally {
            return r;
        }
    }
}
