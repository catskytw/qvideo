//
//  QNVideoCommonSectionView.m
//  QVideo
//
//  Created by Change.Liao on 2014/3/25.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoCommonSectionView.h"

@implementation QNVideoCommonSectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

//- (id)initWithCoder:(NSCoder *)aDecoder{
//    if ((self = [super initWithCoder:aDecoder])) {
//        NSString *bundleName = @"QNVideoCommonSectionView";
//        UIView *subView =[[[NSBundle mainBundle] loadNibNamed:bundleName
//                                                        owner:self
//                                                      options:nil] objectAtIndex:0];
//        [self addSubview:subView];
//    }
//    return self;
//}
@end
