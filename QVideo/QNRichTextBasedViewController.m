//
//  QNRichTextBasedViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/28.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNRichTextBasedViewController.h"
#import "QNVideoTool.h"
#define kBasicTextSize 11.0f
@interface QNRichTextBasedViewController ()

@end

@implementation QNRichTextBasedViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#eeeeee"];
    
    CGRect bounds = self.view.bounds;
    
    //  Create scroll view containing allowing to scroll the FTCoreText view
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
	self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //  Create FTCoreTextView. Everything will be rendered within this view
    self.coreTextView = [[FTCoreTextView alloc] initWithFrame:CGRectInset(bounds, 15.0f, 0)];
	self.coreTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.coreTextView.delegate = self;
    [self.scrollView addSubview:self.coreTextView];
    [self.view addSubview:self.scrollView];
    
    [self settingTextTag];
    
    [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(backNavi:) withTitleText:NSLocalizedString(@"Back", nil)];
    [QNVideoTool generateCustomRightBarItem:self withSelect:@selector(feedBack) withTitleText:NSLocalizedString(@"Feedback", nil)];
    
    UIImage *_image = [UIImage imageNamed:@"icon_navigationbar_qvideo"];
    UIImageView *titleImage = [[UIImageView alloc] initWithImage:_image];
    [self.navigationItem setTitleView:titleImage];

}

- (void)settingCoreText{
    
}

- (void)settingTextTag{
    [self settingCoreText];
    FTCoreTextStyle *defaultStyle = [[FTCoreTextStyle alloc] init];
    defaultStyle.name = FTCoreTextTagDefault;
    defaultStyle.textAlignment = FTCoreTextAlignementLeft;
    defaultStyle.font = [UIFont systemFontOfSize:kBasicTextSize];
    defaultStyle.paragraphInset = UIEdgeInsetsMake(0.0f, 0, 0, 0);

    FTCoreTextStyle *h1Style = [defaultStyle copy];
    h1Style.name = @"h1";
    h1Style.font = [UIFont boldSystemFontOfSize:27.0f];
    h1Style.textAlignment = FTCoreTextAlignementCenter;
    h1Style.color = [UIColor colorWithHexString:@"#111111"];
    h1Style.paragraphInset = UIEdgeInsetsMake(0.0f, 0.0f, 15.0f, 0);

    FTCoreTextStyle *h2Style = [defaultStyle copy];
    h2Style.name = @"h2";
    h2Style.font = [UIFont boldSystemFontOfSize:15.0f];
    h2Style.color = [UIColor colorWithHexString:@"#232323"];
    h2Style.paragraphInset = UIEdgeInsetsMake(20.0f, 0, 0, 0);

    FTCoreTextStyle *h3Style = [defaultStyle copy];
    h3Style.name = @"h3";
    h3Style.font = [UIFont boldSystemFontOfSize:12.0f];
    h3Style.color = [UIColor colorWithHexString:@"#232323"];
    h3Style.paragraphInset = UIEdgeInsetsMake(15.0f, 0, 0, 0);
    
    FTCoreTextStyle *pStyle = [defaultStyle copy];
    pStyle.name = @"p";
    pStyle.color = [UIColor colorWithHexString:@"#555555"];

    FTCoreTextStyle *p1Style = [defaultStyle copy];
    p1Style.name = @"p1";
    p1Style.color = [UIColor colorWithHexString:@"#555555"];
    p1Style.paragraphInset = UIEdgeInsetsMake(2.0f, 0, 20.0f, 0);

    FTCoreTextStyle *p2Style = [defaultStyle copy];
    p2Style.name = @"p2";
    p2Style.textAlignment = FTCoreTextAlignementCenter;
    p2Style.font = [UIFont systemFontOfSize:13.0f];
    p2Style.color = [UIColor colorWithHexString:@"#494949"];
    p2Style.paragraphInset = UIEdgeInsetsMake(0.0f, 0, 20.0f, 0);

    //  HTML list "li" style
    //  We first get default style for "_bullet" tag, rename it to "li"
    //  and then replace the default with new tag
    FTCoreTextStyle *liStyle = [FTCoreTextStyle styleWithName:FTCoreTextTagBullet];
    liStyle.name = @"li";
    liStyle.paragraphInset = UIEdgeInsetsMake(0, 9.0f, 0, 0);
    liStyle.color = [UIColor colorWithHexString:@"#555555"];

    [self.coreTextView changeDefaultTag:FTCoreTextTagBullet toTag:@"li"];
    
    
    //  HTML image "img" style
    //  We first get default style for "_image" tag, rename it to "img"
    //  and then replace the default with new tag
    FTCoreTextStyle *imgStyle = [FTCoreTextStyle styleWithName:FTCoreTextTagImage];
    imgStyle.name = @"img";
    imgStyle.textAlignment = FTCoreTextAlignementCenter;
    
    [self.coreTextView changeDefaultTag:FTCoreTextTagImage toTag:@"img"];
    
    
    //  HTML link anchor "a"
    //  We first get default style for "_link" tag, rename it to "a"
    //  and then replace the default with new tag
    //  Mind you still need to use "_link"-like format
    //  <a>http://url.com|Dislayed text</a> format, not the html "<a href..." format
    FTCoreTextStyle *aStyle = [FTCoreTextStyle styleWithName:FTCoreTextTagLink];
    aStyle.name = @"a";
    aStyle.underlined = YES;
    aStyle.color = [UIColor blueColor];
    
    [self.coreTextView changeDefaultTag:FTCoreTextTagLink toTag:@"a"];
    
    //  Apply styles
    [self.coreTextView addStyles:@[defaultStyle, imgStyle, h1Style, h2Style, h3Style, pStyle, p1Style, p2Style,liStyle, aStyle]];
    
    //  Make self delegate so we receive links actions
    self.coreTextView.delegate = self;
    
    [self.coreTextView setBackgroundColor:[UIColor colorWithHexString:@"#eeeeee"]];
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    //  We need to recalculate fit height on every layout because
    //  when the device orientation changes, the FTCoreText's width changes
    
    //  Make the FTCoreTextView to automatically adjust it's height
    //  so it fits all its rendered text using the actual width
	[self.coreTextView fitToSuggestedHeight];
    
    //  Adjust the scroll view's content size so it can scroll all
    //  the FTCoreTextView's content
    [self.scrollView setContentSize:CGSizeMake(CGRectGetWidth(self.scrollView.bounds), CGRectGetMaxY(self.coreTextView.frame)+15.0f)];
}

- (void)backNavi:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)feedBack{
    [[QNVideoTool share] feedBack: self];
}

+ (NSString *)textFromTextFileNamed:(NSString *)filename{
    return [QNVideoTool textFromTextFileNamed:filename];
}

- (void)coreTextView:(FTCoreTextView *)coreTextView receivedTouchOnData:(NSDictionary *)data{
    NSString *urlString =(NSString *)[data valueForKey:@"url"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[urlString description]]];
}


@end
