//
//  QNVideoItemCell.h
//  QVideo
//
//  Created by Change.Liao on 2013/11/26.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIPhotoGallery/UIPhotoGalleryView.h>
@interface QNVideoItemCell : UITableViewCell

@property(nonatomic) NSInteger gridCount;
@property(nonatomic, strong) NSMutableArray *videoItems;
@property(nonatomic, strong) IBOutlet UIScrollView *videoGridView;
@end
