//
//  QNVideoCommandViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/11/25.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoCommandViewController.h"
#import "QNCommandTableViewCell.h"
#import <QNAPFramework/QNAPFramework.h>
#import <QNAPFramework/QNVideoRender.h>
#import "QNRightCommandHeaderView.h"
#import "QNButton.h"
#import "QNVideoTool.h"

@interface QNVideoCommandViewController ()

@end

@implementation QNVideoCommandViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    _tmpArray = [QNVideoRender MR_findAll];
    _selectionIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    
    self.previousSelection = [NSIndexPath indexPathForItem:0 inSection:0];
    
    //KVO
    [self.parentPanelViewController addObserver:self
                                     forKeyPath:@"homePath"
                                        options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                                        context:nil];
    
    [self addObserver:self forKeyPath:@"previousSelection" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
    
    //multiSelectSwitch
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UITableViewCell *previousCell = [self.commandTable cellForRowAtIndexPath:self.previousSelection];
    [previousCell setBackgroundColor:selectedBackgroundColor];
    [self generateTable:self.parentPanelViewController.homePath];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
/**
 static cell only be valid on UITableViewController, really suck!
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger r = 0;
    switch (section) {
        case 0:
            r = [_sectionOneCommands count];
            break;
        case 1:
            r = [_sectionTwoCommands count];
            break;
        case 2:
            r = [_sectionThreeCommands count];
            break;
        default:
            break;
    }
    return r;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"CommandCell";
    QNCommandTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSString *commandString = nil;
    QNVideoRender *render = nil;
    //clean all status
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    //decide the title in cell
    switch ([indexPath section]) {
        case 0:
            commandString = [_sectionOneCommands objectAtIndex:[indexPath row]];
            break;
        case 1:
            commandString = [_sectionTwoCommands objectAtIndex:[indexPath row]];
            break;
        case 2:{
            render = [_sectionThreeCommands objectAtIndex:[indexPath row]];
            commandString = render.deviceName;
        }
            break;
        default:
            break;
    }
    [cell.commandLabel setText:commandString];
    
    //change background
    if (![indexPath isEqual:self.previousSelection]) {
        [cell setBackgroundColor:defaultBackgroundColor];
    }
    
    //set default output device
    if (render && [QNVideoTool share].defaultRender && [[QNVideoTool share].defaultRender.deviceId isEqualToString:render.deviceId]) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    QNRightCommandHeaderView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"QNRightCommandHeaderView" owner:nil options:nil] objectAtIndex:0];
    switch (section) {
        case 0:
            [sectionView.actionSection setText:NSLocalizedString(@"ViewMode", nil)];
            break;
        case 1:
            [sectionView.actionSection setText:NSLocalizedString(@"ActionTool", nil)];
            break;
        case 2:{
            [sectionView.actionSection setText:NSLocalizedString(@"SelectOutput", nil)];
            sectionView.refreshBtn.hidden = NO;
            [sectionView.refreshBtn addTarget:self action:@selector(reloadRenders:) forControlEvents:UIControlEventTouchUpInside];
        }
            break;
            
        default:
            break;
    }
    return sectionView;
}

- (void)reloadRenders:(id)sender{
    _tmpArray = [QNVideoRender MR_findAll];
    _sectionThreeCommands = _tmpArray;
    NSRange range = NSMakeRange(2, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    
    [self.commandTable reloadSections:section  withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self commandAction:indexPath];
    
    //if MultiSelection, then ....
    QNCommandTableViewCell *selectedCell = (QNCommandTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if ([selectedCell.commandLabel.text isEqualToString:NSLocalizedString(@"MultipleSelect", nil)]) {
        self.parentPanelViewController.isSelectedMode = YES;
        [self.parentPanelViewController setPaneState:MSDynamicsDrawerPaneStateClosed
                                            animated:YES
                               allowUserInterruption:YES
                                          completion:^(){}];
    }else
        self.previousSelection = indexPath; //change the background color via obersering the previousSelection.
}

- (void)generateTable:(VideoFileHomePath)homePath{
    [self generateSectionItems];
    [self.commandTable reloadData];
}

#pragma mark - PrivateMethod
- (void)multipleSelectSwitch:(id)sender{
    UISwitch *switcher = (UISwitch *)sender;
    self.parentPanelViewController.isSelectedMode = switcher.on;
    
    [self generateSectionItems];
    
    NSRange range = NSMakeRange(1, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    
    [self.commandTable reloadSections:section  withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)generateSectionItems{
    DDLogVerbose(@"homePath %i", self.parentPanelViewController.homePath);
    
    if (self.parentPanelViewController.homePath == videoFileHomePathPublic ||
        self.parentPanelViewController.homePath == videoFileHomePathPrivate ||
        self.parentPanelViewController.homePath == videoFileHomePathQSync ) {
        _sectionOneCommands = @[NSLocalizedString(@"Timeline", nil), NSLocalizedString(@"Thumbnail", nil), NSLocalizedString(@"List", nil), NSLocalizedString(@"Folder", nil)];
    }else
        _sectionOneCommands = @[];
    
    if (self.parentPanelViewController.homePath == videoFileHomePathTrashCan) {
        _sectionTwoCommands = @[];
    }else{
        DDLogVerbose(@"isSelectedMode %i", self.parentPanelViewController.isSelectedMode);
        _sectionTwoCommands = @[NSLocalizedString(@"MultipleSelect", nil)];
    }
    _sectionThreeCommands = _tmpArray;
}

- (void)commandAction:(NSIndexPath *)indexPath{
    NSInteger row = [indexPath row];
    NSInteger section = [indexPath section];
    switch (section) {
        case 0:{
            [self changeContent:row];
        }
            break;
        case 1:{
            if (self.parentPanelViewController.homePath == videoFileHomePathTrashCan) {
                //trashcan action
            }else if (row == 0){
                //do nothing
            }else {
                [self actionByMultiSelect:row];
            }
        }
            break;
        case 2:{
            __block UITableView *blockTable = self.commandTable;
            [CXAlertView showMessageAlert:NSLocalizedString(@"SelectOutput", nil) withActionTitle:NSLocalizedString(@"Done", nil) withMessage:NSLocalizedString(@"confirmSelectedOutput", nil) withActionHandler:^(CXAlertView *v, CXAlertButtonItem *i){
                [QNVideoTool share].defaultRender = [_sectionThreeCommands objectAtIndex:row];
                NSIndexSet *set = [NSIndexSet indexSetWithIndex:2];
                [blockTable reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
                
                [v dismiss];
            }];
        }
            break;
        default:
            break;
    }
}

- (void)actionByMultiSelect:(NSInteger)index{
    switch (index) {
        case 0:{ //TODO, multiSelect, do nothing
        }
            break;
        case 1:{ //TODO, share
            [self.parentPanelViewController shareSelectedFilme];
        }
            break;
        case 2:{ //TODO, download
            [self.parentPanelViewController downloadSelectedFilms];
        }
            break;
        case 3:{//TODO, Add To Transcode
            [self.parentPanelViewController addToTransCode];
        }
            break;
        case 4:{ //TODO, Delete
            [self.parentPanelViewController deleteFiles];
        }
            break;
        case 5:{ //TODO, Copy To Collection
            [self.parentPanelViewController addToAlbum];
        }
            break;
        case 6:{
        }
            break;
        default:
            break;
    }
    [self.parentPanelViewController setPaneState:MSDynamicsDrawerPaneStateClosed animated:YES allowUserInterruption:NO completion:^{}];
}

- (void)changeContent:(NSInteger)index{
    switch (index) {
        case 0:
            [self.parentPanelViewController changeContentWithGalleryViewController:YES withCompleteBlock:nil];
            break;
        case 1:
            [self.parentPanelViewController changeContentWithThumbnailViewController:YES withCompleteBlock:nil];
            break;
        case 2:
            [self.parentPanelViewController changeContentWithListViewController:YES withCompleteBlock:nil];
            break;
        case 3:
            [self.parentPanelViewController changeContentWithFolderViewController:YES withCompleteBlock:nil];
            break;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"homePath"])
    {
        NSInteger value = [[change valueForKey:@"new"] intValue];
        [self generateTable:(VideoFileHomePath)value];
    }else if ([keyPath isEqualToString:@"previousSelection"]){
        NSIndexPath *oldIndexPath = [change objectForKey:@"old"];
        UITableViewCell *priviousCell = [self.commandTable cellForRowAtIndexPath:oldIndexPath];
        
        NSIndexPath *newIndexPath = [change objectForKey:@"new"];
        
        if ([newIndexPath isEqual:_selectionIndexPath])
            return;
        
        UITableViewCell *newCell = [self.commandTable cellForRowAtIndexPath:newIndexPath];
        [priviousCell setBackgroundColor:defaultBackgroundColor];
        [newCell setBackgroundColor:selectedBackgroundColor];
    }
}

- (BOOL)searchUISwitch:(UIView *)targetView{
    BOOL r = NO;
    NSArray *views = [targetView subviews];
    for (UIView *view in views) {
        if ([view isKindOfClass:[UISwitch class]]) {
            r = YES;
            break;
        }else{
            r = [self searchUISwitch:view];
        }
    }
    return r;
}
#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
