//
//  QNVideoStatusView.m
//  QVideo
//
//  Created by Change.Liao on 2014/3/18.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoStatusView.h"

@implementation QNVideoStatusView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSString *bundleName = @"QNVideoStatusView";
        UIView *subView =[[[NSBundle mainBundle] loadNibNamed:bundleName
                                                        owner:self
                                                      options:nil] objectAtIndex:0];
        [self addSubview:subView];
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder{
    if ((self = [super initWithCoder:aDecoder])) {
        NSString *bundleName = @"QNVideoStatusView";
        UIView *subView =[[[NSBundle mainBundle] loadNibNamed:bundleName
                                                        owner:self
                                                      options:nil] objectAtIndex:0];
        [self addSubview:subView];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    //just in case
    [self.statusDescription setTextAlignment:NSTextAlignmentCenter];
    [self.statusDescription setNumberOfLines:0];
    [self.statusDescription sizeToFit];
    
    [self.statusDescription setText:NSLocalizedString(@"NoItem", nil)];
}
- (void)setVideoStatus:(QNVideoStatus)status{
    [self.statusDescription setTextColor:defaultBackgroundColor];
    if (self.delegate && [self.delegate respondsToSelector:@selector(changeStatus:withStatus:)]) {
        [self.delegate changeStatus:self withStatus:status];
    }
}

@end
