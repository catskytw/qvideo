//
//  QNAboutTableViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/25.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNAboutTableViewController.h"
#import "UIView+QVideoExt.h"
#import "QNAboutRequirementViewController.h"
#import "QNAboutDisclaimerViewController.h"
#import "QNAboutQNAPViewController.h"
#import "QNAppDelegate.h"
#import "QNTurtorialViewController.h"

@interface QNAboutTableViewController ()

@end

@implementation QNAboutTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _aboutItems = @[@"Tutorial", @"Requirement", @"Disclaimer", @"AboutQNAP"];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_aboutItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"aboutItemCell" forIndexPath:indexPath];

    if (![cell hasSubViewWithTag:9999]) {
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithHexString:@"#d6f3fc"];
        [bgColorView setTag:9999];
        [cell setSelectedBackgroundView:bgColorView];
    }
    
    NSInteger row = [indexPath row];
    NSString *textString = NSLocalizedString([_aboutItems objectAtIndex:row], nil);
    [cell.textLabel setText:textString];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DDLogVerbose(@"select index %i", [indexPath row]);
    NSInteger row = [indexPath row];
    UIViewController *targetViewController = nil;
    switch (row) {
        case 0:{ //Tutorial
            QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
            QNTurtorialViewController *startViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"turtorialViewController"];
            startViewController.forceTutorial = YES;
            targetViewController = startViewController;
        }
            break;
        case 1: //Requirement
            targetViewController = [[QNAboutRequirementViewController alloc] init];
            break;
        case 2: //Disclaimer
            targetViewController = [[QNAboutDisclaimerViewController alloc] init];
            break;
        case 3: //About QNAP
            targetViewController = [[QNAboutQNAPViewController alloc] init];
            break;
        default:
            break;
    }
    if (targetViewController) {
        [self.navigationController pushViewController:targetViewController animated:YES];
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
