//
//  QNVideoShowInfoViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/1/23.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoShowInfoViewController.h"
#import <QNAPFramework/QNAPCommunicationManager.h>
#import "NSString_TimeDuration.h"
#import "QNVideoTool.h"
#import "QNVideoInfoViewController.h"

@interface QNVideoShowInfoViewController ()

@end

@implementation QNVideoShowInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self settingData];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear: animated];
    [self.videoTitle restartLabel];
}
- (void)settingData{
    NSString *content = nil;
    //Title
    [self.videoTitle setText:self.videoItem.cPictureTitle];
    [self.videoTitle setMarqueeType:MLContinuous];
    [self.videoTitle setAnimationCurve:UIViewAnimationOptionCurveLinear];
    [self.videoTitle setFadeLength:20.0f];
    [self.videoTitle restartLabel];
    //ScreenShot
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    [communicationManager.videoStationManager lazyloadingImage:self.videoScreenShot
                                               withPlaceHolder:nil
                                                    withFileId:self.videoItem.f_id
                                             withProgressBlock:nil
                                            withCompletedBlock:^(UIImage *image, NSError *e, SDImageCacheType cacheType){
                                                if (e) {
                                                    DDLogError(@"error while lazyloading image: %@", e);
                                                }
                                            }];
    
    //DurationLabel
    [self.duration setText:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Duration", nil),[NSString stringFromTimeInterval:[self.videoItem.duration doubleValue]]]];
    
    //tag
    [self.tagLabel setText:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Tag", nil),(self.videoItem.keywords)?self.videoItem.keywords:@""]];
    
    //description
    [self.descriptionLabel setText:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Description", nil),(self.videoItem.comment)?self.videoItem.comment:@""]];
    
    //classification
    [self.classification setText:[QNVideoTool classificationValueToString:[self.videoItem.mask integerValue]]];
    
    //rating
    [QNVideoTool configureStarRating:self.ratingStars];
    CGFloat rating = [self.videoItem.rating floatValue]/20.0f;
    self.ratingStars.rating = rating;
    
    //dimensions
    content = [NSString stringWithFormat:@"%@: %ix%i", NSLocalizedString(@"Dimensions", nil),[self.videoItem.iWidth integerValue], [self.videoItem.iHeight integerValue]];
    [self.dimensionLabel setText:content];
    
    //file size
    CGFloat mb = [self.videoItem.iFileSize floatValue] / 1048576.0f;
    content = [NSString stringWithFormat:@"%@: %0.2f MB", NSLocalizedString(@"FileSize", nil), mb];
    [self.fileSizeLabel setText:content];
    
    //file date
    content = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"FileDate", nil),self.videoItem.yearMonthDay];
    [self.fileDateLabel setText:content];
    
    //date imported
    content = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"DateImported", nil),self.videoItem.importYearMonthDay];
    [self.dateImportedLabel setText:content];
    
    //date taken
    content = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"DateTaken", nil),self.videoItem.importYearMonthDay];
    [self.dateTakenLabel setText:content];
    
    //type
    content = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Type", nil),self.videoItem.mime];
    [self.typeLabel setText:content];
    
    //Color level
    [self.colorLevelImage setImage:[QNVideoTool colorLevel:[self.videoItem.colorLevel integerValue]]];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"editVideoMetaData"]){
        QNVideoInfoViewController *editInfoViewController = (QNVideoInfoViewController *)segue.destinationViewController;
        editInfoViewController.videoFileItem = self.videoItem;
        editInfoViewController.outsideDetailViewController = self.outsideDetailViewController;
    }
}

@end
