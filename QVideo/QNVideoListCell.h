//
//  QNVideoListCell.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/24.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EDStarRating/EDStarRating.h>
#import "QNButton.h"

@interface QNVideoListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeDuration;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *mediaType;
@property (weak, nonatomic) IBOutlet UIImageView *colorLevel;
@property (weak, nonatomic) IBOutlet EDStarRating *starRating;
@property (weak, nonatomic) IBOutlet UIImageView *screenShot;
@property (weak, nonatomic) IBOutlet QNButton *playBtn;
@property (weak, nonatomic) IBOutlet UIImageView *selectMark;
@property (weak, nonatomic) IBOutlet UIImageView *hdImageIndicator;
@end
