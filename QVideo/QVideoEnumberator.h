//
//  QVideoEnumberator.h
//  QVideo
//
//  Created by Change.Liao on 2013/11/27.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//
#import <QNAPFramework/QNAPFramework.h>

#ifndef QVideo_QVideoEnumberator_h
#define QVideo_QVideoEnumberator_h

#define defaultBackgroundColor [UIColor colorWithRed:68.0f/256.0f green:68.0f/256.0f blue:68.0f/256.0f alpha:1.0f]
#define selectedBackgroundColor [UIColor colorWithRed:37.0f/256.0f green:202.0f/256.0f blue:169.0f/256.0f alpha:1.0f]
#define defaultGreenColor selectedBackgroundColor
#define PlaceHolderImage [UIImage imageNamed:@"placeHolder.png"]
#define MyCloudServerBaseURL @"http://core.api.alpha-myqnapcloud.com"
#define CLIENT_ID @"521c609775413f6bfec8e59b"
#define CLIENT_SECRET @"LWvRWyHFNDENTZZGCp9kcOEGed18cW02KVnV6bfrvtBL0hpu"
#define MyCloud_ACCOUNT @"changeliao@qnap.com"
#define MyCloud_PASSWORD @"change1269"

#define _highlightColorValue 92.0f/256.0f
#define _backgrounColorValue 70.0f/256.0f

typedef void(^OutsideFetchingBlock)(NSString *fetchUUID, QNQNVideoFileListSuccessBlock success, QNFailureBlock failure);

typedef NS_ENUM(NSInteger, VideoDownloadStatus){
    videoDownloadProgressing,
    videoDownloadPauseBySystem,
    videoDownloadPauseByUser,
    videoDownloadUnknowError,
    videoDownloadCompleted //this status would never be saved in CoreData
};

typedef NS_ENUM(NSInteger, VideoUploadStatus){
    videoUploadProgressing,
    videoUploadPauseBySystem,
    videoUploadPauseByUser,
    videoUploadUnknowError,
    videoUploadCompleted //this status would never be saved in CoreData
};

typedef NS_ENUM (NSInteger, DownloadLimitSize){
    VideoDownload1GB,
    VideoDownload2GB,
    VideoDownload5GB,
    VideoDownloadUnlimit
};

typedef NS_ENUM (NSInteger, PortSelectStrategy){
    PortSelection_InternalPortFirst,
    PortSelection_ExternalPortFirst,
    PortSelection_InternalPortOnly,
    PortSelection_ExternalPortOnly
};

typedef NS_ENUM (NSInteger, QVideoToolBarStyle){
    VideosToolBarStyle,
    AlbumsToolBarStyle,
    TrashcanToolBarStyle,
    ShareToolBarStyle
};

typedef NS_ENUM (NSInteger, QNVideoFilter){
    QNVideoFilterNone,
    QNVideoFilterByTitle,
    QNVideoFilterByDate,
    QNVideoFilterByTag,
    QNVideoFilterByStar,
    QNVideoFilterByColor,
    QNVideoFilterByCollection
};
#endif
