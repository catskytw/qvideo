//
//  QNVideoActionViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/20.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QNAPFramework/QNVideoFileItem.h>

@interface QNVideoActionViewController : UITableViewController
@property (nonatomic, strong) QNVideoFileItem *videoItem;
@end
