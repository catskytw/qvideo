//
//  QNCommandTableViewCell.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/3.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNCommandTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *commandLabel;
@property(nonatomic)BOOL isSelected;
@end
