//
//  CXAlertView+QvideoExt.h
//  QVideo
//
//  Created by Change.Liao on 2014/4/9.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "CXAlertView.h"
#import "CXAlertView/CXAlertView.h"

@interface CXAlertView (QvideoExt)
@property (nonatomic, strong) UILabel *titleLabel;

+ (NSMutableArray *)sharedQueue;

+ (CXAlertView *)showInputTextfieldAlert:(NSString *)title withActionTitle:(NSString *)actionTitle withTextFieldDelegate:(id<UITextFieldDelegate>)targetDelegate withActionHandler:(void(^)(CXAlertView *alert, CXAlertButtonItem *button))cxHandler;

+ (CXAlertView *)showMessageAlert:(NSString *)title withActionTitle:(NSString *)actionTitle withMessage:(NSString *)message withActionHandler:(void(^)(CXAlertView *alert, CXAlertButtonItem *button))cxHandler;

+ (CXAlertView *)showContentViewAlert:(NSString *)title withActionTitle:(NSString *)actionTitle withContentView:(UIView *)contentView withActionHandler:(void(^)(CXAlertView *alert, CXAlertButtonItem *btn))cxHandler;

+ (CXAlertView *)showInputTextfieldAlert:(NSString *)title withActionTitle:(NSString *)actionTitle isSecureType:(BOOL)isSecureType withTextFieldDelegate:(id<UITextFieldDelegate>)targetDelegate withActionHandler:(void(^)(CXAlertView *alert, CXAlertButtonItem *button))cxHandler;

+ (CXAlertView *)showMessageAlert:(NSString *)title withActionTitle:(NSString *)actionTitle withCancelTitle:(NSString *)cancelTitle withMessage:(NSString *)message withActionHandler:(void(^)(CXAlertView *alert, CXAlertButtonItem *button))actionHandler;

+ (void)cleanAllAlertBesideThis:(CXAlertView *)targetAlertView;


@end
