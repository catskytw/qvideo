//
//  QNSearchNASRefreshView.h
//  QVideo
//
//  Created by Change.Liao on 2014/3/11.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNSearchNASRefreshView : UIView
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *searchIndicator;

@end
