//
//  QNAddCollectionTableViewCell.h
//  QVideo
//
//  Created by Change.Liao on 2014/7/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNAddCollectionTableViewCell : UITableViewCell
@property (nonatomic, strong)IBOutlet UIImageView *checkImage;
@end
