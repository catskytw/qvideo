//
//  QNVideoSettingViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/11/25.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoSettingViewController.h"
#import <QNAPFramework/QNVideoStationAPIManager.h>
#import "QNSettingViewController.h"
#import "QNLeftSettingHeaderView.h"
#import "QNVideoCommandViewController.h"
#import "QNNASListViewController.h"
#import "QNVideoTool.h"
#import "QNFilterView.h"
#import "QNVideoColorLabelSelectionView.h"
#import "QNVideoAlbumViewController.h"

@interface QNVideoSettingViewController ()
@end

@implementation QNVideoSettingViewController

- (id)initWithStyle:(UITableViewStyle)style{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self addObserver:self forKeyPath:@"previousSelection" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
    self.previousSelection = 0;
    _firstLoad = YES;
    _currentFilter = QNVideoFilterByTitle;
    
    [self settingLocalizable];
}

- (void)settingLocalizable{
    [self.sharedVideoLabel setText:NSLocalizedString(@"SharedVideo", nil)];
    [self.privateCollectionLabel setText:NSLocalizedString(@"PrivateCollection", nil)];
    [self.qSyncLabel setText:NSLocalizedString(@"QSync", nil)];
    [self.videoCollectionLabel setText:NSLocalizedString(@"VideoCollection", nil)];
    [self.smartCollectionLabel setText:NSLocalizedString(@"SmartCollection", nil)];
    [self.trashCanLabel setText:NSLocalizedString(@"TrashCan", nil)];
    [self.settingLabel setText:NSLocalizedString(@"Setting", nil)];
    [self.localVideoFilesLabel setText:NSLocalizedString(@"LocalVideoFiles", nil)];
    [self.logoutLabel setText:NSLocalizedString(@"Logout", nil)];
    [self decideFilterString];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (_firstLoad) {
        UITableViewCell *firstLoadCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        [firstLoadCell setBackgroundColor:selectedBackgroundColor];
        _firstLoad = NO;
    }
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    QNVideosViewController *_myParentViewController = (QNVideosViewController *)self.parentViewController;
    
    _currentFilter = ([_myParentViewController.videoDataSource isKindOfClass:[QNVideoAlbumViewController class]])?QNVideoFilterByCollection:QNVideoFilterByTitle;
    [self decideFilterString];
    
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context{
    if([keyPath isEqual:@"previousSelection"]){
        NSInteger oldIndex = [[change objectForKey:@"old"] intValue];
        NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:oldIndex inSection:0]; //only one section
        UITableViewCell *priviousCell = [self.tableView cellForRowAtIndexPath:oldIndexPath];
        
        NSInteger newIndex = [[change objectForKey:@"new"] intValue];
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:newIndex inSection:0];
        UITableViewCell *newCell = [self.tableView cellForRowAtIndexPath:newIndexPath];
        
        [priviousCell setBackgroundColor:defaultBackgroundColor];
        [newCell setBackgroundColor:selectedBackgroundColor];
    }
}
#pragma mark - Table view data source
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if ([indexPath section] == 0) return;
    
    NSInteger row = [indexPath row];
    if (row == self.previousSelection)
        return;
    
    QNVideosViewController *_myParentViewController = (QNVideosViewController *)self.parentViewController;
    _myParentViewController.isSelectedMode = NO; //by Kenneth, 切換新的分類, 多選要取消

    switch (row) {
        case 0: //shareVideo
            _myParentViewController.title = NSLocalizedString(@"SharedVideo", nil);
            _myParentViewController.homePath = videoFileHomePathPublic;
            [self changeContentViewController];
            self.previousSelection = row;

            break;
        case 1: {//Private
            if (![[QNVideoTool share].multimediaLogin.usr_home boolValue]) {
                [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                              withActionTitle:nil
                                  withMessage:NSLocalizedString(@"homeDirNotOpen", nil)
                            withActionHandler:nil];
                break;
            }
            BOOL needShowLogin = [self.thisServer.needLoginInPrivate boolValue];
            void(^executePrivate)(void) = ^(){
                _myParentViewController.title = NSLocalizedString(@"PrivateCollection", nil);
                _myParentViewController.homePath = videoFileHomePathPrivate;
                [self changeContentViewController];
                self.previousSelection = row;
            };
            if (needShowLogin) {
                [[QNVideoTool share] showLoginDialog:self.thisServer withResultBlock:^(BOOL success, NSError *e){
                    if (success){
                        executePrivate();
                    }else
                        DDLogError(@"auth login error: %@", e);
                }];
            }else
                executePrivate();
        }
            break;
        case 2:{ //QSync
            if (![[QNVideoTool share].multimediaLogin.qsync boolValue]) {
                [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                              withActionTitle:nil
                                  withMessage:NSLocalizedString(@"QsyncNotOpen", nil)
                            withActionHandler:nil];
                break;
            }
            _myParentViewController.title = NSLocalizedString(@"QSync", nil);

            _myParentViewController.homePath = videoFileHomePathQSync;
            [self changeContentViewController];
            self.previousSelection = row;
        }
            break;
        case 3:{ //Collection
            _myParentViewController.title = NSLocalizedString(@"VideoCollection", nil);

            _myParentViewController.homePath = videoFileHomePathCollections;
            [_myParentViewController changeContentWithAlbumViewController:NO withCompletedBlock:nil];
            self.previousSelection = row;
        }
            break;
        case 4:{ //smart collection
            _myParentViewController.title = NSLocalizedString(@"SmartCollection", nil);

            _myParentViewController.homePath = videoFileHomePathSmartCollections;
            [_myParentViewController changeContentWithAlbumViewController:NO withCompletedBlock:nil];
            self.previousSelection = row;
        }
            break;
        case 5:{ //LocalVideoFile
            _myParentViewController.title = NSLocalizedString(@"LocalVideoFiles", nil);

            [_myParentViewController changeContentWithLocalVideoViewController:YES withCompletedBlock:nil];
            self.previousSelection = row;
            break;
        }

        case 6:{ //trashCan
            if (![[QNVideoTool share].multimediaLogin.usr_recycle boolValue]) {
                [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                              withActionTitle:nil
                                  withMessage:NSLocalizedString(@"recyclebinNotOpen", nil)
                            withActionHandler:nil];
                break;
            }
            _myParentViewController.title = NSLocalizedString(@"TrashCan", nil);

            _myParentViewController.homePath = videoFileHomePathTrashCan;
            [_myParentViewController changeContentWithTrashCanViewController:NO withCompleteBlock:nil];
            [self onlySendRequest];
            self.previousSelection = row;

        }
            break;
        case 7:{ //setting
            _myParentViewController.title = NSLocalizedString(@"Setting", nil);
            [_myParentViewController changeContentWithSettingViewController:YES withCompletedBlock:nil];
            self.previousSelection = row;
        }
            break;
        case 8:
            [self logout:nil];
            break;
        default:
            break;
    }
}

- (void)onlySendRequest{
    QNVideosViewController *_myParentViewController = (QNVideosViewController *)self.parentViewController;
    SEL setErrorSelector = NSSelectorFromString(@"sendRequestAndReload");
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [_myParentViewController.contentViewController performSelector:setErrorSelector withObject:nil];
#pragma clang diagnostic pop

}

- (void)changeContentViewController{
    QNVideosViewController *_myParentViewController = (QNVideosViewController *)self.parentViewController;
    [self factoryOfContentViewController: _myParentViewController.commandViewController.previousSelection];
    [self onlySendRequest];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    QNLeftSettingHeaderView *leftHeader = (QNLeftSettingHeaderView *)[[[NSBundle mainBundle] loadNibNamed:@"QNLeftSettingHeaderView" owner:nil options:nil] objectAtIndex:0];
    [leftHeader.serverTitle setText:self.thisServer.serverName];
    
    [leftHeader addTarget:self action:@selector(changeHeaderString:) forControlEvents:UIControlEventTouchUpInside];
    
    return leftHeader;
}

- (void)changeHeaderString:(id)sender{
    QNLeftSettingHeaderView *leftHeader = (QNLeftSettingHeaderView *)sender;
    if ([leftHeader.serverTitle.text isEqualToString:self.thisServer.serverName]) {
        [leftHeader.serverTitle setText:[NSString stringWithFormat:@"%@:%@", self.thisServer.currentAddress, self.thisServer.account]];
    }else
        [leftHeader.serverTitle setText:self.thisServer.serverName];
}

- (void)logout:(id)sender{
    [self.navigationController setNavigationBarHidden:NO];
    QNVideosViewController *_myParentViewController = (QNVideosViewController *)self.parentViewController;
    [[QNVideoTool share] stopAllRequests];
    _myParentViewController.isSelectedMode = NO;
    [_myParentViewController setPaneState:MSDynamicsDrawerPaneStateClosed
                                 animated:NO
                    allowUserInterruption:NO
                               completion:^(){
                                   for (UIViewController *sampleViewController in self.navigationController.viewControllers) {
                                       if ([sampleViewController isKindOfClass:[QNNASListViewController class]]) {
                                           [self.navigationController popToViewController:sampleViewController animated:YES];
                                           break;
                                       }
                                   }
                               }];
    [[QNVideoTool share] logoutClean];
}

- (void)factoryOfContentViewController:(NSIndexPath *)previousIndex{
    QNVideosViewController *_myParentViewController = (QNVideosViewController *)self.parentViewController;
    NSInteger previousMode = -1;
    if ([previousIndex section] == 0) {
        previousMode = [previousIndex row];
    }

    switch (previousMode) {
        case 0: //Gallery
            [_myParentViewController changeContentWithGalleryViewController:YES
                                                          withCompleteBlock:nil];
            break;
        case 1:
            [_myParentViewController changeContentWithThumbnailViewController:YES
                                                            withCompleteBlock:nil];
            break;
        case 2:
            [_myParentViewController changeContentWithListViewController:YES
                                                       withCompleteBlock:nil];
            break;
        case 3:
            [_myParentViewController changeContentWithFolderViewController:YES
                                                         withCompleteBlock:nil];
            break;
        default:
            break;
    }
}

#pragma mark - Filter-related methods
- (IBAction)changeFilterFactor:(id)sender {
    void(^clearAction)(void) = ^(void){
        [_tmpAlert dismiss];
        [self sendFilterAndRequest:nil];
        [self.searchBar setText:@""];
        [self.searchBar resignFirstResponder];
    };
    QNVideosViewController *videoViewController = (QNVideosViewController *)self.parentViewController;
    BOOL isCollection = (videoViewController.homePath == videoFileHomePathCollections || videoViewController.homePath == videoFileHomePathSmartCollections);
    QNFilterView *filterView = [[QNFilterView alloc] initWithFrame:CGRectMake(0, 0, 240, 160) style:UITableViewStylePlain isCollection:isCollection withActionBlock:^(NSInteger actionIndex){
        /**
         *@[NSLocalizedString(@"Title", nil),
            NSLocalizedString(@"Tag", nil),
            NSLocalizedString(@"Date", nil),
            NSLocalizedString(@"Rating", nil),
            NSLocalizedString(@"ColorLabel", nil)];
         */
        switch (actionIndex) {
            case 0:{ //Title
                _currentFilter = QNVideoFilterByTitle;
                if (_tmpAlert) {
                    [_tmpAlert dismiss];
                }
            }
                break;
            case 1:{ //Tag
                if (_currentFilter == QNVideoFilterByCollection){
                    clearAction();
                    return ;
                }else{
                    _currentFilter = QNVideoFilterByTag;
                    if (_tmpAlert) {
                        [_tmpAlert dismiss];
                    }
                }
            }
                break;
            case 2:{ //Date
                _currentFilter = QNVideoFilterByDate;
                InlineDatePickerTableViewController *durationPicker = [[InlineDatePickerTableViewController alloc]
                                                                       initWithFrame:CGRectMake(0, 0, 300, 220) style:UITableViewStylePlain];
                [durationPicker setTag:9999];
                [durationPicker setting];
                
                CXAlertView *alert = [CXAlertView showContentViewAlert:NSLocalizedString(@"ChooseDate", nil)
                                                       withActionTitle:NSLocalizedString(@"Done", nil)
                                                       withContentView:durationPicker
                                                     withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *item){
                                                         InlineDatePickerTableViewController *datePicker = [alert.contentView.subviews objectAtIndex:0];
                                                         NSDictionary *infoDic = [self generateSearchFilterForAPI:@{@"startDate": datePicker.startDateString,
                                                                                                                    @"endDate": datePicker.endDateString}];
                                                         [self sendFilterAndRequest:infoDic];
                                                         [alert dismiss];
                                                         
                                                     }];
                durationPicker.outsideView = alert;
            }
                break;
            case 3:{ //Rating
                _currentFilter = QNVideoFilterByStar;
                EDStarRating *starRating = [[EDStarRating alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
                [QNVideoTool configureStarRating:starRating];
                starRating.editable = YES;
                starRating.delegate = self;
                starRating.rating = 0.0f;
                
                _tmpAlert = [CXAlertView showContentViewAlert:NSLocalizedString(@"Rating", nil)
                                  withActionTitle:nil
                                  withContentView:starRating
                                withActionHandler:nil];
                

            }
                break;
            case 4:{ //ColorLabel
                _currentFilter = QNVideoFilterByColor;
                QNVideoColorLabelSelectionView *colorBtnView = [[[NSBundle mainBundle] loadNibNamed:@"QNVideoColorLabelSelectionView" owner:nil options:nil] objectAtIndex:0];
                colorBtnView.colorOption = 0;
                [colorBtnView setting];
                
                [CXAlertView showContentViewAlert:NSLocalizedString(@"ColorLabel", nil)
                                  withActionTitle:NSLocalizedString(@"Done", nil)
                                  withContentView:colorBtnView
                                withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                                    [alert dismiss];
                                    QNVideoColorLabelSelectionView *selectionView = colorBtnView;
                                    NSInteger selection = [selectionView.selectedBtn.userInfo intValue];
                                    NSDictionary *dic = [self generateSearchFilterForAPI:@{@"colorLevel": [NSNumber numberWithInt:selection]}];
                                    [self sendFilterAndRequest:dic];
                                }];

            }
                break;
            default:{
                clearAction();
            }
                break;
        }
        [self decideFilterString];
    }];

    filterView.delegate = filterView;
    filterView.dataSource = filterView;
    
    _tmpAlert = [CXAlertView showContentViewAlert:NSLocalizedString(@"SearchFactor", nil)
                                  withActionTitle:nil
                                  withContentView:filterView
                                withActionHandler:nil];

}

- (void)decideFilterString{
    switch (_currentFilter) {
        default:
        case QNVideoFilterByTitle:
            [self.filterBtn setTitle:NSLocalizedString(@"Title", nil) forState:UIControlStateNormal];
            break;
        case QNVideoFilterByCollection:
            [self.filterBtn setTitle:NSLocalizedString(@"Collection", nil) forState:UIControlStateNormal];
            break;
        case QNVideoFilterByColor:
            [self.filterBtn setTitle:NSLocalizedString(@"Color", nil) forState:UIControlStateNormal];
            break;
        case QNVideoFilterByDate:
            [self.filterBtn setTitle:NSLocalizedString(@"Date", nil) forState:UIControlStateNormal];
            break;
        case QNVideoFilterByStar:
            [self.filterBtn setTitle:NSLocalizedString(@"Star", nil) forState:UIControlStateNormal];
            break;
        case QNVideoFilterByTag:
            [self.filterBtn setTitle:NSLocalizedString(@"Tag", nil) forState:UIControlStateNormal];
            break;
    }
}

- (NSDictionary *)generateSearchFilterForAPI:(NSDictionary *)infoDic{
    NSDictionary *rDic = nil;
    switch (_currentFilter) {
        default:
        case QNVideoFilterByTitle:
            rDic = @{@"mt":self.searchBar.text};
            break;
        case QNVideoFilterByColor:{
            NSNumber *myType = (NSNumber *)infoDic[@"colorLevel"];
            rDic = @{@"l":myType};
        }
            break;
        case QNVideoFilterByStar:{
            CGFloat rating = [(NSNumber *)infoDic[@"rating"] floatValue] * 20.0f;
            rDic = @{@"m": @(rating)};
        }
            break;
        case QNVideoFilterByTag:{
            rDic = @{@"k": self.searchBar.text};
        }
            break;
        case QNVideoFilterByDate:{
            NSString *startDate = infoDic[@"startDate"];
            NSString *endDate = infoDic[@"endDate"];
            rDic = @{@"r":[NSString stringWithFormat:@"%@-%@", startDate, endDate]};
        }
            break;
    }
    return rDic;
}

- (NSPredicate *)generateSearchFilter:(NSDictionary *)infoDic{
    NSPredicate *rPredicate = nil;
    switch (_currentFilter) {
        default:
        case QNVideoFilterByTitle:{
            rPredicate = [NSPredicate predicateWithFormat:@"self.cPictureTitle CONTAINS[cd] %@", self.searchBar.text];
        }
            break;
        case QNVideoFilterByColor:{
            NSNumber *myType = (NSNumber *)infoDic[@"colorLevel"];
            rPredicate = [NSPredicate predicateWithFormat:@"self.colorLevel == %@", myType];
        }
            break;
        case QNVideoFilterByStar:{
            CGFloat rating = [(NSNumber *)infoDic[@"rating"] floatValue] * 20.0f;
            rPredicate = [NSPredicate predicateWithFormat:@"self.rating == %@", [NSNumber numberWithFloat:rating]];
        }
            break;
        case QNVideoFilterByTag:{
            rPredicate = [NSPredicate predicateWithFormat:@"self.keywords CONTAINS[cd] %@", self.searchBar.text];
        }
            break;
        case QNVideoFilterByCollection:{
            rPredicate = [NSPredicate predicateWithFormat:@"self.cAlbumTitle CONTAINS[cd] %@", self.searchBar.text];
        }
            break;
        case QNVideoFilterByDate:{
            //TODO, WTF
            NSString *startDate = infoDic[@"startDate"];
            NSString *endDate = infoDic[@"endDate"];
            rPredicate = [NSPredicate predicateWithFormat:@"self.dateModified <= %@ AND self.dateModified >= %@", endDate, startDate];
        }
            break;
    }
    return rPredicate;
}

- (void)sendFilterAndRequest:(NSDictionary *)filterDic{
    QNVideosViewController *_myParentViewController = (QNVideosViewController *)self.parentViewController;
    [_myParentViewController.videoDataSource settingFilterDic:filterDic];
    [_myParentViewController.videoDataSource sendRequestAndReload];
    
    [_myParentViewController setPaneState:MSDynamicsDrawerPaneStateClosed animated:YES allowUserInterruption:YES completion:^{
    }];
    
}

- (void)sendPredicateAndRequest:(NSPredicate *)predicate{
    QNVideosViewController *_myParentViewController = (QNVideosViewController *)self.parentViewController;
    [_myParentViewController.videoDataSource settingAddtionalPredicate:predicate];
    [_myParentViewController.videoDataSource sendRequestAndReload];
    
    [_myParentViewController setPaneState:MSDynamicsDrawerPaneStateClosed animated:YES allowUserInterruption:YES completion:^{
    }];
}

- (void)applyPredicate:(NSPredicate *)predicate{
    QNVideosViewController *_myParentViewController = (QNVideosViewController *)self.parentViewController;
    [_myParentViewController.videoDataSource settingAddtionalPredicate:predicate];
    [_myParentViewController.videoDataSource reloadContent];
    [_myParentViewController setPaneState:MSDynamicsDrawerPaneStateClosed animated:YES allowUserInterruption:YES completion:^{
    }];
}

#pragma mark - SearchBar Delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSDictionary *dic = [self generateSearchFilterForAPI:nil];
    [self sendFilterAndRequest:dic];
    [searchBar resignFirstResponder];
}

#pragma mark - EDStarRatingDelegate
-(void)starsSelectionChanged:(EDStarRating*)control rating:(float)rating{
    NSDictionary *dic = [self generateSearchFilterForAPI:@{@"rating": [NSNumber numberWithFloat:rating]}];
    [self sendFilterAndRequest:dic];
    [_tmpAlert dismiss];
}
@end
