//
//  QNFolderViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/2/7.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNVideosViewController.h"
#import "DTFolderBar.h"

@interface QNFolderViewController : UITableViewController<NSFetchedResultsControllerDelegate, UIActionSheetDelegate, QNVideoDataSource>{
    __block NSArray *_videoFileItems;

    NSMutableArray *_folderArray;
    DTFolderBar *_folderBar;
    NSString *_thisQueryUUID;
    NSMutableArray *selections;
    NSArray *_multiSelectAction;
    NSPredicate *_addtionalPredicate;
    NSDictionary *_filterDic;
    BOOL isSelectMode;
}
@property (nonatomic, strong)id<QNVideoFetchingDelegate>fetchDelegate;
@property (nonatomic, weak)QNVideosViewController *parentPaneViewController;
@property (nonatomic) VideoFileHomePath homePath;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSString *pathPrefix;
@property (nonatomic, strong) NSString *thisQueryUUID;
@property (nonatomic, strong)IBOutlet QNVideoStatusView *statusView;
@property (nonatomic, strong) UIView *bottomBar;
- (IBAction)playVideo:(id)sender;
- (void)switchMultiSelectMode;
@end
