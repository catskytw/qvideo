//
//  QNHeaderTextLabel.m
//  QVideo
//
//  Created by Change.Liao on 2014/2/12.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNHeaderTextLabel.h"

@implementation QNHeaderTextLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, 8, 0, 0};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
