//
//  QNAppDelegate.h
//  QVideo
//
//  Created by Change.Liao on 2013/11/19.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QNAPFramework/VLCMediaFileDiscoverer.h>

@interface QNAppDelegate : UIResponder <UIApplicationDelegate, VLCMediaFileDiscovererDelegate>
@property (nonatomic) BOOL isFinishSaving;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIWindow *secondWindow;
@end
