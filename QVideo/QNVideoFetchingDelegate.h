//
//  QNVideoFetchingDelegate.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/6.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol QNVideoFetchingDelegate <NSObject>
@optional
/**
 *  Send/resend the API in each ViewController for fetching the related data.
 *
 *  @param homePath              see VideoFileHomePath definitions, such as Public, QSync, Private ...etc.
 *  @param contentViewController the current viewcontroller
 *  @param uuid                  To avoid the ambiguous responses from the same APIs invoking many times in a short time, we create an uuid
                                 for each request to distinquish the response.
 *  @param success               execute this block if successful
 *  @param failure               execute this block if in failure.
 */
- (void)fetchingRequest:(VideoFileHomePath)homePath withOriginContentViewController:(UIViewController *)contentViewController withQueryUUID:(NSString *)uuid withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)fetchingRequest:(VideoFileHomePath)homePath withOriginContentViewController:(UIViewController *)contentViewController withInfoDic:(NSDictionary *)dic withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

@end
