//
//  CXAlertView+QvideoExt.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/9.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "CXAlertView+QvideoExt.h"
#import "QNVideoTool.h"

@implementation CXAlertView (QvideoExt)

+ (CXAlertView *)showInputTextfieldAlert:(NSString *)title withActionTitle:(NSString *)actionTitle isSecureType:(BOOL)isSecureType withTextFieldDelegate:(id<UITextFieldDelegate>)targetDelegate withActionHandler:(void(^)(CXAlertView *alert, CXAlertButtonItem *button))cxHandler{
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 250, 30)];
    textField.delegate = targetDelegate;
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.secureTextEntry = isSecureType;
    
    textField.layer.cornerRadius = 5.0f;
    textField.layer.borderWidth = 1.0f;
    textField.layer.borderColor = [UIColor colorWithHexString:@"#a0a0a0" alpha:1.0f].CGColor;
    textField.backgroundColor = [UIColor whiteColor];
    
    CXAlertView *alert = [[CXAlertView alloc] initWithTitle:title
                                                contentView:textField
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)];
    [alert addButtonWithTitle:actionTitle
                         type:CXAlertViewButtonTypeCustom
                      handler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                          cxHandler(alert, btn);
                      }];
    alert.cancelButtonFont = [UIFont systemFontOfSize:18.0f];
    alert.customButtonFont = [UIFont boldSystemFontOfSize:18.0f];
    alert.cancelButtonColor = [UIColor colorWithHexString:@"007aff" alpha:1.0f];
    alert.customButtonColor = [UIColor colorWithHexString:@"007aff" alpha:1.0f];
    
    textField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    [alert setShowBlurBackground:NO];
    
    [alert show];
    
    //hack here to adjust the position
    UIView *containView = [alert.subviews objectAtIndex:0];
    NSString *deviceName = [QNVideoTool deviceModelName];
    if ([deviceName isEqualToString:@"iPhone 3GS"] ||
        [deviceName isEqualToString:@"iPhone 4"] ||
        [deviceName isEqualToString:@"iPhone 4S"] ||
        [deviceName hasSuffix:@"iPad"] ||
        [deviceName hasSuffix:@"x86_64"]
        ) {
        containView.frame = CGRectOffset( containView.frame, 0, -40 ); // offset by an amount
    }
    [CXAlertView cleanAllAlertBesideThis:alert];

    return alert;

}


+ (CXAlertView *)showInputTextfieldAlert:(NSString *)title withActionTitle:(NSString *)actionTitle withTextFieldDelegate:(id<UITextFieldDelegate>)targetDelegate withActionHandler:(void(^)(CXAlertView *alert, CXAlertButtonItem *button))cxHandler{
    CXAlertView *targetAlertView = [CXAlertView showInputTextfieldAlert:title withActionTitle:actionTitle isSecureType:YES withTextFieldDelegate:targetDelegate withActionHandler:cxHandler];

    [CXAlertView cleanAllAlertBesideThis:targetAlertView];
    return targetAlertView;
}

+ (CXAlertView *)showMessageAlert:(NSString *)title withActionTitle:(NSString *)actionTitle withMessage:(NSString *)message withActionHandler:(void(^)(CXAlertView *alert, CXAlertButtonItem *button))cxHandler{
    CXAlertView *alert = [[CXAlertView alloc] initWithTitle:title
                                                    message:message
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)];
    [alert setShowBlurBackground:NO];
    if (actionTitle != nil) {
        [alert addButtonWithTitle:actionTitle
                             type:CXAlertViewButtonTypeCustom
                          handler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                              cxHandler(alert, btn);
                              [alert cleanAllPenddingAlert];
                          }];
    }

    alert.cancelButtonFont = [UIFont systemFontOfSize:18.0f];
    alert.customButtonFont = [UIFont boldSystemFontOfSize:18.0f];
    alert.cancelButtonColor = [UIColor colorWithHexString:@"007aff" alpha:1.0f];
    alert.customButtonColor = [UIColor colorWithHexString:@"007aff" alpha:1.0f];
    
    [alert show];
    
    UILabel *contentLabel = (UILabel *)alert.contentView;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    
    [CXAlertView cleanAllAlertBesideThis:alert];

    return alert;
}

+ (CXAlertView *)showContentViewAlert:(NSString *)title withActionTitle:(NSString *)actionTitle withContentView:(UIView *)contentView withActionHandler:(void(^)(CXAlertView *alert, CXAlertButtonItem *btn))cxHandler{
    UIView *containerView = [[UIView alloc] initWithFrame:contentView.frame];
    [containerView addSubview:contentView];
    
    CXAlertView *alert = [[CXAlertView alloc] initWithTitle:title
                                                contentView:containerView
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)];
    if (actionTitle != nil) {
        [alert addButtonWithTitle:actionTitle
                             type:CXAlertViewButtonTypeCustom
                          handler:^(CXAlertView *_alert, CXAlertButtonItem *btn){
                              cxHandler(_alert, btn);
                          }];
    }
    
    alert.cancelButtonFont = [UIFont systemFontOfSize:18.0f];
    alert.customButtonFont = [UIFont boldSystemFontOfSize:18.0f];
    alert.cancelButtonColor = [UIColor colorWithHexString:@"007aff" alpha:1.0f];
    alert.customButtonColor = [UIColor colorWithHexString:@"007aff" alpha:1.0f];

    [alert show];

    alert.showBlurBackground = NO;
    [CXAlertView cleanAllAlertBesideThis:alert];
    return alert;
}

+ (CXAlertView *)showMessageAlert:(NSString *)title withActionTitle:(NSString *)actionTitle withCancelTitle:(NSString *)cancelTitle withMessage:(NSString *)message withActionHandler:(void(^)(CXAlertView *alert, CXAlertButtonItem *button))actionHandler{
    CXAlertView *alert = [[CXAlertView alloc] initWithTitle:title
                                                    message:message
                                          cancelButtonTitle:cancelTitle];
    if (actionTitle) {
        [alert addButtonWithTitle:actionTitle
                             type:CXAlertViewButtonTypeCustom
                          handler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                              actionHandler(alert, btn);
                              [alert cleanAllPenddingAlert];
                          }];
    }
    
    
    [alert setShowBlurBackground:NO];
    alert.cancelButtonFont = [UIFont systemFontOfSize:18.0f];
    alert.customButtonFont = [UIFont boldSystemFontOfSize:18.0f];
    alert.cancelButtonColor = [UIColor colorWithHexString:@"007aff" alpha:1.0f];
    alert.customButtonColor = [UIColor colorWithHexString:@"007aff" alpha:1.0f];
    
    [alert show];
    
    UILabel *contentLabel = (UILabel *)alert.contentView;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    
    [CXAlertView cleanAllAlertBesideThis:alert];
    
    return alert;
    
}

+ (void)cleanAllAlertBesideThis:(CXAlertView *)targetAlertView{
    NSMutableArray *alerts = [NSMutableArray array];
    for (CXAlertView *alert in [CXAlertView sharedQueue]) {
        if (![alert isEqual:targetAlertView]) {
            [alerts addObject:alert];
        }
    }
    
    while ([alerts count] > 0) {
        CXAlertView *firstAlert = [alerts firstObject];
        [firstAlert dismiss];
        [alerts removeObject:firstAlert];
    }
}
@end
