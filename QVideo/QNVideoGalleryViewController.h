//
//  QNVideoGalleryViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/11/25.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIPhotoGallery/UIPhotoGalleryView.h>
#import <QNAPFramework/QNAPCommunicationManager.h>
#import "QNVideosViewController.h"
#import <PopoverView/PopoverView.h>
#import "QNVideoStatusView.h"
#import "QNButton.h"

const static CGFloat space = 20.0f;
const static CGFloat videoItemWidth = 120.0f;
const static CGFloat headerHeight = 30.0f;


@interface QNVideoGalleryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, PopoverViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>{
    __block NSArray *_videoFileItems;
    NSMutableArray *_yearMonthItems;
    QNButton *_expendBtn;
}
@property (nonatomic, strong)id<QNVideoFetchingDelegate>fetchDelegate;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIView *sortView;
@property(nonatomic, weak)QNVideosViewController *parentPaneViewController;
@property(nonatomic) VideoFileHomePath homePath;
@property(nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) IBOutlet QNVideoStatusView *statusView;
- (void)sendRequestAndReload;
- (IBAction)playVideo:(id)sender;
- (IBAction)showActionItems:(id)sender;

@end
