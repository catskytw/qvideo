//
//  QNTimelineViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/3/26.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <QNAPFramework/QNVideoTimeLineResponse.h>
#import <QNAPFramework/QNVideoTimeLine.h>

#import "QNTimelineViewController.h"
#import "QNTimelineReusableView.h"
#import "QNAppDelegate.h"
#import "QNVideoTool.h"
#import "SKSTableView.h"
#import "SKSTableViewCell.h"

@interface QNTimelineViewController ()

@end

@implementation QNTimelineViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    _timelineTag = @"";

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _timelineTableView = [self generateSKSTable];
    [self generateTimelineData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return [[self.fetchResultsController sections] count];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    QNTimelineReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                          withReuseIdentifier:@"timelineSection"
                                                                 forIndexPath:indexPath];
        NSInteger videoCount = [self countOfVideosInSection:[indexPath section]];
        
        NSString *yearMonth = [self headerString:[indexPath section]];
        UIImage *imageBg = [UIImage imageNamed:@"timeline_bg_number.png"];
        UIImage *fixedImage = [imageBg resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
        [reusableview.bgImageView setImage:fixedImage];
        NSString *countString = (videoCount>99)?@"99+":[NSString stringWithFormat:@"%i", videoCount];
        CGSize textSize = [countString sizeWithAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0f]}];

        [reusableview.numberLabel setText:countString];
        [reusableview.dateLabel setText:yearMonth];
        
        [reusableview.bgImageView setFrame:CGRectMake(reusableview.bgImageView.frame.origin.x,
                                                      reusableview.bgImageView.frame.origin.y,
                                                      textSize.width + 20.0f,
                                                      reusableview.bgImageView.frame.size.height)];
        
        [reusableview.numberLabel setCenter:reusableview.bgImageView.center];
    }
    return reusableview;
}

- (void)updateVideoFileList{
    [self generateFetchedResultsController:@{@"sortKey":@"yearMonthDay,cFileName",
                                             @"isASC":@(NO),
                                             @"sortName":@"Title",
                                             @"groupBy":@"yearMonthDay",
                                             @"predicate": [NSPredicate predicateWithFormat:@"self.uuid==%@", self.thisQueryUUID]}];
    [self reloadContent];
}

- (NSInteger)countOfVideosInSection:(NSInteger)section{
    NSInteger r = 0;
    if ([[self.fetchResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchResultsController sections] objectAtIndex:section];
        r = [sectionInfo numberOfObjects] ;
    }
    return r;
}

- (NSString *)headerString:(NSInteger)section{
    NSString *r = nil;
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchResultsController sections] objectAtIndex:section];
    r = [sectionInfo name];
    return r;
}

#pragma mark - SKSTableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_timelines count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [((NSArray *)_timelines[section]) count];
}

- (NSInteger)tableView:(SKSTableView *)tableView numberOfSubRowsAtIndexPath:(NSIndexPath *)indexPath{
    return [((NSArray *)_timelines[indexPath.section][indexPath.row]) count] - 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"SKSTableViewCell";
    
    SKSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[SKSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.textLabel.text = _timelines[indexPath.section][indexPath.row][0];
    cell.expandable = ([cell.textLabel.text isEqualToString:NSLocalizedString(@"All", nil)])? NO:YES;
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForSubRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"UITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@", _timelines[indexPath.section][indexPath.row][indexPath.subRow]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (CGFloat)tableView:(SKSTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = [indexPath row];
    if (row == 0) {
        [_tmpAlert dismiss];
        _timelineTag = @"";
        [self sendRequestAndReload];
    }
}

- (void)tableView:(SKSTableView *)tableView didSelectSubRowAtIndexPath:(NSIndexPath *)indexPath{
    [_tmpAlert dismiss];
    _tmpAlert = nil;
    _timelineTag = _timelines[indexPath.section][indexPath.row][indexPath.subRow];
    [self sendRequestAndReload];
}

#pragma mark - PrivateMethod
- (void)selectTimeline:(id)sender{
    SKSTableView *myTimelineTable = [self generateSKSTable];
    _tmpAlert = [CXAlertView showContentViewAlert:NSLocalizedString(@"Timeline", nil)
                                  withActionTitle:nil
                                  withContentView:myTimelineTable
                                withActionHandler:nil];
}

- (SKSTableView *)generateSKSTable{
    SKSTableView *tableView = [[SKSTableView alloc] initWithFrame:CGRectMake(0, 0, 240, 200) style:UITableViewStylePlain];
    tableView.SKSTableViewDelegate = self;

    return tableView;
}

- (void)reloadVideoFiles{
    [self generateTimelineData];
    [super reloadVideoFiles];
}

- (void)sendRequestAndReload{
    //success & failure block
    [self hideDataView];
    [self.statusView setVideoStatus:QNVideoStatusTransfering];
    QNQNVideoFileListSuccessBlock successBlock = ^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoFileList *obj){
        [self updateVideoFileList];
        if ([self collectionView:self.collectionView numberOfItemsInSection:0] == 0) {
            [self.statusView setVideoStatus:[self.parentPaneViewController noItemStatus]];
        }else{
            [self showDataView];
        }
    };
    
    QNFailureBlock failureBlock = ^(RKObjectRequestOperation *o, NSError *e){
        DDLogError(@"error while fetching videoList %@", e);
        [self.statusView setVideoStatus:QNVideoStatusError];
    };
    
    self.thisQueryUUID = [[NSUUID UUID] UUIDString];
    if (self.outsideFetchingBlock) {
        self.outsideFetchingBlock(self.thisQueryUUID, successBlock, failureBlock);
    }else if ([self.fetchDelegate respondsToSelector:@selector(fetchingRequest:withOriginContentViewController:withInfoDic:withSuccessBlock:withFailureBlock:)]){
        [self.fetchDelegate fetchingRequest:self.parentPaneViewController.homePath
            withOriginContentViewController:self
                                withInfoDic:@{@"uuid":self.thisQueryUUID, @"timeline":_timelineTag, @"filterDic":(_filterDic)?_filterDic:@{}}
                           withSuccessBlock:successBlock
                           withFailureBlock:failureBlock];
    }else
        [self showDataView];
}

- (void)generateTimelineData{
    [QNVideoTimeLineResponse MR_truncateAll];
    [QNVideoTimeLine MR_truncateAll];
    
    QNVideoStationAPIManager *videoManager = [QNAPCommunicationManager share].videoStationManager;
    [videoManager getTimeLineListWithHomePath:self.parentPaneViewController.homePath
                             withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoTimeLineResponse *obj){
                                 _timelines = [self generateAllSKSTableViewData:obj];
                             }
                             withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                 DDLogError(@"timeline error: %@", e);
                             }];
    
}

- (NSArray *)generateAllSKSTableViewData:(QNVideoTimeLineResponse *)response{
    NSMutableArray *returnArray = [NSMutableArray array];

    NSArray *allTimelines = [response.relationship_timeLine allObjects];
    NSArray *allYears = [allTimelines valueForKeyPath:@"@distinctUnionOfObjects.year"];
    
    NSArray *sortAllYears = [allYears sortedArrayUsingComparator:^(id obj1, id obj2){
        NSComparisonResult r = NSOrderedSame;
        NSComparisonResult result = [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
        r = (result == NSOrderedAscending)?NSOrderedDescending: NSOrderedAscending;
        return r;
    }];
    for (NSString *year in sortAllYears) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.year == %@", year];
        NSArray *allTimelineInYear = [[allTimelines filteredArrayUsingPredicate:predicate] valueForKeyPath:@"yearMonth"];
        NSArray *sortAllTimelineInYear = [allTimelineInYear sortedArrayUsingComparator:^(id obj1, id obj2){
            NSComparisonResult r = NSOrderedSame;
            NSComparisonResult result = [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
            r = (result == NSOrderedAscending)?NSOrderedDescending: NSOrderedAscending;
            return r;
        }];
        NSMutableArray *timelineArray =[NSMutableArray arrayWithArray: sortAllTimelineInYear];
        [timelineArray insertObject:year atIndex:0];
        [returnArray addObject:timelineArray];
    }
    [returnArray insertObject:@[NSLocalizedString(@"All", nil)] atIndex:0];
    return @[returnArray];
}

#pragma mark - reloadContent
- (void)reloadContent{
    [super reloadContent];
}
@end
