//
//  QNSettingPopoverTableView.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/3.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNSettingPopoverTableView : UITableView <UITableViewDataSource, UITableViewDelegate>{
    void(^_completeBlock)(NSInteger row);
}
@property (nonatomic, strong) NSArray *tableData; //NSString

- (id)initWithFrame:(CGRect)frame withData:(NSArray *)data withSelectedAction:(void(^)(NSInteger row))complete;

@end
