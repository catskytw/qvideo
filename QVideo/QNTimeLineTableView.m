//
//  QNTimeLineTableView.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/19.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNTimeLineTableView.h"

@implementation QNTimeLineTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style timeLineData:(NSArray *)timeLines{
    self = [super initWithFrame:frame style:style];
    if (self) {
        self.timeLineItems = timeLines;
    }
    return self;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.timeLineItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"timeLineCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"timeLineCell"];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:12.0f]];
    }
    [cell.textLabel setText:(NSString *)[self.timeLineItems objectAtIndex:[indexPath row]]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 22.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger scrollToTargetSection = [indexPath row];
    [self.parentViewController.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:scrollToTargetSection]
                                               atScrollPosition:UITableViewScrollPositionTop
                                                       animated:YES];
}

@end
