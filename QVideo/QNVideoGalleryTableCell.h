//
//  QNVideoGalleryTableCell.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/20.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNButton.h"
@interface QNVideoGalleryTableCell : UITableViewCell
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *videoDurations;
@property (weak, nonatomic) IBOutlet UILabel *timeLine;
@property (strong, nonatomic) IBOutletCollection(QNButton) NSArray *playBtns;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *videoThumbnails;
@property (weak, nonatomic) IBOutlet UIImageView *numberBgImageView;
@property (weak, nonatomic) IBOutlet UILabel *numberOfVideosLabel;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *hdIndicators;
@property (weak, nonatomic) IBOutlet QNButton *seeMoreBtn;

@property (strong, nonatomic) NSMutableArray *videoItems;
@end
