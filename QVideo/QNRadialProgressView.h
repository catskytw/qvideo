//
//  QNRadialProgressView.h
//  QVideo
//
//  Created by Change.Liao on 2014/2/12.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <MDRadialProgress/MDRadialProgressView.h>
#import <MDRadialProgress/MDRadialProgressLabel.h>
@interface QNRadialProgressView : MDRadialProgressView
@property (nonatomic, strong) UIColor *innerCircleColor;
@property (nonatomic) CGFloat innerCircleWidth;
@property (nonatomic, strong) UIImageView *playPauseImage;

- (void)settingPlayPauseImage:(BOOL)isRunning;
- (void)settingTextLabel;

@end
