//
//  QNAboutViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/4/25.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNAboutViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, strong) IBOutlet UILabel *versionLabel;
- (IBAction)gotoFeedback:(id)sender;
- (IBAction)gotoQNAPWeb:(id)sender;

@end
