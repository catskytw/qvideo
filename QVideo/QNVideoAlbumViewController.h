//
//  QNVideoAlbumViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/4/1.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoThumbnailViewController.h"

@interface QNVideoAlbumViewController : QNVideoThumbnailViewController
@property(nonatomic, strong) NSString *albumID;
- (IBAction)gotoCollectionItems:(id)sender;

- (void)createCollectionProcess;
@end
