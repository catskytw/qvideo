//
//  QNVideoItemCell.m
//  QVideo
//
//  Created by Change.Liao on 2013/11/26.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoItemCell.h"

@implementation QNVideoItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.videoItems = [NSMutableArray new];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
