//
//  QNLocalVideoFileListCell.m
//  QVideo
//
//  Created by Change.Liao on 2014/1/8.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNLocalVideoFileListCell.h"
#import <MDRadialProgress/MDRadialProgressLabel.h>
#import "QNVideoTool.h"

@implementation QNLocalVideoFileListCell
- (void)changeDownloadProgress:(NSNotification *)nofi{
    NSDictionary *dic = nofi.userInfo;
    if ([[dic valueForKey:@"fID"] isEqualToString:self.fID]) {
        NSUInteger progress = (int) round([[dic valueForKey:@"downloadProgress"] floatValue]);
        //TODO maybe bug here?
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSTimeInterval thisTimeInterval = [[NSDate date] timeIntervalSince1970];
            NSTimeInterval detal = thisTimeInterval - self.lastTimeStamp;
            if (detal > 1.0f) {
                self.progressView.progressCounter = (progress==0)?1:progress;
                [self.progressView.label setText:[NSString stringWithFormat:@"%i", progress]];
                CGFloat caculateValue = self.tmpCount / self.tmpInvoke;
                [self.sizeLabel setText:[NSString stringWithFormat:@"%@/s" ,[QNVideoTool transformedValue:@(caculateValue)]]]; //set the downloadRate in the size label
                self.lastTimeStamp = thisTimeInterval;
                self.tmpCount = 0.0f;
                self.tmpInvoke = 0.0f;
            }else{
                self.tmpCount += [[dic valueForKey:@"downloadRate"] floatValue];
                self.tmpInvoke ++;
            }
        });
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadProgress" object:nil];
}
@end
