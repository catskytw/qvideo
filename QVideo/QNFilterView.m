//
//  QNFilterView.m
//  QVideo
//
//  Created by Change.Liao on 2014/7/16.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNFilterView.h"

@implementation QNFilterView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)tableStyle isCollection:(BOOL)isCollection withActionBlock:(void(^)(NSInteger actionIndex))action{
    if (self = [self initWithFrame:frame style:tableStyle]){
        self.actionBlock = action;
        _isCollection = isCollection;
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (!_options) {
        _options = (_isCollection)?
        @[NSLocalizedString(@"Collection", nil), NSLocalizedString(@"Clear", nil)]:
        @[NSLocalizedString(@"Title", nil),
          NSLocalizedString(@"Tag", nil),
          NSLocalizedString(@"Date", nil),
          NSLocalizedString(@"Rating", nil),
          NSLocalizedString(@"ColorLabel", nil),
          NSLocalizedString(@"Clear", nil)];
    }
    return [_options count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"filterOption"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"filterOption"];
    }
    NSInteger row = [indexPath row];
    [cell.textLabel setText: [_options objectAtIndex:row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.actionBlock([indexPath row]);
}

@end
