//
//  QNMoviePlayerViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/1/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

//ChromecastDeviceSearched

#import "QNMoviePlayerViewController.h"
#import <QNAPFramework/VLCConstants.h>
#import <QNAPFramework/QNVideoStationAPIManager.h>
#import <QNAPFramework/QNAPCommunicationManager.h>
#import <QNAPFramework/QNVideoRenderList.h>

#import <QNAPFramework/QNLogin.h>

#import "NSString_TimeDuration.h"
#import "VLCMovieViewController+Extend.h"
#import "UIView+QVideoExt.h"
#import "QNResolutionTableViewController.h"
#import "QNVideoTool.h"

#define SETTING_ARRAY @[NSLocalizedString(@"Quality", nil), NSLocalizedString(@"Subtitle", nil), NSLocalizedString(@"AudioTrack", nil)]

@interface VLCMovieViewController()
- (void)idleTimerExceeded;
- (void)_stopPlayback;
@end

@interface QNMoviePlayerViewController (){
    BOOL isShowingLandscapeView;
    UITapGestureRecognizer *_tapOnVideoRecognizer;
    BOOL isPresent;
}
@end

@implementation QNMoviePlayerViewController
+ (void)initialize {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *appDefaults = @{
                                  kVLCShowRemainingTime : @(NO)
                                  };
    [defaults registerDefaults:appDefaults];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.currentResolution = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"resolution"];

    playerMode = QNVideoLocalPlayerMode;
    [self toggleControlsVisible];
    actionType = ActionSheetForNothing;
    
    _tapOnVideoRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleControlsVisible)];
    _tapOnVideoRecognizer.delegate = self;
    [self.controlView addGestureRecognizer:_tapOnVideoRecognizer];
    
    self.isVideoPlaying = YES;
    [self addObserver:self forKeyPath:@"isVideoPlaying" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(volumeChanged:) name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startStreamVideoToGooglecast) name:@"GCChannelReady" object:nil];

}

- (void)dealloc{
    [self removeObserver:self forKeyPath:@"isVideoPlaying"];
}

- (void)volumeChanged:(NSNotification *)notification
{
    float volume =
    [[[notification userInfo]
      objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"]
     floatValue];
    self.volumnPresentSlider.value = volume;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    if ([keyPath isEqualToString:@"isVideoPlaying"]) {
        [self playPauseBtnImage];
    }
    [self.remainTimeLabel setText:[[_mediaPlayer time] stringValue]];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.title = @"";
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithHexString:@"#FFFFFF" alpha:0.5]];

    [self.navigationController setNavigationBarHidden:NO];
    isPresent = YES;
    
    if (currentPlayTime) {
        _mediaPlayer.time = currentPlayTime;
    }

    [self.parentPaneViewController setPaneDragRevealEnabled:NO forDirection:MSDynamicsDrawerDirectionLeft];
    [self.volumeView setHidden:YES];
    [self playPauseBtnImage];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[UINavigationBar appearance] setBarTintColor:nil];

    [self settingAllLayout:isLandscape];
}

- (void)viewWillDisappear:(BOOL)animated{
    currentPlayTime = _mediaPlayer.time;
    [super viewWillDisappear:animated];
    [self.parentPaneViewController setPaneDragRevealEnabled:YES forDirection:MSDynamicsDrawerDirectionLeft];

    isPresent = NO;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[UINavigationBar appearance] setBarTintColor:nil];
}
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setControlsHidden:(BOOL)hidden animated:(BOOL)animated {
    [super setControlsHidden:hidden animated:animated];
    if (!hidden) {
        self.controlView.hidden = hidden;
        self.proceedView.hidden = hidden;
        self.controllerPanel.hidden = hidden;
    }
    CGFloat alpha = hidden ? 0.0f : 1.0f;
    NSTimeInterval animationDuration = animated ? 0.3 : 0.0;

    [UIView animateWithDuration:animationDuration animations:^(){
        self.controlView.alpha = alpha;
        self.proceedView.alpha = alpha;
        self.controllerPanel.alpha = alpha;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             self.controlView.hidden = hidden;
                             self.proceedView.hidden = hidden;
                             self.controllerPanel.hidden = hidden;
                         }
                     }];

}

- (IBAction)backPriviousViewController:(id)sender{
//    self.navigationController.navigationBar.barTintColor = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (touch.view != self.view && ![touch.view isKindOfClass:[QNMovieControlView class]])
        return NO;
    
    return YES;
}

- (void)idleTimerExceeded {
    if (isPresent) {
        [super idleTimerExceeded];
    }
}

- (void)showRenderNearNAS:(id)sender{
    UIPickerView *renderPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
    renderPickerView.delegate = self;
    renderPickerView.dataSource = self;
    
    [_mediaPlayer pause];
    
    _renders = [QNVideoRender MR_findAll];
    NSString *appVersion = [QNAPCommunicationManager share].videoStationManager.loginInfo.appVersion;
    NSString *firmwareVersion = [QNAPCommunicationManager share].videoStationManager.loginInfo.builtinFirmwareVersion;
    if ([appVersion floatValue] > 2.0 && [firmwareVersion floatValue] > 4.1) {
        if ([_renders count] == 0) {
            [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil) withActionTitle:nil withMessage:NSLocalizedString(@"NoRender", nil) withActionHandler:nil];
        }else{
            [CXAlertView showContentViewAlert:NSLocalizedString(@"SelectRender", nil)
                              withActionTitle:NSLocalizedString(@"Done", nil)
                              withContentView:renderPickerView
                            withActionHandler:^(CXAlertView *a, CXAlertButtonItem *i){
                                [_mediaPlayer pause];
                                
                                UIPickerView *picker = (UIPickerView *)[a contentView];
                                QNVideoRender *targetRender = [_renders objectAtIndex:[picker selectedRowInComponent:0]];
                                [self executeMultiZone:targetRender];
                                [a dismiss];
                            }];
        }
    }else{
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil) withActionTitle:nil withMessage:[NSString stringWithFormat:NSLocalizedString(@"NotSupported", nil), firmwareVersion, appVersion] withActionHandler:nil];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"enterResolutionTable"]) {
        QNResolutionTableViewController *destinationViewController = (QNResolutionTableViewController *)[segue destinationViewController];
        destinationViewController.thisFilmItem = self.thisVideoFileItem;
        destinationViewController.playerViewController = self;
        self.isVideoPlaying = NO;
        [_mediaPlayer pause];
        [self titleInStatus:_mediaPlayer.state];
    }
}
#pragma mark - UIPickerDelegate & DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    QNVideoRender *render = [_renders objectAtIndex:row];
    return render.deviceName;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_renders count];
}

#pragma mark - IBActionSheetDelegate
- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    [actionSheet.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)obj;
            button.titleLabel.font = [UIFont systemFontOfSize:15];
            [button.titleLabel setTextColor:[UIColor blackColor]];

            if ([actionSheet.title isEqualToString:NSLocalizedString(@"ChooseDevice", nil)]) {
                if ([button.titleLabel.text isEqualToString:NSLocalizedString(@"ThisDevice", nil)]) {
                    if (playerMode == QNVideoLocalPlayerMode) 
                        [button.titleLabel setTextColor: [UIColor blueColor]];
                }else if (playerMode == QNVideoGooglecastPlayerMode && ![button.titleLabel.text isEqualToString:NSLocalizedString(@"Cancel", nil)]){
                    GCKDevice *device = [[QNVideoTool share].chromecast.onlineDevices objectAtIndex:idx - 3];
                    if ([button.titleLabel.text isEqualToString:device.friendlyName])
                        [button.titleLabel setTextColor: [UIColor blueColor]];
                }
            }
        }
    }];
    
}
-(void)actionSheet:(UIActionSheet *)_actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (actionType) {
        case ActionSheetForGooglecast:
            [self actionSheetForGooglecast:buttonIndex];
            break;
        case ActionSheetForMultizone:
            [self actionSheetForMultiZone:buttonIndex];
            break;
        case ActionSheetForSetting:
            [self actionSheetForSetting:_actionSheet withButtonIndex:buttonIndex];
            break;
        case ActionSheetForNothing:
            [super actionSheet:(UIActionSheet *)_actionSheet clickedButtonAtIndex:buttonIndex];
            break;
        default:
            break;
    }
    actionType = ActionSheetForNothing;
//    [self syncFrame];
}

- (void)startStreamVideoToGooglecast{
    NSString *videoURL = [QNVideoTool generateGetVideoURL:self.thisVideoFileItem.f_id isDownload:NO];
    NSString *thumbnailURL = [NSString stringWithFormat:@"%@video/api/thumb.php?s=1&m=display&f=%@&t=video&o=1&sid=%@", [QNAPCommunicationManager share].videoStationManager.baseURL, self.thisVideoFileItem.f_id, [QNAPCommunicationManager share].sidForMultimedia];
    //videoTitle(NSString), imageURL(NSString), videoSubtitle(NSString), duration(CGFloat), imageWidth(CGFloat), imageHeight(CGFloat)
    [[QNVideoTool share].chromecast castVideo:videoURL
                                startPosition:[currentPlayTime.value floatValue]
                                  withInfoDic:@{@"videoTitle":self.thisVideoFileItem.cPictureTitle,
                                                @"videoSubtitle":self.thisVideoFileItem.comment,
                                                @"imageURL":thumbnailURL,
                                                @"duration":self.thisVideoFileItem.duration,
                                                @"imageWidth":self.thisVideoFileItem.iWidth,
                                                @"imageHeight":self.thisVideoFileItem.iHeight}];    
    [_mediaPlayer stop];
}

- (void)actionSheetForGooglecast:(NSInteger)buttonIndex{
    NSInteger lastIndex = [[QNVideoTool share].chromecast.onlineDevices count] + 1;
    if (buttonIndex == 0) {
        playerMode = QNVideoLocalPlayerMode;
    }else if (buttonIndex == lastIndex){
        //TODO cancel
    }else{
        playerMode = QNVideoGooglecastPlayerMode;

        GCKDevice *targetDevice = [[QNVideoTool share].chromecast.onlineDevices objectAtIndex:buttonIndex - 1];
        [[QNVideoTool share].chromecast connectToDevice:targetDevice];
    }
}

- (void)actionSheetForMultiZone:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:{
            [self showRenderNearNAS:nil];
        }
            break;
        case 1:
        default:
            break;
    }
}

- (void)actionSheetForSetting:(UIActionSheet *)actionSheet withButtonIndex:(NSInteger)buttonIndex{

    NSString *btnString = [actionSheet buttonTitleAtIndex:buttonIndex];
    NSArray *compareArray = SETTING_ARRAY;
    
    NSInteger count = 0;
    for (NSString *targetTitle in compareArray) {
        if ([targetTitle isEqualToString:btnString])
            break;
        count ++;
    }
    switch (count) {
        case 0:{
            [self performSegueWithIdentifier:@"enterResolutionTable" sender:nil];
        }
            break;
        case 1:{
            if ([_mediaPlayer.videoSubTitlesNames count] == 0) {
                [CXAlertView showMessageAlert:NSLocalizedString(@"Error", nil)
                              withActionTitle:nil
                                  withMessage:NSLocalizedString(@"NoSubtitles", nil)
                            withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){}];
            }else
                [self switchSubtitleTrack:nil];
        }
            break;
        case 2:{
            if ([_mediaPlayer.audioTrackNames count] == 0) {
                [CXAlertView showMessageAlert:NSLocalizedString(@"Error", nil)
                              withActionTitle:nil
                                  withMessage:NSLocalizedString(@"NoAudioTrack", nil)
                            withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){}];
            }else
                [self switchAudioTrack:nil];
        }
            break;
        case 3:
        default:
            break;
    }
}
#pragma mark - Rotation
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
//    [UIView animateWithDuration:0.25f animations:^{
//        [self.controlView setAlpha:0.0f];
//    }];
}

#pragma mark - LoadingRotationView
- (void)toggleControlsVisible {
    [super toggleControlsVisible];
}

- (IBAction)volumeSliderChangeAction:(id)sender{
    if (playerMode == QNVideoMultiZoneMode) {
        QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
        [videoStation setVolume:targetPlayRender.deviceId
                         volume:self.volumnPresentSlider.value
               withSuccessBlock:nil
               withFailureBlock:nil];
    }else
        self.volumnSlider.value = self.volumnPresentSlider.value;
}

- (void)executeMultiZone: (QNVideoRender *)targetRender{
    [self _stopPlayback];

    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    [videoStation playContent:targetRender.deviceId
                        withFileId:@[self.thisVideoFileItem.f_id]
                  withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                      [self enableMultizoneComponent:YES];
                      
                      playerMode = QNVideoMultiZoneMode;
                      [QNVideoTool toastWithTargetViewController:self
                                                        withText:[NSString stringWithFormat: NSLocalizedString(@"MultiZoonSuccess", nil), targetRender.deviceName]
                                                       isSuccess:YES];
                      
                  }
                  withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                      [QNVideoTool toastWithTargetViewController:self
                                                        withText:[NSString stringWithFormat: NSLocalizedString(@"MultiZoonFail", nil), targetRender.deviceName]
                                                       isSuccess:NO];
                  }];
    
}

- (void)enableMultizoneComponent:(BOOL)enable{
    self.multiZoneBg.hidden = !enable;
    self.volumnPresentSlider.hidden = !enable;
    self.showVolumnBtn.hidden = !enable;
}

- (IBAction)showVolumnOrNot:(id)sender{
    self.volumeContainerView.hidden = !self.volumeContainerView.hidden;
}

- (void)showMoreAction:(id)sender{
    actionType = ActionSheetForMultizone;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"MultiZone", nil),nil];
    [actionSheet setCancelButtonIndex:1];
    [actionSheet showInView:self.view];
}
#pragma mark - Notification
- (void)mediaPlayerStateChanged:(NSNotification *)aNotification {
//    [super mediaPlayerStateChanged:aNotification];
    [self titleInStatus:_mediaPlayer.state];
}

#pragma mark - Setting
- (IBAction)setting:(id)sender{
    actionType = ActionSheetForSetting;
    NSMutableArray *options = [NSMutableArray array];
    
    [options addObject:NSLocalizedString(@"Quality", nil)];
    if ([_mediaPlayer.videoSubTitlesNames count] > 0) {
        [options addObject:NSLocalizedString(@"Subtitle", nil)];
    }
    if ([_mediaPlayer.audioTrackNames count] > 0) {
        [options addObject: NSLocalizedString(@"AudioTrack", nil)];
    }
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    for (NSString *title in options)
        [actionSheet addButtonWithTitle:title];
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [actionSheet setCancelButtonIndex:[options count]];
    [actionSheet showInView:self.view];
}

- (void)settingAllLayout:(BOOL)_isLandscape{
    self.toolbar = self.navigationController.navigationBar;
    [self.titleLabel setText:self.thisVideoFileItem.cPictureTitle];
    NSString *duration = [NSString stringFromTimeInterval:[self.thisVideoFileItem.duration doubleValue]];
    [self.totalTime setText:duration];
    [self settingSliderBar];
    [self settingNavi];
    
    [QNVideoTool generateBlackLeftBarItem:self withSelect:@selector(backPriviousViewController:)];
//    [QNVideoTool generateBlackRightBarItem:self withSelect:@selector(showMoreAction:)];
    [self generateGooglecastAndAirplay];
}

- (void)generateGooglecastAndAirplay{
    NSMutableArray *rightActionItems = [NSMutableArray array];
    if ([[QNVideoTool share].chromecast.onlineDevices count] > 0) {
        UIBarButtonItem *chromecast = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"cast_off"] style:UIBarButtonItemStylePlain target:self action:@selector(showGooglecastDevice)];
        [rightActionItems addObject:chromecast];
    }
    [self.navigationItem setRightBarButtonItems:rightActionItems];
}

- (void)showGooglecastDevice{
    actionType = ActionSheetForGooglecast;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"ChooseDevice", nil)
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"ThisDevice", nil)];
    NSInteger count = 1;
    for (GCKDevice *device in [QNVideoTool share].chromecast.onlineDevices){
        [actionSheet addButtonWithTitle:device.friendlyName];
        [[[actionSheet valueForKey:@"_buttons"] objectAtIndex:count] setImage:[UIImage imageNamed:@"cast_off"] forState:UIControlStateNormal];
        count ++;
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [actionSheet setCancelButtonIndex:[[QNVideoTool share].chromecast.onlineDevices count]];
    [actionSheet showInView:self.view];
}

- (void)settingSliderBar{
    UIImage *thumbImage = [UIImage imageNamed:@"icon_playback_time-line-point.png"];
    [self.positionSlider setThumbImage:thumbImage forState:UIControlStateNormal];
    [self.positionSlider setThumbImage:thumbImage forState:UIControlStateHighlighted];
    
    UIImage *tmpImage = [UIImage imageNamed:@"icon_playback_volume_open_point.png"];
    [self.volumnPresentSlider setThumbImage:tmpImage forState:UIControlStateNormal];
    [self.volumnPresentSlider setThumbImage:tmpImage forState:UIControlStateHighlighted];

    for (id aView in self.volumeView.subviews) {
        if ([[[aView class] description] isEqualToString:@"MPVolumeSlider"]) {
            self.volumnSlider = (UISlider *) aView;
            
            self.volumnPresentSlider.value = self.volumnSlider.value;
            break;
        }
    }
    self.volumeView.hidden = YES;
    self.volumnSlider.hidden = YES;
    //just in case.
    [self.volumnSlider setFrame:CGRectOffset(self.volumnSlider.frame, 10000, 10000)];
}

- (void)settingNavi{
    [self.navigationItem setTitle:NSLocalizedString(@"NowPlaying", nil)];
}

#pragma mark - Action

- (IBAction)closePlayback:(id)sender {
    self.isVideoPlaying = NO;
    [self setControlsHidden:NO animated:NO];
    [_mediaPlayer stop];
    [self.positionSlider setValue:0.0f];
    [self titleInStatus:VLCMediaPlayerStateStopped];
}

#pragma mark - PlayPause
- (IBAction)playPause{
    switch (playerMode) {
        case QNVideoLocalPlayerMode:{
            [self localPlayerPlayPauseAction];
        }
            break;
        case QNVideoGooglecastPlayerMode:{
            [self googlecastPlayPauseAction];
        }
            break;
        default:
            break;
    }
}

- (void)localPlayerPlayPauseAction{
    if ([self isVideoPlaying]){
        [_mediaPlayer pause];
        self.isVideoPlaying = NO;
    }
    else{
        [_mediaPlayer play];
        self.isVideoPlaying = YES;
    }
    [self titleInStatus:_mediaPlayer.state];

}

- (void)googlecastPlayPauseAction{
    QNChromecastTool *googlecast = [QNVideoTool share].chromecast;
    if ([googlecast playerStatus].playerState == GCKMediaPlayerStatePlaying || [googlecast playerStatus].playerState == GCKMediaPlayerStateBuffering) {
        self.isVideoPlaying = NO;
        [googlecast devicePause];
    }else{
        self.isVideoPlaying = YES;
        [googlecast devicePlay];
    }
}
- (void)titleInStatus:(VLCMediaPlayerState)status{
    switch (status) {
        case VLCMediaPlayerStatePlaying:
        case VLCMediaPlayerStateOpening:
        case VLCMediaPlayerStateBuffering:{
            [self.navigationItem setTitle:(self.isVideoPlaying)?NSLocalizedString(@"NowPlaying", nil):NSLocalizedString(@"Pause", nil)];
        }
            break;
        case VLCMediaPlayerStatePaused:
            [self.navigationItem setTitle:NSLocalizedString(@"Pause", nil)];
            break;
        default:
            [self.navigationItem setTitle:NSLocalizedString(@"StopPlaying", nil)];
            break;
    }
}

- (void)playPauseBtnImage{
    if ([self isVideoPlaying]){
        [self.playPauseButton setImage:[UIImage imageNamed:@"btn_icon_playback_pause_normal.png"] forState:UIControlStateNormal];
        [self.playPauseButton setImage:[UIImage imageNamed:@"btn_icon_playback_pause_pressed.png"] forState:UIControlStateHighlighted];
    }else{
        [self.playPauseButton setImage:[UIImage imageNamed:@"btn_icon_playback_play_normal.png"] forState:UIControlStateNormal];
        [self.playPauseButton setImage:[UIImage imageNamed:@"btn_icon_playback_play_pressed.png"] forState:UIControlStateHighlighted];
    }
}

- (void)_stopPlayback {
    switch (playerMode) {
        case QNVideoLocalPlayerMode:
            [super _stopPlayback];
            break;
        case QNVideoGooglecastPlayerMode:{
            QNChromecastTool *chromecast = [QNVideoTool share].chromecast;
            [chromecast deviceStop];
        }
            break;
        default:
            break;
    }
}
#pragma mark - Volume
- (IBAction)volumeIncrease:(id)sender{
    if (self.volumnSlider.value < self.volumnSlider.maximumValue)
        self.volumnSlider.value += 1.0f;
}

- (IBAction)volumeDecrease:(id)sender{
    if (self.volumnSlider.value > self.volumnSlider.minimumValue)
        self.volumnSlider.value -= 1.0f;
}

@end
