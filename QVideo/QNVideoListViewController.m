//
//  QNVideoListViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/24.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

/**
 Note: 
 In fact, we need to give a subclass which inherits the layouts and connections from the storyboard, which
 could save our time and maintain effort.
 Unfortunately, iOS don't let us do that, suck really!
 
 Thus, we write the logic for some perposes of each individual subclass in this generic class. A worst 
 design practice I have never written beside this. orz
 */

#import "QNVideoListViewController.h"
#import <QNAPFramework/QNVideoFileItem.h>
#import <QNAPFramework/QNAPCommunicationManager.h>
#import <QNAPFramework/QNVideoFileList.h>

#import "QNVideoListCell.h"
#import "NSString_TimeDuration.h"
#import "QNVideoTool.h"
#import "QNVideoItemDetailViewController.h"
#import <QNAPFramework/VLCMovieViewController.h>
#import "QNMoviePlayerViewController.h"
#import <TSMessages/TSMessage.h>
#import "UIView+QVideoExt.h"
#import "ASViewController.h"

@interface QNVideoListViewController (TrashCan)
@end

@implementation QNVideoListViewController

- (void)viewDidLoad{
    isSelectMode = self.parentPaneViewController.isSelectedMode;
    [super viewDidLoad];
    _previousTitle = self.title;
    selections = [NSMutableArray array];
    _filterDic = @{};
    if(!self.navigationItem.leftBarButtonItem)
        [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
    [QNVideoTool generateRightBarItem:self withSelect:@selector(rightPanelAction:)];
    self.sortingView.parentViewController = self;
    self.statusView.delegate = self.parentPaneViewController;
    self.fetchDelegate = self.parentPaneViewController;
    [self customSetting];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self sendRequestAndReload];
    
    NSString *sortString = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"SortBy", nil),NSLocalizedString(@"Title", nil)];
    [self.sortingView.sortingLabel setText:sortString];

    [self.parentPaneViewController addObserver:self
                                    forKeyPath:@"isSelectedMode"
                                       options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                                       context:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.parentPaneViewController.isSelectedMode = NO;
    [self reloadContent];
    self.parentPaneViewController.videoDataSource = self;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.parentPaneViewController setPaneState:MSDynamicsDrawerPaneStateClosed animated:YES allowUserInterruption:YES completion:^(void){
    }];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.parentPaneViewController.isSelectedMode = NO;
    [selections removeAllObjects];
}

- (void)dealloc{
    [self.parentPaneViewController removeObserver:self forKeyPath:@"isSelectedMode" context:nil];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendRequestAndReload{
    [self hideDataView];
    //success & failure block
    QNQNVideoFileListSuccessBlock successBlock = ^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoFileList *obj){
        [self generateFetchedResultsController:@{@"sortKey":@"cFileName",
                                                 @"isASC":@(YES),
                                                 @"sortName":@"Title",
                                                 @"predicate":[NSPredicate predicateWithFormat:@"self.uuid == %@", self.thisQueryUUID]}];
        [self reloadContent];
        if ([self.tableView numberOfRowsInSection:0] > 0) {
            [self showDataView];
        }else
            [self.statusView setVideoStatus:[self.parentPaneViewController noItemStatus]];
    };
    
    QNFailureBlock failureBlock = ^(RKObjectRequestOperation *o, NSError *e){
        DDLogError(@"error while fetching videoList %@", e);
        [self.statusView setVideoStatus:QNVideoStatusError];
    };
    
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.statusView setVideoStatus:QNVideoStatusTransfering];
    self.thisQueryUUID = [[NSUUID UUID] UUIDString];

    if (self.outsideFetchingBlock) {
        self.outsideFetchingBlock(self.thisQueryUUID, successBlock, failureBlock);
    }else if ([self.fetchDelegate respondsToSelector:@selector(fetchingRequest:withOriginContentViewController:withInfoDic:withSuccessBlock:withFailureBlock:)]){
            [self.fetchDelegate fetchingRequest:self.parentPaneViewController.homePath
                withOriginContentViewController:self
                                    withInfoDic:@{@"uuid":self.thisQueryUUID, @"timeline":@"", @"filterDic":(_filterDic)?_filterDic:@{}}
                               withSuccessBlock:successBlock
                               withFailureBlock:failureBlock];
    }
}

- (void)leftPanelAction:(id)sender{
    [self.parentPaneViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:NO completion:nil];
}

- (void)rightPanelAction:(id)sender{
    [self.parentPaneViewController topRightDropdownMenuAction:self];
}

- (void)customSetting{
    DDLogVerbose(@"customSetting in ListViewController");
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"isSelectedMode"]) {
        DDLogVerbose(@"new key:%@", [change objectForKey:NSKeyValueChangeNewKey]);
        isSelectMode = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
        [self reloadContent];
        
        if (isSelectMode) {
            [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(closeMultiSelection:) withTitleText:NSLocalizedString(@"Cancel", nil)];
        }else{
            [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
            [selections removeAllObjects];
        }
        [self.tableView reloadData];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    return !isSelectMode;
}

- (void)backNavi{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)closeMultiSelection:(id)sender{
    self.parentPaneViewController.isSelectedMode = NO;
    [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects] ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"videoListCell";
    QNVideoListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell withIndexPath:indexPath];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *r = nil;
    if (section == 0) {
        r = self.sortingView;
    }
    return r;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //You should not show this view while selection mode
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 74.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return self.bottomBar;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return (self.parentPaneViewController.isSelectedMode)?self.bottomBar.frame.size.height: 0.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isSelectMode) {
        QNVideoFileItem *thisItem = [self.fetchedResultsController objectAtIndexPath: indexPath];

        BOOL hasSelect = [selections containsObject:thisItem.f_id];
        (hasSelect)?[selections removeObject:thisItem.f_id]:[selections addObject:thisItem.f_id];
        
        //refresh cell
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        [self settingNativationTitle];
        [self settingToolbarItem];
    }
}

- (void)settingNativationTitle{
    if (!self.parentPaneViewController.isSelectedMode) {
        self.title = _previousTitle;
    }else
        self.title = [NSString stringWithFormat:@"%i %@", [selections count], NSLocalizedString(@"VideosSelected", nil)];
}
#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"videoDetail"]) {
        QNVideoItemDetailViewController *targetViewController = (QNVideoItemDetailViewController *)segue.destinationViewController;
        targetViewController.videoItem = ((QNVideoListCell *)sender).playBtn.userInfo;
        targetViewController.title = targetViewController.videoItem.cPictureTitle;
        [selections removeAllObjects];
        [selections addObject:targetViewController.videoItem.f_id];
        targetViewController.parentVideoViewController = self.parentPaneViewController;
    }else if ([segue.identifier isEqualToString:@"playVideo"] || ([segue.identifier isEqualToString:@"playMovie"])){
        QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
        DDLogVerbose(@"sid %@", communicationManager.sidForMultimedia);
        QNVideoFileItem *item = (QNVideoFileItem *)((QNButton *)sender).userInfo;
        //+ (NSString *)generateGetVideoURL:(NSString *)f_id isDownload:(BOOL)isDownload;

        NSString *url = [QNVideoTool generateGetVideoURL:item.f_id isDownload:NO];
        QNMoviePlayerViewController *movieController = (QNMoviePlayerViewController *)segue.destinationViewController;
        movieController.parentPaneViewController = self.parentPaneViewController;
        movieController.thisVideoFileItem = item;
        movieController.url = [NSURL URLWithString:url];
    }
}

#pragma mark - NSFetchedResultController Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    QNVideoListCell *targetCell = (QNVideoListCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (type == NSFetchedResultsChangeUpdate && targetCell){
        [self configureCell:targetCell withIndexPath:indexPath];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

#pragma mark - PrivateMethod
- (void)hideDataView{
    [UIView animateWithDuration:0.5f animations:^(void){
        self.tableView.alpha = 0.0f;
        self.sortingView.alpha = 0.0f;
        self.statusView.alpha = 1.0f;
    }];
}

- (void)showDataView{
    [UIView animateWithDuration:0.5f animations:^(void){
        self.tableView.alpha = 1.0f;
        self.sortingView.alpha = 1.0f;
        self.statusView.alpha = 0.0f;
    }];
}

- (IBAction)playVideo:(id)sender{
    QNVideoFileItem *videoItem = (QNVideoFileItem *)((QNButton *)sender).userInfo;

    if ([videoItem.iWidth intValue] > 720 || [videoItem.iHeight intValue] > 720) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:NSLocalizedString(@"Continue", nil)
                          withMessage:NSLocalizedString(@"HDVidePlayWarning", nil)
                    withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                        [self performSegueWithIdentifier:@"playMovie" sender:sender];
                        [alert dismiss];
                    }];
    }else
        [self performSegueWithIdentifier:@"playMovie" sender:sender];
}

- (void)playVideoItem:(QNVideoFileItem *)videoItem{
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    NSString *urlString = [NSString stringWithFormat:@"%@video/api/video.php?a=display&f=%@&vt=default&sid=%@", communicationManager.videoStationManager.baseURL, videoItem.f_id, communicationManager.sidForMultimedia];
    VLCMovieViewController *movieController = [[VLCMovieViewController alloc] initWithNibName:nil bundle:nil];
    movieController.url = [NSURL URLWithString:urlString];
    
//    [self.navigationController pushViewController:movieController animated:YES];
}

- (void)configureCell:(QNVideoListCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    //fill the data
    QNVideoFileItem *thisItem = [self.fetchedResultsController objectAtIndexPath: indexPath];
    
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    [communicationManager.videoStationManager lazyloadingImage:cell.screenShot
                                               withPlaceHolder:nil
                                                    withFileId:thisItem.f_id
                                             withProgressBlock:^(NSUInteger r, long long e){
                                             }
                                            withCompletedBlock:^(UIImage *i, NSError *e, SDImageCacheType c){
                                                if (e)
                                                    DDLogError(@"WebImage Error: %@ ", e);
                                            }];
    
    [cell.timeDuration setText:[NSString stringFromTimeInterval:[thisItem.duration doubleValue]]];
    CGSize durationSize = [cell.timeDuration.text caculateSizeOfDuration];
    [cell.timeDuration setFrame:CGRectMake(cell.timeDuration.frame.origin.x,
                                           cell.timeDuration.frame.origin.y,
                                           durationSize.width + 3.0f,
                                           durationSize.height)];
    [cell.titleLabel setText:thisItem.cPictureTitle];
    [cell.mediaType setText:thisItem.mime];
    cell.hdImageIndicator.hidden = (MIN([thisItem.iWidth floatValue], [thisItem.iHeight floatValue]) >= 720.0f)?NO:YES;
    cell.playBtn.userInfo = thisItem;
    [cell.playBtn setEnabled:!isSelectMode];
    [cell.colorLevel setImage:[QNVideoTool colorLevel:[thisItem.colorLevel integerValue]]];
    [QNVideoTool configureStarRating:cell.starRating];
    CGFloat rating = [thisItem.rating floatValue]/20.0f;
    
    BOOL selectShowCondition = (self.parentPaneViewController.isSelectedMode);
    [cell.selectMark setHidden:!selectShowCondition];
    if (selectShowCondition) {
        [cell.selectMark setImage:[UIImage imageNamed:([selections containsObject:thisItem.f_id])?@"icon_select":@"icon_normal"]];
    }

    cell.starRating.rating = rating;
}

#pragma mark - ActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    /**
     0. share
     1. Download
     2. Add To Transcode
     3. Delete
     4. Copy To Collection
     5. Cancel
     */
    
    if ([selections count] == 0) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:nil
                          withMessage:NSLocalizedString(@"NoVideoSelected", nil)
                    withActionHandler:nil];
        return;
    }
    
    switch (buttonIndex) {
        case 0:
            [self.parentPaneViewController shareSelectedFilme];
            break;
        case 1:
            [self.parentPaneViewController downloadSelectedFilms];
            break;
        case 2:
            [self.parentPaneViewController addToTransCode];
            break;
        case 3:
            //Private method
            [self deleteProcess];
            break;
        case 4:{
            [self.parentPaneViewController addToAlbum];
        }
            break;
        case 5:
        default:
            [actionSheet dismissWithClickedButtonIndex:5 animated:YES];
            break;
    }
}

#pragma mark - ActionMethod
- (NSFetchedResultsController *)generateFetchedResultsController:(NSDictionary *)info{
    NSString *sortKey = (NSString *)[info valueForKey:@"sortKey"];
    BOOL isASC = [[info valueForKey:@"isASC"] boolValue];
    self.originalPredicate = (NSPredicate *)[info valueForKey:@"predicate"];

    NSFetchedResultsController *fetch = [QNVideoFileItem MR_fetchAllSortedBy:sortKey
                                                                   ascending:isASC
                                                               withPredicate:nil
                                                                     groupBy:nil
                                                                    delegate:(id<NSFetchedResultsControllerDelegate>)self.parentViewController];
    NSString *sortString = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"SortBy", nil),NSLocalizedString([info valueForKey:@"sortName"], nil) ];
    [self.sortingView.sortingLabel setText:sortString];
    self.fetchedResultsController = fetch;
    return fetch;
}

- (void)reloadContent{
    NSPredicate *predicate = nil;
    if (_addtionalPredicate) {
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[_addtionalPredicate, self.originalPredicate]];
    }else
        predicate = self.originalPredicate;
    [NSFetchedResultsController deleteCacheWithName:nil];
    [self.fetchedResultsController.fetchRequest setPredicate:predicate];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView reloadData];
    self.title = _previousTitle;
    
    [self.parentPaneViewController settingToolbarItem:self withToolBarStyle:VideosToolBarStyle];
}

- (NSArray *)multipleSelectedItems{
    return @[@"Share", @"Download", @"AddToTransCode", @"Delete", @"CopyToCollection", @"Cancel"];
}

- (void)multiSelectAction:(id)sender{
    DDLogVerbose(@"multiSelectAction");
    _multiSelectAction = [self multipleSelectedItems];
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:nil];
    
    for (NSString *_action in _multiSelectAction) {
        [sheet addButtonWithTitle:NSLocalizedString(_action, _action)];
    }
    [sheet showInView:self.view];
}

- (void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray*)titles{
    NSString *title1 = (NSString *)[titles objectAtIndex:0];
    NSString *title2 = (NSString *)[titles objectAtIndex:1];
    
    NSDictionary *dataDic = (NSDictionary *)pickerView.userInfo;
    NSString *value = [dataDic valueForKey:title2];

    
    [self generateFetchedResultsController:@{@"sortKey": value,
                                             @"isASC": [title1 isEqualToString:NSLocalizedString(@"Descending", nil)]?@(NO):@(YES),
                                             @"sortName":title2
                                             }];
    [self reloadContent];
}

#pragma mark - Private
- (NSArray *)fetchingAllSelectionsFileId{
    return (NSArray *)selections;
}

- (void)dbOperationAfterSuccessAction:(void(^)(void))successBlock withFailureBlock:(void(^)(NSError *error))failureBlock{
    BOOL s = YES;
    for (NSString *fID in selections) {
        QNVideoFileItem *fileItem = [QNVideoFileItem MR_findFirstByAttribute:@"f_id" withValue:fID];
        s &=[fileItem MR_deleteEntity];
    }
    if (s) {
        [selections removeAllObjects];
        successBlock();
    }else
        failureBlock([NSError errorWithDomain:@"DB operation error!" code:201 userInfo:nil]);

}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self settingToolbarItem];
}

- (void)settingToolbarItem{
    [self.parentPaneViewController settingToolbarItem:self withToolBarStyle:VideosToolBarStyle];
}
/**
 *  Different deleting method in Collection mode, including collections and smart collections
 videoFileHomePathCollections,
 videoFileHomePathSmartCollections,

 */
- (void)deleteProcess{
    if (self.parentPaneViewController.homePath == videoFileHomePathCollections || self.parentPaneViewController.homePath == videoFileHomePathSmartCollections) {
        [self.parentPaneViewController removeFilesFromAlbum];
    }else
        [self.parentPaneViewController deleteFiles];
}
#pragma mark - VideoDataSource
- (NSArray *)multipleSelectedFilms{
    return selections;
}

- (void)switchMultiSelectMode{
    if ([self tableView:self.tableView numberOfRowsInSection:0] > 0) {
        self.parentPaneViewController.isSelectedMode = !self.parentPaneViewController.isSelectedMode;
    }
}

- (NSString *)fetchingCollectionId{
    if (self.parentPaneViewController.homePath == videoFileHomePathCollections || self.parentPaneViewController.homePath == videoFileHomePathSmartCollections) {
        return self.collectionID;
    }
    return nil;
}

#pragma mark - QNVideoDelegate
- (void)settingAddtionalPredicate:(NSPredicate *)predicate{
    _addtionalPredicate = predicate;
}

- (void)settingFilterDic:(NSDictionary *)dic{
    _filterDic = [dic copy];
}
@end
