//
//  QNAboutViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/25.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNAboutViewController.h"
#import "QNVideoTool.h"
#import "QNAboutTableViewController.h"

@interface QNAboutViewController ()

@end

@implementation QNAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(backNavi:) withTitleText:NSLocalizedString(@"Back", nil)];
    [QNVideoTool generateCustomRightBarItem:self withSelect:@selector(feedback:) withTitleText:NSLocalizedString(@"Feedback", nil)];

    UIImage *_image = [UIImage imageNamed:@"icon_navigationbar_qvideo"];
    UIImageView *titleImage = [[UIImageView alloc] initWithImage:_image];
    [self.navigationItem setTitleView:titleImage];

    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [infoDict objectForKey:@"CFBundleVersion"];
    NSString *versionShort = [infoDict objectForKey:@"CFBundleShortVersionString"];
    
    NSString *versionString = [NSString stringWithFormat:@"version:%@.%@", version, versionShort];
    [self.versionLabel setText:versionString];
    // Do any additional setup after loading the view.
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)backNavi:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)feedback:(id)sender{
    [[QNVideoTool share] feedBack: self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)gotoQNAPWeb:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.qnap.com"]];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}


@end
