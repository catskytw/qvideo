//
//  QNVideoTrashCanViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/2/18.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoTrashCanViewController.h"
#import "QNVideoTool.h"
#import <TSMessages/TSMessage.h>

@interface QNVideoTrashCanViewController ()

@end

@implementation QNVideoTrashCanViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)customSetting{
    self.parentPaneViewController.isSelectedMode = YES;

    switch (self.parentPaneViewController.homePath) {
        case videoFileHomePathTrashCan:{
            QNButton *topRightBtn = [QNVideoTool generateCustomRightBarItem:self withSelect:@selector(topRightActionInTrashCan:) withTitleText:NSLocalizedString(@"Select", nil)];
            topRightBtn.userInfo = @(NO);
        }
            break;
        default:
            break;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.parentPaneViewController.isSelectedMode = NO;
}

- (void)topLeftActionInTrashCan:(id)sender{
    self.parentPaneViewController.isSelectedMode = NO;
    [self clearAll];
    QNButton *topRightBtn = [QNVideoTool generateCustomRightBarItem:self withSelect:@selector(topRightActionInTrashCan:) withTitleText:NSLocalizedString(@"Select", nil)];
    topRightBtn.userInfo = @(NO);
    [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)topRightActionInTrashCan:(id)sender{
    DDLogVerbose(@"topRightActionInTrashCan");
    QNButton *btn = (QNButton *)sender;
    if (self.parentPaneViewController.isSelectedMode) {
        btn.userInfo = @(![btn.userInfo boolValue]);
        [btn setTitle:[btn.userInfo boolValue]?NSLocalizedString(@"ClearAll", nil):NSLocalizedString(@"SelectAll", nil) forState:UIControlStateNormal];
        
        if ([btn.userInfo boolValue])
            [self selectAll];
        else
            [self clearAll];
    }else{
        self.parentPaneViewController.isSelectedMode = YES;
        [btn setTitle:NSLocalizedString(@"SelectAll", nil) forState:UIControlStateNormal];
        [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(topLeftActionInTrashCan:) withTitleText:NSLocalizedString(@"Cancel", nil)];
        
    }
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)clearAll{
    [selections removeAllObjects];
    [self settingNativationTitle];
}

- (void)selectAll{
    [self clearAll];
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:0];
    NSInteger totalNum = [sectionInfo numberOfObjects];
    for (NSInteger i=0; i < totalNum; i++) {
        QNVideoFileItem *_item = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        [selections addObject:_item.f_id];
    }
    [self settingNativationTitle];
}

- (NSArray *)multipleSelectedItems{
    return @[@"Delete", @"Recover", @"Cancel"];
}

- (void)settingToolbarItem{
    [self.parentPaneViewController settingToolbarItem:self withToolBarStyle:TrashcanToolBarStyle];
}

#pragma mark - ActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    DDLogVerbose(@"select ActionSheet index %i", buttonIndex);
    
    /**
     0. Delete
     1. Recover
     2. Cancel
     */
    
    BOOL(^hasSelections)(void) = ^(void){
        BOOL r = YES;
        if ([selections count] == 0) {
            [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                          withActionTitle:nil
                              withMessage:NSLocalizedString(@"NoFileDownload", nil)
                        withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *item){}];
            r = NO;
        }
        return r;
    };
    switch (buttonIndex) {
        case 0:
            if (hasSelections())
                [self removeDeletedFilms];
            break;
        case 1:
            if (hasSelections())
                [self recoverDeletedFilms];
            break;
        case 2:
        default:
            [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
            break;
    }
    [self settingNativationTitle];
}

- (void)removeDeletedFilms{
    //Delete
    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    NSArray *fIDs = [self fetchingAllSelectionsFileId];
    
    void(^failureBlock)(NSError *error) = ^(NSError *error){
        [TSMessage showNotificationInViewController:self.navigationController
                                              title:NSLocalizedString(@"Error", nil)
                                           subtitle:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"DeleteError", nil), error]
                                               type:TSMessageNotificationTypeError];
    };
    
    void(^successBlock)(void) = ^(){
        [self dbOperationAfterSuccessAction:^{
            [TSMessage showNotificationInViewController:self.navigationController
                                                  title:NSLocalizedString(@"Great", nil)
                                               subtitle:NSLocalizedString(@"DeleteVideoFromTrashCanSuccess", nil)
                                                   type:TSMessageNotificationTypeSuccess];
            [self sendRequestAndReload];
        }
                           withFailureBlock:^(NSError *error){
                               failureBlock(error);
                           }];
    };
    
    [videoStation deleteFileFromTrashCan:fIDs
                        withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                            [self dbOperationAfterSuccessAction:successBlock withFailureBlock:failureBlock];
                            
                        }
                        withFailureBlock:^(RKObjectRequestOperation *o, NSError *error){
                            failureBlock(error);
                        }];
    DDLogVerbose(@"bottomRightActionInTrashCan");
}

- (void)recoverDeletedFilms{
    //Recover
    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    NSArray *fIDs = [self fetchingAllSelectionsFileId];
    
    void(^failureBlock)(NSError *error) = ^(NSError *error){
        [TSMessage showNotificationInViewController:self.navigationController
                                              title:NSLocalizedString(@"Error", nil)
                                           subtitle:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"RecoverError", nil),error]
                                               type:TSMessageNotificationTypeError];
    };
    
    void(^successBlock)(void) = ^(){
        [self dbOperationAfterSuccessAction:^{
            [TSMessage showNotificationInViewController:self.navigationController
                                                  title:NSLocalizedString(@"Great", nil)
                                               subtitle:NSLocalizedString(@"RecoverSuccess", nil)
                                                   type:TSMessageNotificationTypeSuccess];
            [self sendRequestAndReload];
        }
                           withFailureBlock:^(NSError *error){
                               failureBlock(error);
                           }];
    };
    
    [videoStation recoverFileFromTrashCan:fIDs
                         withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                             [self dbOperationAfterSuccessAction:successBlock withFailureBlock:failureBlock];
                         }
                         withFailureBlock:^(RKObjectRequestOperation *o, NSError *error){
                             failureBlock(error);
                         }];
    
    DDLogVerbose(@"bottomLeftActionInTrashCan");
}
@end
