//
//  QNVideoListViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/24.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QNAPFramework/QNVideoStationAPIManager.h>

#import "QNVideosViewController.h"
#import "QNVideoSortingSectionView.h"
#import "QNVideoFetchingDelegate.h"
#import "IQActionSheetPickerView.h"
#import "QNVideoBottomActionBar.h"

#import "QNButton.h"

@interface QNVideoListViewController : UIViewController<NSFetchedResultsControllerDelegate, IQActionSheetPickerView, QNVideoDataSource, UIActionSheetDelegate>{
    __block NSArray *_videoFileItems;
    //for selection
    BOOL isSelectMode;
    NSString *_previousTitle;
    NSMutableArray *selections;
    NSArray *_multiSelectAction;
    NSString *_thisQueryUUID;
    NSPredicate *_addtionalPredicate;
    NSDictionary *_filterDic;

}
@property (nonatomic, strong)id<QNVideoFetchingDelegate>fetchDelegate;
@property (nonatomic, strong) NSPredicate *originalPredicate;
@property (nonatomic, weak) QNVideosViewController *parentPaneViewController;
@property(nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) IBOutlet QNVideoSortingSectionView *sortingView;
//@property (nonatomic, strong) QNVideoBottomActionBar *bottomBar;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet QNVideoStatusView *statusView;
@property (nonatomic, strong) void(^outsideFetchingBlock)(NSString *fetchUUID, QNQNVideoFileListSuccessBlock success, QNFailureBlock failure);

@property (nonatomic, strong) UIView *bottomBar;
@property (nonatomic, strong) NSString *thisQueryUUID;
@property (nonatomic, strong) NSString *collectionID;

- (NSFetchedResultsController *)generateFetchedResultsController:(NSDictionary *)info;
- (void)backNavi;
- (void)leftPanelAction:(id)sender;
- (void)rightPanelAction:(id)sender;
- (void)settingNativationTitle;

- (NSArray *)fetchingAllSelectionsFileId;
- (void)dbOperationAfterSuccessAction:(void(^)(void))successBlock withFailureBlock:(void(^)(NSError *error))failureBlock;
- (IBAction)playVideo:(id)sender;
- (void)switchMultiSelectMode;

@end
