//
//  QNTimelineReusableView.h
//  QVideo
//
//  Created by Change.Liao on 2014/3/26.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNTimelineReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@end
