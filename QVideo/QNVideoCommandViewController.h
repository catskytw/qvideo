//
//  QNVideoCommandViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/11/25.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNVideosViewController.h"

@interface QNVideoCommandViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    NSArray *_sectionOneCommands;
    NSArray *_sectionTwoCommands;
    NSArray *_sectionThreeCommands;
    NSIndexPath *_selectionIndexPath;
    NSArray *_tmpArray;
}
@property (weak, nonatomic) IBOutlet UITableView *commandTable;
@property (nonatomic) NSIndexPath *previousSelection;

@property (strong, nonatomic) QNVideosViewController *parentPanelViewController;
//- (void)openSelectedViewMode;
//- (void)closeSelectedViewMode;

@end
