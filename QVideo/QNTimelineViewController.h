//
//  QNTimelineViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/3/26.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoThumbnailViewController.h"
#import "SKSTableView.h"

@interface QNTimelineViewController : QNVideoThumbnailViewController <SKSTableViewDelegate, QNVideoDataSource>{
    NSMutableArray *_sectionArray;
    SKSTableView *_timelineTableView;
    NSArray *_timelines;
    CXAlertView *_tmpAlert;
    NSString *_timelineTag;
}

@end
