//
//  QNVideoCommonSectionView.h
//  QVideo
//
//  Created by Change.Liao on 2014/3/25.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNVideoCommonSectionView : UIView
@property (weak, nonatomic) IBOutlet UILabel *sectionLabel;

@end
