//
//  QNVideoShowInfoViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/23.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNVideoFileItem.h"
#import <MarqueeLabel/MarqueeLabel.h>
#import <EDStarRating/EDStarRating.h>
#import "QNVideoItemDetailViewController.h"

@interface QNVideoShowInfoViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UIImageView *videoScreenShot;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet MarqueeLabel *videoTitle;
@property (weak, nonatomic) IBOutlet EDStarRating *ratingStars;
@property (weak, nonatomic) IBOutlet UIImageView *colorLevelImage;
@property (weak, nonatomic) IBOutlet UILabel *classification;
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dimensionLabel;
@property (weak, nonatomic) IBOutlet UILabel *fileSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *fileDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateImportedLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTakenLabel;

@property (nonatomic, strong) QNVideoFileItem *videoItem;
@property (nonatomic, assign) QNVideoItemDetailViewController *outsideDetailViewController;

@end
