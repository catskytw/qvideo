//
//  QNVideoListCell.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/24.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoListCell.h"

@implementation QNVideoListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
