//
//  QNLeftSettingHeaderView.m
//  QVideo
//
//  Created by Change.Liao on 2014/1/22.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNLeftSettingHeaderView.h"

@implementation QNLeftSettingHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib{
    [self.serverTitle setTextColor:[UIColor whiteColor]];
    [self.serverTitle restartLabel];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)logoutAction:(id)sender {
}
@end
