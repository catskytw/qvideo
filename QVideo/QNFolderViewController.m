//
//  QNFolderViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/2/7.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNFolderViewController.h"
#import "QNVideoTool.h"
#import <QNAPFramework/QNVideoFileItem.h>
#import <QNAPFramework/QNVideoFileList.h>

#import <QNAPFramework/QNAPFramework.h>

#import "QNMovieControlView.h"
#import "QNVideoListCell.h"
#import "NSString_TimeDuration.h"
#import "QNFolderCell.h"
#import "QNMoviePlayerViewController.h"
#import "DTFolderItem.h"
#import "QNVideoTool.h"
#import "QNVideoItemDetailViewController.h"

#define headerHeight 40.0f
@interface QNFolderViewController ()

@end

@implementation QNFolderViewController

- (id)initWithStyle:(UITableViewStyle)style{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    selections = [NSMutableArray array];
    _filterDic = @{};

    [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
    [QNVideoTool generateRightBarItem:self withSelect:@selector(rightPanelAction:)];
    
    self.refreshControl = nil;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.statusView = [[QNVideoStatusView alloc] initWithFrame:CGRectMake(0, headerHeight, self.tableView.frame.size.width, self.tableView.frame.size.height - headerHeight)];
    [self.tableView addSubview:self.statusView];
    self.statusView.alpha = 0.0f;
    self.statusView.delegate = self.parentPaneViewController;
    self.fetchDelegate = self.parentPaneViewController;
    self.homePath = self.parentPaneViewController.homePath;
    self.pathPrefix = @"";
    [self sendRequestAndReload];
    [self.parentPaneViewController addObserver:self
                                    forKeyPath:@"isSelectedMode"
                                       options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld                                       context:nil];
}

- (void)reloadContent{
    [self.tableView reloadData];
    [self.parentPaneViewController settingToolbarItem:self withToolBarStyle:VideosToolBarStyle];
}

- (void)sendRequestAndReload{
    [[NSNotificationCenter defaultCenter] postNotificationName:STOP_RECURSIVE_FETCH object:nil];
    [QNVideoFileList MR_truncateAll];
    [QNVideoFileItem MR_truncateAll];
    self.thisQueryUUID = [[NSUUID UUID] UUIDString];
    [self hideDataView];
    [self.statusView setVideoStatus:QNVideoStatusTransfering];
    void(^success)(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoFileList *list) = ^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoFileList *list){
        //TODO stop waitCursor
        NSDictionary *dic = @{@"sortKey":@"mediaType,cPictureTitle",
                              @"isASC":@(YES),
                              @"predicate":[NSPredicate predicateWithFormat:@"self.uuid == %@", self.thisQueryUUID]};
        [self generateFetchedResultsController:dic];
        [self reloadContent];

        if ([[list.relationship_FileItem allObjects] count] == 0) {
            [self.statusView setVideoStatus:[self.parentPaneViewController noItemStatus]];
        }else
            [self showDataView];
    };

    void(^failure)(RKObjectRequestOperation *o, NSError *e) = ^(RKObjectRequestOperation *o, NSError *e){
        DDLogError(@"fetching folder information failure!");
        [self.statusView setVideoStatus:QNVideoStatusError];
    };

    [[QNVideoTool share] recursiveLoadFolderWithDir:self.pathPrefix
                                       withHomePath:self.homePath
                                           withUUID:self.thisQueryUUID
                                      withStartPage:1
                                      withFilterDic:_filterDic
                                   withSuccessBlock:success
                           withEachPageSuccessBlock:success
                                   withFailureBlock:failure];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.parentPaneViewController.isSelectedMode = NO;
}
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NavigationAction
- (void)leftPanelAction:(id)sender{
    [self.parentPaneViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:NO completion:nil];
}

- (void)rightPanelAction:(id)sender{
    [self.parentPaneViewController topRightDropdownMenuAction:self];
}

- (void)backNavi:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    _folderBar = [[DTFolderBar alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 39.0f)];
    DTFolderItem *homeFolderItem = [[DTFolderItem alloc] initWithFolderName:[self stringForCategory] targer:self action:@selector(gotoPath:) isFirstItem:YES];
    homeFolderItem.pathDir = @"";
    
    if (!_folderArray) {
        _folderArray = [NSMutableArray arrayWithArray:@[homeFolderItem]];
    }
    
    [self updateFolderBar];
    return _folderBar;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{    
    return headerHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    QNVideoFileItem *fileItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return [fileItem.mediaType isEqualToString:@"folder"]?40.0f:74.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects] ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //videoListCell
    QNVideoFileItem *videoFileItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *CellIdentifier = ([videoFileItem.mediaType isEqualToString:@"folder"])?@"FolderCell":@"videoListCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [self configureCell:cell withIndexPath:indexPath withVideoFileItem:videoFileItem];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (!self.parentPaneViewController.isSelectedMode) {
        if ([cell isKindOfClass:[QNFolderCell class]]) {
            QNFolderCell *folderCell = (QNFolderCell *)cell;
            QNVideoFileItem *fileItem = (QNVideoFileItem *)folderCell.userInfo;
            
            DTFolderItem *subFolderItem = [[DTFolderItem alloc] initWithFolderName:fileItem.cPictureTitle targer:self action:@selector(gotoPath:) isFirstItem:NO];
            subFolderItem.userInfo = fileItem.cPictureTitle;
            [_folderArray addObject:subFolderItem];
            self.pathPrefix = [self caculatePrefixString];
            [self sendRequestAndReload];
        }
    }else{
        if ([cell isKindOfClass:[QNVideoListCell class]]) {
            QNVideoFileItem *fileItem = [self.fetchedResultsController objectAtIndexPath:indexPath];

            BOOL hasSelect = [selections containsObject:fileItem.f_id];
            (hasSelect)?[selections removeObject:fileItem.f_id]:[selections addObject:fileItem.f_id];

            //refresh cell
            [self.tableView beginUpdates];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView endUpdates];
            [self settingToolbarItem];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return self.bottomBar;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return (self.parentPaneViewController.isSelectedMode)?self.bottomBar.frame.size.height: 0.0f;
}

#pragma mark - NSFetchingDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    UITableViewCell *targetCell = [tableView cellForRowAtIndexPath:indexPath];
    if (type == NSFetchedResultsChangeUpdate && targetCell){
        QNVideoFileItem *fileItem = [self.fetchedResultsController objectAtIndexPath: indexPath];
        [self configureCell:targetCell withIndexPath:indexPath withVideoFileItem:fileItem];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

#pragma mark - PrivateMethod
- (void)configureCell:(UITableViewCell *)inputCell withIndexPath:(NSIndexPath *)indexPath withVideoFileItem:(QNVideoFileItem *)videoFileItem{
    //fill the data
    QNVideoFileItem *thisItem = videoFileItem;
    
    if ([thisItem.mediaType isEqualToString:@"video"]) {
        QNVideoListCell *cell = (QNVideoListCell *)inputCell;
        QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
        [communicationManager.videoStationManager lazyloadingImage:cell.screenShot
                                                   withPlaceHolder:nil
                                                        withFileId:thisItem.f_id
                                                 withProgressBlock:^(NSUInteger r, long long e){
                                                 }
                                                withCompletedBlock:^(UIImage *i, NSError *e, SDImageCacheType c){
                                                    if (e)
                                                        DDLogError(@"WebImage Error: %@ ", e);
                                                }];
        
        [cell.timeDuration setText:[NSString stringFromTimeInterval:[thisItem.duration doubleValue]]];
        [cell.titleLabel setText:thisItem.cPictureTitle];
        [cell.mediaType setText:thisItem.mime];
        cell.hdImageIndicator.hidden = ![thisItem.v720P boolValue];
        cell.playBtn.userInfo = thisItem;
        [cell.colorLevel setImage:[QNVideoTool colorLevel:[thisItem.colorLevel integerValue]]];
        BOOL selectShowCondition = (self.parentPaneViewController.isSelectedMode);
        [cell.selectMark setHidden:!selectShowCondition];
        if (selectShowCondition) {
            [cell.selectMark setImage:[UIImage imageNamed:([selections containsObject:videoFileItem.f_id])?@"icon_select":@"icon_normal"]];
        }

        [QNVideoTool configureStarRating:cell.starRating];
        CGFloat rating = [thisItem.rating floatValue]/20.0f;
        cell.starRating.rating = rating;
    }else if([thisItem.mediaType isEqualToString:@"folder"]){
        QNFolderCell *folderCell = (QNFolderCell *)inputCell;
        [folderCell.folderName setText: thisItem.cPictureTitle];
        folderCell.userInfo = thisItem;
    }
    
}

- (NSString *)stringForCategory{
    NSString *r = nil;
    switch (self.homePath) {
        case videoFileHomePathPublic:
            r = NSLocalizedString(@"SharedVideo", nil);
            break;
        case videoFileHomePathPrivate:
            r = NSLocalizedString(@"PrivateCollection", nil);
            break;
        case videoFileHomePathQSync:
            r = NSLocalizedString(@"QSync", nil);
            break;
            
        default:
            break;
    }
    return r;
}

- (void)updateFolderBar{
    [_folderBar setAllFolderItems:_folderArray animated:YES];
    [_folderBar setBackgroundColor:[UIColor colorWithHexString:@"#e7e7e7" alpha:1]];
}

- (void)hideDataView{
    [UIView animateWithDuration:0.5f animations:^(void){
        self.statusView.alpha = 1.0f;
        self.tableView.separatorColor = [UIColor clearColor];
    }];
}

- (void)showDataView{
    [UIView animateWithDuration:0.5f animations:^(void){
        self.statusView.alpha = 0.0f;
        self.tableView.separatorColor = [UIColor grayColor];
    }];
}

- (NSArray *)multipleSelectedItems{
    return @[@"Share", @"Download", @"AddToTransCode", @"Delete", @"CopyToCollection", @"Cancel"];
}

- (void)switchMultiSelectMode{
    self.parentPaneViewController.isSelectedMode = !self.parentPaneViewController.isSelectedMode;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self settingToolbarItem];
}

- (void)settingToolbarItem{
    [self.parentPaneViewController settingToolbarItem:self withToolBarStyle:VideosToolBarStyle];
}
#pragma mark - ActionMethod
- (void)multiSelectAction:(id)sender{
    DDLogVerbose(@"multiSelectAction");
    _multiSelectAction = [self multipleSelectedItems];
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:nil];
    
    for (NSString *_action in _multiSelectAction) {
        [sheet addButtonWithTitle:NSLocalizedString(_action, _action)];
    }
    [sheet showInView:self.parentPaneViewController.view];
}

- (void)closeMultiSelection:(id)sender{
    self.parentPaneViewController.isSelectedMode = NO;
}

- (void)playVideo:(id)sender{
    QNVideoFileItem *videoItem = (QNVideoFileItem *)((QNButton *)sender).userInfo;
    
    if ([videoItem.iWidth intValue] > 720 || [videoItem.iHeight intValue] > 720) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:NSLocalizedString(@"Continue", nil)
                          withMessage:NSLocalizedString(@"HDVidePlayWarning", nil)
                    withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                        [self performSegueWithIdentifier:@"playMovie" sender:sender];
                        [alert dismiss];
                    }];
    }else
        [self performSegueWithIdentifier:@"playMovie" sender:sender];
}

- (void)playVideoItem:(id)sender{
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    DDLogVerbose(@"sid %@", communicationManager.sidForMultimedia);
    QNVideoFileItem *item = (QNVideoFileItem *)((QNButton *)sender).userInfo;
    //    NSString *urlString = [NSString stringWithFormat:@"%@video/api/video.php?a=display&f=%@&vt=default&sid=%@", communicationManager.videoStationManager.baseURL, item.f_id, communicationManager.sidForMultimedia];
    NSString *urlString = [QNVideoTool generateGetVideoURL:item.f_id isDownload:NO];
    QNMoviePlayerViewController *movieController = [[QNMoviePlayerViewController alloc] initWithNibName:nil bundle:nil];
    movieController.url = [NSURL URLWithString:urlString];
    movieController.thisVideoFileItem = item;
    
    [self.navigationController pushViewController:movieController animated:YES];
}

- (void)gotoPath:(id)sender{
    DTFolderItem *thisItem = (DTFolderItem *)sender;
    DDLogVerbose(@"press here %@", thisItem.pathDir);
    if ([[thisItem pathDir] isEqualToString:self.pathPrefix])
        return;
    //need to caculate the prefix
    self.pathPrefix = @"";
    
    NSInteger selectIndex = 0;
    for (DTFolderItem *item in _folderArray) {
        if ([item.title isEqualToString:thisItem.title])
            break;
        selectIndex ++;
    }
    
    [_folderArray removeObjectsInRange:NSMakeRange(selectIndex + 1, [_folderArray count] - selectIndex - 1)];
    
    self.pathPrefix = [self caculatePrefixString];
    [self sendRequestAndReload];
//    [self.tableView reloadData];
}

- (NSMutableString *)caculatePrefixString{
    
    NSMutableString *_prefixPath = [NSMutableString stringWithFormat:@""];
    if ([_folderArray count] > 1) {
        
        switch (self.homePath) {
            case videoFileHomePathPrivate:
                [_prefixPath appendFormat:@"homes/%@/",self.parentPaneViewController.thisServer.account];
                break;
            case videoFileHomePathQSync:
                [_prefixPath appendFormat:@"homes/%@/.Qsync/",self.parentPaneViewController.thisServer.account];
                break;
            default:
                //do nothing
                break;
        }
    }
    NSInteger count = 0;
    for (DTFolderItem *item in _folderArray) {
        if (count != 0)
            [_prefixPath appendFormat:@"%@/", (NSString *)item.userInfo];
        count ++;
    }
    return _prefixPath;
}

- (NSFetchedResultsController *)generateFetchedResultsController:(NSDictionary *)info{
    NSString *sortKey = (NSString *)[info valueForKey:@"sortKey"];
    BOOL isASC = [[info valueForKey:@"isASC"] boolValue];
    NSPredicate *predicate = nil;
    if (_addtionalPredicate) {
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[_addtionalPredicate, (NSPredicate *)[info valueForKey:@"predicate"]]];
    }else
        predicate = (NSPredicate *)[info valueForKey:@"predicate"];
    
    NSFetchedResultsController *fetch = [QNVideoFileItem MR_fetchAllSortedBy:sortKey
                                                                   ascending:isASC
                                                               withPredicate:predicate
                                                                     groupBy:nil
                                                                    delegate:(id<NSFetchedResultsControllerDelegate>)self.parentViewController];
    self.fetchedResultsController = fetch;
    return fetch;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    return !isSelectMode;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"playMovie"]){
        QNVideoFileItem *item = [(QNButton *)sender userInfo];
        QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
        NSString *urlString = [NSString stringWithFormat:@"%@video/api/video.php?a=display&f=%@&vt=default&sid=%@", communicationManager.videoStationManager.baseURL, item.f_id, communicationManager.sidForMultimedia];
        QNMoviePlayerViewController *movieController = (QNMoviePlayerViewController *)segue.destinationViewController;
        movieController.parentPaneViewController = self.parentPaneViewController;
        movieController.thisVideoFileItem = item;
        movieController.url = [NSURL URLWithString:urlString];
    }else if ([segue.identifier isEqualToString:@"goDetailFromFolder"]){
        QNVideoListCell *cell = (QNVideoListCell *)sender;
        QNVideoFileItem *item = [cell.playBtn userInfo];

        QNVideoItemDetailViewController *detailViewController = (QNVideoItemDetailViewController *)segue.destinationViewController;
        detailViewController.videoItem = item;
        detailViewController.parentVideoViewController = self.parentPaneViewController;
    }

}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"isSelectedMode"]) {
        DDLogVerbose(@"new key:%@", [change objectForKey:NSKeyValueChangeNewKey]);
        isSelectMode = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
        [self reloadContent];
        
        if (isSelectMode) {
            [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(closeMultiSelection:) withTitleText:NSLocalizedString(@"Cancel", nil)];
        }else
            [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
    }
}

#pragma mark - ActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    /**
     0. share
     1. Download
     2. Add To Transcode
     3. Delete
     4. Copy To Collection
     5. Cancel
     */
    if ([selections count] == 0) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:nil
                          withMessage:NSLocalizedString(@"NoVideoSelected", nil)
                    withActionHandler:nil];
        return;
    }

    switch (buttonIndex) {
        case 0:
            [self.parentPaneViewController shareSelectedFilme];
            break;
        case 1:
            [self.parentPaneViewController downloadSelectedFilms];
            break;
        case 2:
            [self.parentPaneViewController addToTransCode];
            break;
        case 3:
            [self.parentPaneViewController deleteFiles];
            break;
        case 4:{
            [self.parentPaneViewController addToAlbum];
        }
            break;
        case 5:
        default:
            [actionSheet dismissWithClickedButtonIndex:5 animated:YES];
            break;
    }
}

- (BOOL)isMultiSelectionsEmpty{
    BOOL r = NO;
    if ([_multiSelectAction count] == 0){
        r = YES;
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:nil
                          withMessage:NSLocalizedString(@"NoVideoSelected", nil)
                    withActionHandler:nil];
        
    }
    return r;
}

#pragma mark - VideoDataSource
- (NSArray *)multipleSelectedFilms{
    return selections;
}

- (void)settingAddtionalPredicate:(NSPredicate *)predicate{
    _addtionalPredicate = predicate;
}

- (void)settingFilterDic:(NSDictionary *)dic{
    _filterDic = [dic copy];
}



@end
