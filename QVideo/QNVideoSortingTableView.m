//
//  QNVideoSortingTableView.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/25.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoSortingTableView.h"
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <QNAPFramework/QNVideoFileItem.h>
#import "QNVideoGalleryViewController.h"
#import <QNAPFramework/QNAPFramework.h>
#import <PopoverView/PopoverView_Configuration.h>

@interface QNVideoSortingTableView (){
    NSDictionary *_selections;
}

@end

@implementation QNVideoSortingTableView
- (id)initWithFrame:(CGRect)frame withParentViewController:(UIViewController *)parentViewController{
    self = [self initWithFrame:frame];
    if (self) {
        self.parentViewController = parentViewController;
        _selections = @{NSLocalizedString(@"Title", nil):@"cPictureTitle",
                        NSLocalizedString(@"FileDate", nil): @"yearMonthDay",
                        NSLocalizedString(@"DateImported", nil):@"importYearMonthDay",
                        NSLocalizedString(@"DateTaken", nil):@"dateCreated",
                        NSLocalizedString(@"FileSize", nil):@"iFileSize",
                        NSLocalizedString(@"Rating", nil):@"rating",
                        NSLocalizedString(@"ColorLabel", nil) :@"colorLevel"};

    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[_selections allKeys] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"selectionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.f]];
    
    NSArray *allKeys = [_selections allKeys];
    NSString *key = [allKeys objectAtIndex:[indexPath row]];
    [cell.textLabel setText:key];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 24.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DDLogVerbose(@"sorting selected! parentViewController %@", self.parentViewController);
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *sortKey = [_selections valueForKey:cell.textLabel.text];
    if ([self.parentViewController respondsToSelector:@selector(generateFetchedResultsController:)]) {
        NSDictionary *info = @{@"sortKey": sortKey, @"isASC":self.ascBtn.userInfo, @"sortName":cell.textLabel.text};
        [self.parentViewController performSelector:@selector(generateFetchedResultsController:) withObject:info];
        if ([self.parentViewController respondsToSelector:@selector(reloadContent)]) {
            [self.parentViewController performSelector:@selector(reloadContent) withObject:nil];
        }
    }
    [self.parentPopoverView dismiss];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = nil;
    if (section == 0) {
        sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 20)];
        self.ascBtn = [QNButton buttonWithType:UIButtonTypeCustom];
        [self.ascBtn setFrame:CGRectMake(self.frame.size.width - 40, 0, 40, 20)];
        [self.ascBtn setBackgroundColor:[UIColor grayColor]];
        [self.ascBtn addTarget:self action:@selector(ascBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self settingASCBtnText:[self.ascBtn.userInfo boolValue]];
        self.ascBtn.userInfo = [NSNumber numberWithBool:YES];
        [sectionView addSubview:self.ascBtn];
    }
    return sectionView;
}


#pragma mark - ActionMethod
- (void)ascBtnAction:(id)sender{
    QNButton *_btn = (QNButton *)sender;
    BOOL _isASC = [_btn.userInfo boolValue];
    _isASC = !_isASC;
    
    _btn.userInfo = [NSNumber numberWithBool:_isASC];
    [self settingASCBtnText:_isASC];
}

#pragma mark - PrivateMethod
- (void)settingASCBtnText:(BOOL)isASC{
    [self.ascBtn.titleLabel setText:(isASC)?@"ASC":@"DESC"];
    [self.ascBtn.titleLabel setTextColor:[UIColor blackColor]];
}
@end
