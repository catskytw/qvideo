//
//  QNSettingPopoverTableView.m
//  QVideo
//
//  Created by Change.Liao on 2014/1/3.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNSettingPopoverTableView.h"

@implementation QNSettingPopoverTableView
- (id)initWithFrame:(CGRect)frame withData:(NSArray *)data withSelectedAction:(void(^)(NSInteger row))complete{
    self = [self initWithFrame:frame];
    if (self) {
        self.tableData = data;
        _completeBlock = complete;
    }
    return self;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"timeLineCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"timeLineCell"];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:12.0f]];
    }
    [cell.textLabel setText:(NSString *)[self.tableData objectAtIndex:[indexPath row]]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 22.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger scrollToTargetSection = [indexPath row];
    _completeBlock(scrollToTargetSection);
}
@end
