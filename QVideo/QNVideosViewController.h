//
//  QNVideosViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/11/22.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MSDynamicsDrawerViewController/MSDynamicsDrawerViewController.h>
#import "NASServer.h"
#import <QNAPFramework/QNVideoStationAPIManager.h>
#import <REMenu/REMenu.h>
#import <CTAssetsPickerController.h>
#import "IQActionSheetPickerView.h"
#import "QNVideoFetchingDelegate.h"
#import "QNVideoStatusView.h"

@class QNVideoCommandViewController;
@class QNVideoSettingViewController;

@protocol QNVideoDataSource <NSObject>
@required
- (NSArray *)multipleSelectedFilms;
- (void)sendRequestAndReload;
- (void)reloadContent;
- (void)cleanupMultipleSelections;
@optional
- (NSString *)fetchingCollectionId;
- (void)settingAddtionalPredicate:(NSPredicate *)predicate;
- (void)settingFilterDic:(NSDictionary *)dic;
- (NSString *)fetchingRemotePath;

@end

@interface QNVideosViewController : MSDynamicsDrawerViewController <NSFetchedResultsControllerDelegate, QNVideoFetchingDelegate, QNVideoStatusDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDataSource, UITableViewDelegate, CTAssetsPickerControllerDelegate, NSURLSessionTaskDelegate, IQActionSheetPickerView, UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    NSArray *_collections;
    NSArray *_resolutions;
    NSMutableArray *_selectedResolutions;
    NSMutableArray *_loadingImages;
    
    NSDate *_previousUploadSendDate;
}
@property (nonatomic, strong) id<QNVideoDataSource>videoDataSource;
@property (nonatomic, strong)NASServer *thisServer;
@property (nonatomic)VideoFileHomePath homePath;
@property (nonatomic) BOOL isSelectedMode;
@property (nonatomic, weak) UIViewController *contentViewController;
@property (nonatomic, strong) QNVideoCommandViewController *commandViewController;
@property (nonatomic, strong) QNVideoSettingViewController *settingViewController;
@property (nonatomic, strong) REMenu *viewModeMenu;
@property (nonatomic, strong) REMenu *moreOptionsMenu;
#pragma mark - Change the contentViewController
/**
 *  <#Description#>
 *
 *  @param isAnimated   <#isAnimated description#>
 *  @param completetion <#completetion description#>
 */
- (void)changeContentWithGalleryViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion;

/**
 *  Change the content in thumbnail style
 *
 *  @param completetion completion block
 */
- (void)changeContentWithThumbnailViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion;

/**
 *  Change the content in List style
 *
 *  @param completetion completion block
 */
- (void)changeContentWithListViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion;

- (void)changeContentWithFolderViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion;

- (void)changeContentWithSettingViewController:(BOOL)isAnimated withCompletedBlock:(void(^)(void))completion;

- (void)changeContentWithLocalVideoViewController:(BOOL)isAnimated withCompletedBlock:(void(^)(void))completion;

- (void)changeContentWithTrashCanViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion;

- (void)changeContentWithTimelineViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion;

- (void)changeContentWithAlbumViewController:(BOOL)isAnimated withCompletedBlock:(void(^)(void))completion;

#pragma mark - FilmAction
/**
 *  Download the selected film(s)
 */
- (void)downloadSelectedFilms;

/**
 *  Share the selected film(s)
 */
- (void)shareSelectedFilme;

/**
 *  Add the selected film(s) to transcode
 */
- (void)addToTransCode;

/**
 *  Delete the selected film(s)
 */
- (void)deleteFiles;

/**
 * remove these file form album, but they still exist in NAS
 */
- (void)removeFilesFromAlbum;
/**
 *  Add the selected film(s) into an album.
 */
- (void)addToAlbum;

/**
 *  Delete the selected album(s)
 */
- (void)deleteAlbums;

#pragma mark - NoVideoItem
- (QNVideoStatus)noItemStatus;

- (void)settingToolbarItem:(UIViewController *)senderViewController withToolBarStyle:(QVideoToolBarStyle)style;

#pragma mark - View Mode DropDown menu

- (void)settingTopRightDropdownMenu:(UIViewController *)viewController;
- (void)topRightDropdownMenuAction:(UIViewController *)onViewController;
@end
