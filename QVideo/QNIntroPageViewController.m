//
//  QNIntroPageViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/5/2.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNIntroPageViewController.h"
#import "QNVideoTool.h"

@interface QNIntroPageViewController ()

@end

@implementation QNIntroPageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self settingCoreTextStyle];
    [self.tutorialImage setImage:[UIImage imageNamed:self.contentImageFileName]];
}

- (void)settingCoreTextStyle{
    [self settingCoreText];
    FTCoreTextStyle *defaultStyle = [[FTCoreTextStyle alloc] init];
    defaultStyle.name = FTCoreTextTagDefault;
    defaultStyle.textAlignment = FTCoreTextAlignementCenter;
    defaultStyle.font = [UIFont systemFontOfSize:13.0f];
    defaultStyle.paragraphInset = UIEdgeInsetsMake(0.0f, 0, 0, 0);
    
    FTCoreTextStyle *h1Style = [defaultStyle copy];
    h1Style.name = @"h1";
    h1Style.font = [UIFont boldSystemFontOfSize:19.0f];
    h1Style.textAlignment = FTCoreTextAlignementCenter;
    h1Style.color = [UIColor colorWithHexString:@"#000000"];
    h1Style.paragraphInset = UIEdgeInsetsMake(19.0f, 0.0f, 0.0f, 0);
    
    FTCoreTextStyle *pStyle = [defaultStyle copy];
    pStyle.name = @"p";
    pStyle.color = [UIColor colorWithHexString:@"#282828"];

    [self.coreTextView addStyles:@[defaultStyle, h1Style, pStyle]];
    
    //  Make self delegate so we receive links actions
    self.coreTextView.delegate = nil;
}

- (void)settingCoreText{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSString *textName = [NSString stringWithFormat:@"%@_%@.txt", self.contentFileName, language];
    self.coreTextView.text = [QNVideoTool textFromTextFileNamed:textName];

}

- (void)coreTextView:(FTCoreTextView *)coreTextView receivedTouchOnData:(NSDictionary *)data{
    //Do nothing
}


@end
