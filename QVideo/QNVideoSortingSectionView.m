//
//  QNVideoSortingSectionView.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/25.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoSortingSectionView.h"
#import <PopoverView/PopoverView.h>
#import "QNVideoSortingTableView.h"
#import "IQActionSheetPickerView.h"

@implementation QNVideoSortingSectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

//- (id)initWithCoder:(NSCoder *)aDecoder {
//    if ((self = [super initWithCoder:aDecoder])) {
//        UIView *subView =[[[NSBundle mainBundle] loadNibNamed:@"QNVideoSortingSectionView"
//                                                        owner:self
//                                                      options:nil] objectAtIndex:0];
//        [self addSubview:subView];
//    }
//    return self;
//}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self.sortingLabel setText:NSLocalizedString(@"SortBy", nil)];
}

- (IBAction)sortingAction:(id)sender{
    IQActionSheetPickerView *selectionView = [[IQActionSheetPickerView alloc] initWithViewController:self];
    NSDictionary *_selections = @{NSLocalizedString(@"Title", nil):@"cPictureTitle",
                                  NSLocalizedString(@"FileDate", nil): @"yearMonthDay",
                                  NSLocalizedString(@"DateImported", nil):@"importYearMonthDay",
                                  NSLocalizedString(@"DateTaken", nil):@"dateCreated",
                                  NSLocalizedString(@"FileSize", nil):@"iFileSize",
                                  NSLocalizedString(@"Rating", nil):@"rating",
                                  NSLocalizedString(@"ColorLabel", nil) :@"colorLevel"};
    NSArray *ascDic = @[NSLocalizedString(@"Descending", nil), NSLocalizedString(@"Ascending", nil)];
    
    selectionView.userInfo = _selections;
    
    selectionView.titlesForComponenets = @[
                                           ascDic, [_selections allKeys]
                                           ];
    CGFloat _width = self.parentViewController.view.frame.size.width / 2.0f;
    selectionView.widthsForComponents = @[@(_width), @(_width)];
    selectionView.title = NSLocalizedString(@"Sorting", nil);
    selectionView.delegate = self.parentViewController;
    [selectionView showInView:self.parentViewController.view];
}

@end
