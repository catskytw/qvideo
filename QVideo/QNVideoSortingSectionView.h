//
//  QNVideoSortingSectionView.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/25.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQActionSheetPickerView.h"

@interface QNVideoSortingSectionView : UIView
@property (weak, nonatomic) UIViewController<IQActionSheetPickerView> *parentViewController;
@property (weak, nonatomic) IBOutlet UILabel *sortingLabel;
@property (weak, nonatomic) IBOutlet UIButton *ascBtn;

- (IBAction)sortingAction:(id)sender;
@end
