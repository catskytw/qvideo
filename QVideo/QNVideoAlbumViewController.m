//
//  QNVideoAlbumViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/1.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoAlbumViewController.h"
#import <QNAPFramework/QNCollection.h>
#import <QNAPFramework/QNAPCommunicationManager.h>
#import "QNVideoThumbnailCellView.h"
#import "QNVideoListViewController.h"
#import "QNVideoTool.h"
@interface QNVideoAlbumViewController ()

@end

@implementation QNVideoAlbumViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateVideoFileList{
    /**
     predicate
     */
    [self generateFetchedResultsController:@{@"sortKey":@"cAlbumTitle", @"isASC":@(YES), @"sortName":@"Title",
                                             @"predicate":[NSPredicate predicateWithFormat:@"self.uuid == %@", self.thisQueryUUID]
                                             }];
    [self reloadContent];
}

- (NSFetchedResultsController *)generateFetchedResultsController:(NSDictionary *)info{
    NSString *sortKey = (NSString *)[info valueForKey:@"sortKey"];
    BOOL isASC = [[info valueForKey:@"isASC"] boolValue];
    NSPredicate *predicate = (NSPredicate *)[info valueForKey:@"predicate"];
    NSString *groupBy = [info valueForKey:@"groupBy"];
    NSFetchedResultsController *fetch = [QNCollection MR_fetchAllSortedBy:sortKey
                                                                ascending:isASC
                                                            withPredicate:nil
                                                                  groupBy:groupBy
                                                                 delegate:(id<NSFetchedResultsControllerDelegate>)self.parentViewController];
    NSString *sortString = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"SortBy", nil),[info valueForKey:@"sortName"]];
    [self.sortingView.sortingLabel setText:sortString];
    
    self.fetchResultsController = fetch;
    return fetch;
}

- (void)configureCell:(QNVideoThumbnailCellView *)cell withIndexPath:(NSIndexPath *)indexPath{
    QNCollection *collectionItem = [self.fetchResultsController objectAtIndexPath:indexPath];
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    [communicationManager.videoStationManager lazyloadingImage:cell.screenShot
                                               withPlaceHolder:nil
                                                    withFileId:collectionItem.iAlbumCover
                                             withProgressBlock:^(NSUInteger r, long long e){}
                                            withCompletedBlock:^(UIImage *i, NSError *e, SDImageCacheType c){
                                                if (e)
                                                    DDLogError(@"WebImage Error: %@ ", e);
                                            }];
    [cell.videoTitle setText:collectionItem.cAlbumTitle];
    cell.viewDetailBtn.userInfo = collectionItem;
    cell.viewDetailBtn.userInteractionEnabled = !self.parentPaneViewController.isSelectedMode;
    cell.playVideoBtn.userInteractionEnabled = !self.parentPaneViewController.isSelectedMode;
    cell.playVideoBtn.userInfo = collectionItem;
    
    cell.hdImageIndicator.hidden = (MIN([collectionItem.coverWidth floatValue], [collectionItem.coverHeight floatValue]) >= 720.0f)?NO:YES;
    NSString *videoCountString = [NSString stringWithFormat:@"%i %@", [collectionItem.videoCount intValue], NSLocalizedString(@"Video", nil)];
    [cell.durationLabel setText:videoCountString];
//    BOOL selectShowCondition = (self.parentPaneViewController.isSelectedMode && [_selections containsObject:collectionItem.iVideoAlbumId]);
//    [cell.selectedMark setHidden:!selectShowCondition];
    BOOL selectShowCondition = (self.parentPaneViewController.isSelectedMode);
    [cell.selectedMark setHidden:!selectShowCondition];
    if (selectShowCondition) {
        [cell.selectedMark setImage:[UIImage imageNamed:([_selections containsObject:collectionItem.iVideoAlbumId])?@"icon_select":@"icon_normal"]];
    }
}

- (IBAction)gotoCollectionItems:(id)sender{
    [self performSegueWithIdentifier:@"gotoCollectionItems" sender:sender];
}

- (NSArray *)multipleSelectedItems{
    return @[@"RemoveCollection", @"Cancel"];
}

- (void)settingToolbarItem{
    [self.parentPaneViewController settingToolbarItem:self withToolBarStyle:AlbumsToolBarStyle];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.parentPaneViewController.isSelectedMode) {
        QNCollection *collectionItem = [self.fetchResultsController objectAtIndexPath:indexPath];
        if ([_selections containsObject:collectionItem.iVideoAlbumId])
            [_selections removeObject:collectionItem.iVideoAlbumId];
        else
            [_selections addObject:collectionItem.iVideoAlbumId];
        
        //only update ONE collectionView
        [collectionView reloadItemsAtIndexPaths:@[indexPath]];
        [self settingToolbarItem];
    }
}

- (void)createCollectionProcess{
    [self performSegueWithIdentifier:@"createCollection" sender:nil];
}

#pragma mark - ActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{    
    /**
     0. remove collection
     */
    if ([_selections count] == 0) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:nil
                          withMessage:NSLocalizedString(@"NoVideoSelected", nil)
                    withActionHandler:nil];
        return;
    }

    switch (buttonIndex) {
        case 0:
            [self.parentPaneViewController deleteAlbums];
            break;
        case 1:
        default:
            [actionSheet dismissWithClickedButtonIndex:5 animated:YES];
            break;
    }
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"gotoCollectionItems"]) {
        [QNVideoFileItem MR_truncateAll];
        
        QNButton *btn = (QNButton *)sender;
        QNCollection *collection = btn.userInfo;
        __block QNVideoListViewController *targetViewController = (QNVideoListViewController *)segue.destinationViewController;
        [QNVideoTool generateCustomLeftBarItem:targetViewController withSelect:@selector(backNavi) withTitleText:NSLocalizedString(@"Back", nil)];
        targetViewController.parentPaneViewController = self.parentPaneViewController;
        targetViewController.title = collection.cAlbumTitle;
        targetViewController.collectionID = collection.iVideoAlbumId;
        
        targetViewController.outsideFetchingBlock = ^(NSString *uuid, QNQNVideoFileListSuccessBlock success, QNFailureBlock failure){
            [[QNVideoTool share] recursiveLoadCollectionVideoFiles:collection.iVideoAlbumId
                                                      withSortType:videoFileListSortByModify
                                                             isASC:NO
                                                     withStartPage:1
                                                      withHomePath:videoFileHomePathPublic
                                                          withUUID:uuid
                                                     withFilterDic:nil
                                                     withEachBlock:success
                                                  withSuccessBlock:success
                                                  withFailureBlock:failure];            
        };
    }
}
#pragma mark - PrivateMethod
- (void)closeMultiSelection:(id)sender{
    self.parentPaneViewController.isSelectedMode = NO;
    [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
}

@end
