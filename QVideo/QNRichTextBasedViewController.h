//
//  QNRichTextBasedViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/4/28.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FTCoreText/FTCoreTextView.h>

@interface QNRichTextBasedViewController : UIViewController<FTCoreTextViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) FTCoreTextView *coreTextView;
+ (NSString *)textFromTextFileNamed:(NSString *)filename;
- (void)settingTextTag;
- (void)settingCoreText;
- (void)backNavi:(id)sender;
- (void)feedBack;
@end
