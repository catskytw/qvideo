//
//  QNVideoSortingTableView.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/25.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PopoverView/PopoverView.h>
#import "QNButton.h"

@interface QNVideoSortingTableView : UITableView<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) UIViewController *parentViewController;
@property (nonatomic, strong) QNButton *ascBtn;
@property (nonatomic, weak) PopoverView *parentPopoverView;

- (id)initWithFrame:(CGRect)frame withParentViewController:(UIViewController *)parentViewController;
- (void)ascBtnAction:(id)sender;
@end
