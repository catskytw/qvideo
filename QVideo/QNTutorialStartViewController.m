//
//  QNTutorialStartViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/30.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNTutorialStartViewController.h"
#import "QNVideoTool.h"
#import "QNTurtorialViewController.h"
@interface QNTutorialStartViewController ()

@end

@implementation QNTutorialStartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"#f8f8f8"]];
    self.goTurtorialBtn.layer.cornerRadius = 5.0f;
    [self.goTurtorialBtn setTitle:NSLocalizedString(@"Go", nil) forState:UIControlStateNormal];
    
    BOOL hasNAS = ([NASServer MR_countOfEntities] > 0);
    if (self.forceTutorial || !hasNAS) {
        [[NSUserDefaults standardUserDefaults] setValue:@(NO) forKey:@"isFirstRun"];
        if (self.forceTutorial) {
            [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(backNavi:) withTitleText:NSLocalizedString(@"Back", nil)];
        }
        [QNVideoTool generateCustomRightBarItem:self withSelect:@selector(feedBack:) withTitleText:NSLocalizedString(@"Feedback", nil)];
        [self performSegueWithIdentifier:@"showTutorialPages" sender:nil];
    }else{
        [self performSegueWithIdentifier:@"enterNasList" sender:nil];
    }
}

- (void)backNavi:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)feedBack:(id)sender{
    [[QNVideoTool share] feedBack: self];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    DDLogVerbose(@"segue id %@", segue.identifier);
    if ([segue.identifier isEqualToString:@"showTutorialPages"]) {
        QNTurtorialViewController *targetTurtorial = (QNTurtorialViewController *)segue.destinationViewController;
        targetTurtorial.forceTutorial = self.forceTutorial;
    }
}
@end
