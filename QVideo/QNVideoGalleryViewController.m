//
//  QNVideoGalleryViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/11/25.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoGalleryViewController.h"
#import <QNAPFramework/QNVideoTimeLineResponse.h>
#import <QNAPFramework/QNVideoTimeLine.h>
#import <QNAPFramework/QNVideoFileItem.h>
#import <QNAPFramework/QNVideoFileList.h>
#import <QNAPFramework/VLCMovieViewController.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>

#import "QNVideoGalleryTableCell.h"
#import "QNVideoItemDetailViewController.h"
#import "NSString_TimeDuration.h"
#import "QNTimeLineTableView.h"
#import "QNVideoTool.h"
#import "QNGallerySectionView.h"
#import "QNMoviePlayerViewController.h"
#import "QNVideoThumbnailViewController.h"
#import "QNButton.h"
#import "UIView+QVideoExt.h"

#define _backgrounColorValue 70.0f/256.0f
#define _highlightColorValue 92.0f/256.0f
#define _cellBackbroundColor 230.0f/256.0f
@interface QNVideoGalleryViewController ()
@end

@interface  QNVideosViewController (Extend)
- (void)setViewUserInteractionEnabled:(BOOL)enabled;
@end

@implementation QNVideoGalleryViewController

//- (id)initWithStyle:(UITableViewStyle)style{
//    self = [super initWithStyle:style];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad{
    [super viewDidLoad];

    [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
    [QNVideoTool generateRightBarItem:self withSelect:@selector(rightPanelAction:)];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self updateTimeList];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.fetchDelegate = self.parentPaneViewController;
    [self settingSortView];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:_cellBackbroundColor
                                                       green:_cellBackbroundColor
                                                        blue:_cellBackbroundColor
                                                       alpha:1.0f]];
    self.statusView.delegate = self.parentPaneViewController;
}

- (void)settingSortView{
    [self.sortView removeAllSubViews];
    QNGallerySectionView *galleryView = [[[NSBundle mainBundle] loadNibNamed:@"QNGallerySectionView" owner:self options:nil] objectAtIndex:0];
    UIImage *image = [UIImage imageNamed:@"timeline_bg_number.png"];
    UIImage *fixedImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    [galleryView.numberBg setImage:fixedImage];
    NSInteger totalVideos = [self countTotalVideos];
    [galleryView.numberLabel setText:totalVideos>999?@"999+":[NSString stringWithFormat:@"%i", totalVideos]];
    [self addBottomBorder:galleryView];
    
    [galleryView.yearMonthBtn addTarget:self action:@selector(showActionItems:) forControlEvents:UIControlEventTouchUpInside];
    [self.sortView addSubview:galleryView];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)leftPanelAction:(id)sender{
    [self.parentPaneViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:NO completion:nil];
}

- (void)rightPanelAction:(id)sender{
    [self.parentPaneViewController topRightDropdownMenuAction:self];
}

- (void)settingActionMenu{
    __block UITableView *blockTable = self.tableView;
    if (_yearMonthItems) {
        [_yearMonthItems removeAllObjects];
    }else
        _yearMonthItems = [NSMutableArray array];
    NSInteger sections = [[self.fetchedResultsController sections] count];
    for (NSInteger i = 0; i < sections; i++) {
        NSInteger actualSection = i + 1;
        NSString *yearMonth = [self headerString:actualSection];
        [_yearMonthItems addObject:yearMonth];
    }

    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(5, 0, 200, 0)];
    pickerView.dataSource = self;
    pickerView.delegate = self;

    [CXAlertView showContentViewAlert:NSLocalizedString(@"ChooseTimeLine", nil) withActionTitle:NSLocalizedString(@"Done", nil) withContentView:pickerView withActionHandler:^(CXAlertView *alertView, CXAlertButtonItem *item){
        UIPickerView *pickerView = pickerView;
        NSInteger selectedIndex = [pickerView selectedRowInComponent:0];
        NSInteger realSelectSection = selectedIndex + 1;
        [blockTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:realSelectSection]
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:YES];
        [alertView dismiss];
    }];
}

- (IBAction)showActionItems:(id)sender{
    [self settingActionMenu];
}
#pragma mark - Communication
- (void)updateTimeList{
    [QNVideoTimeLine MR_truncateAll];
    QNAPCommunicationManager *communication = [QNAPCommunicationManager share];
    [communication.videoStationManager getTimeLineListWithHomePath:videoFileHomePathPublic
                                                  withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoTimeLineResponse *res){
                                                      [self sendRequestAndReload];
                                                  }
                                                  withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                                      DDLogError(@"fetching timeline error!");
                                                  }];
}

- (void)reloadContent{
    [self.tableView reloadData];
}

- (void)sendRequestAndReload{
    //success & failure block
    [self hideDataView];
    [self.statusView setVideoStatus:QNVideoStatusTransfering];
    QNQNVideoFileListSuccessBlock successBlock = ^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoFileList *obj){
        self.fetchedResultsController = [QNVideoFileItem MR_fetchAllSortedBy:@"yearMonth"
                                                                   ascending:YES
                                                               withPredicate:nil
                                                                     groupBy:@"yearMonth"
                                                                    delegate:self
                                                                   inContext:[NSManagedObjectContext MR_defaultContext]];
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self reloadContent];
        
        if ([QNVideoFileItem MR_countOfEntities] == 0) {
            [self.statusView setVideoStatus:[self.parentPaneViewController noItemStatus]];
        }else{
            [self showDataView];
        }
    };

    QNFailureBlock failureBlock = ^(RKObjectRequestOperation *o, NSError *e){
        DDLogError(@"error while fetching videoList %@", e);
        [self.statusView setVideoStatus:QNVideoStatusError];
    };
    
    NSString *_thisQueryUUID = [[NSUUID UUID] UUIDString];
    
    if ([self.fetchDelegate respondsToSelector:@selector(fetchingRequest:withOriginContentViewController:withQueryUUID:withSuccessBlock:withFailureBlock:)]) {
        [self.fetchDelegate fetchingRequest:self.parentPaneViewController.homePath
            withOriginContentViewController:self
                              withQueryUUID:_thisQueryUUID
                           withSuccessBlock:successBlock
                           withFailureBlock:failureBlock];
    }
}

#pragma mark - PlayAction
- (void)playVideo:(id)sender{
    [self performSegueWithIdentifier:@"playMovie" sender:sender];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [[self.fetchedResultsController sections] count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (section == 0)? 0: 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"infiniteGridCell";
    QNVideoGalleryTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [self configureCell:cell withIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 160.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0)
        [self settingSortView];
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return (section == 0)?1:0;
}
#pragma mark - PrivateMethod
- (void)addBottomBorder:(UIView *)targetView{
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, targetView.frame.size.height - 1.0f, targetView.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [targetView.layer addSublayer:bottomBorder];
}

- (void)configureCell:(QNVideoGalleryTableCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    //creating the subview of scrollView
    NSInteger videoCount = [self countOfVideosInSection:[indexPath section] - 1];
    
    if (!cell.videoItems)
        cell.videoItems = [NSMutableArray array];
    else
        [cell.videoItems removeAllObjects];
    
    NSInteger index = 0;
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];

    NSString *yearMonth = [self headerString:[indexPath section]];
    [cell.timeLine setText:yearMonth];
    UIImage *imageBg = [UIImage imageNamed:@"timeline_bg_number.png"];
    UIImage *fixedImage = [imageBg resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    [cell.numberBgImageView setImage:fixedImage];
    [cell.numberOfVideosLabel setText:(videoCount>99)?@"99+":[NSString stringWithFormat:@"%i", videoCount]];
    
    cell.seeMoreBtn.userInfo = yearMonth;
    [cell.seeMoreBtn.titleLabel setText:NSLocalizedString(@"SeeMore", nil)];
    [cell.seeMoreBtn.titleLabel setTextColor:defaultGreenColor];
    cell.seeMoreBtn.hidden = (videoCount > 3)?NO:YES;
    while (index < 3){ //Note: only three videoItems needed by layout spec.
        QNVideoFileItem *fileItem = nil;
        if ([cell.videoItems count] < videoCount) {
            fileItem = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:[indexPath section] - 1 ]];
            [cell.videoItems addObject:fileItem];
            [((UILabel *)[cell.videoDurations objectAtIndex:index]) setText:[NSString stringFromTimeInterval:[fileItem.duration doubleValue]]];
            QNButton *cellPlayBtn = [cell.playBtns objectAtIndex:index];
            cellPlayBtn.userInfo = fileItem;
            ((UIImageView *)[cell.hdIndicators objectAtIndex:index]).hidden = (MIN([fileItem.iWidth floatValue], [fileItem.iHeight floatValue]) >= 720.0f)?NO:YES;
            [communicationManager.videoStationManager lazyloadingImage:[cell.videoThumbnails objectAtIndex:index]
                                                       withPlaceHolder:PlaceHolderImage
                                                            withFileId:fileItem.f_id
                                                     withProgressBlock:^(NSUInteger r, long long e){
                                                     }
                                                    withCompletedBlock:^(UIImage *i, NSError *e, SDImageCacheType c){
                                                        if (e)
                                                            DDLogError(@"WebImage Error: %@ ", e);
                                                    }];
            
        }else{
            [((UILabel *)[cell.videoDurations objectAtIndex:index]) setText:@"--:--"];
            QNButton *cellPlayBtn = [cell.playBtns objectAtIndex:index];
            cellPlayBtn.userInfo = nil;
            ((UIImageView *)[cell.hdIndicators objectAtIndex:index]).hidden = YES;
            UIImageView *videoThumbnail = (UIImageView *)[cell.videoThumbnails objectAtIndex:index];
            [videoThumbnail setImage:PlaceHolderImage];
        }
        index++;
    }
}

- (NSInteger)countTotalVideos{
    NSInteger r = 0;
    for (id <NSFetchedResultsSectionInfo> sectionInfo in [self.fetchedResultsController sections]) {
        r += [sectionInfo numberOfObjects];
    }
    return r;
}

- (NSInteger)countOfVideosInSection:(NSInteger)section{
    
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects] ;
    }else
        return 0;
}

- (CGFloat)caculateThumbnailScrollViewWidth:(NSInteger)section{
    NSInteger countOfVideos = [self countOfVideosInSection:section];
    return (countOfVideos + space) * 120.0f;
}

- (NSString *)headerString:(NSInteger)section{
    NSString *r = nil;
    if (section == 0)
        r = @"ALL";
    else {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section - 1];
        r = [sectionInfo name];
    }
    return r;
}

- (void)hideDataView{
    [UIView animateWithDuration:0.5f animations:^(void){
        self.tableView.alpha = 0.0f;
        self.sortView.alpha = 0.0f;
        self.statusView.alpha = 1.0f;
    }];
}

- (void)showDataView{
    [UIView animateWithDuration:0.5f animations:^(void){
        self.tableView.alpha = 1.0f;
        self.sortView.alpha = 1.0f;
        self.statusView.alpha = 0.0f;
    }];
}
#pragma mark - NSFetchedResultController Delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
//    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    QNVideoGalleryTableCell *targetCell = (QNVideoGalleryTableCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (type == NSFetchedResultsChangeUpdate && targetCell){
        [self configureCell:targetCell withIndexPath:indexPath];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
//    [self.tableView endUpdates];
}

#pragma mark - Navigation
- (void)gotoVideoDetail:(id)sender{
    [self performSegueWithIdentifier:@"pushVideoDetail" sender:sender];
}

- (void)goTimelineDetailViewController:(NSString *)timeline{
    //TODO
}
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"pushVideoDetail"]) {
        QNVideoItemDetailViewController *videoDetail = (QNVideoItemDetailViewController *)[segue destinationViewController];
        [videoDetail.navigationItem setTitle:self.navigationItem.title];
        if ([videoDetail respondsToSelector:@selector(setVideoItem:)]){
            QNButton *infoBtn = (QNButton *)sender;
            [videoDetail setVideoItem:infoBtn.userInfo];
        }
    }else if([segue.identifier isEqualToString:@"playMovie"]){
        QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
        DDLogVerbose(@"sid %@", communicationManager.sidForMultimedia);
        QNVideoFileItem *item = (QNVideoFileItem *)((QNButton *)sender).userInfo;
        /**
         *  若帶f應該也要拿得到
         */
        NSString *urlString = [NSString stringWithFormat:@"%@video/api/video.php?a=display&f=%@&vt=default&sid=%@", communicationManager.videoStationManager.baseURL, item.f_id, communicationManager.sidForMultimedia];
        QNMoviePlayerViewController *movieController = (QNMoviePlayerViewController *)segue.destinationViewController;
        movieController.thisVideoFileItem = item;
        movieController.url = [NSURL URLWithString:urlString];
        movieController.parentPaneViewController = self.parentPaneViewController;
    }else if([segue.identifier isEqualToString:@"viewThumbnailInYearMonth"]){ //viewThumbnailInYearMonth
        QNVideoThumbnailViewController *nextThumbnail = (QNVideoThumbnailViewController *)[segue destinationViewController];
        NSString *yearMonth = ((QNButton *)sender).userInfo;
        [nextThumbnail.navigationItem setTitle:yearMonth];

        NSDictionary *info = @{@"sortKey":@"cFileName",
                               @"isASC":@(YES),
                               @"predicate":[NSPredicate predicateWithFormat:@"self.yearMonth == %@", yearMonth]
                               };
        [nextThumbnail generateFetchedResultsController:info];
        nextThumbnail.needBackPreviousViewController = YES;
    }
}

#pragma mark - PickerDelegate & DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_yearMonthItems count]; //only one component
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [_yearMonthItems objectAtIndex:row];
}

@end
