//
//  QNMoviePlayerViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QNAPFramework/OBSlider.h>
#import "QNImageTextButton.h"
#import <QNAPFramework/VLCMovieViewController.h>
#import <QNAPFramework/QNVideoFileItem.h>
#import <IBActionSheet/IBActionSheet.h>
#import <QNAPFramework/QNVideoRender.h>

#import "QNMovieControlView.h"
#import "QNVideosViewController.h"

typedef enum : NSUInteger {
    QNVideoLocalPlayerMode,
    QNVideoGooglecastPlayerMode,
    QNVideoAirplayPlayerMode,
    QNVideoMultiZoneMode
} QNVideoPlayerMode;

typedef enum : NSUInteger {
    ActionSheetForNothing,
    ActionSheetForSetting,
    ActionSheetForMultizone,
    ActionSheetForGooglecast
} IBActionSheetType;

@interface QNMoviePlayerViewController : VLCMovieViewController<UIGestureRecognizerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIActionSheetDelegate>{
    NSArray *_renders;
    IBActionSheetType actionType;
    QNVideoPlayerMode playerMode;
    VLCTime *currentPlayTime;
    QNVideoRender *targetPlayRender;
}
//@property (nonatomic, strong) VLCMediaPlayer *mediaPlayer;
@property (nonatomic) BOOL isVideoPlaying;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) QNVideoFileItem *thisVideoFileItem;
@property(nonatomic, weak)QNVideosViewController *parentPaneViewController;

@property (weak, nonatomic) IBOutlet UISlider *volumnSlider;
@property (strong, nonatomic) IBOutlet UISlider *volumnPresentSlider;
@property (weak, nonatomic) IBOutlet UILabel *totalTime;
@property (weak, nonatomic) IBOutlet UIView *proceedView;
@property (weak, nonatomic) IBOutlet UIView *collectionsView;
@property (strong, nonatomic) IBOutletCollection(QNImageTextButton) NSArray *videoAggregateBtns;
@property (nonatomic) BOOL controlsHidden;
@property (weak, nonatomic) IBOutlet QNMovieControlView *controlView;
@property (nonatomic, strong) IBOutlet QNButton *showVolumnBtn;
@property (nonatomic, strong) IBOutlet UIImageView *multiZoneBg;
@property (nonatomic, strong) NSString *currentResolution;
@property (weak, nonatomic) IBOutlet UILabel *remainTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *volumeContainerView;
- (IBAction)backPriviousViewController:(id)sender;
- (IBAction)showVolumnOrNot:(id)sender;
- (IBAction)setting:(id)sender;
- (IBAction)volumeSliderChangeAction:(id)sender;

- (IBAction)volumeIncrease:(id)sender;
- (IBAction)volumeDecrease:(id)sender;

@end
