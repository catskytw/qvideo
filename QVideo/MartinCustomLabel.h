//
//  MartinCustomLabel.h
//  QVideo
//
//  Created by Change.Liao on 2014/4/11.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import<Foundation/Foundation.h>
#import<UIKit/UIKit.h>

@interface MartinCustomLabel : UILabel
{
@private
    CGFloat characterSpacing_;
    long    linesSpacing_;
}
@property(nonatomic,assign) CGFloat characterSpacing;
@property(nonatomic,assign) long    linesSpacing;
@property(nonatomic) UIEdgeInsets edgeInset;
- (int)getAttributedStringHeightWidthValue:(int)width;

@end