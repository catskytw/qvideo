//
//  QNVideoThumbnailCellView.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/21.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNButton.h"

@interface QNVideoThumbnailCellView : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *screenShot;
@property (weak, nonatomic) IBOutlet UIImageView *hdImageIndicator;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet QNButton *playVideoBtn;
@property (weak, nonatomic) IBOutlet QNButton *viewDetailBtn;
@property (weak, nonatomic) IBOutlet UIImageView *selectedMark;

@end
