//
//  QNVideoThumbnailViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/12/10.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoThumbnailViewController.h"
#import "QNVideoTool.h"
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <QNAPFramework/QNVideoFileList.h>
#import <QNAPFramework/QNVideoFileItem.h>
#import <QNAPFramework/QNVideoStationAPIManager.h>
#import <MediaPlayer/MediaPlayer.h>
#import <RestKit/RestKit.h>
#import <QNAPFramework/VLCMovieViewController.h>

#import "UIView+QVideoExt.h"
#import "QNVideoThumbnailCellView.h"
#import "QNVideoGalleryViewController.h"
#import "NSString_TimeDuration.h"
#import "QNVideoSortingSectionView.h"
#import "QNVideoThumbnailCellView.h"
#import "QNVideoItemDetailViewController.h"

#import <MarqueeLabel/MarqueeLabel.h>
#import "QNMoviePlayerViewController.h"
#import "QNVideoCommandViewController.h"

#define VideItemLayoutTag 9999

@interface QNVideoThumbnailViewController ()

@end

@implementation QNVideoThumbnailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.sortingView.parentViewController = self;
    self.statusView.delegate = self.parentPaneViewController;
    self.fetchDelegate = self.parentPaneViewController;
    [self reloadVideoFiles];
    NSString *sortString = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"SortBy", nil), NSLocalizedString(@"Title", nil)];
    [self.sortingView.sortingLabel setText:sortString];

    _filterDic = @{};
    _selections = [NSMutableArray array];
    
    [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
    [QNVideoTool generateRightBarItem:self withSelect:@selector(rightPanelAction:)];

}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"isSelectedMode"]) {
        [self reloadContent];
        
        if (self.parentPaneViewController.isSelectedMode) {
            [QNVideoTool generateCustomLeftBarItem:self withSelect:@selector(closeMultiSelection:) withTitleText:NSLocalizedString(@"Cancel", nil)];
        }else{
            [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
            [_selections removeAllObjects];
        }
        [self settingToolbarItem];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear: animated];
    QNVideosViewController *targetCommandViewController = self.parentPaneViewController;
    [targetCommandViewController removeObserver:self forKeyPath:@"isSelectedMode" context:nil];
    [_selections removeAllObjects];

}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self settingToolbarItem];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.parentPaneViewController.isSelectedMode = NO;
    [self reloadContent];
    
    QNVideosViewController *targetCommandViewController = self.parentPaneViewController;
    [targetCommandViewController addObserver:self
                                  forKeyPath:@"isSelectedMode"
                                     options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                                     context:nil];    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.parentPaneViewController.isSelectedMode = NO;
    [self.parentPaneViewController setPaneState:MSDynamicsDrawerPaneStateClosed animated:YES allowUserInterruption:YES completion:^(void){
    }];

}

- (void)reloadVideoFiles{
    [self sendRequestAndReload];
}

- (void)sendRequestAndReload{
    //success & failure block
    [self hideDataView];
    [self.statusView setVideoStatus:QNVideoStatusTransfering];
    QNQNVideoFileListSuccessBlock successBlock = ^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoFileList *obj){
        [self updateVideoFileList];
        if ([self collectionView:self.collectionView numberOfItemsInSection:0] == 0) {
            [self.statusView setVideoStatus:[self.parentPaneViewController noItemStatus]];
        }else{
            [self showDataView];
        }
    };
    
    QNFailureBlock failureBlock = ^(RKObjectRequestOperation *o, NSError *e){
        DDLogError(@"error while fetching videoList %@", e);
        [self.statusView setVideoStatus:QNVideoStatusError];
    };
    
    self.thisQueryUUID = [[NSUUID UUID] UUIDString];
    if (self.outsideFetchingBlock) {
        self.outsideFetchingBlock(self.thisQueryUUID, successBlock, failureBlock);
    }else if ([self.fetchDelegate respondsToSelector:@selector(fetchingRequest:withOriginContentViewController:withInfoDic:withSuccessBlock:withFailureBlock:)]){
        [self.fetchDelegate fetchingRequest:self.parentPaneViewController.homePath
            withOriginContentViewController:self
                                withInfoDic:@{@"uuid":self.thisQueryUUID, @"timeline":@"", @"filterDic":(_filterDic)?_filterDic:@{}}
                           withSuccessBlock:successBlock
                           withFailureBlock:failureBlock];
    }else
        [self showDataView];
}

- (void)hideDataView{
    [UIView animateWithDuration:0.5f animations:^(void){
        self.collectionView.alpha = 0.0f;
        self.sortingView.alpha = 0.0f;
        self.statusView.alpha = 1.0f;
    }];
}

- (void)showDataView{
    [UIView animateWithDuration:0.5f animations:^(void){
        self.collectionView.alpha = 1.0f;
        self.sortingView.alpha = 1.0f;
        self.statusView.alpha = 0.0f;
    }];
}

#pragma mark - PrivateMethod
- (void)updateVideoFileList{
    [self generateFetchedResultsController:@{@"sortKey":@"cPictureTitle", @"isASC":@(YES), @"sortName":@"Title",
                                             @"predicate":[NSPredicate predicateWithFormat:@"self.uuid == %@", self.thisQueryUUID]
                                             }];
    [self reloadContent];
}

- (NSArray *)multipleSelectedItems{
    return @[@"Share", @"Download", @"AddToTransCode", @"Delete", @"CopyToCollection", @"Cancel"];
}
//- (void)settingSortingTitle:(NSString *)sortKey{
//    NSString *sortString = [NSString stringWithFormat:@"Sorted by: %@", sortKey];
//    [self.sortingView.sortingLabel setText:sortString];
//}

#pragma mark - ScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    DDLogVerbose(@"end scroll here");
}

#pragma mark - CollectionView Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSInteger r = 0;
    if ([[self.fetchResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchResultsController sections] objectAtIndex:section];
        r = [sectionInfo numberOfObjects];
    }
    return r;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    QNVideoThumbnailCellView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"thumbnailCell" forIndexPath:indexPath];
    [self configureCell:cell withIndexPath:indexPath];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.parentPaneViewController.isSelectedMode) {
        QNVideoFileItem *fileItem = [self.fetchResultsController objectAtIndexPath:indexPath];
        if ([_selections containsObject:fileItem.f_id])
            [_selections removeObject:fileItem.f_id];
        else
            [_selections addObject:fileItem.f_id];
        
        //only update ONE collectionView
        [collectionView reloadItemsAtIndexPaths:@[indexPath]];
        [self settingToolbarItem];
    }
}

- (void)configureCell:(QNVideoThumbnailCellView *)cell withIndexPath:(NSIndexPath *)indexPath{
    DDLogVerbose(@"indexPath %i, %i", [indexPath section], [indexPath row]);
    QNVideoFileItem *fileItem = [self.fetchResultsController objectAtIndexPath:indexPath];
    
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    [communicationManager.videoStationManager lazyloadingImage:cell.screenShot
                                               withPlaceHolder:nil
                                                    withFileId:fileItem.f_id
                                             withProgressBlock:^(NSUInteger r, long long e){}
                                            withCompletedBlock:^(UIImage *i, NSError *e, SDImageCacheType c){
                                                if (e)
                                                    DDLogError(@"WebImage Error: %@ ", e);
                                            }];
    [cell.videoTitle setText:fileItem.cPictureTitle];
    cell.viewDetailBtn.userInfo = fileItem;
    cell.viewDetailBtn.userInteractionEnabled = !self.parentPaneViewController.isSelectedMode;
    cell.playVideoBtn.userInfo = fileItem;
    cell.playVideoBtn.userInteractionEnabled = !self.parentPaneViewController.isSelectedMode;
    
    cell.hdImageIndicator.hidden = (MIN([fileItem.iWidth floatValue], [fileItem.iHeight floatValue]) >= 720.0f)?NO:YES;
    [cell.durationLabel setText:[NSString stringFromTimeInterval:[fileItem.duration doubleValue]]];
    CGSize textSize = [cell.durationLabel.text caculateSizeOfDuration];
    [cell.durationLabel setFrame:CGRectMake(cell.durationLabel.frame.origin.x,
                                            cell.durationLabel.frame.origin.y,
                                            textSize.width + 3,
                                            textSize.height)];

    BOOL selectShowCondition = (self.parentPaneViewController.isSelectedMode);
    [cell.selectedMark setHidden:!selectShowCondition];
    if (selectShowCondition) {
        [cell.selectedMark setImage:[UIImage imageNamed:([_selections containsObject:fileItem.f_id])?@"icon_select":@"icon_normal"]];
    }
}

#pragma mark - NSFetchedResultControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    DDLogVerbose(@"controllerWillChangeContent");
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    DDLogVerbose(@"didChangeObject");
    QNVideoThumbnailCellView *cell = (QNVideoThumbnailCellView *)[self.collectionView cellForItemAtIndexPath:indexPath];
    [self configureCell:cell withIndexPath:indexPath];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    DDLogVerbose(@"controllerDidChangecontent");
}
#pragma mark - ActionMethod
- (void)closeMultiSelection:(id)sender{
    self.parentPaneViewController.isSelectedMode = NO;
    [QNVideoTool generateLeftBarItem:self withSelect:@selector(leftPanelAction:)];
}

- (void)leftPanelAction:(id)sender{
    if (self.needBackPreviousViewController) {
        [self.navigationController popViewControllerAnimated:YES];
    }else
        [self.parentPaneViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:NO completion:nil];
}

- (void)rightPanelAction:(id)sender{
    [self.parentPaneViewController topRightDropdownMenuAction:self];
}

- (void)gotoVideoDetail:(id)sender{
    [self performSegueWithIdentifier:@"videoDetail" sender:sender];
}

- (void)playVideo:(id)sender{
    QNVideoFileItem *videoItem = (QNVideoFileItem *)((QNButton *)sender).userInfo;
    
    if ([videoItem.iWidth intValue] > 720 || [videoItem.iHeight intValue] > 720) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:NSLocalizedString(@"Continue", nil)
                          withMessage:NSLocalizedString(@"HDVidePlayWarning", nil)
                    withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                        [self performSegueWithIdentifier:@"playMovie" sender:sender];
                        [alert dismiss];
                    }];
    }else
        [self performSegueWithIdentifier:@"playMovie" sender:sender];
}

- (void)playVideoItem:(QNVideoFileItem *)item{
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    DDLogVerbose(@"sid %@", communicationManager.sidForMultimedia);
//    NSString *urlString = [NSString stringWithFormat:@"%@video/api/video.php?a=display&f=%@&vt=default&sid=%@", communicationManager.videoStationManager.baseURL, item.f_id, communicationManager.sidForMultimedia];
    NSString *urlString = [QNVideoTool generateGetVideoURL:item.f_id isDownload:NO];
    QNMoviePlayerViewController *movieController = [[QNMoviePlayerViewController alloc] initWithNibName:nil bundle:nil];
    movieController.url = [NSURL URLWithString:urlString];
    movieController.thisVideoFileItem = item;
    
    [self.parentPaneViewController.navigationController pushViewController:movieController animated:YES];
}

- (void)settingToolbarItem{
    [self.parentPaneViewController settingToolbarItem:self withToolBarStyle:VideosToolBarStyle];
}

//- (void)multiSelectAction:(id)sender{
//    DDLogVerbose(@"multiSelectAction");
//    _multiSelectAction = [self multipleSelectedItems];
//    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil)
//                                                       delegate:self
//                                              cancelButtonTitle:nil
//                                         destructiveButtonTitle:nil
//                                              otherButtonTitles:nil];
//    
//    for (NSString *_action in _multiSelectAction) {
//        [sheet addButtonWithTitle:NSLocalizedString(_action, _action)];
//    }
//    [sheet showInView:self.view];
//}

- (void)reloadContent{
    NSPredicate *predicate = nil;
    if (_addtionalPredicate) {
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[_addtionalPredicate, self.originalPredicate]];
    }else
        predicate = self.originalPredicate;
    [NSFetchedResultsController deleteCacheWithName:nil];
    [self.fetchResultsController.fetchRequest setPredicate:predicate];
    [self.collectionView reloadData];
}

- (NSFetchedResultsController *)generateFetchedResultsController:(NSDictionary *)info{
    NSString *sortKey = (NSString *)[info valueForKey:@"sortKey"];
    BOOL isASC = [[info valueForKey:@"isASC"] boolValue];
    self.originalPredicate = (NSPredicate *)[info valueForKey:@"predicate"];
//    NSPredicate *predicate = nil;
//    if (_addtionalPredicate) {
//        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[_addtionalPredicate, (NSPredicate *)[info valueForKey:@"predicate"]]];
//    }else{
//        predicate = (NSPredicate *)[info valueForKey:@"predicate"];
//    }
    NSString *groupBy = [info valueForKey:@"groupBy"];
    NSFetchedResultsController *fetch = [QNVideoFileItem MR_fetchAllSortedBy:sortKey
                                                                   ascending:isASC
                                                               withPredicate:nil
                                                                     groupBy:groupBy
                                                                    delegate:(id<NSFetchedResultsControllerDelegate>)self.parentViewController];
    NSString *sortString = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"SortBy", nil),NSLocalizedString([info valueForKey:@"sortName"], nil) ];
    [self.sortingView.sortingLabel setText:sortString];
    self.fetchResultsController = fetch;
    return fetch;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    //You can fire any segue while selectMode is YES.
    return !self.parentPaneViewController.isSelectedMode;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"videoDetail"]) {
        QNVideoItemDetailViewController *videoDetail = (QNVideoItemDetailViewController *)[segue destinationViewController];
        [videoDetail.navigationItem setTitle:self.navigationItem.title];
        if ([videoDetail respondsToSelector:@selector(setVideoItem:)]){
            QNButton *infoBtn = (QNButton *)sender;
            QNVideoFileItem *fileItem = (QNVideoFileItem *)infoBtn.userInfo;
            [videoDetail setVideoItem:fileItem];
            [videoDetail.navigationItem setTitle:fileItem.cPictureTitle];
            [_selections removeAllObjects];
            [_selections addObject:fileItem.f_id];
        }
        
        videoDetail.parentVideoViewController = self.parentPaneViewController;
    }else if([segue.identifier isEqualToString:@"gotoSortSelection"]){ //gotoSortSelection
        
    }else if([segue.identifier isEqualToString:@"playMovie"]){
        QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
        DDLogVerbose(@"sid %@", communicationManager.sidForMultimedia);
        QNVideoFileItem *item = (QNVideoFileItem *)((QNButton *)sender).userInfo;
//        NSString *urlString = [NSString stringWithFormat:@"%@video/api/video.php?a=display&f=%@&vt=default&sid=%@", communicationManager.videoStationManager.baseURL, item.f_id, communicationManager.sidForMultimedia];
        NSString *urlString = [QNVideoTool generateGetVideoURL:item.f_id isDownload:NO];

        QNMoviePlayerViewController *movieController = (QNMoviePlayerViewController *)segue.destinationViewController;
        movieController.thisVideoFileItem = item;
        movieController.parentPaneViewController = self.parentPaneViewController;
        movieController.url = [NSURL URLWithString:urlString];
    }
}
#pragma mark - ActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{    
    /**
     0. share
     1. Download
     2. Add To Transcode
     3. Delete
     4. Copy To Collection
     5. Cancel
     */
    if ([_selections count] == 0) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:nil
                          withMessage:NSLocalizedString(@"NoVideoSelected", nil)
                    withActionHandler:nil];
        return;
    }

    switch (buttonIndex) {
        case 0:
            [self.parentPaneViewController shareSelectedFilme];
            break;
        case 1:
            [self.parentPaneViewController downloadSelectedFilms];
            break;
        case 2:
            [self.parentPaneViewController addToTransCode];
            break;
        case 3:{
            [self.parentPaneViewController deleteFiles];
        }
            break;
        case 4:{
            [self.parentPaneViewController addToAlbum];
        }
            break;
        case 5:
        default:
            [actionSheet dismissWithClickedButtonIndex:5 animated:YES];
            break;
    }
}

- (void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray*)titles{
    NSString *title1 = (NSString *)[titles objectAtIndex:0];
    NSString *title2 = (NSString *)[titles objectAtIndex:1];
    
    NSDictionary *dataDic = (NSDictionary *)pickerView.userInfo;
    NSString *value = [dataDic valueForKey:title2];
    /**
     NSString *sortKey = (NSString *)[info valueForKey:@"sortKey"];
     BOOL isASC = [[info valueForKey:@"isASC"] boolValue];
     */
    [self generateFetchedResultsController:@{@"sortKey": value,
                                             @"isASC": [title1 isEqualToString:NSLocalizedString(@"Descending", nil)]?@(NO):@(YES),
                                             @"sortName":title2
                                             }];
    [self reloadContent];
}

#pragma mark - VideoDataSource
- (NSArray *)multipleSelectedFilms{
    return _selections;
}
- (void)switchMultiSelectMode{
    self.parentPaneViewController.isSelectedMode = !self.parentPaneViewController.isSelectedMode;
}
- (void)settingAddtionalPredicate:(NSPredicate *)predicate{
    _addtionalPredicate = predicate;
}

- (void)settingFilterDic:(NSDictionary *)dic{
    _filterDic = [dic copy];
}
@end
