//
//  QNTutorialStartViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/4/30.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNTutorialStartViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *goTurtorialBtn;
@property (nonatomic) BOOL forceTutorial;
@end
