//
//  ASViewController.m
//  ASTextViewDemo
//
//  Created by Adil Soomro on 4/14/14.
//  Copyright (c) 2014 Adil Soomro. All rights reserved.
//

#import "ASViewController.h"
#import "ASTextField.h"
#import <MagicalRecord/CoreData+MagicalRecord.h>

@interface ASViewController ()
@end

@implementation ASViewController


- (void)awakeFromNib
{
    //setup text field with respective icons
    [_userNameField setupTextFieldWithType:ASTextFieldTypeRound withIconName:@"password_icon"];
    [_passwordField setupTextFieldWithType:ASTextFieldTypeRound withIconName:@"password_icon"];
    [self.forbiddenBtn setTitle:NSLocalizedString(@"NotShowNextTime", nil) forState:UIControlStateNormal];
    [self.contentMessage setText:NSLocalizedString(@"loginMessageInPrivate", nil)];
    self.forbiddenBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
}

- (void)settingCheckBoxIconImage{
    _isShowLoginNextTime = [self.targetNASServer.needLoginInPrivate boolValue];
    [self.checkBoxIcon setImage:[UIImage imageNamed:(!_isShowLoginNextTime)?@"icon_checkbox_on":@"icon_checkbox"]];
}
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self resignAllResponders];
    return YES;
}

- (IBAction)showThisAlertOrNot:(id)sender{
    self.targetNASServer.needLoginInPrivate = [NSNumber numberWithBool:!_isShowLoginNextTime];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error){
        [self settingCheckBoxIconImage];
    }];
}

- (IBAction)letMeIn:(id)sender {
    [self resignAllResponders];
}

- (void)resignAllResponders{
    [_passwordField resignFirstResponder];
}

@end
