//
//  QNFilterView.h
//  QVideo
//
//  Created by Change.Liao on 2014/7/16.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ActionBlock)(NSInteger actionIndex);

@interface QNFilterView : UITableView <UITableViewDelegate, UITableViewDataSource>{
    NSArray *_options;
    BOOL _isCollection;
}
@property (nonatomic, strong) ActionBlock actionBlock;

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)tableStyle isCollection:(BOOL)isCollection withActionBlock:(void(^)(NSInteger actionIndex))action;

@end
