//
//  QNRadialProgressView.m
//  QVideo
//
//  Created by Change.Liao on 2014/2/12.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNRadialProgressView.h"
@interface MDRadialProgressView (Extend)
- (void)drawCenter:(CGContextRef)contextRef withViewSize:(CGSize)viewSize andCenter:(CGPoint)center;

@end

@implementation QNRadialProgressView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)settingTextLabel{
    if (self.playPauseImage) {
        [self.playPauseImage removeFromSuperview];
        self.playPauseImage = nil;
    }
    [self.label setHidden:NO];
}

- (void)settingPlayPauseImage:(BOOL)isRunning{
    if (!self.playPauseImage) {
        self.playPauseImage = [[UIImageView alloc] initWithFrame:CGRectMake(16, 16, 12, 12)];
        [self addSubview:self.playPauseImage];
    }
    NSString *imageString = (isRunning)?@"playback_pause_press":@"playback_play_press";
    [self.playPauseImage setImage:[UIImage imageNamed:imageString]];
    [self.label setHidden:YES];
}

- (void)drawCenter:(CGContextRef)contextRef withViewSize:(CGSize)viewSize andCenter:(CGPoint)center{
    [super drawCenter:contextRef withViewSize:viewSize andCenter:center];
    CGFloat space = 8.0f;

    CGRect innerCircleRect = CGRectMake(center.x - (viewSize.width/2.0f) + space, center.y - (viewSize.height/2.0f) + space, viewSize.width - (2*space) , viewSize.height - (2*space));
    
    CGContextSetLineWidth(contextRef, self.innerCircleWidth);
    CGContextSetStrokeColorWithColor(contextRef,
                                     self.innerCircleColor.CGColor);
    CGContextAddEllipseInRect(contextRef, innerCircleRect);
    CGContextStrokePath(contextRef);
}


@end
