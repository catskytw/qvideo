//
//  QNImageTextButton.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNButton.h"

@interface QNImageTextButton : QNButton
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *btnText;

@end
