//
//  QNResolutionTableViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNResolutionTableViewController.h"
#import "QNVideoTool.h"
#import "QNVideoCommonSectionView.h"
#import "QNResolutionAlert.h"

@interface QNResolutionTableViewController ()

@end

@implementation QNResolutionTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self settingHeaderView];
    _resolutions = [NSMutableArray arrayWithObject:@{@"resolutionItem":@"Original file", @"isOnTheFly":@(NO)}];
    [self findCurrentResolution];
    [self generateResolutions];
}

- (void)generateResolutions{
    if ([self.thisFilmItem.v240P boolValue]) {
        [_resolutions addObject:@{@"resolutionItem":@"240p", @"isOnTheFly":@(NO)}];
    }else if (_currentResolution >= 240){
        [_resolutions addObject:@{@"resolutionItem":@"240p", @"isOnTheFly":@(YES)}];
    }
    
    if ([self.thisFilmItem.v360P boolValue]) {
        [_resolutions addObject:@{@"resolutionItem":@"360p", @"isOnTheFly":@(NO)}];
    }else if (_currentResolution >= 360){
        [_resolutions addObject:@{@"resolutionItem":@"360p", @"isOnTheFly":@(YES)}];
    }
    
    if ([self.thisFilmItem.v480P boolValue]) {
        [_resolutions addObject:@{@"resolutionItem":@"480p", @"isOnTheFly":@(NO)}];
    }else if (_currentResolution >= 480){
        [_resolutions addObject:@{@"resolutionItem":@"480p", @"isOnTheFly":@(YES)}];
    }
    
    if ([self.thisFilmItem.v720P boolValue]) {
        [_resolutions addObject:@{@"resolutionItem":@"720p", @"isOnTheFly":@(NO)}];
    }else if (_currentResolution >= 720){
        [_resolutions addObject:@{@"resolutionItem":@"720p", @"isOnTheFly":@(YES)}];
    }
    
    if ([self.thisFilmItem.v1080P boolValue]) {
        [_resolutions addObject:@{@"resolutionItem":@"1080p", @"isOnTheFly":@(NO)}];
    }else if (_currentResolution >= 1080){
        [_resolutions addObject:@{@"resolutionItem":@"1080p", @"isOnTheFly":@(YES)}];
    }
}
- (void)back:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)settingHeaderView{
    NSString *r = NSLocalizedString(@"Quality_Upper", nil);
    CGFloat estimateTextHeight = [QNVideoTool heightOfCellWithIngredientLine:r
                                                          withSuperviewWidth:self.view.frame.size.width];
    
    QNVideoCommonSectionView *backgroundView = [[[NSBundle mainBundle] loadNibNamed:@"QNVideoCommonSectionView" owner:nil options:nil] objectAtIndex:0];
    [backgroundView setFrame:CGRectMake(0, 0, self.view.frame.size.width, 25 + 13 +estimateTextHeight)];
    [backgroundView.sectionLabel setText:r];
    [backgroundView.sectionLabel sizeToFit];
    self.headerView = backgroundView;
}

#pragma mark - 
#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return self.headerView.frame.size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_resolutions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"resolutionCell"];
    NSInteger row = [indexPath row];
    NSDictionary *resolutionDic = [_resolutions objectAtIndex:row];
    
    NSString *selectedResolutionString = [resolutionDic valueForKey:@"resolutionItem"];
    BOOL isRealtime = [[resolutionDic valueForKey:@"isOnTheFly"] boolValue];
    
    NSString *cellString = ([selectedResolutionString isEqualToString:@"Original file"])?NSLocalizedString(@"OriginalFile", nil):selectedResolutionString;
    NSString *showResolutionString = [NSString stringWithFormat:@"%@%@", cellString, (isRealtime)?NSLocalizedString(@"realTimeTransCode", nil):@""];
    [cell.textLabel setText: showResolutionString];
    
//    NSString *resolutionInSetting = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"resolution"];

    NSString *resolutionInSetting = nil;
    resolutionInSetting = self.playerViewController.currentResolution;
    
    if (resolutionInSetting != nil && [resolutionInSetting isEqualToString:selectedResolutionString]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else
        cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    void(^selectResolutionProcess)(void) = ^(void){
        NSDictionary *resolutionDic = [_resolutions objectAtIndex:[indexPath row]];
        //TODO: pass the resolution backforward the player?
        [self.tableView reloadData];
        
        NSString *urlString = [QNVideoTool generateVideoOnlineURL:self.thisFilmItem.f_id withResolution:[resolutionDic valueForKeyPath:@"resolutionItem"]];
        self.playerViewController.url = [NSURL URLWithString:urlString];
        self.playerViewController.currentResolution = [resolutionDic valueForKeyPath:@"resolutionItem"];
        [self.navigationController popViewControllerAnimated:YES];
    };
    
    NSDictionary *resolutionDic = [_resolutions objectAtIndex:[indexPath row]];
    BOOL isRealtime = [[resolutionDic valueForKey:@"isOnTheFly"] boolValue];

    if (!isRealtime) {
        [[NSUserDefaults standardUserDefaults] setValue:[resolutionDic valueForKeyPath:@"resolutionItem"] forKey:@"resolution"];
        NSString *urlString = [QNVideoTool generateGetVideoURL:self.thisFilmItem.f_id isDownload:NO];
        self.playerViewController.currentResolution =[resolutionDic valueForKeyPath:@"resolutionItem"];
        self.playerViewController.url = [NSURL URLWithString:urlString];
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        NSNumber *number = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"needAskResolution"];
        BOOL needAsk = (number == nil)? YES: [number boolValue];
        if (needAsk) {
            QNResolutionAlert *alert = [[[NSBundle mainBundle] loadNibNamed:@"QNResolutionAlert"
                                                                      owner:nil
                                                                    options:nil] objectAtIndex:0];
            
            [CXAlertView showContentViewAlert:NSLocalizedString(@"TranscodingInRealTime", nil)
                              withActionTitle:NSLocalizedString(@"Continue", nil)
                              withContentView:alert
                            withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                                [alert dismiss];
                                selectResolutionProcess();
                            }];
        }else
            selectResolutionProcess();
    }

}

#pragma mark - PrivateMethod

- (void)findCurrentResolution{
    _currentResolution = MIN([self.thisFilmItem.iHeight floatValue], [self.thisFilmItem.iWidth floatValue]);
}
@end
