//
//  QNGallerySectionView.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/20.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNGallerySectionView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *numberBg;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UIButton *yearMonthBtn;
@end
