//
//  QNTimelineReusableView.m
//  QVideo
//
//  Created by Change.Liao on 2014/3/26.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNTimelineReusableView.h"

@implementation QNTimelineReusableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
