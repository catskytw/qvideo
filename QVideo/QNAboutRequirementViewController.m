//
//  QNAboutRequirementViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/28.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNAboutRequirementViewController.h"
#import "QNVideoTool.h"
@interface QNAboutRequirementViewController ()

@end

@implementation QNAboutRequirementViewController
- (void)settingCoreText{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];

    NSString *textName = [NSString stringWithFormat:@"Requirement_%@.txt", language];
    self.coreTextView.text = [[self class] textFromTextFileNamed:textName];
}
@end
