//
//  QNAddCollectionTableViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/7/22.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QNAPFramework/QNVideoCollectionResponse.h>
#import <QNAPFramework/QNCollection.h>
#import "QNAddCollectionTableViewCell.h"

@interface QNAddCollectionTableViewController : UITableViewController<UITextFieldDelegate>
@property (nonatomic, strong) QNCollection *collection;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UITextField *collectionName;
@property (weak, nonatomic) IBOutlet UIDatePicker *validDate;
@property (weak, nonatomic) IBOutlet UIImageView *sharedWithUserImage;
@property (weak, nonatomic) IBOutlet UIImageView *editByAnyoneImage;
@property (weak, nonatomic) IBOutlet UIImageView *editByOwnerImage;
@property (weak, nonatomic) IBOutlet UIImageView *publicImage;
@property (weak, nonatomic) IBOutlet UIImageView *validForeverImage;
@property (weak, nonatomic) IBOutlet UIImageView *validDateImage;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *sharedSubItems;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *publicSubItems;

- (IBAction)dateValueChanged:(UIDatePicker *)picker;

@end
