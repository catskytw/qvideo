//
//  QNAboutDisclaimerViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/28.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNAboutDisclaimerViewController.h"

@interface QNAboutDisclaimerViewController ()

@end

@implementation QNAboutDisclaimerViewController
- (void)settingCoreText{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];

    NSString *textName = [NSString stringWithFormat:@"Disclaimer_%@.txt", language];
    self.coreTextView.text = [[self class] textFromTextFileNamed:textName];
}
@end
