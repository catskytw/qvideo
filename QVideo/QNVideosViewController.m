//
//  QNVideosViewController.m
//  QVideo
//
//  Created by Change.Liao on 2013/11/22.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <QNAPFramework/QNCollection.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <REMenu/REMenu.h>
#import "QNVideosViewController.h"
#import "QNVideoCommandViewController.h"
#import "QNVideoSettingViewController.h"
#import "QNVideoGalleryViewController.h"
#import "QNTimelineViewController.h"
#import "QNVideoThumbnailViewController.h"
#import "QNVideoListViewController.h"
#import "QNSettingViewController.h"
#import "QNVideoFileItem.h"
#import "QNAppDelegate.h"
#import "QNVideoTool.h"
#import "QNDownloadListViewController.h"
#import "QNFolderViewController.h"
#import "QNVideoSharingViewController.h"
#import "QNVideoTrashCanViewController.h"
#import "QNVideoAlbumViewController.h"
#import "VideoDownload.h"
#import "VideoUpload.h"
#import "ASViewController.h"

@interface QNVideosViewController ()

@end

@implementation QNVideosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    _resolutions = @[@"240p", @"360p", @"480p", @"720p", @"1080p"];
    _selectedResolutions = [NSMutableArray array];
    //adding style
    MSDynamicsDrawerScaleStyler *styler = [MSDynamicsDrawerScaleStyler styler];
    styler.closedScale= 0.1f;
    [self addStylersFromArray:@[styler, [MSDynamicsDrawerFadeStyler styler]] forDirection:MSDynamicsDrawerDirectionLeft];

    
    self.homePath = videoFileHomePathPublic;
    
    //adding the viewControllers on left/right side
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];

    self.commandViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"videoCommandViewController"];
    self.settingViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"videoSettingViewController"];
    self.commandViewController.parentPanelViewController = self;
    
    //setting data for sub viewControllers
    [self.settingViewController setThisServer:self.thisServer];
    
    [self setDrawerViewController:self.settingViewController forDirection:MSDynamicsDrawerDirectionLeft];

    //Enter SharedVideo while startup.
    self.title = NSLocalizedString(@"SharedVideo", nil);
    [self changeContentWithGalleryViewController:NO withCompleteBlock:nil];
    
    [self.navigationController setNavigationBarHidden:YES];
    [QNVideoTool creatingDownloadFolder];
    
    _loadingImages = [NSMutableArray array];
    NSArray *images = @[@"Searching_1",
                        @"Searching_2",
                        @"Searching_3",
                        @"Searching_4",
                        @"Searching_5",
                        @"Searching_6",
                        @"Searching_7",
                        @"Searching_8"
                        ];
    
    for (NSString *imageString in images) {
        UIImage *image = [UIImage imageNamed:imageString];
        [_loadingImages addObject:image];
    }

    _previousUploadSendDate = [NSDate date];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)settingToolbarItem:(UIViewController *)senderViewController withToolBarStyle:(QVideoToolBarStyle)style{
    UIToolbar *targetToolBar = senderViewController.navigationController.toolbar;
    NSInteger count = [[self.videoDataSource multipleSelectedFilms] count];
    if (self.isSelectedMode && count > 0) {
        switch (style) {
            case VideosToolBarStyle:
                [self settingToolbarItemsForVideos:targetToolBar withViewController:senderViewController];
                break;
            case AlbumsToolBarStyle:
                [self settingToolbarItemsForAlbum:targetToolBar withViewController:senderViewController];
                break;
            case TrashcanToolBarStyle:
                [self settingToolbarItemsForTrashcan:targetToolBar withViewController:senderViewController];
                break;
            case ShareToolBarStyle:{
                [self settingToolbarItemsForShare:targetToolBar withViewController:senderViewController];
            }
                break;
            default:
                break;
        }
        
        DDLogInfo(@"toolbar frame %@, self.view frame %@", NSStringFromCGRect(targetToolBar.frame), NSStringFromCGRect(self.view.frame));
        if (targetToolBar.frame.origin.y >= self.view.frame.size.height) {
            [targetToolBar setFrame:CGRectMake(targetToolBar.frame.origin.x,
                                               targetToolBar.frame.origin.y - targetToolBar.frame.size.height, targetToolBar.frame.size.width,
                                               targetToolBar.frame.size.height)];
        }
        [senderViewController.navigationController.toolbar setHidden:NO];

    }else{
        [senderViewController.navigationController.toolbar setHidden:YES];
        [senderViewController setToolbarItems:@[]];
        [self.moreOptionsMenu close];
    }
}
#pragma mark - ToolBar methods
/**
 *  Setting the toolbar for videos' selection
 *
 *  @param targetToolbar
 */
- (void)settingToolbarItemsForVideos:(UIToolbar *)targetToolbar withViewController:(UIViewController *)viewController{
    //share, download, delete, more(add into collection, add into transcode
    NSMutableArray *actionBtns = [NSMutableArray array];
    NSInteger count = 0;
    if ([self.videoDataSource respondsToSelector:@selector(multipleSelectedFilms)]) {
        count = [[self.videoDataSource multipleSelectedFilms] count];
        UIBarButtonItem *countBtn = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"%i", count] style:UIBarButtonItemStylePlain target:nil action:nil];
        [actionBtns addObject:countBtn];
    }
    UIBarButtonItem *shareBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_share"] style:UIBarButtonItemStylePlain target:self action:@selector(shareSelectedFilme)];
    
    UIBarButtonItem *download = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"multiple-select_download"] style:UIBarButtonItemStylePlain target:self action:@selector(downloadSelectedFilms)];
    
    //TODO fix here, removeFilesFromAlbum
    BOOL isDeleteFromAlbum = (self.homePath == videoFileHomePathCollections || self.homePath == videoFileHomePathSmartCollections);

    UIBarButtonItem *delete = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"multiple-select_delete"] style:UIBarButtonItemStylePlain target:self action:(isDeleteFromAlbum)?@selector(removeFilesFromAlbum):@selector(deleteFiles)];
    UIBarButtonItem *more = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_more1"] style:UIBarButtonItemStylePlain target:self action:@selector(moreOption)];
    
    [actionBtns addObjectsFromArray: @[
                                       shareBtn,
                                       download,
                                       delete,
                                       more]];
    
    NSMutableArray *allBtns = [NSMutableArray arrayWithObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    
    for (UIBarButtonItem *item in actionBtns) {
        [allBtns addObject:item];
        [allBtns addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    }
    
    [viewController setToolbarItems:allBtns];
}

- (void)settingToolbarItemsForAlbum:(UIToolbar *)targetToolbar withViewController:(UIViewController *)viewController{
    NSMutableArray *actionBtns = [NSMutableArray array];
    NSInteger count = 0;
    if ([self.videoDataSource respondsToSelector:@selector(multipleSelectedFilms)]) {
        count = [[self.videoDataSource multipleSelectedFilms] count];
        UIBarButtonItem *countBtn = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"%i", count] style:UIBarButtonItemStylePlain target:nil action:nil];
        [actionBtns addObject:countBtn];
    }

    UIBarButtonItem *removeAlbum = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"multiple-select_delete"] style:UIBarButtonItemStylePlain target:self action:@selector(deleteAlbums)];
    [actionBtns addObject:removeAlbum];

    NSMutableArray *allBtns = [NSMutableArray arrayWithObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    
    for (UIBarButtonItem *item in actionBtns) {
        [allBtns addObject:item];
        [allBtns addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    }

    [viewController setToolbarItems:allBtns];
}

- (void)settingToolbarItemsForTrashcan:(UIToolbar *)targetToolbar withViewController:(UIViewController *)viewController{
    NSMutableArray *actionBtns = [NSMutableArray array];
    NSInteger count = 0;
    if ([self.videoDataSource respondsToSelector:@selector(multipleSelectedFilms)]) {
        count = [[self.videoDataSource multipleSelectedFilms] count];
        UIBarButtonItem *countBtn = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"%i", count] style:UIBarButtonItemStylePlain target:nil action:nil];
        [actionBtns addObject:countBtn];
    }

    UIBarButtonItem *removeAlbum = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"multiple-select_delete"] style:UIBarButtonItemStylePlain target:viewController action:@selector(removeDeletedFilms)];
    [actionBtns addObject:removeAlbum];
    
    UIBarButtonItem *recovery = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_recover"] style:UIBarButtonItemStylePlain target:viewController action:@selector(recoverDeletedFilms)];
    [actionBtns addObject:recovery];
    
    NSMutableArray *allBtns = [NSMutableArray arrayWithObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    
    for (UIBarButtonItem *item in actionBtns) {
        [allBtns addObject:item];
        [allBtns addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    }
    
    [viewController setToolbarItems:allBtns];
}

- (void)settingToolbarItemsForShare:(UIToolbar *)targetToolbar withViewController:(UIViewController *)viewController{
    NSMutableArray *actionBtns = [NSMutableArray array];
    UIBarButtonItem *email = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"email"] style:UIBarButtonItemStylePlain target:viewController action:@selector(shareByEmail)];
    UIBarButtonItem *sms = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"sms"] style:UIBarButtonItemStylePlain target:viewController action:@selector(shareBySMS)];
    UIBarButtonItem *sharelink = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"linked"] style:UIBarButtonItemStylePlain target:viewController action:@selector(copyShareLinked)];
    
    [actionBtns addObjectsFromArray:@[email, sms, sharelink]];
    NSMutableArray *allBtns = [NSMutableArray arrayWithObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    
    for (UIBarButtonItem *item in actionBtns) {
        [allBtns addObject:item];
        [allBtns addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    }
    [viewController setToolbarItems:allBtns];
}
- (void)moreOption{
    /***
     1.add into transcode
     2.add into collection
     */
    if (!self.moreOptionsMenu) {
        REMenuItem *addIntoTranscode = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"AddToTransCode", nil)
                                                                   image:nil
                                                        highlightedImage:nil
                                                                  action:^(REMenuItem *item){
                                                                      [self addToTransCode];
                                                                  }];
        
        REMenuItem *addIntoCollection = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"CopyToCollection", nil)
                                                                    image:nil
                                                         highlightedImage:nil
                                                                   action:^(REMenuItem *item){
                                                                       [self addToAlbum];
                                                                   }];
        self.moreOptionsMenu = [[REMenu alloc] initWithItems:@[addIntoCollection, addIntoTranscode]];
        [self settingActionMenuLayout:self.moreOptionsMenu];
    }
    if (self.moreOptionsMenu.isOpen) {
        [self.moreOptionsMenu close];
    }else{
        CGFloat width = 120;
        CGFloat viewHeight = 128.0f;
        CGFloat yPosition = self.view.frame.size.height - viewHeight - 20.0f;
        [self.moreOptionsMenu showFromRect:CGRectMake(self.view.frame.size.width - width, yPosition, width, self.view.frame.size.height) inView:self.view];
    }
}

#pragma mark - fetchingDelegate
- (void)fetchingRequest:(VideoFileHomePath)homePath withOriginContentViewController:(UIViewController *)contentViewController withQueryUUID:(NSString *)uuid withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    [self fetchingRequest:homePath withOriginContentViewController:contentViewController withInfoDic:@{@"uuid":uuid, @"timeline":@""} withSuccessBlock:success withFailureBlock:failure];
}

- (void)fetchingRequest:(VideoFileHomePath)homePath withOriginContentViewController:(UIViewController *)contentViewController withInfoDic:(NSDictionary *)dic withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    NSString *uuid = dic[@"uuid"];
    //stop all recursivly fetching
    [[NSNotificationCenter defaultCenter] postNotificationName:STOP_RECURSIVE_FETCH object:nil];
    //send the request
    [QNVideoFileItem MR_truncateAll];
    NSDictionary *filterDic = dic[@"filterDic"];

    switch (homePath) {
        case videoFileHomePathPublic:{
            NSString *timeline = dic[@"timeline"];
            [[QNVideoTool share] recursiveUpdateTimelineVideoFiles:homePath
                                                      withTimeline:timeline
                                                    withPageNumber:1
                                                          withUUID:uuid
                                                     withFilterDic:filterDic
                                          withEachPageSuccessBlock:success
                                                  withSuccessBlock:success withFailureBlock:failure];
        }
            break;
        case videoFileHomePathPrivate:
        case videoFileHomePathQSync:{
            [[QNVideoTool share] recursiveUpdateVideoFiles:homePath
                                            withPageNumber:1
                                                  withUUID:uuid
                                             withFilterDic:filterDic
                                  withEachPageSuccessBlock:success
                                          withSuccessBlock:success
                                          withFailureBlock:failure];
        }
            break;
        case videoFileHomePathSmartCollections:
        case videoFileHomePathCollections:
            [[QNVideoTool share] recursiveLoadCollections:homePath withUUID:uuid withFilterDic:filterDic withSuccessBlock:success withEachPageSuccessBlock:success withFailureBlock:failure];
            break;
        case videoFileHomePathTrashCan:
            [[QNVideoTool share] recursiveLoadTrashCanVideoFilesWithUUID:uuid withStartPage:1 withSuccessBlock:success withFilterDic:filterDic withEachPageSuccessBlock:success withFailureBlock:failure];
            break;
        default:
            //do nothing
            break;
    }
}

#pragma mark - Change Central Content
//this layout is deprecated!
- (void)changeContentWithGalleryViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion{
    [self changeContentWithTimelineViewController:isAnimated withCompleteBlock:completetion];
}

- (void)changeContentWithTimelineViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNVideoThumbnailViewController *paneViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"gotoTimelineMode"];
    paneViewController.parentPaneViewController = self;
    [paneViewController setTitle:self.title];
    self.videoDataSource = paneViewController;
    self.contentViewController = paneViewController;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:paneViewController];
    [self setPaneViewController:navController animated:isAnimated completion:completetion];
}

- (void)replaceContentWithTimelineViewController:(UIViewController *)originalViewController{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNVideoThumbnailViewController *paneViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"gotoTimelineMode"];
    paneViewController.outsideFetchingBlock = [self detectOutsideFetchingBlock:originalViewController];
    paneViewController.parentPaneViewController = self;
    paneViewController.needBackPreviousViewController = YES;
    [paneViewController setTitle:originalViewController.title];
    self.videoDataSource = paneViewController;
    
    UINavigationController *navController = originalViewController.navigationController;
    [navController popViewControllerAnimated:NO];
    [navController pushViewController:paneViewController animated:NO];
}


- (void)changeContentWithThumbnailViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNVideoThumbnailViewController *paneViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"videoThumbnailViewController"];
    paneViewController.parentPaneViewController = self;
    [paneViewController setTitle:self.title];
    self.contentViewController = paneViewController;
    self.videoDataSource = paneViewController;

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:paneViewController];
    [self setPaneViewController:navController animated:isAnimated completion:completetion];
}

- (void)replaceContentWithThumbnailViewController:(UIViewController *)originalViewController{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNVideoThumbnailViewController *paneViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"videoThumbnailViewController"];
    paneViewController.outsideFetchingBlock = [self detectOutsideFetchingBlock:originalViewController];
    paneViewController.parentPaneViewController = self;
    [paneViewController setTitle:originalViewController.title];
    self.videoDataSource = paneViewController;
    
    UINavigationController *navController = originalViewController.navigationController;
    [navController popViewControllerAnimated:NO];
    [navController pushViewController:paneViewController animated:NO];
}

- (void)changeContentWithListViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNVideoListViewController *paneViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"videoListViewController"];
    paneViewController.parentPaneViewController = self;
    [paneViewController setTitle:self.title];
    self.contentViewController = paneViewController;
    self.videoDataSource = paneViewController;

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:paneViewController];
    [self setPaneViewController:navController animated:isAnimated completion:completetion];
}

- (void)replaceContentWithListViewController:(UIViewController *)originalViewController{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNVideoListViewController *paneViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"videoListViewController"];
    paneViewController.outsideFetchingBlock = [self detectOutsideFetchingBlock:originalViewController];
    paneViewController.parentPaneViewController = self;
    [paneViewController setTitle:originalViewController.title];
    self.videoDataSource = paneViewController;
    
    UINavigationController *navController = originalViewController.navigationController;
    [navController popViewControllerAnimated:NO];
    [navController pushViewController:paneViewController animated:NO];
}

- (void)changeContentWithTrashCanViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNVideoTrashCanViewController *paneViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"trashcanViewController"];
    paneViewController.parentPaneViewController = self;
    [paneViewController setTitle:self.title];
    self.contentViewController = paneViewController;
    self.videoDataSource = paneViewController;

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:paneViewController];
    [self setPaneViewController:navController animated:isAnimated completion:completetion];
    
    /***
     TODO:
     1. fix the selected mode
     2. give the action of trashcan in this viewController
     */
    
}

- (void)changeContentWithFolderViewController:(BOOL)isAnimated withCompleteBlock:(void(^)(void))completetion{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNFolderViewController *paneViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"videoFolderViewController"];
    paneViewController.parentPaneViewController = self;
    [paneViewController setTitle:self.title];
    self.contentViewController = paneViewController;
    self.videoDataSource = paneViewController;

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:paneViewController];
    [self setPaneViewController:navController animated:isAnimated completion:completetion];

}

- (void)changeContentWithSettingViewController:(BOOL)isAnimated withCompletedBlock:(void(^)(void))completion{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNSettingViewController *settingViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"appSettingViewController"];
    settingViewController.parentPaneViewController = self;
    settingViewController.isNaviMode = NO;
    [settingViewController setTitle:self.title];
    self.videoDataSource = nil;

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:settingViewController];
    [self setPaneViewController:navController animated:isAnimated completion:completion];
}

//downloadListViewController
- (void)changeContentWithLocalVideoViewController:(BOOL)isAnimated withCompletedBlock:(void(^)(void))completion{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNDownloadListViewController *settingViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"downloadListViewController"];
    settingViewController.parentPaneViewController = self;
    [settingViewController setTitle:self.title];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:settingViewController];
    [self setPaneViewController:navController animated:isAnimated completion:completion];
}

- (void)changeContentWithAlbumViewController:(BOOL)isAnimated withCompletedBlock:(void(^)(void))completion{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNVideoAlbumViewController *albumViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"videoAlbumViewController"];
    albumViewController.parentPaneViewController = self;
    [albumViewController setTitle:self.title];
    self.videoDataSource = albumViewController;

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:albumViewController];
    [self setPaneViewController:navController animated:isAnimated completion:completion];
}

- (void)pushContentWithSharingViewController:(BOOL)isAnimated withFilmIDs:(NSArray *)filmIDs withCompletedBlock:(void(^)(void))completetion{
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNVideoSharingViewController *sharingViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"videoSharingViewController"];
    sharingViewController.fileIds = [NSArray arrayWithArray: filmIDs];
    [sharingViewController setTitle:@""];
    //    self.videoDataSource = nil;
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController pushViewController:sharingViewController animated:YES];
}


#pragma mark - MultiSelect Action
- (void)downloadSelectedFilms{
    BOOL hasFilesDownload = NO;
    NSArray *selections = nil;
    UIViewController *presentViewController = (UIViewController *)self.videoDataSource;

    if (self.videoDataSource != nil) {
        selections = [NSArray arrayWithArray: [self.videoDataSource multipleSelectedFilms]];
        
        for (NSString *filmId in selections) {
            QNVideoFileItem *filmItem = [QNVideoFileItem MR_findFirstByAttribute:@"f_id" withValue:filmId];
            [QNVideoTool downloadFile:filmItem];
            hasFilesDownload = YES;

            //turn off the selectedmode if any file is downloading.
            if (hasFilesDownload) 
                self.isSelectedMode = NO;
        }
        [self enterDownloadListViewController];
    }
    
    [QNVideoTool toastWithTargetViewController:presentViewController.navigationController
                                      withText:(hasFilesDownload)?NSLocalizedString(@"hasFilesDownloaded", nil):NSLocalizedString(@"NoFileDownload", nil)
                                     isSuccess:hasFilesDownload];
}

- (void)shareSelectedFilme{
    NSArray *selections = nil;
    if (self.videoDataSource != nil){
        selections = [self.videoDataSource multipleSelectedFilms];
        [self pushContentWithSharingViewController:YES withFilmIDs:selections withCompletedBlock:^{}];
    }
}

- (void)addToTransCode{
    NSArray *selections = nil;
    if (self.videoDataSource != nil){
        selections = [self.videoDataSource multipleSelectedFilms];
        [self addToTransCodeAction:selections];
    }
}

- (void)deleteFiles{
    NSArray *selections = nil;
    if (self.videoDataSource != nil){
        selections = [self.videoDataSource multipleSelectedFilms];
        [self deleteFilesIntoTrashcan:selections];
    }
}

- (void)addToAlbum{
    NSArray *selections = nil;
    if (self.videoDataSource != nil){
        selections = [self.videoDataSource multipleSelectedFilms];
        [self addToAlbumAction:selections];
    }
}

- (void)deleteAlbums{
    NSArray *selections = nil;
    if (self.videoDataSource != nil){
        selections = [self.videoDataSource multipleSelectedFilms];
        [self deleteAlbumAction:selections];
    }
}

- (void)removeFilesFromAlbum{
    NSArray *selections = nil;
    NSString *collectionID = nil;
    if (self.videoDataSource != nil){
        selections = [self.videoDataSource multipleSelectedFilms];
        if ([self.videoDataSource respondsToSelector:@selector(fetchingCollectionId)]) {
            collectionID = [self.videoDataSource fetchingCollectionId];
        }
        [self removeFilesFromAlbumAction:selections withCollectionID:collectionID completionBlock:^(BOOL success, NSError *error){
            [QNVideoTool toastWithTargetViewController:(UIViewController *)self.videoDataSource
                                              withText:(success)?NSLocalizedString(@"removeVideosSuccess", nil): NSLocalizedString(@"removeVideosFail", nil)
                                             isSuccess:success];
            if ([self.videoDataSource respondsToSelector:@selector(sendRequestAndReload)]) {
                [self.videoDataSource sendRequestAndReload];
            }
        }];
    }
}
#pragma mark - PrivateMethod
- (void)selectVideosInGallery{
    CTAssetsPickerController *assetPicker = [[CTAssetsPickerController alloc] init];
    assetPicker.assetsFilter = [ALAssetsFilter allVideos];
    
    assetPicker.delegate = self;
    [self.navigationController pushViewController:assetPicker animated:YES];
}

- (BOOL)selectVideosFromCamera{
    UIViewController *controller = self;
    id<UIImagePickerControllerDelegate, UINavigationControllerDelegate>delegate = self;
    if (([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == NO) || (delegate == nil) || (controller == nil))
        return NO;
    
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays a control that allows the user to choose picture or
    // movie capture, if both are available:
    cameraUI.mediaTypes =
    [UIImagePickerController availableMediaTypesForSourceType:
     UIImagePickerControllerSourceTypeCamera];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    cameraUI.delegate = delegate;
    //presentViewController:animated:completion
    [self.contentViewController presentViewController:cameraUI animated:YES completion:^(){}];
    return YES;

}

- (void)enterDownloadListViewController{
    UIViewController *presentViewController = (UIViewController *)self.videoDataSource;
    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate];
    QNDownloadListViewController *downloadViewController = [appDelegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"downloadListViewController"];
    if (presentViewController.navigationController) {
        [presentViewController.navigationController pushViewController:downloadViewController animated:YES];
    }
}

- (OutsideFetchingBlock)detectOutsideFetchingBlock:(UIViewController *)originalViewController{
    OutsideFetchingBlock fetchingBlock = nil;
    if ([originalViewController respondsToSelector:@selector(outsideFetchingBlock)]) {
        fetchingBlock = [originalViewController valueForKeyPath:@"outsideFetchingBlock"];
    }
    return fetchingBlock;
}

- (void)addToTransCodeAction:(NSArray *)filmIDs{
    UITableView *transCodeView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 200, 80) style:UITableViewStylePlain];
    transCodeView.delegate = self;
    transCodeView.dataSource = self;
    [transCodeView setBackgroundColor:[UIColor clearColor]];
    UINavigationController *_navigationController = ((UIViewController *)self.videoDataSource).navigationController;

    [CXAlertView showContentViewAlert:NSLocalizedString(@"SelectResolutions", nil)
                      withActionTitle:NSLocalizedString(@"Done", nil)
                      withContentView:transCodeView
                    withActionHandler:^(CXAlertView *alertView, CXAlertButtonItem *item){
                        [alertView dismiss];

                        if ([_selectedResolutions count] == 0) {
                            [QNVideoTool toastWithTargetViewController:_navigationController
                                                              withText:NSLocalizedString(@"needResolutions", nil)
                                                             isSuccess:YES];
                            return;
                        }
                        QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
                        __block BOOL isSendOut = YES; //TODO, async checking here?
                        for (NSString *fID in filmIDs) {
                            [videoStation videoTransCode:videoDegree360
                                         withResolutions:_selectedResolutions
                                              withFileID:fID
                                        withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                            isSendOut &= YES;
                                        }
                                        withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                            isSendOut &= NO;
                                        }];
                        }
                        [QNVideoTool toastWithTargetViewController:_navigationController
                                                          withText:(isSendOut)? NSLocalizedString(@"addTransCodeSuccess", nil):NSLocalizedString(@"addTransCodeFailure", nil)
                                                         isSuccess:isSendOut];
                        self.isSelectedMode = NO;
                    }];
}

- (void)deleteFilesIntoTrashcan:(NSArray *)filmIDs{
    DDLogVerbose(@"deleteFiles");
    [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                  withActionTitle:NSLocalizedString(@"Done", nil)
                      withMessage:NSLocalizedString(@"deleteFileWarning", nil)
                withActionHandler:^(CXAlertView *alertView, CXAlertButtonItem *item){
                    [[QNVideoTool share] loadingStartAnimation:self.navigationController.navigationBar];
                    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
                    [videoStation deleteFileToTrashCan:videoFileHomePathPublic
                                             withFiles:filmIDs
                                      withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                          [[QNVideoTool share] loadingStopAnimation];
                                          [QNVideoTool toastWithTargetViewController:(UIViewController *)self.videoDataSource
                                                                            withText:NSLocalizedString(@"DeleteSuccess", nil)
                                                                           isSuccess:YES];
                                          [self.videoDataSource sendRequestAndReload];
                                          self.isSelectedMode = NO;
                                      }
                                      withFailureBlock:^(RKObjectRequestOperation *o, NSError *error){
                                          [[QNVideoTool share] loadingStopAnimation];
                                          [QNVideoTool toastWithTargetViewController:(UIViewController *)self.videoDataSource
                                                                            withText:NSLocalizedString(@"DeleteFailure", nil)
                                                                           isSuccess:NO];
                                      }];
                    [alertView dismiss];
                }];
}

- (void)addToAlbumAction:(NSArray *)fIDs{
    _collections = [QNCollection MR_findAll];
    if ([_collections count] == 0) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:nil
                          withMessage:NSLocalizedString(@"NoCollection", nil)
                    withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *item){}];
    }else{
        UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
        picker.delegate = self;
        picker.dataSource = self;
        
        [CXAlertView showContentViewAlert:NSLocalizedString(@"ChooseAlbum", nil)
                          withActionTitle:NSLocalizedString(@"Done", nil)
                          withContentView:picker
                        withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                            QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
                            UIPickerView *_pickerView = picker;
                            NSInteger selectedIndex = [_pickerView selectedRowInComponent:0];
                            QNCollection *targetCollection = [_collections objectAtIndex:selectedIndex];
                            UINavigationController *_navigationController = ((UIViewController *)self.videoDataSource).navigationController;

                            [videoStation addToCollection:targetCollection.iVideoAlbumId
                                              withFileIDS:fIDs
                                         withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                             
                                             [QNVideoTool toastWithTargetViewController:_navigationController
                                                                               withText:[NSString stringWithFormat:NSLocalizedString(@"AddAlbumSuccess", nil), targetCollection.cAlbumTitle]
                                                                              isSuccess:YES];
                                             self.isSelectedMode = NO;
                                         }
                                         withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                             [QNVideoTool toastWithTargetViewController:_navigationController
                                                                               withText:[NSString stringWithFormat:NSLocalizedString(@"AddAlbumError", nil), targetCollection.cAlbumTitle]
                                                                              isSuccess:NO];
                                         }];
                            [alert dismiss];
                        }];
    }
}

- (BOOL)isContainThisResolution:(NSString *)resolutionCompareString{
    BOOL r = NO;
    for (NSString *resolution in _selectedResolutions) {
        if ([resolution isEqualToString:resolutionCompareString]) {
            r = YES;
            break;
        }
    }
    return r;
}

- (void)removeFilesFromAlbumAction:(NSArray *)fileIDs withCollectionID:(NSString *)collectionID completionBlock:(void(^)(BOOL success, NSError *error))completion{
    [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                  withActionTitle:NSLocalizedString(@"Continue", nil)
                      withMessage:NSLocalizedString(@"deleteInAlbum", nil)
                withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *btn){
                    [alert dismiss];
                    
                    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
                    [videoStation removeFromCollection:fileIDs
                                      withCollectionID:collectionID
                                      withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                          completion(YES, nil);
                                      }
                                      withFailureBlock:^(RKObjectRequestOperation *o, NSError *error){
                                          completion(NO, error);
                                      }];
                }];
}

- (void)deleteAlbumAction:(NSArray *)collectionIDs{
    [CXAlertView showMessageAlert:NSLocalizedString(@"DeleteCollection", nil)
                  withActionTitle:NSLocalizedString(@"Done", nil)
                      withMessage:NSLocalizedString(@"DeleteCollectionDetail", nil)
                withActionHandler:^(CXAlertView *alert, CXAlertButtonItem *item){
                    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
                    __block NSMutableArray *_allCollectionIDs = [NSMutableArray arrayWithArray:collectionIDs];
                    
                    [alert dismiss];
                    for (NSString *collectionID in collectionIDs) {
                        [videoStation removeCollection:collectionID
                                      withSuceessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                          DDLogVerbose(@"success %@", r.firstObject);
                                          NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.iVideoAlbumId == %@", collectionID];
                                          [QNCollection MR_deleteAllMatchingPredicate:predicate inContext:[NSManagedObjectContext MR_defaultContext]];
                                          [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
                                          [_allCollectionIDs removeObject:collectionID];
                                          if ([_allCollectionIDs count] > 0) { //still wait another request
                                              //TODO wait cursor?
                                          }else{
                                              [QNVideoTool toastWithTargetViewController:(UIViewController *)self.videoDataSource
                                                                                withText:NSLocalizedString(@"AlbumsDeleteSuccess", nil)
                                                                               isSuccess:YES];
                                              [self.videoDataSource reloadContent];
                                          }
                                      }
                                      withFailureblock:^(RKObjectRequestOperation *o, NSError *e){
                                          DDLogError(@"error %@", e);
                                          NSString *contentString = [NSString stringWithFormat:NSLocalizedString(@"AlbumDeleteFailure", nil), collectionID, e.domain];
                                          [QNVideoTool toastWithTargetViewController:(UIViewController *)self.videoDataSource
                                                                            withText:contentString
                                                                           isSuccess:NO];
                                      }];
                    }
                }];
    
}
#pragma mark - UITableViewDelegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_resolutions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"resolutionCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"resolutionCell"];
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    NSString *contentString = [_resolutions objectAtIndex:[indexPath row]];
    [cell.textLabel setText:contentString];
    cell.accessoryType = ([self isContainThisResolution:contentString])?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *selectedString = [_resolutions objectAtIndex:[indexPath row]];
    if ([self isContainThisResolution: selectedString]) {
        [_selectedResolutions removeObject:selectedString];
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    }else{
        [_selectedResolutions addObject:selectedString];
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    }
    return indexPath;
}

#pragma mark - UIPickerDataSource & Delegate
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_collections count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    QNCollection *targetCollection = (QNCollection *)[_collections objectAtIndex: row];
    return targetCollection.cAlbumTitle;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

#pragma mark - QNVideoStatusDelegate
- (void)changeStatus:(UIView *)statusView withStatus:(QNVideoStatus)status{
    QNVideoStatusView *targetView = (QNVideoStatusView *)statusView;
    [targetView.statusDescription setTextColor:[UIColor colorWithHexString:@"#333333"]];
    DDLogVerbose(@"statusDescription frame %@", NSStringFromCGRect(targetView.statusDescription.frame));
    [targetView.statusIcon stopAnimating];
    
    [targetView.statusDescription setFrame: CGRectMake(83, 220, 155, 18)];
    [targetView.statusDescription setText:NSLocalizedString(@"NoItem", nil)];

    switch (status) {
        case QNVideoStatusError:{
            [targetView.statusIcon setImage:[UIImage imageNamed:@"bg_default.png"]];
            [targetView.statusDescription setText:NSLocalizedString(@"TransferError", nil)];
            
            [targetView.statusDescription setFrame:CGRectMake(20, 220, 280, 300)];
        }
            break;
        case QNVideoStatusNoItemInCollection:{
            [targetView.statusIcon setImage:[UIImage imageNamed:@"default_Video Collection "]];
        }
            break;
        case QNVideoStatusNoItemInSmartCollection:{
            [targetView.statusIcon setImage:[UIImage imageNamed:@"default_Smart Collection"]];
        }
            break;
        case QNVideoStatusNoItemInTrashcan:{
            [targetView.statusIcon setImage:[UIImage imageNamed:@"default_Trash Can "]];
        }
            break;
        case QNVideoStatusNoItemInPrivate:{
            [targetView.statusIcon setImage:[UIImage imageNamed:@"default_Private Collection"]];
        }
            break;
        case QNVideoStatusNoItemInQsync:{
            [targetView.statusIcon setImage:[UIImage imageNamed:@"default_Qsync"]];
        }
            break;

        case QNVideoStatusNoItem:{
            [targetView.statusIcon setImage:[UIImage imageNamed:@"default_Shared Videos.png"]];
        }
            break;
        case QNVideoStatusTransfering:{
            [targetView.statusIcon setAnimationImages:_loadingImages];
            [targetView.statusIcon setAnimationRepeatCount:0];
            [targetView.statusIcon setAnimationDuration:1.0f];
            [targetView.statusIcon startAnimating];
            
            [targetView.statusDescription setText:NSLocalizedString(@"SearchingData", nil)];
        }
            break;
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    //We need to close the Pane while rotating the screen, or would see some abnormal, out of control layout.
    if (self.paneState != MSDynamicsDrawerPaneStateClosed) {
        [self setPaneState:MSDynamicsDrawerPaneStateClosed
                  animated:YES
     allowUserInterruption:NO
                completion:^{
                }];
    }
}

- (QNVideoStatus)noItemStatus{
    QNVideoStatus status;
    switch (self.homePath) {
        case videoFileHomePathPrivate:
            status = QNVideoStatusNoItemInPrivate;
            break;
        case videoFileHomePathQSync:
            status = QNVideoStatusNoItemInQsync;
            break;
        case videoFileHomePathCollections:
            status = QNVideoStatusNoItemInCollection;
            break;
        case videoFileHomePathSmartCollections:
            status = QNVideoStatusNoItemInSmartCollection;
            break;
        case videoFileHomePathTrashCan:
            status = QNVideoStatusNoItemInTrashcan;
            break;
        default:
            status = QNVideoStatusNoItem;
            break;
    }
    return status;
}

#pragma mark - TopRight dropdown menu
- (void)settingTopRightDropdownMenuOnAlbum:(UIViewController *)viewController{
    [self settingTopRightDropdownMenu:viewController isRenew:YES];
}

- (void)settingTopRightDropdownMenu:(UIViewController *)viewController{
    [self settingTopRightDropdownMenu:viewController isRenew:NO];
}

- (void)settingTopRightDropdownMenu:(UIViewController *)viewController isRenew:(BOOL)isRenew{
    NSMutableArray *allItems = [NSMutableArray array];
    if (![viewController isKindOfClass:[QNVideoAlbumViewController class]]) {
        REMenuItem *timeline = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"Timeline", nil)
                                                           image:nil
                                                highlightedImage:nil
                                                          action:^(REMenuItem *item){
                                                              if (isRenew) {
                                                                  [self changeContentWithGalleryViewController:NO withCompleteBlock:nil];
                                                              }else{
                                                                  [self replaceContentWithTimelineViewController:viewController];
                                                              }
                                                              
                                                          }];
        REMenuItem *thumbnail = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"Thumbnail", nil)
                                                            image:nil
                                                 highlightedImage:nil
                                                           action:^(REMenuItem *item){
                                                               if (isRenew) {
                                                                   [self changeContentWithThumbnailViewController:NO withCompleteBlock:nil];
                                                               }else
                                                                   [self replaceContentWithThumbnailViewController:viewController];
                                                           }];
        REMenuItem *list = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"List", nil)
                                                       image:nil
                                            highlightedImage:nil
                                                      action:^(REMenuItem *item){
                                                          if (isRenew) {
                                                              [self changeContentWithListViewController:NO withCompleteBlock:nil];
                                                          }else
                                                              [self replaceContentWithListViewController:viewController];
                                                      }];
        
        [allItems addObjectsFromArray:@[timeline, thumbnail, list]];
        if (self.homePath != videoFileHomePathCollections && self.homePath != videoFileHomePathSmartCollections) {
            REMenuItem *folder = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"Folder", nil)
                                                             image:nil
                                                  highlightedImage:nil
                                                            action:^(REMenuItem *item){
                                                                /***In videoFileHomePathCollections & videoFileHomePathSmartCollection, we don't need to replace the viewmode, thus there is no any replaceContentWithXXX for "isRenew" flag
                                                                 */
                                                                [self changeContentWithFolderViewController:NO withCompleteBlock:nil];
                                                            }];
            [allItems addObject:folder];
        }
        
        REMenuItem *upload = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"Upload", nil)
                                                         image:nil
                                              highlightedImage:nil
                                                        action:^(REMenuItem *item){
                                                            UIViewController *viewController = (UIViewController *)self.videoDataSource;
                                                            IQActionSheetPickerView *actions = [[IQActionSheetPickerView alloc] initWithViewController:self];
                                                            actions.delegate = self;
                                                            actions.titlesForComponenets = @[@[NSLocalizedString(@"uploadFromGallery", nil), NSLocalizedString(@"uploadFromCamera", nil)]];
                                                            [actions showInView:viewController.view];
                                                        }];
        [allItems addObject:upload];
    }
    REMenuItem *refresh = nil;
    refresh = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"RefreshAction", nil)
                                          image:nil
                               highlightedImage:nil
                                         action:^(REMenuItem *item){
                                             if ([self.videoDataSource respondsToSelector:@selector(sendRequestAndReload)]) {
                                                 [self.videoDataSource sendRequestAndReload];
                                             }
                                         }];
    [allItems addObject:refresh];
    self.viewModeMenu = [[REMenu alloc] initWithItems:allItems];
    [self settingActionMenuLayout:self.viewModeMenu];
}

- (void)settingActionMenuLayout:(REMenu *)actionMenu{
    actionMenu.backgroundColor = [UIColor colorWithRed:_backgrounColorValue green:_backgrounColorValue blue:_backgrounColorValue alpha:0.95];
    actionMenu.itemHeight = 44.0f;
    actionMenu.textColor = [UIColor whiteColor];
    actionMenu.font = [UIFont systemFontOfSize:18.0f];
    actionMenu.separatorHeight = 0.0f;
    actionMenu.separatorColor = [UIColor clearColor];
    actionMenu.highlightedBackgroundColor = [UIColor colorWithRed:_highlightColorValue green:_highlightColorValue blue:_highlightColorValue alpha:1.0f];
    actionMenu.textAlignment = NSTextAlignmentLeft;
    actionMenu.textOffset = CGSizeMake(18, 0);
    actionMenu.borderWidth = 0.0f;
}

- (void)topRightDropdownMenuAction:(UIViewController *)onViewController{
    CGFloat width = 120.0f;
    if (self.viewModeMenu.isOpen) {
        [self.viewModeMenu close];
    }else{
        [self settingTopRightDropdownMenu:onViewController];
        [self.viewModeMenu showFromRect:CGRectMake(onViewController.view.frame.size.width - width, 0, width, onViewController.view.frame.size.height) inView:onViewController.view];
    }
}

- (void)uploadVideo:(NSURL *)videoURL withAsset:(ALAsset *)obj{
    void(^completed)(id response, NSError *error) = ^(id response, NSError *error){
        if (error){
            [QNVideoTool toastWithTargetViewController:(UIViewController *)self.videoDataSource
                                              withText:[NSString stringWithFormat: NSLocalizedString(@"UploadError", nil), obj.defaultRepresentation.filename]
                                             isSuccess:NO];
        }
        else{
            //                [QNVideoTool toastWithTargetViewController:(UIViewController *)self.videoDataSource
            //                                                  withText:NSLocalizedString(@"", nil)
            //                                                 isSuccess:YES];
        }
        
    };
    NSString *albumID = @"";
    NSString *remotePath = @"";
    if ([self.videoDataSource respondsToSelector:@selector(fetchingCollectionId)]) {
        albumID = [self.videoDataSource fetchingCollectionId];
    }
    
    if ([self.videoDataSource respondsToSelector:@selector(fetchingRemotePath)]) {
        remotePath = [self.videoDataSource fetchingRemotePath];
    }
    
    [[QNAPCommunicationManager share].videoStationManager uploadVideoFilm:albumID
                                                            withLocalPath:videoURL
                                                             withHomePath:self.homePath
                                                           withRemotePath:remotePath
                                                             withFileName:obj.defaultRepresentation.filename
                                                 withNSURLSessionDelegate:self
                                                         withStartupBlock:^(NSURLSessionUploadTask *uploadTask){
                                                             ALAssetRepresentation *rep = [obj defaultRepresentation];
                                                             NSString *fileName = [rep filename];
                                                             VideoUpload *upload = [VideoUpload MR_createEntity];
                                                             upload.filmID = uploadTask.taskDescription;
                                                             upload.filmTitle = fileName;
                                                             upload.thumbnail = [UIImage imageWithCGImage:obj.thumbnail];
                                                             upload.uploadStatus = @(videoUploadProgressing);
                                                             [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *e){
                                                                 if (s) {
                                                                     [QNVideoTool toastWithTargetViewController:(UIViewController *)self.videoDataSource
                                                                                                       withText:[NSString stringWithFormat:NSLocalizedString(@"AddUploadTask", nil),
                                                                                                                 obj.defaultRepresentation.filename]
                                                                                                      isSuccess:YES];
                                                                 }else{
                                                                     [QNVideoTool toastWithTargetViewController:(UIViewController *)self.videoDataSource
                                                                                                       withText:[NSString stringWithFormat:NSLocalizedString(@"AddUploadTaskfail", nil),
                                                                                                                 obj.defaultRepresentation.filename]
                                                                                                      isSuccess:NO];
                                                                 }
                                                             }];
                                                         }
                                                       withCompletedBlock:^(id response, NSError *error){
                                                           completed(response, error);
                                                       }];
}
#pragma mark - CTAssetsPickerDelegate
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    for (ALAsset *obj in assets) {
        NSURL *fileURL = [[obj defaultRepresentation] url];
        [self uploadVideo:fileURL withAsset:obj];
    }
    [picker.navigationController popViewControllerAnimated:NO];
    [self enterDownloadListViewController];
}

- (void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker{
    [picker.navigationController popViewControllerAnimated:YES];
}

- (void)uploadCompleted:(NSString *)fID{
    NSManagedObjectContext *defaultContext = [NSManagedObjectContext MR_defaultContext];
    VideoUpload *upload = [VideoUpload MR_findFirstByAttribute:@"filmID" withValue:fID inContext:defaultContext];
    
    if (!upload)
        return;
    
    upload.uploadStatus = @(videoUploadCompleted);
    [defaultContext MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *e){
        if (s) {
            UIViewController *presentViewController = (UIViewController *)self.videoDataSource;

            [[NSNotificationCenter defaultCenter] postNotificationName:@"UploadCompleted" object:nil];
            [QNVideoTool toastWithTargetViewController:presentViewController.navigationController
                                              withText:[NSString stringWithFormat: NSLocalizedString(@"UploadCompleted", nil), upload.filmTitle]
                                             isSuccess:YES];

        }else{
            DDLogError(@"Error %@", e);
        }
    }];
}

#pragma mark - NSURLSessionDelegate
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    DDLogVerbose(@"sent: %lld, totalByteSent: %lld, totalExpectedSend: %lld", bytesSent, totalBytesSent, totalBytesExpectedToSend);
    NSDate *nowDate = [NSDate date];
    CGFloat uploadRate = 0.0f;

    //to mininum the numbers of nsnotifications.
    //downloadProgress
    if (_previousUploadSendDate) {
        NSTimeInterval duration = [nowDate timeIntervalSince1970] - [_previousUploadSendDate timeIntervalSince1970];
        uploadRate = (duration == 0)? 0:bytesSent / duration;
    }
    CGFloat progress = 0.0f;
    if (totalBytesExpectedToSend >= 0) {
        progress = ((double)totalBytesSent / (double)(totalBytesExpectedToSend)) * 100.0f;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadProgress"
                                                        object:nil
                                                      userInfo:@{@"fID":task.taskDescription,
                                                                 @"downloadProgress":@(progress),
                                                                 @"downloadRate":@(uploadRate)}];
    _previousUploadSendDate = nowDate;
    if (progress >= 100.0f)
        [self uploadCompleted:task.taskDescription];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error{
    _previousUploadSendDate = nil;
    NSManagedObjectContext *defaultContext = [NSManagedObjectContext MR_defaultContext];
    VideoUpload *upload = [VideoUpload MR_findFirstByAttribute:@"filmID" withValue:task.description inContext:defaultContext];
    
    if (!upload) {
        upload = [VideoUpload MR_createEntity];
        upload.filmID = task.taskDescription;
    }
    
    upload.uploadStatus = (error)?@(videoUploadPauseBySystem):@(videoUploadCompleted);
    [defaultContext MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *e){
        if (s) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UploadCompleted" object:nil];
        }else{
            DDLogError(@"Error %@", e);
        }
    }];
}

#pragma mark - IQActionSheetPickerView
- (void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray*)titles{
    NSString *title = [titles objectAtIndex:0];
    if ([title isEqualToString:NSLocalizedString(@"uploadFromGallery", nil)]) {
        [self selectVideosInGallery];
    }else if ([title isEqualToString:NSLocalizedString(@"uploadFromCamera", nil)]){
        [self selectVideosFromCamera];
    }
}


#pragma mark - 
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    // Handle a still image capture
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)
        == kCFCompareEqualTo) {
        //TODO maybe an error message?
    }
    
    // Handle a movie capture
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeMovie, 0)
        == kCFCompareEqualTo) {
        
        NSString *moviePath = (NSString *)[info[UIImagePickerControllerMediaURL] path];
        
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
            NSURL *videoURL = [NSURL fileURLWithPath:moviePath isDirectory:NO];
            ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
            [assetslibrary writeVideoAtPathToSavedPhotosAlbum:videoURL completionBlock:^(NSURL *assetURL, NSError *error){
                if (error) {
                    DDLogError(@"can't write the asset %@", error);
                }else
                [assetslibrary assetForURL:videoURL
                               resultBlock:^(ALAsset *asset){
                                   [self uploadVideo:assetURL withAsset:asset];
                                   [self.contentViewController dismissViewControllerAnimated:YES completion:^(){}];
                                   [self enterDownloadListViewController];
                               }
                              failureBlock:^(NSError *e){
                                  DDLogError(@"can't find asset %@", e);
                              }];
            }];

        }
    }
    
    
}

@end
