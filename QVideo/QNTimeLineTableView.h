//
//  QNTimeLineTableView.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/19.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNVideoGalleryViewController.h"

@interface QNTimeLineTableView : UITableView <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) QNVideoGalleryViewController *parentViewController;
@property (nonatomic, weak) NSArray *timeLineItems;

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style timeLineData:(NSArray *)timeLines;
@end
