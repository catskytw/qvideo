//
//  QNVideoShareCollectionViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/3/20.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoShareCollectionViewController.h"
#import <QNAPFramework/QNVideoFileItem.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <QNAPFramework/QNAPCommunicationManager.h>
#import "NSString_TimeDuration.h"
#import "QNVideoShareCell.h"

@interface QNVideoShareCollectionViewController ()

@end

@implementation QNVideoShareCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [[self selectedFilmIDs] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *fId = [self.selectedFilmIDs objectAtIndex:[indexPath row]];
    QNVideoFileItem *targetFile = [QNVideoFileItem MR_findFirstByAttribute:@"f_id" withValue:fId];
    
    QNVideoShareCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QNVideoShareCell" forIndexPath:indexPath];
    QNAPCommunicationManager *communicationManager = [QNAPCommunicationManager share];
    [communicationManager.videoStationManager lazyloadingImage:cell.snapShot
                                               withPlaceHolder:nil
                                                    withFileId:fId
                                             withProgressBlock:^(NSUInteger r, long long e){}
                                            withCompletedBlock:^(UIImage *i, NSError *e, SDImageCacheType c){
                                                if (e)
                                                    DDLogError(@"WebImage Error: %@ ", e);
                                            }];
    [cell.filmTitle setText:targetFile.cPictureTitle];
    cell.deleteBtn.userInfo = targetFile;
    cell.hdImageIndicator.hidden = (MIN([targetFile.iWidth floatValue], [targetFile.iHeight floatValue]) >= 720.0f)?NO:YES;
    [cell.durationLabel setText:[NSString stringFromTimeInterval:[targetFile.duration doubleValue]]];
    return cell;
}

- (IBAction)removeFilmItem:(id)sender{
    if ([self.selectedFilmIDs count] <= 1) {
        [CXAlertView showMessageAlert:NSLocalizedString(@"Warning", nil)
                      withActionTitle:nil
                          withMessage:NSLocalizedString(@"CantLessThanOneVideo", nil)
                    withActionHandler:nil];
    }else{
        QNButton *pressedBtn = (QNButton *)sender;
        QNVideoFileItem *fileItem = (QNVideoFileItem *)pressedBtn.userInfo;
        [self.selectedFilmIDs removeObject:fileItem.f_id];
        
        [self.collectionView reloadData];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
