//
//  QNAddCollectionTableViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/7/22.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNAddCollectionTableViewController.h"
#import "QNVideoTool.h"
#import "QNVideoAlbumViewController.h"

#define checkImage [UIImage imageNamed:@"icon_checkbox_on"]
#define uncheckImage [UIImage imageNamed:@"icon_checkbox"]
@interface QNAddCollectionTableViewController ()

@end

@implementation QNAddCollectionTableViewController

- (id)initWithStyle:(UITableViewStyle)style{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [QNVideoTool generateBlackLeftBarItem:self withSelect:@selector(backNavi)];
    [QNVideoTool generateCustomRightBarItem:self withSelect:@selector(createCollectionAction:) withTitleText:NSLocalizedString(@"Done", nil)];
    [self.validDate setDate:[NSDate date]];
    [self settingDateString:self.validDate.date];
    if (!self.collection) {
        self.collection = [QNCollection MR_createEntity];
        self.collection.editbyo = @(YES);
    }
    
    [self updateAllLayout];
}

- (void)settingDateString:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd 23:59:59"];

    [self.dateLabel setText:[formatter stringFromDate:date]];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)updateCollectionPresent{
    if (self.collection) {
        
    }else
        DDLogError(@"The instance of QNCollection is nil in QNAddCollectionTableViewController!");
}

- (void)backNavi{
    [self.collection MR_deleteEntity];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL s, NSError *error){
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - PrivateMethod
- (void)enableSharedOptions:(BOOL)isEnable{
    if (isEnable) {
        for (UIView *shareItem in self.sharedSubItems) {
            if ([shareItem isMemberOfClass:[UILabel class]]) {
                [((UILabel *)shareItem) setTextColor:[UIColor blackColor]];
            }
        }
    }else{
        for (UIView *shareItem in self.sharedSubItems) {
            if ([shareItem isMemberOfClass:[UILabel class]]) {
                [((UILabel *)shareItem) setTextColor:[UIColor lightGrayColor]];
            }else if ([shareItem isMemberOfClass:[UIImageView class]]){
                [self tintImageView:(UIImageView *)shareItem];
            }
        }
    }
}

- (void)enablePublicOptions:(BOOL)isEnable{
    if (isEnable) {
        for (UIView *shareItem in self.publicSubItems) {
            if ([shareItem isMemberOfClass:[UILabel class]]) {
                [((UILabel *)shareItem) setTextColor:[UIColor blackColor]];
            }else if ([shareItem isMemberOfClass:[UIDatePicker class]]){
                UIDatePicker *datePicker = (UIDatePicker *)shareItem;
                [datePicker setHidden:([self.collection.expiration isEqualToString:@"0"])];
            }
        }
    }else{
        for (UIView *shareItem in self.publicSubItems) {
            if ([shareItem isMemberOfClass:[UILabel class]]) {
                [((UILabel *)shareItem) setTextColor:[UIColor lightGrayColor]];
            }else if ([shareItem isMemberOfClass:[UIImageView class]]){
                [self tintImageView:(UIImageView *)shareItem];
            }else if ([shareItem isMemberOfClass:[UIDatePicker class]]){
                UIDatePicker *datePicker = (UIDatePicker *)shareItem;
                [datePicker setHidden:YES];
            }
        }
    }
}

- (void)tintImageView:(UIImageView *)targetImageView{
    UIImageView *imageView = (UIImageView *)targetImageView;
    UIImage *originalImage = imageView.image;
    UIImage *imageForRendering = [originalImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    imageView.image = imageForRendering;
    imageView.tintColor = [UIColor lightGrayColor];

}
#pragma mark - Action
- (IBAction)dateValueChanged:(UIDatePicker *)picker{
    [self settingDateString:picker.date];
}

- (void)createCollectionAction:(id)sender{
    if ([self.collectionName.text isEqualToString:@""]) {
        [QNVideoTool toastWithTargetViewController:self withText:NSLocalizedString(@"collectionNameEmptyError", nil) isSuccess:NO];
        return;
    }
    QNVideoStationAPIManager *videoStation = [QNAPCommunicationManager share].videoStationManager;
    [videoStation createCollection:self.collectionName.text
                    collectionType:videoAlbums
                          isOpened:[self.collection.public boolValue]
                          isShared:[self.collection.shared boolValue]
                     isEditByOwner:[self.collection.editbyo boolValue]
                withExpireDateTime:self.collection.expiration
                  withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoCollectionCreateResponse *response){
                      [QNVideoTool toastWithTargetViewController:self.navigationController withText:NSLocalizedString(@"createCollectionSuccess", nil) isSuccess:YES];
                      [self.navigationController popViewControllerAnimated:YES];
                      UIViewController *topViewController = [self.navigationController topViewController];
                      if ([topViewController isMemberOfClass:[QNVideoAlbumViewController class]]) {
                          [(QNVideoAlbumViewController *)topViewController sendRequestAndReload];
                      }
                  }
                  withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                      [QNVideoTool toastWithTargetViewController:self.navigationController withText:NSLocalizedString(@"createCollectionError", nil) isSuccess:NO];
                  }];
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = [indexPath row];
    switch (row) {
        case 0:
        default:
            break;
        case 1:{
            self.collection.shared = @(![self.collection.shared boolValue]);
        }
            break;
        case 2:{
            if ([self.collection.shared boolValue]) {
                self.collection.editbyo = @(NO);
            }
        }
            break;
        case 3:{
            if ([self.collection.shared boolValue]) {
                self.collection.editbyo = @(YES);
            }
        }
            break;
        case 4:{
            self.collection.public = @(![self.collection.public boolValue]);
        }
            break;
        case 5:{
            if ([self.collection.public boolValue]) {
                self.collection.expiration = @"0";
            }
        }
            break;
        case 6:{
            if ([self.collection.public boolValue]) {
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy/MM/dd 23:59:59"];
                self.collection.expiration = [formatter stringFromDate:self.validDate.date];
            }
        }
            break;
    }
    [self updateAllLayout];
}

- (void)updateAllLayout{
    [self.sharedWithUserImage setImage:([self.collection.shared boolValue])?checkImage:uncheckImage];
    if ([self.collection.shared boolValue]) {
        [self.editByAnyoneImage setImage:[self.collection.editbyo boolValue]?uncheckImage:checkImage];
        [self.editByOwnerImage setImage:[self.collection.editbyo boolValue]?checkImage:uncheckImage];
    }
    
    [self.publicImage setImage:[self.collection.public boolValue]?checkImage:uncheckImage];
    if ([self.collection.public boolValue]) {
        [self.validForeverImage setImage:([self.collection.expiration isEqualToString:@"0"])?checkImage:uncheckImage];
        [self.validDateImage setImage:[self.collection.expiration isEqualToString:@"0"]?uncheckImage:checkImage];
    }
    
    [self enableSharedOptions:[self.collection.shared boolValue]];
    [self enablePublicOptions:[self.collection.public boolValue]];
}
@end
