//
//  QNVideoThumbnailViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/10.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QNAPFramework/QNVideoStationAPIManager.h>
#import "QNVideosViewController.h"
#import "QNVideoSortingSectionView.h"
#import "IQActionSheetPickerView.h"
@interface QNVideoThumbnailViewController : UIViewController<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, NSFetchedResultsControllerDelegate, IQActionSheetPickerView, QNVideoDataSource, UIActionSheetDelegate, UIScrollViewDelegate>{
    NSMutableArray *_selections;
    NSArray *_multiSelectAction;
    NSPredicate *_addtionalPredicate;
    NSDictionary *_filterDic;
}
@property (nonatomic, strong) NSPredicate *originalPredicate;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong)id<QNVideoFetchingDelegate>fetchDelegate;
@property (nonatomic, weak) QNVideosViewController *parentPaneViewController;
@property (nonatomic, strong) NSFetchedResultsController *fetchResultsController;
@property (nonatomic, strong) IBOutlet QNVideoSortingSectionView *sortingView;
@property (nonatomic) BOOL needBackPreviousViewController;
@property (weak, nonatomic) IBOutlet QNVideoStatusView *statusView;
@property (nonatomic, strong) UIView *bottomBar;
@property (nonatomic, strong)NSString *thisQueryUUID;
@property (nonatomic, strong) void(^outsideFetchingBlock)(NSString *fetchUUID, QNQNVideoFileListSuccessBlock success, QNFailureBlock failure);

//- (void)sendRequestAndReload;
- (NSFetchedResultsController *)generateFetchedResultsController:(NSDictionary *)info;

- (IBAction)gotoVideoDetail:(id)sender;
- (IBAction)playVideo:(id)sender;
- (void)switchMultiSelectMode;
//Maybe you should overwrite these functions
- (NSArray *)multipleSelectedItems;
- (void)reloadVideoFiles;
- (void)hideDataView;
- (void)showDataView;
@end
