//
//  QNVideoInfoViewController.h
//  QVideo
//
//  Created by Change.Liao on 2013/12/22.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EDStarRating/EDStarRating.h>
#import <QNAPFramework/QNVideoFileItem.h>
#import <MarqueeLabel/MarqueeLabel.h>
#import <AMTagListView/AMTagListView.h>
#import "QNVideoItemDetailViewController.h"
#import <SZTextView/SZTextView.h>

@interface QNVideoInfoViewController : UITableViewController<EDStarRatingProtocol, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextViewDelegate>{
}
@property (weak, nonatomic) QNVideoFileItem *videoFileItem;
@property (weak, nonatomic) IBOutlet MarqueeLabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *classificationLabel;
@property (weak, nonatomic) IBOutlet EDStarRating *starRating;
@property (weak, nonatomic) IBOutlet UIImageView *tagImage;
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, assign) QNVideoItemDetailViewController *outsideDetailViewController;
@property (weak, nonatomic) IBOutlet UIImageView *colorImage;
@property (nonatomic, strong) CXAlertView *showAlert;
@property (nonatomic, strong) AMTagListView *tagListView;
@property (nonatomic, strong) IBOutlet UIView *tagView;

@property (weak, nonatomic) IBOutlet UILabel *fixTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *fixClassificationLabel;
@property (weak, nonatomic) IBOutlet UILabel *fixColorLabel;
@property (weak, nonatomic) IBOutlet UILabel *fixRatingLabel;
@property (weak, nonatomic) IBOutlet UILabel *fixTagLabel;
@property (weak, nonatomic) IBOutlet UILabel *fixDescription;
@property (weak, nonatomic) IBOutlet SZTextView *descriptionTextView;
- (IBAction)addTagAction:(id)sender;
- (IBAction)modifyDescription:(id)sender;

@end
