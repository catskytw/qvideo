//
//  QNResolutionTableViewController.h
//  QVideo
//
//  Created by Change.Liao on 2014/4/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QNAPFramework/QNVideoFileItem.h>
#import "QNMoviePlayerViewController.h"

@interface QNResolutionTableViewController : UITableViewController{
    NSMutableArray *_resolutions;
    CGFloat _currentResolution;
}
@property (nonatomic, strong)QNMoviePlayerViewController *playerViewController;
@property (nonatomic, strong)UIView *headerView;
@property (nonatomic, strong)QNVideoFileItem *thisFilmItem;
@end
