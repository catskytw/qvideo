//
//  QNIntroductionWelcomeViewController.m
//  QVideo
//
//  Created by Change.Liao on 2014/4/30.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNIntroductionWelcomeViewController.h"

@interface QNIntroductionWelcomeViewController ()

@end

@implementation QNIntroductionWelcomeViewController
- (void)viewDidLoad{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"#f8f8f8"]];
    [self.coreTextView setBackgroundColor:[UIColor colorWithHexString:@"#f8f8f8"]];
}

- (void)settingCoreText{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSString *textName = [NSString stringWithFormat:@"Welcome_%@.txt", language];
    self.coreTextView.text = [[self class] textFromTextFileNamed:textName];
}
@end
